; -*- coding: utf-8 -*-
; DTCforthMSP430FRxxxxSD_LOAD.asm

; Tested with MSP-EXP430FR5969 launchpad
; Copyright (C) <2015>  <J.M. THOORENS>
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;-----------------------------------------------------------------------
; READ and SD_ACCEPT users primitive
;-----------------------------------------------------------------------

; primitive to read sequentially a file opened by READ" or by LOAD".
; sectors are loaded in buffer and BufferLen leave the count of loaded bytes.
; when the last sector of file is loaded in buffer, the handle is closed.

; notice that READ" load the first file sector in buffer.

; file can be opened also with LOAD" that creates a link with previous LOAD"ed file
; in this case, READ_FILE read sequentially all and only all LOAD"ed files before end.


; ==================================;
HandlesInit                         ; all zeroes
; ==================================;
    MOV     #HandleMax,W            ;
    MOV     #FirstHandle,X          ;
FI_HandlesInitLoop                  ;
    MOV     #0,HDLB_Token(X)        ;
    ADD     #HandleLenght,X         ;
    SUB     #1,W                    ;
    JNZ     FI_HandlesInitLoop      ;
    MOV     #0,&CurrentHdl          ;
    RET                             ;
; ----------------------------------;

; called by SD_ACCEPT and READ ; IP is free
; ==================================;
Read_File                           ;WXY   --
; ==================================;
RHF_RefreshHandle                   ; 
    MOV     #0,&BufferPtr           ; reset BufferPtr (the buffer is already read)
    CMP     #bytsPerSec,&BufferLen  ;
    JNZ     RHF_CloseHandle         ; because this last and incomplete sector is already read
    MOV     &CurrentHdl,X           ;
    SUB #bytsPerSec,HDLL_CurSize(X)   ; HDLL_CurSize is decremented of one sector lenght
    SUBC    #0,HDLL_CurSize+2(X)      ;
    ADD.B   #1,HDLB_ClustOfst(X)    ; current cluster offset is incremented
    CMP.B &SecPerClus,HDLB_ClustOfst(X) ; Cluster Bound reached ?
    JZ      RHF_SearchNextCluster   ; yes
    ADD     #1,&SectorL             ; no
    ADDC    #0,&SectorL+2           ;
; ----------------------------------;
RHF_ComputeBufferLen                ;WXY <== RHF_SearchNextCluster
; ----------------------------------;
    MOV     #bytsPerSec,&BufferLen  ;
    CMP     #0,HDLL_CurSize+2(X)    ; CurSizeL > 65535 ?
    JNZ     ReadSectorL             ; yes
    CMP #bytsPerSec,HDLL_CurSize(X) ; no ; CurSizeL >= 512 ?
    JC      ReadSectorL             ; yes ; then RET
    MOV     HDLL_CurSize(X),&BufferLen; no ; set BufferLen
; ==================================;
ReadSectorL                         ;
; ==================================;
    MOV     &SectorL,W              ; Low
    MOV     &SectorL+2,X            ; High
    JMP     ReadSectorWX            ; then RET
; ----------------------------------;

; ----------------------------------;
RHF_CloseHandle                     ;VWXY case of READ"ed or LOAD"ed file
; ----------------------------------;
CloseHandle                         ;
; ----------------------------------; <<<<< get end of sector >>>>>>
    MOV     #0,&BufferLen           ; to inform the READ user program that file is closed
    MOV     &CurrentHdl,X           ;
    CMP     #FirstHandle,X          ;
    JNC     RHF_EndOfFile           ; case of handle u< firsthandle
    CMP     #HandleOutOfBound,X     ;
    JC      RHF_EndOfFile           ; case of handle >= lasthandle

    .IFDEF SD_CARD_READ_WRITE
    CMP.B   #2,HDLB_Token(X)        ; updated file ?
    JNZ     CloseHandleNext         ; no
UpdatedFileCaseOf                   ;
;    MOV     X,&CurrentHdl           ;
    CALL    #OPWW_WriteBuffer       ;
    CALL    #OPWW_UpdateDirectory   ;VWXY 
    MOV     #PARENEMIT,&EMIT+2      ; restore (EMIT)
    MOV     &CurrentHdl,X           ;
CloseHandleNext                     ;
    .ENDIF

    MOV.B   #0,HDLB_Token(X)        ; close handle
    MOV     @X,X                    ; X = previous handle (or 0)
    MOV     X,&CurrentHdl           ;
    CMP     #0,X                    ; no more handle ?
    JZ      RHF_EndOfFile           ; yes
    CMP.B   #-1,HDLB_Token(X)       ; previous LOAD"ed file ?
    JZ      RHF_ComputeLinkedHandle ; yes
; ----------------------------------;
RHF_EndOfFile                       ;
; ----------------------------------;
    MOV     #0,&CurrentHdl          ; reset CurrentHdl before case of
    RET                             ;
; ----------------------------------;

; ----------------------------------;
RHF_ComputeLinkedHandle             ;WXY  X = handle of the previous LOAD"ed file
; ----------------------------------;
    MOV     HDLW_BUFofst(X),&BufferPtr ; restore BufferPtr saved by LOAD"
    MOV    HDLW_CurClust(X),&Cluster; reload sector that contains LOAD" command
    CALL    #ComputeClusFrstSect    ; use no registers ??
    MOV.B   HDLB_ClustOfst(X),Y     ;
    ADD     Y,&SectorL              ;
    ADDC    #0,&SectorL+2           ;
    JMP     RHF_ComputeBufferLen    ;WXY then read sector, then return to user routine
; ----------------------------------;

; ----------------------------------;
RHF_SearchNextCluster               ;WXY as the end of file is not reached, we should find a next cluster
; ----------------------------------;
    MOV.B   #0,HDLB_ClustOfst(X)    ; reset Current_Cluster sectors offset
    MOV     HDLW_CurClust(X),&Cluster  ;
    CALL    #FindClusterFATsector   ;
    MOV     #0,X                    ;
    CALL    #ReadSectorWX           ; and load it
    MOV     &CurrentHdl,X           ; 
    MOV     &FAToffset,Y            ; W = FAToffset in BUFFER
    MOV     BUFFER(Y),HDLW_CurClust(X) ;
; ==================================;
LoadClusFirstSect                   ;WXY <== LOAD" and READ" input to read first file sector
; ==================================;
    MOV    HDLW_CurClust(X),&Cluster;
    CALL    #ComputeClusFrstSect    ; use no registers
    JMP     RHF_ComputeBufferLen    ;WXY then read sector, then return to user routine
; ----------------------------------;


;-----------------------------------------------------------------------
; Open_File subroutines
;-----------------------------------------------------------------------


; input : Cluster
; ClusterLow*2 = displacement in 512 bytes sector   ==> FAToffset
; ClusterHigh  = relative FATsector + orgFAT1       ==> FATsector
; ----------------------------------;
FindClusterFATSector                ; Input : Cluster ; Output: FATsector, FAToffset
; ----------------------------------;
    MOV.B   &Cluster,X              ;3 ClusterLO * 2 = offset in sector
    ADD     X,X                     ;1 X = offset in FATsector
    MOV     X,&FAToffset            ;3       
    MOV.B   &Cluster+1,W            ;3 ClusterHI     = orgFAT1 offset
    ADD     &OrgFAT1,W              ;3
    MOV     W,&FATsector            ;3
    RET                             ;4
; ----------------------------------;


; use no registers if hardware MPY
; ----------------------------------; Input : Cluster
ComputeClusFrstSect                 ; If Cluster = 1 ==> RootDirectory ==> SectorL = OrgRootDir
; ----------------------------------; Output: SectorL of Cluster
    MOV     #0,&SectorL+2           ;
    MOV     &OrgRootDir,&SectorL    ;
    CMP     #1,&Cluster             ; cluster 1 ?
    JZ      CCFS_End                ; yes, sectorL is done
CCFS_AllOtherCLuster                ;
    MOV     &OrgClusters,&SectorL   ; OrgClusters = sector of virtual cluster 0
    .IFDEF MPY
    MOV     &Cluster,&MPY           ;
    MOV     &SecPerClus,&OP2        ;
    ADD     &RES0,&SectorL          ;
    ADDC    &RES1,&SectorL+2        ;
    .ELSE
    .word   156Eh                   ; PUSHM TOS,Y (6+1 push,TOS=E)
    PUSH    @PSP                    ;
    MOV     &Cluster,0(PSP)         ;
    MOV     &SecPerClus,TOS         ;
    MOV     #CCFS_NEXT,IP           ;
    MOV     #UMSTAR,PC              ;
CCFS_NEXT   FORTHtoASM              ;
    ADD     @PSP,&SectorL           ;
    ADDC    TOS,&SectorL+2          ;
    MOV     @RSP+,0(PSP)            ;
    .word   1768h                   ; POPM Y,TOS (6+1 pop,Y=8)
    .ENDIF
CCFS_End
    RET                             ; 
; ----------------------------------;

; ----------------------------------; input : X = countdown_of_spaces, Y = name pointer in buffer
ParseEntryNameSpaces                ;XY
; ----------------------------------; output: Z flag, Y is set after the last space char
    CMP     #0,X                    ; 
    JZ      PENS_End                ;
; ----------------------------------; input : X = countdown_of_spaces, Y = name pointer in buffer
ParseEntryNameSpacesLoop            ; here X must be > 0
; ----------------------------------; output: Z flag, Y is set after the last space char
    CMP.B   #32,BUFFER(Y)           ; SPACE ? 
    JNZ     PENS_End                ;
    ADD     #1,Y                    ;
    SUB     #1,X                    ;
    JNZ     ParseEntryNameSpacesLoop;
PENS_End                            ;
    RET                             ; 
; ----------------------------------; 

; common subroutine for LOAD", READ", WRITE"
; Input : EntryOfst, Cluster = EntryOfst(HDLW_FirstClus())
; init handle(HDLL_DIRsect,HDLW_DIRofst,HDLW_FirstClus,HDLW_CurClust,HDLL_CurSize)
; Output: Cluster = first Cluster of file, X = CurrentHdl
; ----------------------------------; input : Cluster, EntryOfst
GetFreeHandle                       ;UWXY init handle(HDLL_DIRsect,HDLW_DIRofst,HDLW_FirstClus = HDLW_CurClust,HDLL_CurSize)
; ----------------------------------; output : X = CurrentHdl*,
    MOV     #8,S                    ; 8 = file already open error
    MOV     #HandleMax,W            ; as handle count down
    MOV     #FirstHandle,X          ;
    MOV     #0,Y                    ; init previous handle of first handle = 0 !
; ----------------------------------;
GFH_Loop                            ;
; ----------------------------------;
    CMP.B   #0,HDLB_Token(X)        ; free handle ?
    JZ      GFH_FreeHandleFound     ; yes
GFH_AlreadyOpenTest                 ;
    CMP     &Cluster,HDLW_FirstClus(X);
    JZ      GFH_AlreadyOpen         ; error 8 abort ===> 
    MOV     X,Y                     ; handle is occupied, keep it in Y as previous handle
    ADD     #HandleLenght,X         ; 
    SUB     #1,W                    ; 
    JNZ     GFH_Loop                ;
; ----------------------------------;
    ADD     S,S                     ; 16 = no more handle error
    JMP     GFH_NoMoreHandle        ; abort ===>
; ----------------------------------;
GFH_FreeHandleFound                 ; X = new handle, Y = previous handle
; ----------------------------------;
    MOV     #0,S                    ; prepare GFH_HappyEnd
; ----------------------------------;
GFH_SavePreviousHandleBufferPtr     ;   
; ----------------------------------;
    CMP.B   #-1,HDLB_Token(Y)       ; if HDLB_Token(Y) = -1 ==> previous opened file was opened by LOAD"
    JNZ     GFH_InitHandle          ;
    ADD     &TOIN,HDLW_BUFofst(Y)   ; compute and save previous handle BufferPtr
; ----------------------------------;
GFH_InitHandle                      ;
; ----------------------------------;
    MOV     X,&CurrentHdl           ;
    MOV     Y,HDLW_PrevHDL(X)       ; init handle link to previous handle
    MOV     #1,HDLB_Token(X)        ; marks handle as read and clear cluster_sectors_offset
    MOV     &SectorL,HDLL_DIRsect(X); init handle DIRsectorL
    MOV &SectorL+2,HDLL_DIRsect+2(X); 
    MOV     &EntryOfst,Y            ;
    MOV     Y,HDLW_DIRofst(X)       ; init handle BUFFER offset of DIR entry
    MOV     &Cluster,HDLW_FirstClus(X); init handle firstcluster of file (to identify file)
    MOV     &Cluster,HDLW_CurClust(X)  ; init handle CurrentCluster
    MOV BUFFER+28(Y),HDLL_CurSize(X)  ; init handle LOW currentSizeL
    MOV BUFFER+30(Y),HDLL_CurSize+2(X);
    MOV     #0,HDLW_BUFofst(X)      ; reset buffer offset
GFH_HappyEnd                        ; X = CurrentHdl* = handle*, Y = Entry buffer offset
GFH_AlreadyOpen                     ; CurrentHdl, X = already_open_handle, Y = previous handle
GFH_NoMoreHandle                    ; CurrentHdl, X = 8th Handle
    RET                             ;
; ----------------------------------;

    .IFDEF SD_CARD_READ_WRITE

;-----------------------------------------------------------------------
; SD_READ_WRITE FORTH words
;-----------------------------------------------------------------------

;Z READ"         --
; parse string until " is encountered, convert counted string in StringZ
; then parse stringZ until char '0'.
; media identifiers "A:", "B:" ... are ignored (only one SD_Card),
; char "\" as first one initializes rootDir as SearchDir.
; if file found, if not already open and if free handle...
; ...open the file as read and return the handle in CurrentHdl.
; then load first sector in buffer, bufferLen and bufferPtr are ready for read
; currentHdl keep handle that is flagged as "read".

; to read sequentially next sectors use READ word. A flag is returned : true if file is closed.
; the last sector so is in buffer.

; if pathname is a directory, change current directory.
; if an error is encountered, no handle is set, error message is displayed.

; READ" acts also as CD dos command : 
;     - READ" a:\misc\" set a:\misc as current directory
;     - READ" a:\" reset current directory to root
;     - READ" ..\" change to parent directory

; to close all files type : WARM (or COLD, RESET)

; ----------------------------------;
    FORTHWORDIMM "READ\34"          ; immediate
; ----------------------------------;
READDQ
    MOV     #2,W                    ; W = OpenType
    JMP     Open_File            ;
; ----------------------------------;

;Z READ            -- f
; sequentially read a file opened by READ".
; sectors are loaded in BUFFER and BufferLen leave the count of loaded bytes.
; when the last sector of file is loaded in buffer, the handle is closed and flag is true (<>0).

; ----------------------------------;
    FORTHWORD "READ"                ; -- tf when handle is closed
; ----------------------------------;
    SUB     #2,PSP                  ;
    MOV     TOS,0(PSP)              ;
    MOV     &CurrentHdl,TOS         ;
    CALL    #Read_File              ;
    SUB     &CurrentHdl,TOS         ; -- true flag when handle is closed
    mNEXT                           ;
; ----------------------------------;



;Z WRITE" pathame"   --       immediate
; open or create the file designed by pathname.
; an error occurs if the file is already opened.
; the last sector of the file is loaded in buffer, and bufferPtr leave the address of the first free byte.
; compile state : compile WRITE" pathname"
; exec state : open or create entry selected by pathname
; ----------------------------------;
    FORTHWORDIMM "WRITE\34"         ; immediate
; ----------------------------------;
WRITEDQ
    MOV     #4,W                    ; W = OpenType
    JMP     Open_File               ;
; ----------------------------------;

;Z WRITE            -- 
; sequentially write the BUFFER content in a file opened by WRITE".
; ----------------------------------;
    FORTHWORD "WRITE"               ;
; ----------------------------------;
    CALL    #Write_File             ;
    mNEXT                           ;
; ----------------------------------;


;Z CLOSE      --     
; close current handle
; ----------------------------------;
    FORTHWORD "CLOSE"               ;
; ----------------------------------;
    CALL    #CloseHandle            ;
    mNEXT                           ;
; ----------------------------------;



;Z DEL" pathame"   --       immediate
; compile state : compile DEL" pathname"
; exec state : DELETE entry selected by pathname

; ----------------------------------;
    FORTHWORDIMM "DEL\34"           ; immediate
; ----------------------------------;
DELDQ
    MOV     #8,W                    ; W = OpenType
    JMP     Open_File            ;
; ----------------------------------;


    .ENDIF ; SD_CARD_READ_WRITE

;-----------------------------------------------------------------------
; SD_CARD_LOADER FORTH word
;-----------------------------------------------------------------------
;Z LOAD" pathame"   --       immediate
; compile state : compile LOAD" pathname"
; exec state : open a file from SD card via its pathname
; see Open_File primitive for pathname conventions 
; the opened file becomes the new input stream for INTERPRET
; this command is recursive, limited only by the count of free handles (up to 8)

; LOAD" acts also as dos command "CD" : 
;     - LOAD" \misc\" set a:\misc as current directory
;     - LOAD" \" reset current directory to root
;     - LOAD" ..\" change to parent directory

; ----------------------------------;
    FORTHWORDIMM "LOAD\34"          ; immediate
; ----------------------------------;
    MOV     #1,W                    ; W = OpenType
; ----------------------------------;


; ======================================================================
; SD_CARD_LOADER and SD_READ_WRITE primitive
; ======================================================================
; Open_File               --
; primitive for LOAD" READ" CREATE" WRITE" DEL"
; store OpenType on TOS,
; compile state : compile OpenType, compile SQUOTE and the string of provided pathname
; exec state :  open a file from SD card via its pathname
;               convert counted string found at HERE in a StringZ then parse it
;                   media identifiers "A:", "B:" ... are ignored (only one SD_Card),
;                   char "\" as first one initializes rootDir as SearchDir.
;               if file found, if not already open and if free handle...
;                   ...open the file as read and return the handle in CurrentHdl.
;               if the pathname is a directory, change current directory, no handle is set.
;               if an error is encountered, no handle is set, an error message is displayed.

; ----------------------------------;
Open_File                           ; --
; ----------------------------------;
    SUB     #2,PSP                  ;
    MOV     TOS,0(PSP)              ;
    MOV     W,TOS                   ; -- Open_type (0=LOAD", 1=READ", 2=WRITE", 4=DEL")
    CMP     #0,&STATE               ;
    JZ      OPEN_EXEC               ;
OPEN_COMP                           ;
    mDOCOL                          ; if compile state
    .word   lit,lit,COMMA,COMMA     ; compile open_type as literal
    .word   SQUOTE                  ; compile string_exec + string
    .word   lit,SQUOTE2HERE,COMMA   ; compile move in-line string to a counted string at HERE 
    .word   lit,ParenOpen,COMMA     ; compile (OPEN)
    .word   EXIT    

OPEN_EXEC                           ;
    mDOCOL                          ; if exec state
    .word   lit,34,WORDD            ; -- open_type HERE
    FORTHtoASM                      ;
    MOV     @RSP+,IP                ;
; ----------------------------------;
ParenOpen                           ; open_type HERE --
; ----------------------------------;
    SUB     #2,PSP                  ; make room for DIRsector
    MOV     &Sectorl,&MemSectorL    ;
    MOV     &Sectorl+2,&MemSectorL+2; for any error after LOAD"
; ----------------------------------;
OPN_CountedToStringZ                ;
; ----------------------------------;
    MOV.B   @TOS+,Y                 ; Y=count, TOS = HERE+1
    ADD     TOS,Y                   ; Y = end of counted string      
    MOV.B   #0,0(Y)                 ; open_type address_of_stringZ --
; ----------------------------------;
OPN_PathName                        ;
; ----------------------------------;
    MOV     &DIRcluster,&Cluster    ;
    CMP.B   #0,0(TOS)               ; first char = 0 ? 
    MOV     #1,S                    ; error 1
    JZ      OPN_NoPathName          ; abort ===>
    CMP.B   #':',1(TOS)             ; A: B: C: ... in pathname ?
    JNZ     OPN_AntiSlashStartTest  ; no
    ADD     #2,TOS                  ; yes : skip because not used, only one SD_card
OPN_AntiSlashStartTest              ;
    CMP.B   #5Ch,0(TOS)             ; "\" as first char ?
    JNZ     OPN_SearchDirSector     ; no
    ADD     #1,TOS                  ; yes : skip '\' char
    MOV     #1,&Cluster             ;       cluster 1 = RootDir
; ----------------------------------;
OPN_EndOfDIRstringZtest             ; <=== dir found in path
; ----------------------------------;
    CMP.B   #0,0(TOS)               ;   End of pathname ?
    JZ      OPN_SetCurrentDIR       ; yes
; ----------------------------------;
OPN_SearchDirSector                 ;
; ----------------------------------;
    MOV     TOS,&Pathname            ; save name addr
    CALL    #ComputeClusFrstSect    ;
    MOV     #32,0(PSP)              ; init countdown of DIR sectors
; ----------------------------------;
OPN_LoadSectorDir                   ; <=== Dir Sector loopback
; ----------------------------------;
    CALL    #ReadSectorL            ; use S,W,X registers
; ----------------------------------;
    MOV     #2,S                    ; prepare no such file error
    MOV     #0,W                    ; init entries count
; ----------------------------------;
OPN_SearchEntryInSector             ; <=== DIR Entry loopback
; ----------------------------------;
    MOV     &pathname,TOS           ; reload Pathname
    MOV     W,Y                     ; 1
    .word   0E58h                   ; 5 RLAM #4,Y --> * 16
    ADD     Y,Y                     ; 1           --> * 2
    MOV     Y,&EntryOfst            ; 
    CMP.B   #0,BUFFER(Y)            ; free entry ? (end of entries in DIR)
    JZ      OPN_NoSuchFile          ; output used by create ===>
    MOV     #8,X                    ; count of chars in entry name
OPN_CompareName8chars               ;
    CMP.B   @TOS+,BUFFER(Y)         ; compare Pathname(char) with DirEntry(char)
    JNZ     OPN_FirstCharMismatch   ;
    ADD     #1,Y                    ;
    SUB     #1,X                    ;
    JNZ     OPN_CompareName8chars   ; loopback if chars 1 to 7 of stringZ and DirEntry are equal
    ADD     #1,TOS                  ; 9th char of Pathname is always a dot
; ----------------------------------;
OPN_FirstCharMismatch               ;
    CMP.B   #'.',-1(TOS)            ; FirstNotEqualChar of Pathname = dot ?
    JZ      OPN_DotFound            ;
; ----------------------------------;
OPN_DotNotFound                     ; 
; ----------------------------------;
    ADD     #3,X                    ; for next cases not equal chars of entry until 11 must be spaces
    CALL #ParseEntryNameSpaces      ; for X + 3 chars
    JNZ     OPN_EntryMismatch       ; if a char entry <> space  
OPN_AntiSlashTest                   ;
    CMP.B   #5Ch,-1(TOS)            ; FirstNotEqualChar of Pathname = "\" ?
    JZ      OPN_EntryFound          ;
OPN_EndOfStringZtest                ;
    CMP.B   #0,-1(TOS)              ; FirstNotEqualChar of Pathname =  0  ?
    JZ      OPN_EntryFound          ;
; ----------------------------------;
OPN_EntryMismatch                   ;
; ----------------------------------;
    ADD     #1,W                    ; inc entry
    CMP     #16,W                   ; 16 entry in a sector
    JNZ     OPN_SearchEntryInSector ; ===> loopback for search next entry
; ----------------------------------;
    ADD     #1,&SectorL             ;
    ADDC    #0,&SectorL+2           ;
    SUB     #1,0(PSP)               ; dec count of Dir sectors
    JNZ     OPN_LoadSectorDir       ; ===> loopback for next DIR sector
; ----------------------------------;
OPN_EndOfDIR                        ;
; ----------------------------------;
    MOV     #4,S                    ; error 4
    JMP     OPN_DIRisFull           ; abort ===> 
; ----------------------------------;

; ----------------------------------;
OPN_DotFound                        ; not equal chars of entry name until 8 must be spaces
; ----------------------------------;
    CMP.B   #'.',-2(TOS)            ; LastCharEqual = dot ?
    JZ      OPN_EntryMismatch       ; case of first DIR entry = "." and Pathname = "..\" 
    CALL    #ParseEntryNameSpaces   ; parse X spaces, X{0,...,7}
    JNZ     OPN_EntryMismatch       ; if a char entry <> space
    MOV     #3,X                    ;
OPN_CompareExtChars                 ;
    CMP.B   @TOS+,BUFFER(Y)         ; compare stringZ(char) with DirEntry(char)
    JNZ     OPN_ExtCharNotEqual     ;
    ADD     #1,Y                    ;
    SUB     #1,X                    ;
    JNZ     OPN_CompareExtChars     ; nothing to do if chars equal
    JMP     OPN_EntryFound          ;
OPN_ExtCharNotEqual                 ;
    CMP.B   #0,-1(TOS)              ;
    JNZ     OPN_EntryMismatch       ;     
    CMP.B   #5Ch,-1(TOS)            ; FirstCharNotEqual = "\" ?
    JNZ     OPN_EntryMismatch       ;
    CALL    #ParseEntryNameSpaces   ; parse X spaces, X{0,...,3}
    JNZ     OPN_EntryMismatch       ; if a char entry <> space
; ----------------------------------;
OPN_EntryFound                      ; Y points on the file attribute (11th byte of entry)
; ----------------------------------;
    MOV     &EntryOfst,Y            ; reload DIRentry
    MOV     BUFFER+26(Y),&Cluster   ; first cluster of file
    BIT.B   #10h,BUFFER+11(Y)       ; test if Directory or File
    JZ      OPN_FileFound           ;
; ----------------------------------;
OPN_DIRfound                        ; entry is a DIRECTORY
; ----------------------------------;
    CMP     #0,&Cluster             ; case of ".." entry, when parent directory is root
    JNZ     OPN_DIRfoundNext        ;
    MOV     #1,&Cluster             ; set cluster RootDIR
OPN_DIRfoundNext                    ;
    CMP.B   #0,-1(TOS)              ; FirstCharNotEqual =  0  ?
    JNZ     OPN_EndOfDIRstringZtest ; no : FirstCharNotEqual = "\"
; ----------------------------------;
OPN_SetCurrentDIR                   ; yes
; ----------------------------------;
    MOV     &Cluster,&DIRcluster    ;
    ADD     #4,PSP                  ;
    MOV     @PSP+,TOS               ; --
    mNEXT                           ; happy end
; ----------------------------------;
OPN_FileFound                       ; entry is a file
; ----------------------------------; 
    CALL    #GetFreeHandle          ;UWXY init handle(HDLL_DIRsect,HDLW_DIRofst,HDLW_FirstClus = HDLW_CurClust,HDLL_CurSize)
; ----------------------------------; output : X = CurrentHdl*, S = ReturnError
OPN_NomoreHandle                    ; S = error 16
OPN_alreadyOpen                     ; S = error 8
OPN_DIRisFull                       ; S = error 4
OPN_NoSuchFile                      ; S = error 2
OPN_NoPathName                      ; S = error 1
    ADD     #2,PSP                  ;
    MOV     @PSP+,W                 ; W = open_type
    MOV     @PSP+,TOS               ; --
; ----------------------------------; then go to selected OpenType subroutine (OpenType = W register)


; ======================================================================
; LOAD" primitive as part of Open_File
; input : S = OpenError, W = open_type, X = CurrentHdl
;         SectorL = DIRsector, Buffer = [DIRsector]
; output: nothing else abort on error
; ======================================================================
    
; ----------------------------------;
    .IFDEF SD_CARD_READ_WRITE       ;
    CMP     #1,W                    ; open_type = LOAD"
    JNZ     OPEN_QREAD              ; next step
    .ENDIF                          ;
; ----------------------------------; here W is free
OPEN_LOAD                           ;
; ----------------------------------;
    CMP     #0,S                    ; open file happy end ?
    JZ      OPL_MarkHandleAsLoad    ; yes
; ----------------------------------;
    MOV     S,IP                    ; no, reload previous SectorL in interpretation before LOAD"
    MOV     &MemSectorL,&SectorL    ;
    MOV     &MemSectorL+2,&SectorL+2;
    CALL    #ReadSectorL            ; use S,W,X
    MOV     IP,S                    ;
; ----------------------------------;
OPEN_Error                          ; S= error
; ----------------------------------;
; Error 1  : PathNameNotFound       ; S = error 1
; Error 2  : NoSuchFile             ; S = error 2
; Error 4  : DIRisFull              ; S = error 4
; Error 8  : alreadyOpen            ; S = error 8
; Error 16 : NomoreHandle           ; S = error 16
; ----------------------------------;
    mDOCOL                          ;
    .word   XSQUOTE                 ;
    .byte   11,"< OpenError"        ;
    .word   SD_QABORTYES            ; to insert S error as flag, no return
; ----------------------------------;


; ----------------------------------;
OPL_MarkHandleAsLoad                ;
; ----------------------------------;
    MOV.B   #-1,HDLB_Token(X)         ; first, mark handle as "LOAD"ed file
    MOV     X,Y                     ; save X for OPL_ReadFirstSector
OPL_ComputeLinkLoop
    MOV     @Y,Y                    ; Y = previous handle
OPL_ComputeLink                     ;
    CMP     #0,Y                    ; end of opened handles ?
    JZ      OPL_ItIsTheFirstLoaded  ; yes
    CMP.B   #-1,HDLB_Token(Y)         ; no previous opened file was also opened by LOAD" ?
    JNZ     OPL_ComputeLinkLoop     ;    no
    JMP     OPL_ReadFirstSector     ;    yes
OPL_ItIsTheFirstLoaded              ;
    MOV     &TICKSOURCE,&SAVEtsLEN  ;
    SUB     &TOIN,&SAVEtsLEN        ; save TIB remaining lenght  
    MOV     &TICKSOURCE+2,&SAVEtsPTR;
    ADD     &TOIN,&SAVEtsPTR        ; save TIB pointer
    MOV     #SD_ACCEPT,&ACCEPT+2    ; redirect ACCEPT to SD_ACCEPT
    MOV     &TICKSOURCE,&TOIN       ; to abort interpret (same as BACKSLASH) then run SD_ACCEPT 
; ----------------------------------;
OPL_ReadFirstSector                 ; X = CurrentHdl
; ----------------------------------;
    CALL    #LoadClusFirstSect      ;STWXY
    MOV     #0,&BufferPtr           ;
    mNEXT
; ----------------------------------;




;-----------------------------------------------------------------------
; SD TOOLS
;-----------------------------------------------------------------------

    .IFDEF SD_TOOLS
    .include "ADDON\SD_TOOLS.asm"
    .ENDIF ;  SD_READ_WRITE_TOOLS






