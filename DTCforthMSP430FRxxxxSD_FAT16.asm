; -*- coding: utf-8 -*-
; DTCforthMSP430FRxxxxSD_FAT16.asm

; Copyright (C) <2015>  <J.M. THOORENS>
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.

; FAT is little endian structure.

; BPB_RsvdSecCnt : sectors reserved for boot code
; BPB_RootEntCnt : must be set so that the root directory occupies entire sectors


; fat_offset = active_cluster * 2
; fat_sector = first_fat_sector + (fat_offset / sector_size)
; ent_offset = fat_offset mod sector_size


; ALL VALUES ARE LITTLE ENDIAN !

; we assume that SDSC Card (up to 2GB) is FAT16 with a byte addressing
; and that SDHC Card (4GB up to 32GB) is FAT32 with a block addressing (block = 512 bytes)
; let's test only FAT16/FAT32  

; Formatage FA16 d'une SDSC Card 2GB
; First sector of physical drive (sector 0) content :
; ---------------------------------------------------
; dec@| HEX@ =  HEX                                                     decimal
; 454 |0x1C6 = 89 00  : FirstSector (of logical drive)   BS_FirstSector  = 137
; 
; FirstSector of logical drive (sector 0) content :
; -------------------------------------------------
; dec@| HEX@ =  HEX                                                     decimal
; 11  | 0x0B = 00 02        : 512 bytes/sector           BPB_BytsPerSec  = 512
; 13  | 0x0D = 40           : 64 sectors/cluster         BPB_SecPerClus  = 64
; 14  | 0x0E = 02 00        : 2 reserved sectors         BPB_RsvdSecCnt  = 2
; 16  | 0x10 = 02           : 2 FATs                     BPB_NumFATs     = 2 (always 2)
; 17  | 0x11 = 00 02        : 512 entries/directory      BPB_RootEntCnt  = 512
; 19  | 0x13 = 00 00        : BPB_TotSec16 (> 65535)     BPB_TotSec16    = 0
; 22  | 0x16 = EB 00        : 235 sectors/FAT (FAT16)    BPB_FATSize     = 235
; 32  | 0x20 = 77 9F 3A 00  : ‭3841911‬ total sectors      BPB_TotSec32    = ‭3841911‬
; 54  | 0x36 = "FAT16   "                                BS_FilSysType

; cluster = memory allocation unit, up to 0xFFF7 for FAT16
; during formating the media, the number of clusters is calculated as to be close 65527,
; to minimize the size of the allocation unit.

; all values below are evaluated in sectors
; OrgRootDirL    = BS_FirstSector + BPB_RsvdSecCnt + (BPB_FATSize * BPB_NumFATs) = 609
; OrgCluster     = OrgRootDir - 2*BPB_SecPerClus                = 513 (virtual value)
; RootDirSize    = BPB_RootEntCnt * 32 / BPB_BytsPerSec         = 32 sectors
; OrgDatas       = OrgRootDir + RootDirSize                     = 641
; FirstSectorOfCluster(n) = OrgCluster + n*BPB_SecPerClus       = cluster(3) = 705



; ==================================;
RW_Sector_CMD                       ; input : SD_CMD_FRM(5) = CMD17 or CMD24 (read or write Sector CMD)
; ==================================; in SPI mode CRC is not required, but CMD frame must be ended with 1 !
    BIC.B   #SD_CS,&SD_CSOUT        ; SD_CS low
    BIT.B   #SD_CD,&SD_CDIN         ; memory card present ?
    JNZ     NO_SD_CARD              ; ==> COLD
                                    ; FAT16 : CMD17/24 address = Sector * BPB_BytsPerSec.
    MOV     #1,&SD_CMD_FRM          ; $(01 00 xx xx xx CMD)
    ADD     W,W                     ; shift left one SectorL
    ADDC.B  X,X                     ;
    MOV     W,&SD_CMD_FRM+2         ; $(01 00 ll LL xx CMD)
    MOV.B   X,&SD_CMD_FRM+4         ; $(01 00 ll LL hh CMD) 
; ==================================;
SDbusyLoop                          ; previous command might be with a long long long busy time... 
; ==================================;
    CALL #SPI_GET                   ;
    CMP.B   #-1,W                   ; FFh expected value : MISO = 1
    JNZ SDbusyLoop                  ; until FF read, i.e. MISO = 1
    MOV     #0,W                    ; set expected R1 response = 0 = ok
; ==================================;
sendCommand                         ;
; ==================================;         W = expected return value
                                    ; input : SD_CMD_FRM : {CRC,byte_l,byte_L,byte_h,byte_H,CMD} 
                                    ;         W = expected return value
                                    ; output  W is unchanged, X = -1, flag Z is positionned
                                    ;
                                    ; reverts bytes before send : $(CMD hh LL ll 00 CRC)
    MOV     #5,X                    ; warning : X = index AND countdown !
Send_CMD_PUT                        ; move little endian --> big endian
    MOV.B   SD_CMD_FRM(X),&SD_TXBUF ;5 (5+1) x spi_put
    CMP     #0,&SD_BRW              ;3 full speed ?
    JZ      FullSpeedSend           ;2 yes, no need to add NOP(s) to avoid SD_error
Send_CMD_Loop                       ;  no: case of low speed during memCardInit
    BIT     #UCRXIFG,&SD_IFG        ;3
    JZ      Send_CMD_Loop           ;2
    CMP.B   #0,&SD_RXBUF            ;3 to clear UCRXIFG
FullSpeedSend                       ;
    SUB.B   #1,X                    ;1
    JHS     Send_CMD_PUT            ;2 U>= : don't skip SD_CMD_FRM(0) !
                                    ; here X=0xFF, so wait CMD return expected value with SPI_GET sending 256*FFh max
                                    ; host must provide height clock cycles to complete operation
;    MOV     #4,X                    ; min value to pass made in PRC SD_Card init 
;    MOV     #16,X                   ; min value to pass Transcend SD_Card init
;    MOV     #32,X                   ; min value to pass Panasonic SD_Card init
;    MOV     #64,X                   ; min value to pass SanDisk SD_Card init
;    MOV     #8000,X                 ; max value
; ----------------------------------;
Wait_Command_Response               ; expect W = return value during X delay time
; ----------------------------------;
    SUB     #1,X                    ;
    JN      SPI_WAIT_end            ; 
    MOV.B   #-1,&SD_TXBUF           ;3 SPI(put) = FF
    CMP     #0,&SD_BRW              ;3 full speed ?
    JZ      FullSpeedGET            ;2 yes
cardResp_Getloop                    ;  no: case of low speed during memCardInit
    BIT     #UCRXIFG,&SD_IFG        ;3
    JZ      cardResp_Getloop        ;2
FullSpeedGET                        ;
    NOP2                            ;2 adjusted to avoid SD_error
    CMP.B   &SD_RXBUF,W             ; return value = ExpectedValue ?
    JNZ     Wait_Command_Response   ;
SPI_WAIT_end                        ;
    RET                             ; W is unchanged, X = -1
; ----------------------------------; SR(Z) = 1 <==> Return value = expected value


; SPI_GET and SPI_PUT are adjusted for SD_CLK = SMCLK = MCLK

; ==================================;
SPI_GET                             ; PUT(FFh);
; ==================================; output : W = returned byte, X = 0
    MOV #1,X                        ;1
; ==================================;
SPI_X_GET                           ; PUT(FFh) X time
; ==================================; output : W = last returned byte, X = 0
    MOV #-1,W                       ;1
; ==================================;
SPI_PUT                             ; PUT(W) X time : PUT Wlow then PUT Whigh and so forth
; ==================================; output : W = last returned byte, X = 0
                                    ;
            MOV.B W,&SD_TXBUF       ;3 put W low value then W high value and so forth
            CMP #0,&SD_BRW          ;3 full speed ?
            JZ FullSpeedPut         ;2 
SPI_PUTWAIT BIT #UCRXIFG,&SD_IFG    ;3
            JZ SPI_PUTWAIT          ;2
            CMP.B #0,&SD_RXBUF      ;3 reset RX flag
FullSpeedPut
;            NOP                     ;1 adjusted to avoid SD error
            SWPB W                  ;1
            SUB #1,X                ;1
            JNZ SPI_PUT             ;2
SPI_PUT_END MOV.B &SD_RXBUF,W       ;3
            RET                     ;4
; ----------------------------------;


; ==================================;
readSectorWX                        ; use S,W,X registers only
; ==================================;
    BIS     #1,S                    ; preset sd_read error
    MOV.B   #51h,&SD_CMD_FRM+5      ; CMD17 = READ_SINGLE_BLOCK (at address of first byte of sectorL)
    CALL    #RW_Sector_CMD          ; use WX
    JNE     SD_CARD_ERROR           ; error if R1 <> 0 
; ----------------------------------;
SDreadFEhLoop                       ; wait SD_Card sending FEh
; ----------------------------------;
    CALL #SPI_GET                   ;
    CMP.B   #-2,W                   ; FEh expected value
    JNZ SDreadFEhLoop               ;
; ----------------------------------; X = 0
;    MOV     #0,X                    ; as bytes count and index
; ----------------------------------;
RS_SectorLoop                       ; 16 cycles loop read byte
; ----------------------------------;
    MOV.B   #-1,&SD_TXBUF           ; 3 put FF
    NOP3                            ; 3 adjusted to avoid read SD_error
    ADD     #1,X                    ; 1
    CMP     #BytsPerSec,X           ; 2
    MOV.B   &SD_RXBUF,BUFFER-1(X)   ; 5
    JNZ     RS_SectorLoop           ; 2
; ----------------------------------;
RS_SkipCRC16                        ;
; ----------------------------------;
    MOV     #2,X                    ; PUT 2 bytes to skip CRC16
    CALL    #SPI_X_GET              ;
; ----------------------------------;
SD_RW_HappyEnd
; ----------------------------------;
    BIC     #3,S                    ; reset read and write errors
    BIS.B   #SD_CS,&SD_CSOUT        ; SD_CS = high
    RET                             ; 
; ----------------------------------;

    .IFDEF SD_CARD_READ_WRITE

; ==================================;
WriteSectorWX                       ; use S,W,X registers only
; ==================================;
    BIS     #2,S                    ; preset sd_write error
    MOV.B   #058h,SD_CMD_FRM+5      ; CMD24 = WRITE_SINGLE_BLOCK
    CALL    #RW_Sector_CMD          ; use WX
    JNE     SD_CARD_ERROR           ; ReturnError = 2
    MOV     #0FEFFh,W               ; PUT FFFEh
    MOV     #2,X                    ; put 16 bits
    CALL    #SPI_PUT                ; X = 0
;    MOV     #0,X                    ; init count of TX bytes
; ----------------------------------;
WS_SectorLoop                       ; 11 cycles loop write
; ----------------------------------;
    MOV.B   BUFFER(X),&SD_TXBUF     ; 5
    NOP                             ; 1 adjusted to avoid write SD_error
    ADD     #1,X                    ; 1
    CMP     #BytsPerSec,X           ; 2
    JNZ     WS_SectorLoop           ; 2
; ----------------------------------;
WS_CRC16                            ; not verified by SD_card in SPI mode
; ----------------------------------;
    MOV     #3,X                    ; PUT 2 bytes to skip CRC16 + 1 byte to get data token in W
    CALL    #SPI_X_GET              ;
; ----------------------------------;
checkWriteState                     ;
; ----------------------------------;
    BIC.B   #0E1h,W                 ; apply mask for Data response
    CMP.B   #4,W                    ; data accepted
    JZ      SD_RW_HappyEnd          ;

    .ENDIF                          ; SD_CARD_READ_WRITE

; ----------------------------------;
SD_CARD_ERROR                       ;
; ----------------------------------;
    BIS.B #SD_CS,&SD_CSOUT          ; SD_CS = high
    BIT.B #SD_CD,&SD_CDIN           ; SD_memory in SD_Card module ?
    JZ SD_CARD_ERRNEXT              ; yes
NO_SD_CARD
    MOV #COLD,PC                    ; no ==> force reset
SD_CARD_ERRNEXT                     ;
    SWPB S                          ; High Level error in High byte
    ADD W,S                         ; add SPI(GET) return value to high level error
    mDOCOL                          ;
    .word   XSQUOTE
    .byte   11,"< SD Error!"
    .word   SD_QABORTYES            ;

; SD Error n°
; High byte
; 2  = CMD24    write error 
; 1  = CMD17    read error which can be added to the following errors
; 4  = CMD0     time out (GO_IDLE_STATE)
; 8  = CMD1     time out (SEND_OP_COND)
; 10 = ACMD41   time out (APP_SEND_OP_COND)
; 20 = CMD16    time out (SET_BLOCKLEN)
; 40 = not FAT16

; low byte = CMD R1 response : |0|7|6|5|4|3|2|1|
; 1th bit = In Idle state
; 2th bit = Erase reset
; 3th bit = Illegal command
; 4th bit = Command CRC error
; 5th bit = erase sequence error
; 6th bit = address error
; 7th bit = parameter error

; ==================================; no input
memCardInit                         ; use S,W,X,Y,BUFFER
; ==================================; output: BSP sector in buffer if ReturnError = 0
    MOV     #4,S                    ; preset error 4R1
    BIC.B   #SD_CS,&SD_CSOUT        ; set SD_CS output low to switch in SPI mode
    MOV     #95h,&SD_CMD_FRM        ; $(95 00)
    MOV     #0,&SD_CMD_FRM+2        ; $(95 00 00 00)
    MOV     #4000h,&SD_CMD_FRM+4    ; $(95 00 00 00 00 40); send CMD0 
    MOV     #1,W                    ; expected R1 response = 1 = idle state
    CALL    #sendCommand            ; send command CMD0 : GO_IDLE_STATE
    JNE     SD_CARD_ERROR           ;
; ----------------------------------;
; Send CMD8                         ; mandatory if SD_Card >= V2.x
; ----------------------------------;
    ADD     S,S                     ; preset error 8R1 for CMD1
    MOV     #0AA87h,&SD_CMD_FRM     ; $(87 AA)
    MOV     #1,&SD_CMD_FRM+2        ; $(87 AA 01 00 
    MOV     #4800h,&SD_CMD_FRM+4    ; $(87 AA 01 00 00 48); send CMD8 
    MOV     #1,W                    ; expected R7 response = idle state
    CALL    #sendCommand            ; send Command CMD8 : SEND_IF_COND
; ----------------------------------;
; prepare next frame and time delay ;
; ----------------------------------;
    MOV.B   #0,&SD_CMD_FRM+1        ; $(xx 00 ...
    MOV     #0,&SD_CMD_FRM+2        ; $(xx 00 00 00 ...
    MOV.B   #-1,Y                   ; init time out delay = 255 * [(CMD55 + ACMD41)|(CMD11)]
; ----------------------------------;
    JNE     Init_CMD1               ; if CMD8 returns error 
    MOV     #4,X
    CALL    #SPI_X_GET              ; skip end of response type R7 (4 bytes)
; ----------------------------------;
; Init with ACMD41                  ;
; ----------------------------------;
    ADD     S,S                     ; preset error 10R1 for ACMD41
SEND_ACMD41                         ;
    MOV     #7700h,&SD_CMD_FRM+4    ; $(xx 00 00 00 00 77)
    MOV     #1,W                    ; expected R1 response = idle
    CALL    #sendCommand            ; send Command CMD55 : APP_CMD
    MOV     #6900h,&SD_CMD_FRM+4    ; $(xx 00 00 00 00 69)
    MOV     #0,W                    ; expected R1 response = 0 : SD_Card ready
    CALL    #sendCommand            ; send Command ACMD41 : APP_SEND_OP_COND
    JZ      MCI_setBLockLength      ; 
    SUB     #1,Y                    ;
    JNZ     SEND_ACMD41             ;
    JMP     SD_CARD_ERROR           ; on time out
; ----------------------------------;
; INIT with CMD1                    ; alternative to CMD8
; ----------------------------------;
Init_CMD1                           ;
    MOV     #4100h,&SD_CMD_FRM+4    ; $(xx 00 00 00 00 41)
    MOV     #0,W                    ; 0 = SD_Card ready
SEND_CMD1                           ;
    CALL    #sendCommand            ; Command = CMD1 : SEND_OP_COND
    JZ      CMD1_End                ; 
    SUB     #1,Y                    ;
    JNZ     SEND_CMD1               ;
    JMP     SD_CARD_ERROR           ; on time out
CMD1_End                            ;
    ADD     S,S                     ;
; ----------------------------------;
MCI_setBLockLength                  ; set block = 512 bytes (buffer size)
    ADD     S,S                     ; preset error 20R1 for CMD16
    MOV     #02h,&SD_CMD_FRM+2      ; $(xx 00 02 00 00 yy)
    MOV.B   #50h,&SD_CMD_FRM+5      ; $(xx 00 02 00 00 50)  50h = CMD16 : SET_BLOCKLEN = 0x200
    CALL    #SDbusyLoop             ; wait before sendCommand, that reverts bytes $(50 00 00 02 00 xx) before send
    JNZ     SD_CARD_ERROR           ; ReturnError = 20R1 ; send command ko
; ----------------------------------;
MCI_SetHighSpeed                    ; end of init SD ==> SD_CLK = SMCLK
    BIS     #1,&SD_CTLW0            ; Software reset
    MOV     #0,&SD_BRW              ; UCxxBRW
    BIC     #1,&SD_CTLW0            ; release from reset
; ----------------------------------;
MCI_GetFirstSector                  ;
    MOV     #0,W                    ;
    MOV     #0,X                    ;
    CALL    #readSectorWX           ; read bootstrap sector
; ----------------------------------;
MCI_ReadBootSector                  ;
    MOV     #454,W                  ;
    MOV     BUFFER(W),W             ; BS_FirstSector
    MOV     W,&FirstSector          ;
    MOV     #0,X                    ; 
    CALL    #readSectorWX           ; use SWX
    BIS.B   #SD_CS,&SD_CSOUT        ; SD_CS = 1
; ----------------------------------;
FAT16_init
; ----------------------------------;
FI_identifyFileSystem               ;
    ADD     S,S                     ; preset error 40h ; SD card not FAT16
    MOV     #BUFFER,Y
FI_FAT16test
    CMP.B   #'F',54(Y)
    JNE     SD_CARD_ERROR           ;
;    CMP.B   #'A',55(Y)
;    JNE     SD_CARD_ERROR           ;
;    CMP.B   #'T',56(Y)
;    JNE     SD_CARD_ERROR           ;
;    CMP.B   #'1',57(Y)
;    JNE     SD_CARD_ERROR           ;
    CMP.B   #'6',58(Y)
    JNE     SD_CARD_ERROR           ;
; ----------------------------------;
FI_SetFileSystemInfos               ;
; ----------------------------------;
    MOV     #0,&TotSectorsL+2       ;
    MOV.B   19(Y),&TotSectorsL      ; BPB_TotSec16LO
    MOV.B   20(Y),&TotSectorsL+1    ; BPB_TotSec16HI
    CMP     #0,&TotSectorsL         ;
    JNZ     FI_TotSectEnd           ;
    MOV     32(Y),&TotSectorsL      ; BPB_TotSec32LO
    MOV     34(Y),&TotSectorsL+2    ; BPB_TotSec32HI
FI_TotSectEnd                       ;
    MOV.B   13(Y),&SecPerClus       ; BPB_SecPerClus
    MOV     &FirstSector,X          ;
    ADD     14(Y),X                 ; BPB_RsvdSecCnt
    MOV     X,&OrgFAT1              ; set OrgFAT1
    MOV     X,&CurFATsector         ; init last FAT acces for free cluster
    MOV     22(Y),W                 ; BPB_FATsize
    MOV     W,&FATSize              ;
    ADD     W,X                     ;
    MOV     X,&OrgFAT2              ; OrgFAT1 + FATsize = OrgFAT2
    ADD     W,X                     ; OrgFAT2 + FATsize = OrgRootDir
    MOV     X,&OrgRootDir           ; set OrgRootDir
    MOV     #1,&DIRcluster          ; as RootDir
    ADD     #32,X                   ; OrgRootDir + RootDirSize = OrgDatas
    MOV     X,&OrgDatas             ; OrgDatas = Cluster 2 address
    SUB     &SecPerClus,X           ;
    SUB     &SecPerClus,X           ;
    MOV     X,&OrgClusters          ; virtual cluster 0 address
    RET                             ; 
; ----------------------------------;

