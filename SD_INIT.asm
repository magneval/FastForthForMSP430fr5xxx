; -*- coding: utf-8 -*-
; http://patorjk.com/software/taag/#p=display&f=Banner&t=Fast Forth

; Fast Forth For Texas Instrument MSP430FRxxxx FRAM devices
; Copyright (C) <2015>  <J.M. THOORENS>
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.


    MOV     #0A981h,&SD_CTLW0       ; UCxxCTL1  = CKPH, MSB, MST, SPI_3, SMCLK  + UCSWRST
    MOV     #FREQUENCY*3,&SD_BRW    ; UCxxBRW init SPI CLK = 333 kHz ( < 400 kHz)


    .IFDEF MSP_EXP430FR5739 

; COLD default state : Px{DIR,SEL0,SEL1,SELC,IE,IFG,IV} = 0 ; PX{OUT,REN} = 1 ; Px{IN,IES} = ?

; P2.2 - RF.16                  <--- CD  SD_CardAdapter (Card Detect)
SD_CD           .equ  4
SD_CDIN         .equ  P2IN
; P2.3 - RF.10                  ---> CS  SD_CardAdapter (Card Select)
SD_CS           .equ  8
SD_CSOUT        .equ  P2OUT
    BIS.B #SD_CS,&P2DIR ; SD_CS output high

; P2.4 - RF.14 UCA1 CLK         ---> CLK SD_CardAdapter (SCK)  
; P2.5 - RF.7  UCA1 TXD/SIMO    ---> SDI SD_CardAdapter (MOSI)
; P2.6 - RF.5  UCA1 RXD/SOMI    <--- SDO SD_CardAdapter (MISO)
    BIS.B #070h,&P2SEL1 ; Configure UCA1 pins P2.4 as UCA1CLK, P2.5 as UCA1SIMO & P2.6 as UCA1SOMI
                        ; P2DIR.x is controlled by eUSCI_A0 module
    BIC.B #070h,&P2REN  ; disable pullup resistors for SIMO/SOMI/SCK pins

    .ENDIF
    .IFDEF MSP_EXP430FR5969 

; COLD default state : Px{DIR,SEL0,SEL1,SELC,IE,IFG,IV} = 0 ; PX{OUT,REN} = 1 ; Px{IN,IES} = ?

; P4.2                <--- SD_CD (Card Detect)
SD_CD           .equ  4
SD_CDIN         .equ  P4IN
; P4.3                ---> SD_CS (Card Select)
SD_CS           .equ  8
SD_CSOUT        .equ  P4OUT
    BIS.B #SD_CS,&P4DIR  ; SD_CS output high

; P2.4  UCA1     CLK  ---> SD_CLK
; P2.5  UCA1 TX/SIMO  ---> SD_SDI
; P2.6  UCA1 RX/SOMI  <--- SD_SDO
    BIS.B   #070h,  &P2SEL1 ; Configure UCA1 pins P2.4 as UCA1CLK, P2.5 as UCA1SIMO & P2.6 as UCA1SOMI
                            ; P2DIR.x is controlled by eUSCI_A0 module
    BIC.B   #070h,  &P2REN  ; disable pullup resistors for SIMO/SOMI/SCK pins

    .ENDIF
    .IFDEF MSP_EXP430FR5994 

; COLD default state : Px{DIR,SEL0,SEL1,SELC,IE,IFG,IV} = 0 ; PX{OUT,REN} = 1 ; Px{IN,IES} = ?

; P7.2/UCB2CLK                        - SD_CD (Card Detect)
SD_CD           .equ  4 ; P7.2
SD_CDIN         .equ  P7IN
; P4.0/A8                             - SD_CS (Card Select)
SD_CS           .equ  1 ; P4.0
SD_CSOUT        .equ  P4OUT
    BIS.B #SD_CS,&P4DIR ; SD_CS output high

; P2.2/TB0.2/UCB0CLK                  - SD_CLK
; P1.6/TB0.3/UCB0SIMO/UCB0SDA/TA0.0   - SD_SDI
; P1.7/TB0.4/UCB0SOMI/UCB0SCL/TA1.0   - SD_SDO
    BIS #04C0h,&PASEL1  ; Configure UCB0 pins P1.6 as UCB0SIMO, P1.7 as UCB0SOMI& UCB0 pins P2.2 as UCB0CLK
                        ; PxDIR.x is controlled by eUSCI_A0 module
    BIC #04C0h,&PAREN   ; disable pullup resistors for SIMO/SOMI/SCK pins

    .ENDIF
    .IFDEF MSP_EXP430FR6989 

; COLD default state : Px{DIR,SEL0,SEL1,SELC,IE,IFG,IV} = 0 ; PX{OUT,REN} = 1 ; Px{IN,IES} = ?

; P2.7                <--- SD_CD (Card Detect)
SD_CD           .equ  80h
SD_CDIN         .equ  P2IN
; P2.6                ---> SD_CS (Card Select)
SD_CS           .equ  40h
SD_CSOUT        .equ  P2OUT
    BIS.B #SD_CS,&P2DIR ; SD_CS output high

; P2.2  UCA0     CLK  ---> SD_CLK
; P2.0  UCA0 TX/SIMO  ---> SD_SDI
; P2.2  UCA0 RX/SOMI  <--- SD_SDO
    BIS.B #007h,&P2SEL0 ; Configure UCA1 pins P2.2 as UCA0CLK, P2.0 as UCA0SIMO & P2.1 as UCA0SOMI
                        ; P2DIR.x is controlled by eUSCI_A0 module
    BIC.B #007h,&P2REN  ; disable pullup resistors for SIMO/SOMI/SCK pins

    .ENDIF
    .IFDEF MSP_EXP430FR4133 

; COLD default state : Px{DIR,SEL0,SEL1,SELC,IE,IFG,IV} = 0 ; PX{OUT,REN} = 1 ; Px{IN,IES} = ?

; P8.0                <--- SD_CD (Card Detect)
SD_CD           .equ  1
SD_CDIN         .equ  P8IN
; P8.1                ---> SD_CS (Card Select)
SD_CS           .equ  2
SD_CSOUT        .equ  P8OUT
    BIS.B #SD_CS,&P8DIR ; SD_CS output high

; P5.1  UCB0     CLK  ---> SD_CLK
; P5.2  UCB0 TX/SIMO  ---> SD_SDI
; P5.3  UCB0 RX/SOMI  <--- SD_SDO
    BIS.B   #00Eh,&P5SEL1   ; Configure UCB0 pins P5.1 as CLK, P5.2 as SIMO & P5.3 as SOMI
                            ; P2DIR.x is controlled by eUSCI_A0 module
    BIC.B   #00Eh,&P5REN    ; disable pullup resistors for SIMO/SOMI/SCK pins

    .ENDIF
    .IFDEF CHIPSTICK_FR2433 

; COLD default state : Px{DIR,SEL0,SEL1,SELC,IE,IFG,IV} = 0 ; PX{OUT,REN} = 1 ; Px{IN,IES} = ?

; P2.3                <--- SD_CD (Card Detect)
SD_CD           .equ  8
SD_CDIN         .equ  P2IN
; P2.2                ---> SD_CS (Card Select)
SD_CS           .equ  4
SD_CSOUT        .equ  P2OUT
    BIS.B #SD_CS,&P2DIR ; SD_CS output high

; P1.1  UCB0     CLK  ---> SD_CLK
; P1.2  UCB0 TX/SIMO  ---> SD_SDI
; P1.3  UCB0 RX/SOMI  <--- SD_SDO
    BIS.B   #00Eh,&P1SEL0   ; Configure UCB0 pins P1.1 as CLK, P1.2 as SIMO & P1.3 as SOMI
                            ; P1DIR.x is controlled by eUSCI_B0 module
    BIC.B   #00Eh,&P1REN    ; disable pullup resistors for SIMO/SOMI/SCK pins

    .ENDIF
    .IFDEF MY_MSP430FR5738

; COLD default state : Px{DIR,SEL0,SEL1,SELC,IE,IFG,IV} = 0 ; PX{OUT,REN} = 1 ; Px{IN,IES} = ?

; P2.3 as SD_CD
SD_CD           .equ  08h
SD_CDIN         .equ  P2IN
; P2.4 as SD_CS
SD_CS           .equ  10h
SD_CSOUT        .equ  P2OUT
    BIS.B #SD_CS,&P2DIR ; SD_CS output high

; P2.2/UCB0CLK                ---> SD_CardAdapter CLK (SCK)   default value
; P1.6/UCB0SIMO/UCB0SDA/TA0.0 ---> SD_CardAdapter SDI (MOSI)  default value
; P1.7/UCB0SOMI/UCB0SCL/TA1.0 <--- SD_CardAdapter SDO (MISO)  default value
    BIS #04C0h,&PASEL1  ; Configure UCB0 pins P2.2 as UCB0CLK, P1.6 as UCB0SIMO & P1.7 as UCB0SOMI
                        ; P2DIR.x is controlled by eUSCI_B0 module
    BIC #04C0h,&PAREN   ; disable pullup resistors for SIMO/SOMI/CLK pins

    .ENDIF
    .IFDEF MY_MSP430FR5738_1

; COLD default state : Px{DIR,SEL0,SEL1,SELC,IE,IFG,IV} = 0 ; PX{OUT,REN} = 1 ; Px{IN,IES} = ?

; P2.3 as SD_CD
SD_CD           .equ  08h
SD_CDIN         .equ  P2IN
; P2.4 as SD_CS
SD_CS           .equ  10h
SD_CSOUT        .equ  P2OUT
    BIS.B #SD_CS,&P2DIR ; SD_CS output high

; P2.2/UCB0CLK                ---> SD_CardAdapter CLK (SCK)   default value
; P1.6/UCB0SIMO/UCB0SDA/TA0.0 ---> SD_CardAdapter SDI (MOSI)  default value
; P1.7/UCB0SOMI/UCB0SCL/TA1.0 <--- SD_CardAdapter SDO (MISO)  default value
    BIS #04C0h,&PASEL1  ; Configure UCB0 pins P2.2 as UCB0CLK, P1.6 as UCB0SIMO & P1.7 as UCB0SOMI
                        ; P2DIR.x is controlled by eUSCI_B0 module
    BIC #04C0h,&PAREN   ; disable pullup resistors for SIMO/SOMI/CLK pins

    .ENDIF
    .IFDEF MY_MSP430FR5948 

; COLD default state : Px{DIR,SEL0,SEL1,SELC,IE,IFG,IV} = 0 ; PX{OUT,REN} = 1 ; Px{IN,IES} = ?

; P2.3                <--- SD_CD (Card Detect)
SD_CD           .equ  08h
SD_CDIN         .equ  P2IN 
; P2.7                ---> SD_CS (Card Select)
SD_CS           .equ  80h
SD_CSOUT        .equ  P2OUT
    BIS.B #SD_CS,&P2DIR ; SD_CS output high

; P2.4  UCA1     CLK  ---> SD_CLK
; P2.5  UCA1 TX/SIMO  ---> SD_SDI
; P2.6  UCA1 RX/SOMI  <--- SD_SDO
    BIS.B #070h,&P2SEL1 ; Configure UCA1 pins P2.4 as UCA1CLK, P2.5 as UCA1SIMO & P2.6 as UCA1SOMI
                        ; P2DIR.x is controlled by eUSCI_A0 module
    BIC.B #070h,&P2REN  ; disable pullup resistors for SIMO/SOMI/SCK pins

    .ENDIF
    .IFDEF MY_MSP430FR5948_1 ; = new version of MY_MSP430FR5948

; COLD default state : Px{DIR,SEL0,SEL1,SELC,IE,IFG,IV} = 0 ; PX{OUT,REN} = 1 ; Px{IN,IES} = ?

; P2.7                ---> SD_CD (Card Detect)
SD_CD           .equ  80h
SD_CDIN         .equ  P2IN
; P2.3                <--- SD_CS (Card Select)
SD_CS           .equ  08h
SD_CSOUT        .equ  P2OUT
    BIS.B #SD_CS,&P2DIR ; SD_CS output high

; P2.4  UCA1     CLK  ---> SD_CLK
; P2.5  UCA1 TX/SIMO  ---> SD_SDI
; P2.6  UCA1 RX/SOMI  <--- SD_SDO
    BIS.B #070h,&P2SEL1 ; Configure UCA1 pins P2.4 as UCA1CLK, P2.5 as UCA1SIMO & P2.6 as UCA1SOMI
                        ; P2DIR.x is controlled by eUSCI_A0 module
    BIC.B #070h,&P2REN  ; disable pullup resistors for SIMO/SOMI/SCK pins

    .ENDIF

    BIC     #1,&SD_CTLW0            ; release from reset

