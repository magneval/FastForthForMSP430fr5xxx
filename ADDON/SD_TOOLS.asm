; -*- coding: utf-8 -*-
; http://patorjk.com/software/taag/#p=display&f=Banner&t=Fast Forth

; Fast Forth For Texas Instrument MSP430FRxxxx FRAM devices
; Copyright (C) <2015>  <J.M. THOORENS>
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.



    .IFNDEF UTILITY
    .include "ADDON\UTILITY.asm"
    .ENDIF


; dump FAT sector of last entry
; ----------------------------------;
            FORTHWORD "FAT"         ;VWXY Display CurFATsector
; ----------------------------------;
    MOV     &CurFATsector,W         ; W = FATsectorLO
FATnext                             ;
    SUB     #2,PSP                  ;
    MOV     TOS,0(PSP)              ;
    MOV     W,TOS                   ;
FATReadSectorW                      ;
    MOV     #0,X                    ; X = FATsectorHI = 0
    CALL    #readSectorWX           ;SXY
    mDOCOL                          ;
    .word   UDOT                    ;
    .word   lit,1e00h,lit,200h,DUMP ;    
    .word   CR,EXIT                 ;

; dump DIR sector of opened file or first sector of current DIR by default
; ----------------------------------;
            FORTHWORD "DIR"         ;TWXY Display DIR sector of CurrentHdl or CurrentDir sector by default 
; ----------------------------------;
    PUSH    &SectorL                ;
    PUSH    &SectorL+2              ;
    MOV     &CurrentHdl,X           ;
    CMP     #0,X                    ;
    JZ      LoadCurrentDIR          ;
    MOV     HDLL_DIRsect(X),&SectorL   ; store DIRsector in handle
    MOV HDLL_DIRsect+2(X),&SectorL+2   ;
    JMP     LoadSectorDIR           ;
LoadCurrentDIR
    MOV     &DIRcluster,&Cluster    ; initialised at 1
    CALL    #ComputeClusFrstSect    ;
LoadSectorDIR
    MOV     &SectorL,W              ;
    MOV     @RSP+,&SectorL+2        ;
    MOV     @RSP+,&SectorL          ;
    JMP     FATnext

; read sector and dump it
; ----------------------------------;
    FORTHWORD "SD_VIEW"             ;       0< sector <65536 --
; ----------------------------------;
    MOV     TOS,W                   ; W = sectorLO
    JMP     FATReadSectorW          ;
; ----------------------------------;
