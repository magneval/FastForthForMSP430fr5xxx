!MSP430FR4133.pat

@define{@read{a:/config/gema/MSP430FR2xxx.pat}}

! ----------------------------------------------
! MSP430FR5739 MEMORY MAP
! ----------------------------------------------
! 0000-0FFF = peripherals (4 KB)
! 1000-17FF = ROM bootstrap loader BSL0..3 (4x512 B)
! 1800-187F = info B (FRAM 128 B)
! 1880-18FF = info A (FRAM 128 B)
! 1900-19FF = N/A (mirrored into info A/B)
! 1A00-1A7F = TLV device descriptor info (FRAM 128 B)
! 1A80-1BFF = unused (385 B)
! 1C00-1FFF = RAM (1 KB)
! 2000-C1FF = unused (41472 B)
! C200-FF7F = code memory (FRAM 15743 B)
! FF80-FFFF = interrupt vectors (FRAM 127 B)
! ----------------------------------------------
INFOSTART=0x1800!
INFOBSTART=0x1800!
INFOBEND=0x19FF!
INFOEND=0x19FF!
TLVSTAT=0x1A00!         Device Descriptor Info (Tag-Lenght-Value)
TLVEND=0x1A7F! 
RAMSTART=0x2000!
RAMEND=0x27FF!
PROGRAMSTART=0xC400!    Code space start
SIGNATURES=0xFF80!      JTAG/BSL signatures
JTAG_SIG1=0xFF80!       if 0 (electronic fuse=0) enable JTAG/SBW; must be reset by wipe.
JTAG_SIG2=0xFF82!       if JTAG_SIG1=0xAAAA, length of password string @ JTAG_PASSWORD
BSL_SIG1=0xFF84!  
BSL_SIG2=0xFF86!  
JTAG_PASSWORD=0xFF88!   256 bits
INTVECT=0xFFE2!         FFE2-FFFF
BSL_PASSWORD=0xFFE0!    256 bits


LCD_Vec=0xFFE2!
P2_Vec=0xFFE4!
P1_Vec=0xFFE6!
ADC10_B_Vec=0xFFE8!
eUSCI_B0_Vec=0xFFEA!
eUSCI_A0_Vec=0xFFEC!
WDT_Vec=0xFFEE!
RTC_Vec=0xFFF0!
TA1_x_Vec=0xFFF2!
TA1_0_Vec=0xFFF4!
TA0_x_Vec=0xFFF6!
TA0_0_Vec=0xFFF8!
U_NMI_Vec=0xFFFA!
S_NMI_Vec=0xFFFC!
RST_Vec=0xFFFE!

TA0CTL=0x300!       \ TA0 control                 
TA0CCTL0=0x302!     \ Capture/compare control 0   
TA0CCTL1=0x304!     \ Capture/compare control 1   
TA0CCTL2=0x306!     \ Capture/compare control 2   
TA0R=0x310!         \ TA0 counter register        
TA0CCR0=0x312!      \ Capture/compare register 0  
TA0CCR1=0x314!      \ Capture/compare register 1  
TA0CCR2=0x316!      \ Capture/compare register 2  
TA0EX0=0x320!       \ TA0 expansion register 0    
TA0IV=0x32E!        \ TA0 interrupt vector        

TA1CTL=0x340!       \ TA1 control                 
TA1CCTL0=0x342!     \ Capture/compare control 0   
TA1CCTL1=0x344!     \ Capture/compare control 1   
TA1CCTL2=0x346!     \ Capture/compare control 2   
TA1R=0x350!         \ TA1 counter register        
TA1CCR0=0x352!      \ Capture/compare register 0  
TA1CCR1=0x354!      \ Capture/compare register 1  
TA1CCR2=0x356!      \ Capture/compare register 2  
TA1EX0=0x360!       \ TA1 expansion register 0    
TA1IV=0x36E!        \ TA1 interrupt vector        

