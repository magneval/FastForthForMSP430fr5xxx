!MSP430FR_FastForth.pat

! ============================================
! SR bits :
! ============================================
\#C=\#1!            Carry flag = SR(0)
\#Z=\#2!            Zero flag = SR(1)
\#N=\#4!            Negative flag = SR(2)
GIE=8!          Enable Int = SR(3)
CPUOFF=0x10!    CPUOFF = SR(4)    
OSCOFF=0x20!    OSCOFF = SR(5)
SCG0=0x40!      SCG0 = SR(6)      
SCG1=0x80!      SCG1 = SR(7)
V=0x100!        oVerflow flag = SR(8)
UF1=0x200!      User Flag 1   = SR(9)
UF2=0x400!      User Flag 2   = SR(10)
UF3=0x800!      User Flag 3   = SR(11)

C\@=C\@!
C\!=C\!!
C\,=C\,!
! ============================================
! PORTx, Reg  bits :
! ============================================
BIT0=1!
BIT1=2!
BIT2=4!
BIT3=8!
BIT4=0x10!
BIT5=0x20!
BIT6=0x40!
BIT7=0x80!
BIT8=0x100!
BIT9=0x200!
BIT10=0x400!
BIT11=0x800!
BIT12=0x1000!
BIT13=0x2000!
BIT14=0x4000!
BIT15=0x8000!

! ============================================
! symbolic codes :
! ============================================
RET=MOV \@R1+,R0!
NOP=MOV 0,R3!       \ one word one cycle
NOP2=0x3C00 ,!      \ compile JMP 0: one word two cycles
NOP3=MOV R0,R0!     \ one word three cycles
NEXT=MOV \@R13+,R0!

! ============================================
! FastForth RAM memory map (>= 1k):
! ============================================
LSATCK=0x1C00!      \ leave stack,      grow up
PSTACK=0x1C92!      \ parameter stack,  grow down
RSTACK=0x1D04!      \ Return stack,     grow down
PAD=0x1D28!         \ user scratch pad buffer
TIB=0x1D7E!         \ Terminal input buffer
BUFFER=0x1E00!      \ SD_Card buffer


! ============================================
! FastForth INFO(DCBA) memory map (256 bytes):
! ============================================

! ----------------------
! KERNEL CONSTANTS
! ----------------------
INI_THREAD=0x1800!      .word THREADS
INI_TERM=0x1802!        .word TERMINAL_INT
INI_KEY=0x1804!         .word PARENKEY
INI_QKEY=0x1806!        .word PARENKEYTST
INI_ACCEPT=0x1808!      .word PARENACCEPT
INI_YEMIT=0x180A!       .word 4882h         ; MOV Y,&adr
INI_EMIT=0x180C!        .word PARENEMIT
INI_CR=0x180E!          .word PARENCR
INI_WARM=0x1810!        .word PARENWARM

! ----------------------
! SOME HERETIC VARIABLES
! ----------------------
SAVE_SYSRSTIV=0x1812!   to enable SYSRSTIV read
LPM_MODE=0x1814!        LPM0 is the default mode
INIDP=0x1816!
INIVOC=0x1818!
INILATEST=0x181A!
CAPS=0x181C!            CAPS ON/OFF flag
LEAVEPTR=0x181E!        Leave-stack pointer
LATEST=0x1820!          5 words : NFA, VOC_PFA, LFA, CFA, CSP of last created word
LAST_NFA=0x1820!
LAST_THREAD=0x1822!
LAST_OLD_NFA=0x1824!
LAST_CFA=0x1826!
LAST_CSP=0x1828!

MEM_CURRENT=0x182A!     NFA_n
!FORGET_NFA_nP1=0x182C!  NFA_n+1
OPCODE=0x182F!          OPCODE adr
ASMTYPE=0x1830!         keep the opcode complement

! ----------------------
! LESS HERETIC VARIABLES
! ----------------------
HP=0x1832!              HOLD ptr
TICKSOURCE=0x1834!      len of input stream
TICKSOURCEP2=0x1836!    adrs of input stream
\>IN=0x1838!            >IN
DP=0x183A!              dictionary ptr
LASTVOC=0x183C!         keep VOC-LINK
CURRENT=0x183E!         CURRENT dictionnary ptr
CONTEXT=0x1840!         CONTEXT dictionnary space
BASE=0x1850!
STATE=0x1852!


! ---------------------------------------
! FAT16 FileSystemInfos 
! ---------------------------------------
FirstSector=0x1854!
OrgFAT1=0x1856!
FATSize=0x1858!
OrgFAT2=0x185A!
OrgRootDir=0x185C!
OrgDatas=0x185E!
OrgClusters=0x1860!         Sector of Cluster 0
SecPerClus=0x1862!
TotSectorsL=0x1864!         Long word
TotSectorsH=0x1866!
CurFATsector=0x1868!

! ---------------------------------------
! SD command
! ---------------------------------------
SD_CMD_FRM=0x186A!  6 bytes SD_CMDx inverted frame ${CRC,ll,LL,hh,HH,CMD}
SD_CMD_FRM0=0x186A! CRC:ll  word access
SD_CMD_FRM1=0x186B! ll      byte access
SD_CMD_FRM2=0x186C! LL:hh   word access
SD_CMD_FRM3=0x186D! hh      byte access
SD_CMD_FRM4=0x186E! HH:CMD  word access
SD_CMD_FRM5=0x186F! CMD     byte access
SectorL=0x1870!     2 words
SectorH=0x1872!

! ---------------------------------------
! BUFFER management
! ---------------------------------------
BufferPtr=0x1874! 
BufferLen=0x1876!

! ---------------------------------------
! FAT entry
! ---------------------------------------
Cluster=0x1878!     16 bits wide (FAT16)
NewCluster=0x187A!  16 bits wide (FAT16) 
FATsector=0x187C!  
FAToffset=0x187E!   in BUFFER

! ---------------------------------------
! DIR entry
! ---------------------------------------
DIRcluster=0x1880!  contains the Cluster of current directory ; 1 if FAT16 root directory
EntryOfst=0x1882!  
pathname=0x1884!    address of pathname string

! ---------------------------------------
! Handle Pointer
! ---------------------------------------
CurrentHdl=0x1886!  contains the address of the last opened file structure, or 0

! ---------------------------------------
! Load file operation
! ---------------------------------------
SAVEtsLEN=0x1888!              of previous ACCEPT
SAVEtsPTR=0x188A!              of previous ACCEPT
MemSectorL=0x188C!             double word current Sector of previous LOAD"ed file
MemSectorH=0x188E!

! ---------------------------------------
! Handle structure
! ---------------------------------------
! three handle tokens : 
! token = 0 : free handle
! token = 1 : file to read
! token = 2 : file updated (write)
! token =-1 : LOAD"ed file (source file)

! offset values
HDLW_PrevHDL=0!     previous handle ; used by LOAD"
HDLB_Token=2!       token
HDLB_ClustOfst=3!   Current sector offset in current cluster (Byte)
HDLL_DIRsect=4!     Dir SectorL (Long)
HDLH_DIRsect=6!
HDLW_DIRofst=8!     BUFFER offset of Dir entry
HDLW_FirstClus=0x0A!File First Cluster (identify the file)
HDLW_CurClust=0x0C! Current Cluster
HDLL_CurSize=0x0E!  written size / not yet read size (Long)
HDLH_CurSize=0x10!
HDLW_BUFofst=0x12!  BUFFER offset ; used by LOAD" and by WRITE"

HandleMax=5!
HandleLenght=0x14!

!OpenedFirstFile     ; "openedFile" structure 
FirstHandle=0x1890!
!   .word 0,0,0,0,0,0,0,0,0,0
!   .word 0,0,0,0,0,0,0,0,0,0
!   .word 0,0,0,0,0,0,0,0,0,0
!   .word 0,0,0,0,0,0,0,0,0,0
!   .word 0,0,0,0,0,0,0,0,0,0
!   .word 0,0,0,0,0,0,0,0,0,0
HandleOutOfBound=0x18F4!

SD_READ=0x18F4!     W = SectorLO  X = SectorHI
SD_WRITE=0x18F6!    W = SectorLO  X = SectorHI
