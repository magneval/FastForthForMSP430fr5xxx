!MSP430fr57xx.pat

\#LPM4,=\#0xF8,!
\#LPM3,=\#0xD8,!
\#LPM2,=\#0x98,!
\#LPM1,=\#0x58,!
\#LPM0,=\#0x18,!



SFRIE1=0x100!       \ SFR enable register
SFRIFG1=0x102!      \ SFR flag register
SFRRPCR=0x104!      \ SFR reset pin control

PMMCTL0=0x120!      \ PMM Control 0
PMMIFG=0x12A!       \ PMM interrupt flags 
PM5CTL0=0x130!      \ PM5 Control 0

FRCTLCTL0=0x140!    \ FRAM control 0    
GCCTL0=0x144!       \ General control 0 
GCCTL1=0x146!       \ General control 1 

CRC16DI=0x150!      \ CRC data input                  
CRCDIRB=0x152!      \ CRC data input reverse byte     
CRCINIRES=0x154!    \ CRC initialization and result   
CRCRESR=0x156!      \ CRC result reverse byte  

WDTCTL=0x15C!        \ WDT control register

CSCTL0=0x160!       \ CS control 0
CSCTL0_H=0x161!     \ 
CSCTL1=0x162!       \ CS control 1 
CSCTL2=0x164!       \ CS control 2 
CSCTL3=0x166!       \ CS control 3 
CSCTL4=0x168!       \ CS control 4 
CSCTL5=0x16A!       \ CS control 5 
CSCTL6=0x16C!       \ CS control 6 

SYSCTL=0x180!       \ System control              
SYSJMBC=0x186!      \ JTAG mailbox control        
SYSJMBI0=0x188!     \ JTAG mailbox input 0        
SYSJMBI1=0x18A!     \ JTAG mailbox input 1        
SYSJMBO0=0x18C!     \ JTAG mailbox output 0       
SYSJMBO1=0x18E!     \ JTAG mailbox output 1       
SYSBERRIV=0x198!    \ Bus Error vector generator  
SYSUNIV=0x19A!      \ User NMI vector generator   
SYSSNIV=0x19C!      \ System NMI vector generator 
SYSRSTIV=0x19E!     \ Reset vector generator      

REFCTL=0x1b0!       \ Shared reference control 

PAIN=0x200!
PAOUT=0x202!
PADIR=0x204!
PAREN=0x206!
PASEL0=0x20A!
PASEL1=0x20C!
P1IV=0x20E!
PASELC=0x216!
PAIES=0x218!
PAIE=0x21A!
PAIFG=0x21C!
P2IV=0x21E!

P1IN=0x200!
P1OUT=0x202!
P1DIR=0x204!
P1REN=0x206!
P1SEL0=0x20A!
P1SEL1=0x20C!
P1SELC=0x216!
P1IES=0x218!
P1IE=0x21A!
P1IFG=0x21C!

P2IN=0x201!
P2OUT=0x203!
P2DIR=0x205!
P2REN=0x207!
P2SEL0=0x20B!
P2SEL1=0x20D!
P2SELC=0x217!
P2IES=0x218!
P2IE=0x21B!
P2IFG=0x21D!

PBIN=0x220!
PBOUT=0x222!
PBDIR=0x224!
PBREN=0x226!
PBSEL0=0x22A!
PBSEL1=0x22C!
P3IV=0x22E!
PBSELC=0x236!
PBIES=0x238!
PBIE=0x23A!
PBIFG=0x23C!
P4IV=0x23E!

P3IN=0x220!
P3OUT=0x222!
P3DIR=0x224!
P3REN=0x226!
P3SEL0=0x22A!
P3SEL1=0x22C!
P3SELC=0x236!
P3IES=0x238!
P3IE=0x23A!
P3IFG=0x23C!

P4IN=0x221!
P4OUT=0x223!
P4DIR=0x225!
P4REN=0x227!
P4SEL0=0x22B!
P4SEL1=0x22D!
P4SELC=0x237!
P4IES=0x238!
P4IE=0x23B!
P4IFG=0x23D!

PJIN=0x320!
PJOUT=0x322!
PJDIR=0x324!
PJREN=0x326!
PJSEL0=0x32A!
PJSEL1=0x32C!
PJSELC=0x336!

TA0CTL=0x340!       \ TA0 control                 
TA0CCTL0=0x342!     \ Capture/compare control 0   
TA0CCTL1=0x344!     \ Capture/compare control 1   
TA0CCTL2=0x346!     \ Capture/compare control 2   
TA0R=0x350!         \ TA0 counter register        
TA0CCR0=0x352!      \ Capture/compare register 0  
TA0CCR1=0x354!      \ Capture/compare register 1  
TA0CCR2=0x356!      \ Capture/compare register 2  
TA0EX0=0x360!       \ TA0 expansion register 0    
TA0IV=0x36E!        \ TA0 interrupt vector        

TA1CTL=0x380!       \ TA1 control                 
TA1CCTL0=0x382!     \ Capture/compare control 0   
TA1CCTL1=0x384!     \ Capture/compare control 1   
TA1CCTL2=0x386!     \ Capture/compare control 2   
TA1R=0x390!         \ TA1 counter register        
TA1CCR0=0x392!      \ Capture/compare register 0  
TA1CCR1=0x394!      \ Capture/compare register 1  
TA1CCR2=0x396!      \ Capture/compare register 2  
TA1EX0=0x3A0!       \ TA1 expansion register 0    
TA1IV=0x3AE!        \ TA1 interrupt vector        

TB0CTL=0x3C0!       \ TB0 control                 
TB0CCTL0=0x3C2!     \ Capture/compare control 0   
TB0CCTL1=0x3C4!     \ Capture/compare control 1   
TB0CCTL2=0x3C6!     \ Capture/compare control 2   
TB0R=0x3D0!         \ TB0 counter register        
TB0CCR0=0x3D2!      \ Capture/compare register 0  
TB0CCR1=0x3D4!      \ Capture/compare register 1  
TB0CCR2=0x3D6!      \ Capture/compare register 2  
TB0EX0=0x3E0!       \ TB0 expansion register 0    
TB0IV=0x3EE!        \ TB0 interrupt vector        

TB1CTL=0x400!       \ TB1 control                 
TB1CCTL0=0x402!     \ Capture/compare control 0   
TB1CCTL1=0x404!     \ Capture/compare control 1   
TB1CCTL2=0x406!     \ Capture/compare control 2   
TB1R=0x410!         \ TB1 counter register        
TB1CCR0=0x412!      \ Capture/compare register 0  
TB1CCR1=0x414!      \ Capture/compare register 1  
TB1CCR2=0x416!      \ Capture/compare register 2  
TB1EX0=0x420!       \ TB1 expansion register 0    
TB1IV=0x42E!        \ TB1 interrupt vector        

TB2CTL=0x440!       \ TB2 control                 
TB2CCTL0=0x442!     \ Capture/compare control 0   
TB2CCTL1=0x444!     \ Capture/compare control 1   
TB2CCTL2=0x446!     \ Capture/compare control 2   
TB2R=0x450!         \ TB2 counter register        
TB2CCR0=0x452!      \ Capture/compare register 0  
TB2CCR1=0x454!      \ Capture/compare register 1  
TB2CCR2=0x456!      \ Capture/compare register 2  
TB2EX0=0x460!       \ TB2 expansion register 0    
TB2IV=0x46E!        \ TB2 interrupt vector        

RTCCTL0=0x4A0!      \ RTC control 0                                   
RTCCTL1=0x4A1!      \ RTC control 1                                   
RTCCTL2=0x4A2!      \ RTC control 2                                   
RTCCTL3=0x4A3!      \ RTC control 3                                   
RTCPS0CTL=0x4A8!    \ RTC prescaler 0 control                         
RTCPS1CTL=0x4AA!    \ RTC prescaler 1 control                         
RTCPS0=0x4AC!       \ RTC prescaler 0                                 
RTCPS1=0x4AD!       \ RTC prescaler 1                                 
RTCIV=0x4AE!        \ RTC interrupt vector word                       
RTCSEC=0x4B0!       \ RTC seconds, RTC counter register 1 RTCSEC,     
RTCMIN=0x4B1!       \ RTC minutes, RTC counter register 2 RTCMIN,     
RTCHOUR=0x4B2!      \ RTC hours, RTC counter register 3 RTCHOUR,      
RTCDOW=0x4B3!       \ RTC day of week, RTC counter register 4 RTCDOW, 
RTCDAY=0x4B4!       \ RTC days                                        
RTCMON=0x4B5!       \ RTC month                                       
RTCYEARL=0x4B6!     \ RTC year low                                    
RTCYEARH=0x4B7!     \ RTC year high                                   
RTCAMIN=0x4B8!      \ RTC alarm minutes                               
RTCAHOUR=0x4B9!     \ RTC alarm hours                                 
RTCADOW=0x4BA!      \ RTC alarm day of week                           
RTCADAY=0x4BB!      \ RTC alarm days                                  
BIN2BCD=0x4BC!      \ Binary-to-BCD conversion register               
BCD2BIN=0x4BE!      \ BCD-to-binary conversion register               
RTCHOLD=0x40!

MPY=0x4C0!          \ 16-bit operand 1 � multiply                     
MPYS=0x4C2!         \ 16-bit operand 1 � signed multiply              
MAC=0x4C4!          \ 16-bit operand 1 � multiply accumulate          
MACS=0x4C6!         \ 16-bit operand 1 � signed multiply accumulate   
OP2=0x4C8!          \ 16-bit operand 2                                
RESLO=0x4CA!        \ 16 � 16 result low word                         
RESHI=0x4CC!        \ 16 � 16 result high word                        
SUMEXT=0x4CE!       \ 16 � 16 sum extension register                  
MPY32L=0x4D0!       \ 32-bit operand 1 � multiply low word
MPY32H=0x4D2!       \ 32-bit operand 1 � multiply high word           
MPYS32L=0x4D4!      \ 32-bit operand 1 � signed multiply low word     
MPYS32H=0x4D6!      \ 32-bit operand 1 � signed multiply high word    
MAC32L=0x4D8!       \ 32-bit operand 1 � multiply accumulate low word         
MAC32H=0x4DA!       \ 32-bit operand 1 � multiply accumulate high word        
MACS32L=0x4DC!      \ 32-bit operand 1 � signed multiply accumulate low word  
MACS32H=0x4DE!      \ 32-bit operand 1 � signed multiply accumulate high word 
OP2L=0x4E0!         \ 32-bit operand 2 � low word                 
OP2H=0x4E2!         \ 32-bit operand 2 � high word                
RES0=0x4E4!         \ 32 � 32 result 0 � least significant word   
RES1=0x4E6!         \ 32 � 32 result 1                            
RES2=0x4E8!         \ 32 � 32 result 2                            
RES3=0x4EA!         \ 32 � 32 result 3 � most significant word    
MPY32CTL0=0x4EC!    \ MPY32 control register 0                    

DMA0CTL=0x500!      \ DMA channel 0 control                   
DMA0SAL=0x502!      \ DMA channel 0 source address low        
DMA0SAH=0x504!      \ DMA channel 0 source address high       
DMA0DAL=0x506!      \ DMA channel 0 destination address low   
DMA0DAH=0x508!      \ DMA channel 0 destination address high  
DMA0SZ=0x50A!       \ DMA channel 0 transfer size             
DMA1CTL=0x510!      \ DMA channel 1 control                   
DMA1SAL=0x512!      \ DMA channel 1 source address low        
DMA1SAH=0x514!      \ DMA channel 1 source address high       
DMA1DAL=0x516!      \ DMA channel 1 destination address low   
DMA1DAH=0x518!      \ DMA channel 1 destination address high  
DMA1SZ=0x51A!       \ DMA channel 1 transfer size             
DMA2CTL=0x520!      \ DMA channel 2 control                   
DMA2SAL=0x522!      \ DMA channel 2 source address low        
DMA2SAH=0x524!      \ DMA channel 2 source address high       
DMA2DAL=0x526!      \ DMA channel 2 destination address low   
DMA2DAH=0x528!      \ DMA channel 2 destination address high  
DMA2SZ=0x52A!       \ DMA channel 2 transfer size             
DMACTL0=0x530!      \ DMA module control 0                    
DMACTL1=0x532!      \ DMA module control 1                    
DMACTL2=0x534!      \ DMA module control 2                    
DMACTL3=0x536!      \ DMA module control 3                    
DMACTL4=0x538!      \ DMA module control 4                    
DMAIV=0x53A!        \ DMA interrupt vector                    

MPUCTL0=0x5A0!      \ MPU control 0             
MPUCTL1=0x5A2!      \ MPU control 1             
MPUSEG=0x5A4!       \ MPU Segmentation Register 
MPUSAM=0x5A6!       \ MPU access management     

UCA0CTLW0=0x5C0!    \ eUSCI_A control word 0        
UCA0CTLW1=0x5C2!    \ eUSCI_A control word 1        
UCA0BRW=0x5C6!         
UCA0BR0=0x5C6!      \ eUSCI_A baud rate 0           
UCA0BR1=0x5C7!      \ eUSCI_A baud rate 1           
UCA0MCTLW=0x5C8!    \ eUSCI_A modulation control    
UCA0STAT=0x5CA!     \ eUSCI_A status                
UCA0RXBUF=0x5CC!    \ eUSCI_A receive buffer        
UCA0TXBUF=0x5CE!    \ eUSCI_A transmit buffer       
UCA0ABCTL=0x5D0!    \ eUSCI_A LIN control           
UCA0IRTCTL=0x5D2!   \ eUSCI_A IrDA transmit control 
UCA0IRRCTL=0x5D3!   \ eUSCI_A IrDA receive control  
UCA0IE=0x5DA!       \ eUSCI_A interrupt enable      
UCA0IFG=0x5DC!      \ eUSCI_A interrupt flags       
UCA0IV=0x5DE!       \ eUSCI_A interrupt vector word 

UCA1CTLW0=0x5E0!    \ eUSCI_A control word 0        
UCA1CTLW1=0x5E2!    \ eUSCI_A control word 1        
UCA1BRW=0x5E6!         
UCA1BR0=0x5E6!      \ eUSCI_A baud rate 0           
UCA1BR1=0x5E7!      \ eUSCI_A baud rate 1           
UCA1MCTLW=0x5E8!    \ eUSCI_A modulation control    
UCA1STAT=0x5EA!     \ eUSCI_A status                
UCA1RXBUF=0x5EC!    \ eUSCI_A receive buffer        
UCA1TXBUF=0x5EE!    \ eUSCI_A transmit buffer       
UCA1ABCTL=0x5F0!    \ eUSCI_A LIN control           
UCA1IRTCTL=0x5F2!   \ eUSCI_A IrDA transmit control 
UCA1IRRCTL=0x5F3!   \ eUSCI_A IrDA receive control  
UCA1IE=0x5FA!       \ eUSCI_A interrupt enable      
UCA1IFG=0x5FC!      \ eUSCI_A interrupt flags       
UCA1IV=0x5FE!       \ eUSCI_A interrupt vector word 

UCB0CTLW0=0x640!    \ eUSCI_B control word 0          
UCB0CTLW1=0x642!    \ eUSCI_B control word 1 
UCB0BRW=0x646!         
UCB0BR0=0x646!      \ eUSCI_B bit rate 0              
UCB0BR1=0x647!      \ eUSCI_B bit rate 1              
UCB0STATW=0x648!    \ eUSCI_B status word 
UCBCNT0=0x649!      \ eUSCI_B hardware count           
UCB0TBCNT=0x64A!    \ eUSCI_B byte counter threshold  
UCB0RXBUF=0x64C!    \ eUSCI_B receive buffer          
UCB0TXBUF=0x64E!    \ eUSCI_B transmit buffer         
UCB0I2COA0=0x654!   \ eUSCI_B I2C own address 0       
UCB0I2COA1=0x656!   \ eUSCI_B I2C own address 1       
UCB0I2COA2=0x658!   \ eUSCI_B I2C own address 2       
UCB0I2COA3=0x65A!   \ eUSCI_B I2C own address 3       
UCB0ADDRX=0x65C!    \ eUSCI_B received address        
UCB0ADDMASK=0x65E!  \ eUSCI_B address mask            
UCB0I2CSA=0x660!    \ eUSCI I2C slave address         
UCB0IE=0x66A!       \ eUSCI interrupt enable          
UCB0IFG=0x66C!      \ eUSCI interrupt flags           
UCB0IV=0x66E!       \ eUSCI interrupt vector word     

\#UCTXACK,=\#0x20,!
\#UCTR,=\#0x10,!

ADC10CTL0=0x700!    \ ADC10_B Control register 0               
ADC10CTL1=0x702!    \ ADC10_B Control register 1               
ADC10CTL2=0x704!    \ ADC10_B Control register 2               
ADC10LO=0x706!      \ ADC10_B Window Comparator Low Threshold  
ADC10HI=0x708!      \ ADC10_B Window Comparator High Threshold 
ADC10MCTL0=0x70A!   \ ADC10_B Memory Control Register 0        
ADC10MEM0=0x712!    \ ADC10_B Conversion Memory Register       
ADC10IE=0x71A!      \ ADC10_B Interrupt Enable                 
ADC10IFG=0x71C!     \ ADC10_B Interrupt Flags                  
ADC10IV=0x71E!      \ ADC10_B Interrupt Vector Word            

\#ADCON,=\#0x10,!
\#ADCSTART,=\#0x03,!

CDCTL0=0x8C0!       \ Comparator_D control register 0     
CDCTL1=0x8C2!       \ Comparator_D control register 1     
CDCTL2=0x8C4!       \ Comparator_D control register 2     
CDCTL3=0x8C6!       \ Comparator_D control register 3     
CDINT=0x8CC!        \ Comparator_D interrupt register     
CDIV=0x8CE!         \ Comparator_D interrupt vector word  
