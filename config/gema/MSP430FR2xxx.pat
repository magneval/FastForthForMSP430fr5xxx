!MSP430FR2xxx.pat

LPM4=0xF8!
LPM3=0xD8!
LPM0=0x18!


SFRIE1=0x100!       \ SFR enable register
SFRIFG1=0x102!      \ SFR flag register
SFRRPCR=0x104!      \ SFR reset pin control

PMMCTL0=0x120!      \ PMM Control 0
PMMCTL1=0x122!      \ PMM Control 0
PMMCTL2=0x124!      \ PMM Control 0
PMMIFG=0x12A!       \ PMM interrupt flags 
PM5CTL0=0x130!      \ PM5 Control 0

SYSCTL=0x140!       \ System control 
SYSBSLC=0x142!      \ Bootstrap loader configuration area             
SYSJMBC=0x146!      \ JTAG mailbox control        
SYSJMBI0=0x148!     \ JTAG mailbox input 0        
SYSJMBI1=0x14A!     \ JTAG mailbox input 1        
SYSJMBO0=0x14C!     \ JTAG mailbox output 0       
SYSJMBO1=0x14E!     \ JTAG mailbox output 1       
SYSBERRIV=0x158!    \ Bus Error vector generator  
SYSUNIV=0x15A!      \ User NMI vector generator   
SYSSNIV=0x15C!      \ System NMI vector generator 
SYSRSTIV=0x15E!     \ Reset vector generator      
SYSCFG0=0x160!      \ System configuration 0 
SYSCFG1=0x162!      \ System configuration 1 
SYSCFG2=0x164!      \ System configuration 2 

CSCTL0=0x180!       \ CS control 0 
CSCTL1=0x182!       \ CS control 1 
CSCTL2=0x184!       \ CS control 2 
CSCTL3=0x186!       \ CS control 3 
CSCTL4=0x188!       \ CS control 4 
CSCTL5=0x18A!       \ CS control 5 
CSCTL6=0x18C!       \ CS control 6 

FRCTLCTL0=0x1A0!    \ FRAM control 0    
GCCTL0=0x1A4!       \ General control 0 
GCCTL1=0x1A6!       \ General control 1 

CRC16DI=0x1C0!      \ CRC data input                  
CRCDIRB=0x1C2!      \ CRC data input reverse byte     
CRCINIRES=0x1C4!    \ CRC initialization and result   
CRCRESR=0x1C6!      \ CRC result reverse byte  

WDTCTL=0x1CC!        \ WDT control register


PAIN=0x200!
PAOUT=0x202!
PADIR=0x204!
PAREN=0x206!
PASEL0=0x20A!
PASEL1=0x20C!
P1IV=0x20E!
PASELC=0x216!
PAIES=0x218!
PAIE=0x21A!
PAIFG=0x21C!
P2IV=0x21E!

P1IN=0x200!
P1OUT=0x202!
P1DIR=0x204!
P1REN=0x206!
P1SEL0=0x20A!
P1SEL1=0x20C!
P1SELC=0x216!
P1IES=0x218!
P1IE=0x21A!
P1IFG=0x21C!

P2IN=0x201!
P2OUT=0x203!
P2DIR=0x205!
P2REN=0x207!
P2SEL0=0x20B!
P2SEL1=0x20D!
P2SELC=0x217!
P2IES=0x218!
P2IE=0x21B!
P2IFG=0x21D!

P3IN=0x220!
P3OUT=0x222!
P3DIR=0x224!
P3REN=0x226!
P3SEL0=0x22A!
P3SEL1=0x22C!


RTCCTL=0x300!       \ RTC control                                  
RTCIV=0x304!        \ RTC interrupt vector word                       
RTCMOD=0x308!       \ RTC modulo                                       
RTCCNT=0x30C!       \ RTC counter register    

MPY=0x4C0!          \ 16-bit operand 1 � multiply                     
MPYS=0x4C2!         \ 16-bit operand 1 � signed multiply              
MAC=0x4C4!          \ 16-bit operand 1 � multiply accumulate          
MACS=0x4C6!         \ 16-bit operand 1 � signed multiply accumulate   
OP2=0x4C8!          \ 16-bit operand 2                                
RESLO=0x4CA!        \ 16 � 16 result low word                         
RESHI=0x4CC!        \ 16 � 16 result high word                        
SUMEXT=0x4CE!       \ 16 � 16 sum extension register                  
MPY32L=0x4D0!       \ 32-bit operand 1 � multiply low word            
MPY32H=0x4D2!       \ 32-bit operand 1 � multiply high word           
MPYS32L=0x4D4!      \ 32-bit operand 1 � signed multiply low word     
MPYS32H=0x4D6!      \ 32-bit operand 1 � signed multiply high word    
MAC32L=0x4D8!       \ 32-bit operand 1 � multiply accumulate low word         
MAC32H=0x4DA!       \ 32-bit operand 1 � multiply accumulate high word        
MACS32L=0x4DC!      \ 32-bit operand 1 � signed multiply accumulate low word  
MACS32H=0x4DE!      \ 32-bit operand 1 � signed multiply accumulate high word 
OP2L=0x4E0!         \ 32-bit operand 2 � low word                 
OP2H=0x4E2!         \ 32-bit operand 2 � high word                
RES0=0x4E4!         \ 32 � 32 result 0 � least significant word   
RES1=0x4E6!         \ 32 � 32 result 1                            
RES2=0x4E8!         \ 32 � 32 result 2                            
RES3=0x4EA!         \ 32 � 32 result 3 � most significant word    
MPY32CTL0=0x4EC!    \ MPY32 control register 0                    



UCA0CTLW0=0x500!    \ eUSCI_A control word 0        
UCA0CTLW1=0x502!    \ eUSCI_A control word 1        
UCA0BRW=0x506!         
UCA0BR0=0x506!      \ eUSCI_A baud rate 0           
UCA0BR1=0x507!      \ eUSCI_A baud rate 1           
UCA0MCTLW=0x508!    \ eUSCI_A modulation control    
UCA0STAT=0x50A!     \ eUSCI_A status                
UCA0RXBUF=0x50C!    \ eUSCI_A receive buffer        
UCA0TXBUF=0x50E!    \ eUSCI_A transmit buffer       
UCA0ABCTL=0x510!    \ eUSCI_A LIN control           
UCA0IRTCTL=0x512!   \ eUSCI_A IrDA transmit control 
UCA0IRRCTL=0x513!   \ eUSCI_A IrDA receive control  
UCA0IE=0x51A!       \ eUSCI_A interrupt enable      
UCA0IFG=0x51C!      \ eUSCI_A interrupt flags       
UCA0IV=0x51E!       \ eUSCI_A interrupt vector word 

UCA1CTLW0=0x520!    \ eUSCI_A control word 0        
UCA1CTLW1=0x522!    \ eUSCI_A control word 1        
UCA1BRW=0x526!         
UCA1BR0=0x526!      \ eUSCI_A baud rate 0           
UCA1BR1=0x527!      \ eUSCI_A baud rate 1           
UCA1MCTLW=0x528!    \ eUSCI_A modulation control    
UCA1STAT=0x52A!     \ eUSCI_A status                
UCA1RXBUF=0x52C!    \ eUSCI_A receive buffer        
UCA1TXBUF=0x52E!    \ eUSCI_A transmit buffer       
UCA1ABCTL=0x530!    \ eUSCI_A LIN control           
UCA1IRTCTL=0x532!   \ eUSCI_A IrDA transmit control 
UCA1IRRCTL=0x533!   \ eUSCI_A IrDA receive control  
UCA1IE=0x53A!       \ eUSCI_A interrupt enable      
UCA1IFG=0x53C!      \ eUSCI_A interrupt flags       
UCA1IV=0x53E!       \ eUSCI_A interrupt vector word 


UCB0CTLW0=0x540!    \ eUSCI_B control word 0          
UCB0CTLW1=0x542!    \ eUSCI_B control word 1 
UCB0BRW=0x546!         
UCB0BR0=0x546!      \ eUSCI_B bit rate 0              
UCB0BR1=0x547!      \ eUSCI_B bit rate 1              
UCB0STATW=0x548!    \ eUSCI_B status word 
UCBCNT0=0x549!      \ eUSCI_B hardware count           
UCB0TBCNT=0x54A!    \ eUSCI_B byte counter threshold  
UCB0RXBUF=0x54C!    \ eUSCI_B receive buffer          
UCB0TXBUF=0x54E!    \ eUSCI_B transmit buffer         
UCB0I2COA0=0x554!   \ eUSCI_B I2C own address 0       
UCB0I2COA1=0x556!   \ eUSCI_B I2C own address 1       
UCB0I2COA2=0x558!   \ eUSCI_B I2C own address 2       
UCB0I2COA3=0x55A!   \ eUSCI_B I2C own address 3       
UCB0ADDRX=0x55C!    \ eUSCI_B received address        
UCB0ADDMASK=0x55E!  \ eUSCI_B address mask            
UCB0I2CSA=0x560!    \ eUSCI I2C slave address         
UCB0IE=0x56A!       \ eUSCI interrupt enable          
UCB0IFG=0x56C!      \ eUSCI interrupt flags           
UCB0IV=0x56E!       \ eUSCI interrupt vector word     

UCTXACK=0x20!
UCTR=0x10!

LCDCTL0=0x600!      \ LCD control register 0   
LCDCTL1=0x602!      \ LCD control register 1   
LCDBLKCTL=0x604!    \ LCD blink control register     
LCDMEMCTL=0x606!    \ LCD memory control register     
LCDVCTL=0x608!      \ LCD voltage control register   
LCDPCTL0=0x60A!     \ LCD port control 0    
LCDPCTL1=0x60C!     \ LCD port control 1    
LCDPCTL2=0x60E!     \ LCD port control 2    
LCDCSS0=0x614!      \ LCD COM/SEG select register   
LCDCSS1=0x616!      \ LCD COM/SEG select register   
LCDCSS2=0x618!      \ LCD COM/SEG select register   
LCDIV=0x61E!        \ LCD interrupt vector 
LCDM0=0x620!        \ LCD memory 0 
LCDM1=0x621!        \ LCD memory 1 
LCDM2=0x622!        \ LCD memory 2 
LCDM3=0x623!        \ LCD memory 3 
LCDM4=0x624!        \ LCD memory 4 
LCDM5=0x625!        \ LCD memory 5 
LCDM6=0x626!        \ LCD memory 6 
LCDM7=0x627!        \ LCD memory 7 
LCDM8=0x628!        \ LCD memory 8 
LCDM9=0x629!        \ LCD memory 9 
LCDM10=0x62A!       \ LCD memory 10 
LCDM11=0x62B!       \ LCD memory 11 
LCDM12=0x62C!       \ LCD memory 12 
LCDM13=0x62D!       \ LCD memory 13 
LCDM14=0x62E!       \ LCD memory 14 
LCDM15=0x62F!       \ LCD memory 15 
LCDM16=0x630!       \ LCD memory 16 
LCDM17=0x631!       \ LCD memory 17 
LCDM18=0x632!       \ LCD memory 18 
LCDM19=0x633!       \ LCD memory 19  
LCDM20=0x634!       \ LCD memory 20 
LCDM21=0x635!       \ LCD memory 21 
LCDM22=0x636!       \ LCD memory 22 
LCDM23=0x637!       \ LCD memory 23 
LCDM24=0x638!       \ LCD memory 24 
LCDM25=0x639!       \ LCD memory 25 
LCDM26=0x63A!       \ LCD memory 26 
LCDM27=0x63B!       \ LCD memory 27 
LCDM28=0x63C!       \ LCD memory 28 
LCDM29=0x63D!       \ LCD memory 29  
LCDM30=0x63E!       \ LCD memory 30 
LCDM31=0x63F!       \ LCD memory 31 
LCDM32=0x640!       \ LCD memory 32 
LCDM33=0x641!       \ LCD memory 33 
LCDM34=0x642!       \ LCD memory 34 
LCDM35=0x643!       \ LCD memory 35 
LCDM36=0x644!       \ LCD memory 36 
LCDM37=0x645!       \ LCD memory 37 
LCDM38=0x646!       \ LCD memory 38 
LCDM39=0x647!       \ LCD memory 39  
LCDBM0=0x640!       \ LCD blinking memory 0 
LCDBM1=0x641!       \ LCD blinking memory 1 
LCDBM2=0x642!       \ LCD blinking memory 2 
LCDBM3=0x643!       \ LCD blinking memory 3 
LCDBM4=0x644!       \ LCD blinking memory 4 
LCDBM5=0x645!       \ LCD blinking memory 5 
LCDBM6=0x646!       \ LCD blinking memory 6 
LCDBM7=0x647!       \ LCD blinking memory 7 
LCDBM8=0x648!       \ LCD blinking memory 8 
LCDBM9=0x649!       \ LCD blinking memory 9 
LCDBM10=0x64A!      \ LCD blinking memory 10 
LCDBM11=0x64B!      \ LCD blinking memory 11 
LCDBM12=0x64C!      \ LCD blinking memory 12 
LCDBM13=0x64D!      \ LCD blinking memory 13 
LCDBM14=0x64E!      \ LCD blinking memory 14 
LCDBM15=0x64F!      \ LCD blinking memory 15 
LCDBM16=0x650!      \ LCD blinking memory 16 
LCDBM17=0x651!      \ LCD blinking memory 17 
LCDBM18=0x652!      \ LCD blinking memory 18 
LCDBM19=0x653!      \ LCD blinking memory 19 


BAKMEM0=0x660!      \ Backup Memory 0     
BAKMEM1=0x662!      \ Backup Memory 1     
BAKMEM2=0x664!      \ Backup Memory 2     
BAKMEM3=0x666!      \ Backup Memory 3     
BAKMEM4=0x668!      \ Backup Memory 4     
BAKMEM5=0x66A!      \ Backup Memory 5     
BAKMEM6=0x66C!      \ Backup Memory 6     
BAKMEM7=0x66E!      \ Backup Memory 7     
BAKMEM8=0x670!      \ Backup Memory 8     
BAKMEM9=0x672!      \ Backup Memory 9     
BAKMEM10=0x674!     \ Backup Memory 10    
BAKMEM11=0x676!     \ Backup Memory 11    
BAKMEM12=0x678!     \ Backup Memory 12    
BAKMEM13=0x67A!     \ Backup Memory 13    
BAKMEM14=0x67C!     \ Backup Memory 14    
BAKMEM15=0x67E!     \ Backup Memory 15    


ADC10CTL0=0x700!    \ ADC10_B Control register 0               
ADC10CTL1=0x702!    \ ADC10_B Control register 1               
ADC10CTL2=0x704!    \ ADC10_B Control register 2               
ADC10LO=0x706!      \ ADC10_B Window Comparator Low Threshold  
ADC10HI=0x708!      \ ADC10_B Window Comparator High Threshold 
ADC10MCTL0=0x70A!   \ ADC10_B Memory Control Register 0        
ADC10MEM0=0x712!    \ ADC10_B Conversion Memory Register       
ADC10IE=0x71A!      \ ADC10_B Interrupt Enable                 
ADC10IFG=0x71C!     \ ADC10_B Interrupt Flags                  
ADC10IV=0x71E!      \ ADC10_B Interrupt Vector Word            

ADCON=0x10!
ADCSTART=0x03!

