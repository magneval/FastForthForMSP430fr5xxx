!MSP430fr5948.pat

@define{@read{a:/config/gema/MSP430FR59xx.pat}}

! ----------------------------------------------
! MSP430fr5948 MEMORY MAP
! ----------------------------------------------
! 0000-0FFF = peripherals (4 KB)
! 1000-17FF = ROM bootstrap loader BSL0..3 (4x512 B)
! 1800-187F = info D (FRAM 128 B)
! 1880-18FF = info C (FRAM 128 B)
! 1900-197F = info B (FRAM 128 B) 
! 1980-19FF = info A (FRAM 128 B) 
! 1A00-1A7F = TLV device descriptor info (FRAM 128 B)
! 1A80-1BFF = unused (385 B)
! 1C00-1FFF = RAM (1 KB)
! 2000-C1FF = unused (41472 B)
! C200-FF7F = code memory (FRAM 15743 B)
! FF80-FFFF = interrupt vectors (FRAM 127 B)
! ----------------------------------------------
INFOSTART=0x1800!
INFODSTART=0x1800!
INFODEND=0x187F!
INFOCSTART=0x1880!
INFOCEND=0x18FF!
INFOBSTART=0x1900!
INFOBEND=0x197F!
INFOASTART=0x1980!
INFOAEND=0x19FF!
TLVSTAT=0x1A00!         Device Descriptor Info (Tag-Lenght-Value)
TLVEND=0x1A7F! 
RAMSTART=0x1C00!
RAMEND=0x1FFF!
PROGRAMSTART=0x4400!    Code space start
SIGNATURES=0xFF80!      JTAG/BSL signatures
JTAG_SIG1=0xFF80!       if 0 (electronic fuse=0) enable JTAG/SBW; must be reset by wipe.
JTAG_SIG2=0xFF82!       if JTAG_SIG1=0xAAAA, length of password string @ JTAG_PASSWORD
BSL_SIG1=0xFF84!  
BSL_SIG2=0xFF86!  
JTAG_PASSWORD=0xFF88!   256 bits
INTVECT=0xFFCC!         FFCC-FFFF
BSL_PASSWORD=0xFFE0!    256 bits


AES_Vec=0xFFCC!
RTC_Vec=0xFFCE!
P4_Vec=0xFFD0!
P3_Vec=0xFFD2!
TB2_x_Vec=0xFFD4!
TB2_0_Vec=0xFFD6!
P2_Vec=0xFFD8!
TB1_x_Vec=0xFFDA!
TB1_0_Vec=0xFFDC!
P1_Vec=0xFFDE!
TA1_x_Vec=0xFFE0!
TA1_0_Vec=0xFFE2!
DMA_Vec=0xFFE4!
eUSCI_A1_Vec=0xFFE6!
TA0_x_Vec=0xFFE8!
TA0_0_Vec=0xFFEA!
ADC10_B_Vec=0xFFEC!
eUSCI_B0_Vec=0xFFEE!
eUSCI_A0_Vec=0xFFF0!
WDT_Vec=0xFFF2!
TB0_x_Vec=0xFFF4!
TB0_0_Vec=0xFFF6!
COMP_D_Vec=0xFFF8!
U_NMI_Vec=0xFFFA!
S_NMI_Vec=0xFFFC!
RST_Vec=0xFFFE!

