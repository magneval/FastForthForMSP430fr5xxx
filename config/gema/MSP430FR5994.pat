!MSP430fr5994.pat

@define{@read{a:/config/gema/MSP430FR59xx.pat}}


! ----------------------------------------------
! MSP430FR5994 MEMORY MAP
! ----------------------------------------------
! 000A-001F = tiny RAM
! 0020-0FFF = peripherals (4 KB)
! 1000-17FF = ROM bootstrap loader BSL0..3 (4x512 B)
! 1800-187F = FRAM info D (128 B)
! 1880-18FF = FRAM info C (128 B)
! 1900-197F = FRAM info B (128 B)
! 1980-19FF = FRAM info A (128 B)
! 1A00-1AFF = FRAM TLV device descriptor info (256 B)
! 1B00-1BFF = unused (256 B)
! 1C00-2BFF = RAM (4KB)
! 2C00-3BFF = sharedRAM (4kB)
! 4400-FF7F = FRAM code memory (FRAM) (MSP430FR59x8/9)
! 8000-FF7F = FRAM code memory (FRAM) (MSP430FR59x7/8/9)
! FF80-FFFF = FRAM interrupt vectors and signatures (FRAM)

! ----------------------------------------------
! PAGESIZE        .equ 512         ; MPU unit
! ----------------------------------------------
! BSL                         
! ----------------------------------------------
BSL=0x1000!
! ----------------------------------------------
! FRAM                          ; INFO B, A, TLV
! ----------------------------------------------
INFOSTART=0x1800!
INFODSTART=0x1800!
INFODEND=0x187F!
INFOCSTART=0x1880!
INFOCEND=0x18FF!
INFOBSTART=0x1900!
INFOBEND=0x197F!
INFOASTART=0x1980!
INFOAEND=0x19FF!
TLVSTART=0x1A00!        Device Descriptor Info (Tag-Lenght-Value)
TLVEND=0x1AFF!
! ----------------------------------------------
! RAM
! ----------------------------------------------
TinyRAM=0x0A!
TinyRAMEnd=0x1F!
RAMSTART=0x1C00!
RAMEND=0x2BFF!
SharedRAMSTART=0x2C00!
SharedRAMEND=0x3BFF!
! ----------------------------------------------
! FRAM
! ----------------------------------------------
PROGRAMSTART=0x4000!    Code space start
!FRAMEND=0x43FFF!       256 k FRAM
SIGNATURES=0xFF80!      JTAG/BSL signatures
JTAG_SIG1=0xFF80!       if 0, enable JTAG/SBW
JTAG_SIG2=0xFF82!       if JTAG_SIG1=0xAAAA, length of password string @ JTAG_PASSWORD
BSL_SIG1=0xFF84!     
BSL_SIG2=0xFF86!     
JTAG_PASSWORD=0xFF88!   256 bits max
IPE_SIG_VALID=0xFF88!   one word
IPE_STR_PTR_SRC=0xFF8A! one word
INTVECT=0xFFB4!         FFB4-FFFF
BSL_PASSWORD=0xFFE0!    256 bits


LEA_Vec=0xFFB4!
P8_Vec=0xFFB6!
P7_Vec=0xFFB8!
eUSCI_B3_Vec=0xFFBA!
eUSCI_B2_Vec=0xFFBC!
eUSCI_B1_Vec=0xFFBE!
eUSCI_A3_Vec=0xFFC0!
eUSCI_A2_Vec=0xFFC2!
P6_Vec=0xFFC4!
P5_Vec=0xFFC6!
TA4_x_Vec=0xFFC8!
TA4_0_Vec=0xFFCA!
AES_Vec=0xFFCC!
RTC_C_Vec=0xFFCE!
P4_Vec=0xFFD0!
P3_Vec=0xFFD2!
TA3_x_Vec=0xFFD4!
TA3_0_Vec=0xFFD6!
P2_Vec=0xFFD8!
TA2_x_Vec=0xFFDA!
TA2_0_Vec=0xFFDC!
P1_Vec=0xFFDE!
TA1_x_Vec=0xFFE0!
TA1_0_Vec=0xFFE2!
DMA_Vec=0xFFE4!
eUSCI_A1_Vec=0xFFE6!
TA0_x_Vec=0xFFE8!
TA0_0_Vec=0xFFEA!
ADC12_B_Vec=0xFFEC!
eUSCI_B0_Vec=0xFFEE!
eUSCI_A0_Vec=0xFFF0!
WDT_Vec=0xFFF2!
TB0_x_Vec=0xFFF4!
TB0_0_Vec=0xFFF6!
COMP_E_Vec=0xFFF8!
U_NMI_Vec=0xFFFA!
S_NMI_Vec=0xFFFC!
RST_Vec=0xFFFE!
