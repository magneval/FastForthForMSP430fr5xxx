!MSP430FR2633.pat

@define{@read{a:/config/gema/MSP430FR2xxx.pat}}

! ----------------------------------------------
! MSP430FR2633 MEMORY MAP
! ----------------------------------------------
! 0000-0FFF = peripherals (4 KB)
! 1000-17FF = ROM bootstrap loader BSL0..3 (4x512 B)
! 1800-187F = info B (FRAM 128 B)
! 1880-18FF = info A (FRAM 128 B)
! 1900-19FF = N/A (mirrored into info A/B)
! 1A00-1A7F = TLV device descriptor info (FRAM 128 B)
! 1A80-1BFF = unused (385 B)
! 1C00-1FFF = RAM (1 KB)
! 2000-C1FF = unused (41472 B)
! C200-FF7F = code memory (FRAM 15743 B)
! FF80-FFFF = interrupt vectors (FRAM 127 B)
! ----------------------------------------------
INFOSTART=0x1800!
INFOBSTART=0x1800!
INFOBEND=0x19FF!
INFOEND=0x19FF!
TLVSTAT=0x1A00!         Device Descriptor Info (Tag-Lenght-Value)
TLVEND=0x1A7F! 
RAMSTART=0x2000!
RAMEND=0x2FFF!
PROGRAMSTART=0xC400!    Code space start
SIGNATURES=0xFF80!      JTAG/BSL signatures
JTAG_SIG1=0xFF80!       if 0 (electronic fuse=0) enable JTAG/SBW; must be reset by wipe.
JTAG_SIG2=0xFF82!       if JTAG_SIG1=0xAAAA, length of password string @ JTAG_PASSWORD
BSL_SIG1=0xFF84!  
BSL_SIG2=0xFF86!  
JTAG_PASSWORD=0xFF88!   256 bits
INTVECT=0xFFD8!         FFD8-FFFF
BSL_PASSWORD=0xFFE0!    256 bits


CAPTIVATE_Vec=0xFFD8!
P2_Vec=0xFFDA!
P1_Vec=0xFFDC!
ADC10_B_Vec=0xFFDE!
eUSCI_B0_Vec=0xFFE0!
eUSCI_A1_Vec=0xFFE2!
eUSCI_A0_Vec=0xFFE4!
WDT_Vec=0xFFE6!
RTC_Vec=0xFFE8!
TA3_x_Vec=0xFFEA!
TA3_0_Vec=0xFFEC!
TA2_x_Vec=0xFFEE!
TA2_0_Vec=0xFFF0!
TA1_x_Vec=0xFFF2!
TA1_0_Vec=0xFFF4!
TA0_x_Vec=0xFFF6!
TA0_0_Vec=0xFFF8!
U_NMI_Vec=0xFFFA!
S_NMI_Vec=0xFFFC!
RST_Vec=0xFFFE!

