!MSP430FR2433.pat

@define{@read{a:/config/gema/MSP430FR2xxx.pat}}

! ----------------------------------------------
! MSP430FR2433 MEMORY MAP
! ----------------------------------------------
! 0000-0FFF = peripherals (4 KB)
! 1000-17FF = ROM bootstrap loader BSL1 (2k)
! 1800-19FF = info B (FRAM 512 B)
! 1A00-1A7F = TLV device descriptor info (FRAM 128 B)
! 1A80-1FFF = unused
! 2000-2FFF = RAM (4 KB)
! 2800-C3FF = unused
! C400-FF7F = code memory (FRAM 15232 B)
! FF80-FFFF = interrupt vectors (FRAM 128 B)
! FFC00-FFFFF = BSL2 (2k)
! ----------------------------------------------
!PAGESIZE        .equ 512         ; MPU unit
! ----------------------------------------------
! BSL                           
! ----------------------------------------------
BSL1=0x1000!
BSL2=0xFFC00!
! ----------------------------------------------
! FRAM                          ; INFO B, TLV
! ----------------------------------------------
INFOSTART =0x1800!
INFOBSTART=0x1800!
INFOBEND=0x19FF!
INFOEND=0x19FF!
TLVSTART=0x1A00!    Device Descriptor Info (Tag-Lenght-Value)
TLVEND=0x1A7F!
! ----------------------------------------------
! RAM
! ----------------------------------------------
RAMSTART=0x2000!
RAMEND=0x2FFF!
! ----------------------------------------------
! FRAM
! ----------------------------------------------
PROGRAMSTART=0xC400!    Code space start
SIGNATURES=0xFF80!      JTAG/BSL signatures
JTAG_SIG1=0xFF80!       if 0 (electronic fuse=0) enable JTAG/SBW ; reset by wipe and by S1+<reset>
JTAG_SIG2=0xFF82!       if JTAG_SIG <> |0xFFFFFFFF, 0x00000000|, SBW and JTAG are locked
BSL_SIG1=0xFF84!        
BSL_SIG2=0xFF86!        
JTAG_PASSWORD=0xFF88!   256 bits
INTVECT=0xFFDA!         FFDA-FFFF
BSL_PASSWORD=0xFFE0!    256 bits
! ----------------------------------------------


P2_Vec=0xFFDA!
P1_Vec=0xFFDC!
ADC10_B_Vec=0xFFDE!
eUSCI_B0_Vec=0xFFE0!
eUSCI_A1_Vec=0xFFE2!
eUSCI_A0_Vec=0xFFE4!
WDT_Vec=0xFFE6!
RTC_Vec=0xFFE8!
TA3_x_Vec=0xFFEA!
TA3_0_Vec=0xFFEC!
TA2_x_Vec=0xFFEE!
TA2_0_Vec=0xFFF0!
TA1_x_Vec=0xFFF2!
TA1_0_Vec=0xFFF4!
TA0_x_Vec=0xFFF6!
TA0_0_Vec=0xFFF8!
U_NMI_Vec=0xFFFA!
S_NMI_Vec=0xFFFC!
RST_Vec=0xFFFE!


TA0CTL=0x380!       \ TA0 control                 
TA0CCTL0=0x382!     \ Capture/compare control 0   
TA0CCTL1=0x384!     \ Capture/compare control 1   
TA0CCTL2=0x386!     \ Capture/compare control 2   
TA0R=0x390!         \ TA0 counter register        
TA0CCR0=0x392!      \ Capture/compare register 0  
TA0CCR1=0x394!      \ Capture/compare register 1  
TA0CCR2=0x396!      \ Capture/compare register 2  
TA0EX0=0x3A0!       \ TA0 expansion register 0    
TA0IV=0x3AE!        \ TA0 interrupt vector        

TA1CTL=0x3C0!       \ TA1 control                 
TA1CCTL0=0x3C2!     \ Capture/compare control 0   
TA1CCTL1=0x3C4!     \ Capture/compare control 1   
TA1CCTL2=0x3C6!     \ Capture/compare control 2   
TA1R=0x3D0!         \ TA1 counter register        
TA1CCR0=0x3D2!      \ Capture/compare register 0  
TA1CCR1=0x3D4!      \ Capture/compare register 1  
TA1CCR2=0x3D6!      \ Capture/compare register 2  
TA1EX0=0x3E0!       \ TA1 expansion register 0    
TA1IV=0x3EE!        \ TA1 interrupt vector        

TA2CTL=0x400!       \ TA2 control                 
TA2CCTL0=0x402!     \ Capture/compare control 0   
TA2CCTL1=0x404!     \ Capture/compare control 1   
TA2R=0x410!         \ TA2 counter register        
TA2CCR0=0x412!      \ Capture/compare register 0  
TA2CCR1=0x414!      \ Capture/compare register 1  
TA2EX0=0x420!       \ TA2 expansion register 0    
TA2IV=0x42E!        \ TA2 interrupt vector        

TA3CTL=0x440!       \ TA3 control                 
TA3CCTL0=0x442!     \ Capture/compare control 0   
TA3CCTL1=0x444!     \ Capture/compare control 1   
TA3R=0x450!         \ TA3 counter register        
TA3CCR0=0x452!      \ Capture/compare register 0  
TA3CCR1=0x454!      \ Capture/compare register 1  
TA3EX0=0x460!       \ TA3 expansion register 0    
TA3IV=0x46E!        \ TA3 interrupt vector        

