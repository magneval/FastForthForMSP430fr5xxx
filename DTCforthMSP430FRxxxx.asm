; -*- coding: utf-8 -*-
; http://patorjk.com/software/taag/#p=display&f=Banner&t=Fast Forth

; Fast Forth For Texas Instrument MSP430FRxxxx FRAM devices
; Copyright (C) <2015>  <J.M. THOORENS>
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;----------------------------------------------------------------------------------
; Vingt fois sur le métier remettez votre ouvrage, 
; Polissez-le sans cesse, et le repolissez,
; Ajoutez quelquefois, et souvent effacez. 
;
; Boileau, L'Art poétique
;----------------------------------------------------------------------------------

;===============================================================================================
;===============================================================================================
; before assembling or programming you must set DEVICE in param1 and TARGET in param2 (SHIFT+F8)
; according to the TARGET "switched" below
; example : your TARGET = MSP_EXP430FR5969 (notice the underscore) ==> DEVICE = MSP430FR5969 
;===============================================================================================
;===============================================================================================

; to measure the size in bytes of MAIN program do this :
;   - open the target.txt file (TI format) with your text editor,
;   - select lines of MAIN program,
;   - if lines are ended by CR+LF substract number of selected lines from to number of chars selected,
;   - divide then result by 3.

;-----------------------------------------------------------------------------------------------------
; TARGET configuration SWITCHES ; bytes values are measured for DTC1 model, 8MHz 921600bds settings
;-----------------------------------------------------------------------------------------------------
;                                                                     TOTAL - SUM of (INFO+SIG+VECTORS) = MAIN PROG
;MSP_EXP430FR5739   ; compile for MSP-EXP430FR5739 launchpad        ; 4022  - 142    ( 84 + 8 +  50   ) = 3880 bytes 
;MSP_EXP430FR5969   ;; compile for MSP-EXP430FR5969 launchpad        ; 3990  - 144    ( 84 + 8 +  52   ) = 3846 bytes 
;MSP_EXP430FR5994   ; compile for MSP-EXP430FR5994 launchpad        ; 4024  - 168    ( 84 + 8 +  76   ) = 3856 bytes
;MSP_EXP430FR6989   ; compile for MSP-EXP430FR6989 launchpad        ; 4022  - 150    ( 84 + 8 +  58   ) = 3872 bytes 
;MSP_EXP430FR4133   ; compile for MSP-EXP430FR4133 launchpad        ; 4040  - 122    ( 84 + 8 +  30   ) = 3918 bytes
;CHIPSTICK_FR2433   ; compile for the "CHIPSTICK" of M. Ken BOAK    ; 3968  - 130    ( 84 + 8 +  38   ) = 3848 bytes
;MY_MSP430FR5738    ; compile for my own MSP430FR5738 miniboard     ; 3968  - 142    ( 84 + 8 +  50   ) = 3826 bytes
;MY_MSP430FR5738_1  ; compile for my own MSP430FR5738_1 miniboard   ; 3968  - 142    ( 84 + 8 +  50   ) = 3826 bytes
;MY_MSP430FR5734_1  ; compile for my own MSP430FR5734_1 miniboard   ; 3978  - 142    ( 84 + 8 +  50   ) = 3826 bytes
;MY_MSP430FR5948    ; compile for my own MSP430FR5948 miniboard     ; 3974  - 144    ( 84 + 8 +  52   ) = 3830 bytes
;MY_MSP430FR5948_1  ; compile for my own MSP430FR5948_1 miniboard   ; 3974  - 144    ( 84 + 8 +  52   ) = 3830 bytes 
JMJ_BOX            ; compile for JMJ_BOX MSP430FR5738              ; 3968  - 142    ( 84 + 8 +  50   ) = 3826 bytes 
;PA8_PA_MSP430      ; compile for PA8_PA_MSP430 MSP430FR5738        ; 3968  - 142    ( 84 + 8 +  50   ) = 3826 bytes 

; choose DTC (Direct Threaded Code) model, if you don't know, choose 1
DTC .set 1  ; DTC model 1 : DOCOL = CALL rDOCOL           14 cycles 1 word      shortest DTC model
            ; DTC model 2 : DOCOL = PUSH IP, CALL rEXIT   13 cycles 2 words     good compromize for mix FORTH/ASM code
            ; DTC model 3 : inlined DOCOL                  9 cycles 4 words     fastest

FREQUENCY   .set 8 ; fully tested at 0.5,1,2,4,8,16 (and 24 for MSP430FR57xx) MHz
THREADS     .set 16 ; 1, 4, 8, 16 search entries in dictionnary. 16 is an optimum: speed up to 8 the compilation rate.

TERMINALBAUDRATE    .set 921600
TERMINALXONXOFF     ;; to allow XON/XOFF flow control (PL2303TA/CP2102 devices)
;TERMINALCTSRTS      ; to allow Hardware flow control (FT232RL device)

;LF_XTAL            ; VLOCLK/LFXCLK switch for ACLK ; comment if no 32768Hz XTAL else RTC_B/C write operations are halted !

;-----------------------------------------------------------------------
; KERNEL ADD-ON SWITCHES ;
;-----------------------------------------------------------------------
MSP430ASSEMBLER    ;; + 1894 bytes : add embedded assembler with TI syntax; without, you can do all but all much more slowly...

;SD_CARD_LOADER     ; + 1718 bytes to LOAD source files from SD_card
;SD_CARD_READ_WRITE ; +  972 bytes to create, read, write, close and del files
VOCABULARY_SET     ; +  108 bytes : add VOCABULARY FORTH ASSEMBLER ALSO PREVIOUS ONLY DEFINITIONS (FORTH83, not ANSI)
;LOWERCASE          ; +   30 bytes : enable to EMIT strings in lowercase.
BACKSPACE_ERASE    ; +   24 bytes : replace BS by ERASE, for BS visual comfort

;-------------------------------------------------------------------------------------------------
; OPTIONAL KERNELL ADD-ON SWITCHES, because their source file can be downloaded by FASTFORTH later 
;-------------------------------------------------------------------------------------------------
;ANS_CORE_COMPLIANT ; +  894 bytes : required to pass coretest.4th ; instead you can download ANS_COMPLEMENT_xW_MPY.f
;ARITHMETIC         ; +  280 bytes : add S>D M* SM/REM FM/MOD * /MOD / MOD */MOD /MOD */ (included with ANS_CORE_COMPLIANT)
;DOUBLE             ; +  130 bytes : add 2@ 2! 2DUP 2SWAP 2OVER (included with ANS_CORE_COMPLIANT)
;ALIGNMENT          ; +   24 bytes : add ALIGN ALIGNED (included with ANS_CORE_COMPLIANT)
;PORTABILITY        ; +   42 bytes : add CHARS CHAR+ CELLS CELL+ (included with ANS_CORE_COMPLIANT)
UTILITY            ; +  390 bytes : add .S WORDS U.R DUMP ?
;SD_TOOLS           ; for trivial DIR, FAT & SD_VIEW sector, include UTILITY

;-----------------------------------------------------------------------
; XON/XOFF control flow configuration ; up to 285kBd/MHz with ECHO
;-----------------------------------------------------------------------

; the cheapest and best : UARTtoUSB cable with Prolific PL2303TA (supply current = 8 mA)
; ---------------------------------------------------------------------------------------------------
; WARNING ! if you use it as supply for your target, open box before to weld red wire on 3v3 pad !
; ---------------------------------------------------------------------------------------------------
; tested with USBtoUART PL2303TA, 1m of cable andand thru an si8622EC-B-IS digital isolator
; ---------------------------------------------------------------------------------------------------
; 9600,19200,38400,57600,115200,134400 (500kHz)
; + 161280,201600,230400,268800 (1MHz)
; + 403200,460800,614400 (2MHz)     notice the ratio F/bds=2000000/614400=3.25
; + 806400,921600,1228800 (4MHz)    notice the ratio F/bds=4000000/1228800=3.25
; + (8MHz)
; + 2457600,3000000 (16MHz)
; + 6000000 (24MHz MSP430FR57xx)

; UARTtoUSB module with Silabs CP2102 (supply current = 20 mA)
; ---------------------------------------------------------------------------------------------------
; WARNING ! if you use it as supply for your target, connect VCC on the wire 3v3 !
; ---------------------------------------------------------------------------------------------------
; 9600,19200,38400,57600 (500kHz)
; + 115200 (1MHz)
; + 230400 (2MHz)
; + 460800 (4MHz)
; + 921600,1382400,1843200 (8MHz,16MHz,24MHz)
; notice that you must program the CP2102 device to add speeds 1382400, 1843200 bds.

; Launchpad     UARTtoUSB device
;        RX <-- TX
;        TX --> RX

; TERATERM config terminal      : NewLine receive : AUTO,
;                                 NewLine transmit : CR+LF
;                                 Size : 128 chars x 49 lines (adjust lines to your display)

; TERATERM config serial port   : 9600 to 6000000 Bds,
;                                 8bits, no parity, 1Stopbit,
;                                 XON/XOFF flow control,
;                                 delay = 0ms/line, 0ms/char

; don't forget : save new TERATERM configuration !


;-----------------------------------------------------------------------
; Hardware control flow configuration with FT232RL device only
;-----------------------------------------------------------------------

; UARTtoUSB module with FTDI FT232RL
;===============================================================================================
; WARNING ! buy a FT232RL module with a switch 5V/3V3 and select 3V3 !
;===============================================================================================
; 9600,19200,38400,57600,115200 (500kHz)
; + 230400 (1MHz)
; + 460800 (2MHz)
; + 921600 (4,8,16 MHz)

; Launchpad     UARTtoUSB device
;        RX <-- TX
;        TX --> RX
;       RTS --> CTS

; notice that the control flow seems not necessary for TX

; TERATERM config terminal      : NewLine receive : AUTO,
;                                 NewLine transmit : CR+LF (so FT232RL can test its CTS line during transmit LF)
;                                 Size : 128 chars x 49 lines (adjust lines to your display)

; TERATERM config serial port   : 9600 to 921600 Bds,
;                                 8bits, no parity, 1Stopbit,
;                                 Hardware flow control,
;                                 delay = 0ms/line, 0ms/char

; don't forget : save new TERATERM configuration !

; ----------------------------------------------------------------------
; Compilation speed
; ----------------------------------------------------------------------
; MSP43FR6989 @ 8 MHz, 1.8432Mbd terminal, 16 threads FAST FORTH, CP2102.
; JMJ-BOX.4th x 24 = 300kb of source without comments ==> 1832 x 24 = 43968 bytes of code
; download rate without echo 54 kb/s ==> 5.55s / 44k bytes of code ( 1kb of code / s / MHz )
; ==> 8.8s for 65k of code, to compare with elptronic FlashPro-430-STD with 3.3s via JTAG, 9s via SBW.
; it's best @ 16 MHz, 3Mbds...
;---------------------------------------------------------------------------------------------------
; LOW ENERGY MSP430FRxxxx FORTH computer @ 500 kHz with XON/XOFF terminal @ 115200bds  
;---------------------------------------------------------------------------------------------------
; i.e. MSP43fr5969 supply current when download a source file  : 175 µA.
; download rate: 3.62 kb of source / s ==> 0.5kb of code / s 


; ----------------------------------------------------------------------
; assembled with MACROASSEMBLER AS (http://john.ccac.rwth-aachen.de:8000/as/)
; ----------------------------------------------------------------------

    .cpu MSP430
    .include "mspregister.asm" ;
;    macexp off             ; unrem to hide macro results

    .include "DeviceMAP.map" ; to define config device

; ----------------------------------------------------------------------
; DTCforthMSP430FR5xxx Init vocabulary pointers:
; ----------------------------------------------------------------------

    .IF THREADS = 1

voclink     .set 0                      ; init vocabulary links
forthlink   .set 0
asmlink     .set 0

FORTHWORD   .MACRO  name
            .word   forthlink
forthlink   .set    $
lastword    .set    $
            .byte   STRLEN(name),name
;            .align  2
            .ENDM

FORTHWORDIMM .MACRO  name
            .word   forthlink
forthlink   .set    $
lastword    .set    $
            .byte   STRLEN(name)+128,name
;            .align  2
            .ENDM

asmword     .MACRO  name
            .word   asmlink
asmlink     .set    $
lastword    .set    $
            .byte   STRLEN(name),name
;            .align  2
            .ENDM

    .ELSE
    .include "ForthThreads.mac"
    .ENDIF

; ----------------------------------------------------------------------
; DTCforthMSP430FR5xxx RAM memory map:
; ----------------------------------------------------------------------

; name              words   ; comment

;LSTACK=L0                  ; ----- 1C00
                            ; |
LSTACK_SIZE .equ    16      ; | grows up
                            ; |
                            ; V
                            ;
                            ; ^
                            ; |
PSTACK_SIZE .equ    57      ; | grows down
                            ; |
;PSTACK=S0                  ; ----- 1C92
                            ;
                            ; ^
                            ; |
RSTACK_SIZE .equ    57      ; | grows down
                            ; |
;RSTACK=R0                  ; ---- 1D04

                            ; aligned buffers only required for terminal tasks.

; names             bytes   ; comments

                            ; ^
                            ; |
HOLD_SIZE   .equ    34      ; | grows down  (ans spec. : HOLD_SIZE >= (2*n) + 2 char, with n = 16 bits/cell
                            ; |
;BASE_HOLD                  ; ----- 1D26
;PAD                        ; ----- 1D28
                            ; |
PAD_SIZE    .equ    84      ; | grows up    (ans spec. : PAD_SIZE >= 84 chars)
                            ; |
                            ; v
                            ; ------1D7C
;TIB                        ; ----- 1D7E
                            ; |
TIB_SIZE    .equ    130     ; | grows up    (ans spec. :Terminal input buffer <= 128 chars)
                            ; |
                            ; v
;BUFFER                     ; ----- 1E00
;INPUT_BUFFER               ; 512 bytes buffer
                            ; ----- 1FFF

LSTACK      .equ    RAMSTART
PSTACK      .equ    LSTACK+(LSTACK_SIZE*2)+(PSTACK_SIZE*2)
RSTACK      .equ    PSTACK+(RSTACK_SIZE*2)
BASE_HOLD   .equ    RSTACK+HOLD_SIZE
PAD         .equ    BASE_HOLD+2
TIB         .equ    PAD+PAD_SIZE+2
BUFFER      .equ    TIB+TIB_SIZE
BUFEND      .equ    BUFFER + 200h

; ----------------------------------------------------------------------
; DTCforthMSP430FR5xxx INFO(DCBA) >= 256 bytes memory map:
; ----------------------------------------------------------------------

    .org    INFOSTART

; ----------------------
; KERNEL CONSTANTS
; ----------------------
INI_THREAD      .word THREADS
INI_TERM        .word TERMINAL_INT
INI_KEY         .word PARENKEY
INI_QKEY        .word PARENKEYTST
INI_ACCEPT      .word PARENACCEPT
INI_YEMIT       .word 4882h         ; MOV Y,&adr
INI_EMIT        .word PARENEMIT
INI_CR          .word PARENCR
INI_WARM        .word PARENWARM

; ----------------------
; SOME HERETIC VARIABLES
; ----------------------

SAVE_SYSRSTIV   .word 0                 ; to enable SYSRSTIV read
LPM_MODE        .word CPUOFF+GIE        ; LPM0 is the default mode
INIDP           .word ROMDICT           ; define RST_STATE
INIVOC          .word lastvoclink       ; define RST_STATE
INILATEST       .word latestword        ; define RST_STATE 

CAPS            .word -1                ; CAPS ON/OFF flag
LEAVEPTR        .word 0                 ; Leave-stack pointer
LAST_NFA        .word 0                 ; NFA, VOC_PFA, LFA, CFA, CSP of last created word
LAST_THREAD     .word 0
LAST_OLD_NFA    .word 0                 ; for MARKER
LAST_CFA        .word 0
LAST_CSP        .word 0

MEM_CURRENT     .word 0                 ; CURRENT memory for ASSEMBLER
                .word 0                 ;
OPCODE          .word 0                 ; OPCODE adr
ASMTYPE         .word 0                 ; keep the opcode complement


; ----------------------
; LESS HERETIC VARIABLES
; ----------------------

HP              .word 0                 ; HOLD ptr
TICKSOURCE      .word 0,0               ; len, addr of input stream
TOIN            .word 0
DDP             .word 0
LASTVOC         .word 0                 ; keep VOC-LINK
CURRENT         .word 0                 ; CURRENT dictionnary ptr
CONTEXT         .word 0,0,0,0,0,0,0,0   ; CONTEXT dictionnary space
BASE            .word 10                ; numeric base
STATE           .word 0                 ; Interpreter state

    .IFDEF SD_CARD_LOADER
; ---------------------------------------
; FAT16 Constants
; ---------------------------------------
BytsPerSec      .equ 512
; ---------------------------------------
; FAT16 FileSystemInfos 
; ---------------------------------------
FirstSector     .word 0 ; 0x1854
OrgFAT1         .word 0  
FATSize         .word 0
OrgFAT2         .word 0  
OrgRootDir      .word 0
OrgDatas        .word 0
OrgClusters     .word 0 ; Sector of Cluster 0
SecPerClus      .word 0
TotSectorsL     .word 0,0
CurFATsector    .word 0
; ---------------------------------------
; SD command
; ---------------------------------------
SD_CMD_FRM      .byte 0,0,0,0,0,0   ; SD_CMDx inverted frame ${95,ll,LL,hh,HH,CMD} 
SectorL         .word 0
SectorH         .word 0
; ---------------------------------------
; BUFFER management
; ---------------------------------------
BufferPtr       .word 0
BufferLen       .word 0
; ---------------------------------------
; FAT entry
; ---------------------------------------
Cluster         .word 0   ; 16 bits wide (FAT16)
NewCluster      .word 0   ; 16 bits wide (FAT16) 
FATsector       .word 0
FAToffset       .word 0   ; in BUFFER
; ---------------------------------------
; DIR entry
; ---------------------------------------
DIRcluster      .word 0     ; contains the Cluster of current directory ; 1 if FAT16 root directory
EntryOfst       .word 0
pathname        .word 0     ; address of pathname string
; ---------------------------------------
; Handle Pointer
; ---------------------------------------
CurrentHdl      .word 0     ; contains the address of the last opened file structure, or 0
; ---------------------------------------
; Load file operation
; ---------------------------------------
SAVEtsLEN       .word 0     ; of previous ACCEPT
SAVEtsPTR       .word 0     ; of previous ACCEPT
MemSectorL      .word 0,0   ; current Sector of previous LOAD"ed file
; ---------------------------------------
; Handle structure
; ---------------------------------------
; three handle tokens : 
; HDLB_Token= 0 : free handle
;           = 1 : file to read
;           = 2 : file updated (write)
;           =-1 : LOAD"ed file (source file)

; offset values
HDLW_PrevHDL    .equ 0  ; previous handle ; used by LOAD"
HDLB_Token      .equ 2  ; token
HDLB_ClustOfst  .equ 3  ; Current sector offset in current cluster (Byte)
HDLL_DIRsect    .equ 4  ; Dir SectorL (Long)
HDLW_DIRofst    .equ 8  ; BUFFER offset of Dir entry
HDLW_FirstClus  .equ 10 ; File First Cluster (identify the file)
HDLW_CurClust   .equ 12 ; Current Cluster
HDLL_CurSize    .equ 14 ; written size / not yet read size (Long)
HDLW_BUFofst    .equ 18 ; BUFFER offset ; used by LOAD" and by WRITE"

HandleMax       .equ 5
HandleLenght    .equ 20

;OpenedFirstFile     ; structure  "openedFile"
FirstHandle     .word 0,0,0,0,0,0,0,0,0,0
                .word 0,0,0,0,0,0,0,0,0,0
                .word 0,0,0,0,0,0,0,0,0,0
                .word 0,0,0,0,0,0,0,0,0,0
                .word 0,0,0,0,0,0,0,0,0,0
HandleOutOfBound

SD_READ     .word ReadSectorWX
    .IFDEF SD_CARD_READ_WRITE
SD_WRITE    .word WriteSectorWX
    .ENDIF ; SD_CARD_READ_WRITE
    .ENDIF ; SD_CARD_LOADER

; ----------------------------------------------------------------------
; DTCforthMSP430FR5xxx program (FRAM) memory
; ----------------------------------------------------------------------

    .org    PROGRAMSTART

; ----------------------------------------------------------------------
; DTCforthMSP430FR5xxx REGISTER USAGE
; ----------------------------------------------------------------------

    .SWITCH DTC
    .CASE 1 ; DOCOL = CALL rDOCOL

RSP         .reg    SP      ; RSP = Return Stack Pointer (return stack)

; DOxxx registers           ; must be saved before use and restored after use
rDODOES     .reg    r4
rDOCON      .reg    r5
rDOVAR      .reg    r6
rDOCOL      .reg    R7

; Scratch registers
Y           .reg    R8 
X           .reg    R9 
W           .reg    R10
T           .reg    R11
S           .reg    R12

; Forth virtual machine
IP          .reg    R13      ; interpretative pointer
TOS         .reg    R14      ; first PSP cell
PSP         .reg    R15      ; PSP = Parameters Stack Pointer (stack data)

    .CASE 2 ; DOCOL = PUSH IP + CALL rEXIT

RSP         .reg    SP      ; RSP = Return Stack Pointer (return stack)

; DOxxx registers           ; must be saved before use and restored after use
rDODOES     .reg    r4
rDOCON      .reg    r5
rDOVAR      .reg    r6
rEXIT       .reg    R7

; Scratch registers
Y           .reg    R8 
X           .reg    R9 
W           .reg    R10
T           .reg    R11
S           .reg    R12

; Forth virtual machine
IP          .reg    R13      ; interpretative pointer
TOS         .reg    R14      ; first PSP cell
PSP         .reg    R15      ; PSP = Parameters Stack Pointer (stack data)

    .CASE 3  ; INLINED DOCOL

RSP         .reg    SP      ; RSP = Return Stack Pointer (return stack)

; DOxxx registers           ; must be saved before use and restored after use
rDODOES     .reg    r4
rDOCON      .reg    r5
rDOVAR      .reg    r6

; Scratch registers
R           .reg    R7
Y           .reg    R8 
X           .reg    R9 
W           .reg    R10
T           .reg    R11
S           .reg    R12

; Forth virtual machine
IP          .reg    R13      ; interpretative pointer
TOS         .reg    R14      ; first PSP cell
PSP         .reg    R15      ; PSP = Parameters Stack Pointer (stack data)

    .ENDCASE ; DTC

; ----------------------------------------------------------------------
; DEFINING EXECUTIVE WORDS - DTC model
; ----------------------------------------------------------------------

; ----------------------------------------------------------------------
; very nice FAST FORTH added feature: 
; ----------------------------------------------------------------------
; as IP is calculated from the PC value we can place the low to high level
; switch "COLON" anywhere in a word, i.e. not only at its beginning.
; ----------------------------------------------------------------------


    .SWITCH DTC
    .CASE 1 ; DOCOL = CALL rDOCOL

mNEXT       .MACRO          ; return for low level words (written in assembler)
            MOV @IP+,PC     ; 4 fetch code address into PC, IP=PFA
            .ENDM           ; 4 cycles,1word = ITC -2cycles -1 word

NEXT        .equ    4D30h   ; 4 MOV @IP+,PC

FORTHtoASM  .MACRO          ; compiled by HI2LO
            .word   $+2     ; 0 cycle
            .ENDM           ; 0 cycle, 1 word

ASMtoFORTH  .MACRO          ; compiled by LO2HI
            CALL #EXIT      ;
            .ENDM           ; 2 words, 10~

DOCOL1      .equ    1287h   ; 4 CALL R7 ; [R7] is set as xdocol by COLD

mDOCOL      .MACRO          ; compiled by :
            CALL R7         ;
            .ENDM           ; 14~ 1 word

xdocol                      ; 4 for CALL rDOCOL
            MOV @RSP+,W     ; 2
            PUSH IP         ; 3     save old IP on return stack
            MOV W,IP        ; 1     set new IP to PFA
            MOV @IP+,PC     ; 4     = NEXT
                            ; 14 = ITC +4

    .CASE 2 ; DOCOL = PUSH IP + CALL rEXIT

mNEXT       .MACRO
            MOV @IP+,PC     ; 4 fetch code address into PC, IP=PFA
            .ENDM           ; 4cycles,1word = ITC -2cycles -1 word

NEXT        .equ    4D30h   ; 4 MOV @IP+,PC

FORTHtoASM  .MACRO          ; compiled by HI2LO
            .word   $+2     ; 0 cycle
            .ENDM           ; 0 cycle, 1 word

ASMtoFORTH  .MACRO          ; compiled by LO2HI
            CALL rEXIT      ;    CALL EXIT
            .ENDM           ; 10 cycles, 1 word

mDOCOL      .MACRO          ; compiled by : and by COLON
            PUSH IP         ; 3
            CALL rEXIT      ; 10 CALL EXIT
            .ENDM           ; 13 cycles (ITC+3), two words

DOCOL1      .equ    120Dh   ; 3 PUSH IP
DOCOL2      .equ    1287h   ; 4 CALL rEXIT ; [rEXIT] is set as EXIT by COLD

    .CASE 3 ; inlined DOCOL

mNEXT       .MACRO          ; return for low level words (written in assembler)
            MOV @IP+,PC     ; 4 fetch code address into PC, IP=PFA
            .ENDM           ; 4 cycles,1word = ITC -2cycles -1 word

NEXT        .equ    4D30h   ; 4 MOV @IP+,PC

FORTHtoASM  .MACRO          ; compiled by HI2LO
            .word   $+2     ; 0 cycle
            .ENDM           ; 0 cycle, 1 word

ASMtoFORTH  .MACRO          ; compiled by LO2HI
            MOV PC,IP       ; 1 
            ADD #4,IP       ; 1 
            MOV @IP+,PC     ; 4 NEXT
            .ENDM           ; 6 cycles, 3 words

mDOCOL      .MACRO          ; compiled by : and by COLON
            PUSH IP         ; 3 
            MOV PC,IP       ; 1 
            ADD #4,IP       ; 1 
            MOV @IP+,PC     ; 4 NEXT
            .ENDM           ; 9 cycles (ITC -1), 4 words

DOCOL1      .equ    120Dh   ; 3 PUSH IP
DOCOL2      .equ    400Dh   ; 1 MOV PC,IP
DOCOL3      .equ    522Dh   ; 1 ADD #4,IP 

    .ENDCASE ; DTC

; mDOVAR leave on parameter stack the PFA of a VARIABLE definition

mDOVAR      .MACRO          ; compiled by VARIABLE
            CALL rDOVAR     ;    CALL RFROM    
            .ENDM           ; 14 cycles (ITC+4), 1 word

DOVAR       .equ    1286h   ; 4 CALL rDOVAR ; [rDOVAR] is set as RFROM by COLD


; mDOCON  leave on parameter stack the [PFA] of a CONSTANT definition

mDOCON      .MACRO          ; compiled by CONSTANT
            CALL rDOCON     ;    CALL xdocon
            .ENDM           ; 16 cycles (ITC+4), 1 word

DOCON       .equ    1285h   ; 4 CALL rDOCON ; [rDOCON] is set as xdocon by COLD

xdocon  ;   -- constant     ; 4 for CALL rDOCON
            SUB #2,PSP      ; 1 make room on stack
            MOV TOS,0(PSP)  ; 3 push first PSP cell
            MOV @RSP+,TOS   ; 2 TOS=CONSTANT address
            MOV @TOS,TOS    ; 2 TOS=CONSTANT
            MOV @IP+,PC     ; 4 execute next word
                            ; 16 = ITC (+4)

; mDODOES  leave on parameter stack the PFA of a CREATE definition

mDODOES     .MACRO          ; compiled  by DOES>
            CALL rDODOES    ;    CALL xdodoes 
            .ENDM           ; 19 cycles (ITC-2), 1 word

DODOES      .equ    1284h   ; 4 CALL rDODOES ; [rDODOES] is set as xdodoes by COLD

xdodoes   ; -- a-addr       ; 4 for CALL rDODOES
            SUB #2,PSP      ; 1
            MOV TOS,0(PSP)  ; 3 save TOS on parameters stack
            MOV @RSP+,TOS   ; 2 TOS = CFA address of master word, i.e. address of its first cell after DOES>
            PUSH IP         ; 3 save IP on return stack
            MOV @TOS+,IP    ; 2 IP = CFA of Master word, TOS = BODY of created word
            MOV @IP+,PC     ; 4 Execute Master word

; ----------------------------------------------------------------------
; INTERPRETER LOGIC
; ----------------------------------------------------------------------

;C EXIT     --      exit a colon definition; CALL #EXIT performs ASMtoFORTH
            FORTHWORD "EXIT"
EXIT        MOV     @RSP+,IP    ; 2 pop previous IP (or next PC) from return stack
            MOV     @IP+,PC     ; 4 = NEXT
                                ; 6 = ITC - 2

;Z lit      -- x    fetch inline literal to stack
; This is the primitive compiled by LITERAL.
            FORTHWORD "LIT"
lit         SUB     #2,PSP      ; 2  push old TOS..
            MOV     TOS,0(PSP)  ; 3  ..onto stack
            MOV     @IP+,TOS    ; 2  fetch new TOS value
            MOV     @IP+,PC     ; 4  NEXT
                                ; 11 = ITC - 2

; ----------------------------------------------------------------------
; STACK OPERATIONS
; ----------------------------------------------------------------------

;C DUP      x -- x x      duplicate top of stack
            FORTHWORD "DUP"
DUP         SUB     #2,PSP          ; 2  push old TOS..
            MOV     TOS,0(PSP)      ; 3  ..onto stack
            mNEXT                   ; 4

;C ?DUP     x -- 0 | x x    DUP if nonzero
            FORTHWORD "?DUP"
QDUP        CMP     #0,TOS          ; 2  test for TOS nonzero
            JNZ     DUP             ; 2
            mNEXT                   ; 4

;C DROP     x --          drop top of stack
            FORTHWORD "DROP"
DROP        MOV     @PSP+,TOS       ; 2
            mNEXT                   ; 4

;C SWAP     x1 x2 -- x2 x1    swap top two items
            FORTHWORD "SWAP"
SWAP        MOV     @PSP,W          ; 2
            MOV     TOS,0(PSP)      ; 3
            MOV     W,TOS           ; 1
            mNEXT                   ; 4

;C OVER    x1 x2 -- x1 x2 x1
            FORTHWORD "OVER"
OVER        SUB     #2,PSP          ; 2 -- x1 x x2
            MOV     TOS,0(PSP)      ; 3 -- x1 x2 x2
            MOV     2(PSP),TOS      ; 2 -- x1 x2 x1
            mNEXT                   ; 4

;C ROT    x1 x2 x3 -- x2 x3 x1
            FORTHWORD "ROT"
ROT         MOV     @PSP,W          ; 2 fetch x2
            MOV     TOS,0(PSP)      ; 3 store x3
            MOV     2(PSP),TOS      ; 3 fetch x1
            MOV     W,2(PSP)        ; 3 store x2
            mNEXT                   ; 4

;C >R    x --   R: -- x   push to return stack
            FORTHWORD ">R"
TOR         PUSH    TOS
            MOV     @PSP+,TOS
            mNEXT

;C R>    -- x    R: x --   pop from return stack ; CALL #RFROM performs DOVAR
            FORTHWORD "R>"
RFROM       SUB     #2,PSP          ; 1
            MOV     TOS,0(PSP)      ; 3
            MOV     @RSP+,TOS       ; 2
            mNEXT                   ; 4

;C R@    -- x     R: x -- x   fetch from rtn stk
            FORTHWORD "R@"
RFETCH      SUB     #2,PSP
            MOV     TOS,0(PSP)
            MOV     @RSP,TOS
            mNEXT

;Z SP@  -- a-addr       get data stack pointer
;            FORTHWORD "SP@"
SPFETCH     SUB     #2,PSP
            MOV     TOS,0(PSP)
            MOV     PSP,TOS
            mNEXT

;C DEPTH    -- +n        number of items on stack
            FORTHWORD "DEPTH"
DEPTH:      SUB     #2,PSP
            MOV     TOS,0(PSP)
            MOV     #PSTACK,TOS
            SUB     PSP,TOS       ; PSP-S0--> TOS
            RRA     TOS           ; TOS/2   --> TOS
            mNEXT

; ----------------------------------------------------------------------
; MEMORY OPERATIONS
; ----------------------------------------------------------------------

;C @       a-addr -- x   fetch cell from memory
            FORTHWORD "@"
FETCH       MOV     @TOS,TOS
            mNEXT


;C !        x a-addr --   store cell in memory
            FORTHWORD "!"
STORE       MOV     @PSP+,0(TOS)    ;4
            MOV     @PSP+,TOS       ;2
            mNEXT                   ;4

;C C@     c-addr -- char   fetch char from memory
            FORTHWORD "C@"
CFETCH      MOV.B   @TOS,TOS
            mNEXT


;C C!      char c-addr --    store char in memory
            FORTHWORD "C!"
CSTORE      MOV     @PSP+,W     ;2
            MOV.B   W,0(TOS)    ;3
            MOV     @PSP+,TOS   ;2
            mNEXT

; ----------------------------------------------------------------------
; ARITHMETIC OPERATIONS
; ----------------------------------------------------------------------

;C +       n1/u1 n2/u2 -- n3/u3     add n1+n2
            FORTHWORD "+"
PLUS        ADD     @PSP+,TOS
            mNEXT

;C -      n1/u1 n2/u2 -- n3/u3    subtract n1-n2
            FORTHWORD "-"
MINUS       MOV     @PSP+,W     ; 2
            SUB     TOS,W       ; 1
            MOV     W,TOS       ; 1
            mNEXT

;C AND    x1 x2 -- x3            logical AND
            FORTHWORD "AND"
ANDD        AND     @PSP+,TOS
            mNEXT

;C OR     x1 x2 -- x3           logical OR
            FORTHWORD "OR"
ORR         BIS     @PSP+,TOS
            mNEXT

;C XOR    x1 x2 -- x3            logical XOR
            FORTHWORD "XOR"
XORR        XOR     @PSP+,TOS
            mNEXT

;C NEGATE   x1 -- x2            two's complement
            FORTHWORD "NEGATE"
NEGATE      XOR     #-1,TOS
            ADD     #1,TOS
            mNEXT

;C ABS     n1 -- +n2     absolute value
            FORTHWORD "ABS"
ABBS:       CMP     #0,TOS       ; 1
            JN      NEGATE
            mNEXT

;C 1+      n1/u1 -- n2/u2       add 1 to TOS
            FORTHWORD "1+"
ONEPLUS     ADD     #1,TOS
            mNEXT

;C 1-      n1/u1 -- n2/u2     subtract 1 from TOS
            FORTHWORD "1-"
ONEMINUS    SUB     #1,TOS
            mNEXT

;C MAX    n1 n2 -- n3       signed maximum
            FORTHWORD "MAX"
MAX:        CMP     @PSP,TOS    ; n2-n1
            JL      SELn1       ; n2<n1
SELn2:      ADD     #2,PSP
            mNEXT

;C MIN    n1 n2 -- n3       signed minimum
            FORTHWORD "MIN"
MIN:        CMP     @PSP,TOS    ; n2-n1
            JL      SELn2       ; n2<n1
SELn1:      MOV     @PSP+,TOS
            mNEXT

; ----------------------------------------------------------------------
; COMPARAISON OPERATIONS
; ----------------------------------------------------------------------

;C 0=     n/u -- flag    return true if TOS=0
            FORTHWORD "0="
ZEROEQUAL   SUB     #1,TOS      ; borrow (clear cy) if TOS was 0
            SUBC    TOS,TOS     ; TOS=-1 if borrow was set
            mNEXT

;C 0<     n -- flag      true if TOS negative
            FORTHWORD "0<"
ZEROLESS    ADD     TOS,TOS     ; set carry if TOS negative
            SUBC    TOS,TOS     ; TOS=-1 if carry was clear
            XOR     #-1,TOS     ; TOS=-1 if carry was set
            mNEXT

;C =      x1 x2 -- flag         test x1=x2
            FORTHWORD "="
EQUAL:      SUB     @PSP+,TOS   ; 2
            JNZ     TOSFALSE    ; 2 --> +4
TOSTRUE:    MOV     #-1,TOS     ; 2 (MOV @R3+,TOS)
            mNEXT               ; 4

;C <      n1 n2 -- flag        test n1<n2, signed
            FORTHWORD "<"
LESS:       MOV     @PSP+,W     ; 2 W=n1
            SUB     TOS,W       ; 1 W=n1-n2 flags set
            JL      TOSTRUE     ; 2
TOSFALSE    MOV     #0,TOS      ; 1
            mNEXT               ; 4

;C >     n1 n2 -- flag         test n1>n2, signed
            FORTHWORD ">"
GREATER:    SUB     @PSP+,TOS   ; 2 TOS=n2-n1
            JL      TOSTRUE     ; 2
            MOV     #0,TOS      ; 1
            mNEXT               ; 4

;C U<    u1 u2 -- flag       test u1<u2, unsigned
            FORTHWORD "U<"
ULESS:      MOV     @PSP+,W     ; 2
            SUB     TOS,W       ; 1 u1-u2 in W, cy clear if borrow
            JNC     TOSTRUE     ; 2
            MOV     #0,TOS      ; 1
            mNEXT               ; 4

; ----------------------------------------------------------------------
; BRANCH and LOOP OPERATIONS
; ----------------------------------------------------------------------

;Z branch   --                  branch always
;            FORTHWORD "BRANCH"
BRAN        MOV     @IP,IP      ; 2
            mNEXT               ; 4

;Z ?branch   x --              branch if TOS = zero
;            FORTHWORD "?BRANCH"
QBRAN       CMP     #0,TOS      ; 1  test TOS value
            MOV     @PSP+,TOS   ; 2  pop new TOS value (doesn't change flags)
            JZ      bran        ; 2  if TOS was zero, take the branch = 11 cycles
            ADD     #2,IP       ; 1  else skip the branch destination
            mNEXT               ; 4  ==> branch not taken = 10 cycles

;;Z ?0branch   x --              branch if TOS <> zero
;            FORTHWORD "?0BRANCH"
QZBRAN      CMP     #0,TOS      ; 1  test TOS value
            MOV     @PSP+,TOS   ; 2  pop new TOS value (doesn't change flags)
            JNZ     bran        ; 2  if TOS was not zero, take the branch = 11 cycles
            ADD     #2,IP       ; 1  else skip the branch destination
            mNEXT                ; 4  ==> branch not taken = 10 cycles

;Z (do)    n1|u1 n2|u2 --  R: -- sys1 sys2      run-time code for DO
;                                               n1|u1=limit, n2|u2=index
;            FORTHWORD "(DO)"

xdo         MOV     #8000h,X        ;2 compute 8000h-limit "fudge factor"
            SUB     @PSP+,X         ;2
            MOV     TOS,Y           ;1 loop ctr = index+fudge
            MOV     @PSP+,TOS       ;2 pop new TOS
            ADD     X,Y             ;1
            .word 01519h            ;4 PUSHM X,Y, i.e. PUSHM LIMIT, INDEX
            mNEXT                   ;4

;Z (loop)   R: sys1 sys2 --  | sys1 sys2
;                        run-time code for LOOP
; Add 1 to the loop index.  If loop terminates, clean up the
; return stack and skip the branch.  Else take the inline branch.
; Note that LOOP terminates when index=8000h.
;            FORTHWORD "(LOOP)"

xloop       ADD     #1,0(RSP)   ;4 increment INDEX
            BIT     #100h,SR    ;2 is overflow bit set?
            JZ      bran        ;2 no overflow = loop
            ADD     #2,IP       ;1 overflow = loop done, skip branch ofs
            ADD     #4,RSP      ;1 empty RSP
            mNEXT               ;4
                                ;14~ taken or not taken loop

;Z (+loop)   n --   R: sys1 sys2 --  | sys1 sys2
;                        run-time code for +LOOP
; Add n to the loop index.  If loop terminates, clean up the
; return stack and skip the branch. Else take the inline branch.
;            FORTHWORD "(+LOOP)"

xploop      ADD     TOS,0(RSP)  ; increment INDEX by TOS value
            MOV     @PSP+,TOS   ; get new TOS, doesn't change flags
            BIT     #100h,SR    ; is overflow bit set?
            JZ      bran        ; no overflow = loop
            ADD     #2,IP       ; overflow = loop done, skip branch ofs
            ADD     #4,RSP      ; empty RSP
            mNEXT
                                ; 16~ taken or not taken loop

;C I        -- n   R: sys1 sys2 -- sys1 sys2
;C                  get the innermost loop index
            FORTHWORD "I"
II          SUB     #2,PSP          ; make room in TOS
            MOV     TOS,0(PSP)
            MOV     @RSP,TOS        ; index = loopctr - fudge
            SUB     2(RSP),TOS
            mNEXT

;C J        -- n   R: 4*sys -- 4*sys
;C                  get the second loop index
            FORTHWORD "J"
JJ          SUB     #2,PSP          ; make room in TOS
            MOV     TOS,0(PSP)
            MOV     4(RSP),TOS      ; index = loopctr - fudge
            SUB     6(RSP),TOS
            mNEXT

;C UNLOOP   --   R: sys1 sys2 --  drop loop parms
            FORTHWORD "UNLOOP"
UNLOOP      ADD     #4,RSP      ; empty RSP
            mNEXT

; ----------------------------------------------------------------------
; SYSTEM VARIABLES & CONSTANTS
; ----------------------------------------------------------------------

;C >IN     -- a-addr       holds offset in input stream
            FORTHWORD ">IN"
FTOIN       mDOCON
            .word   TOIN    ; VARIABLE address in INFO space

;C BASE    -- a-addr       holds conversion radix
            FORTHWORD "BASE"
FBASE       mDOCON
            .word   BASE    ; VARIABLE address in INFO space

;C STATE   -- a-addr       holds compiler state
            FORTHWORD "STATE"
FSTATE      mDOCON
            .word   STATE   ; VARIABLE address in INFO space

;C BL      -- char            an ASCII space
            FORTHWORD "BL"
FBLANK       mDOCON
            .word   32

; ----------------------------------------------------------------------
; MULTIPLY
; ----------------------------------------------------------------------

    .IFNDEF MPY ; if no hardware MPY

; T.I. SIGNED MULTIPLY SUBROUTINE: U1 x U2 -> Ud

;C UM*     u1 u2 -- ud   unsigned 16x16->32 mult.
            FORTHWORD "UM*"
UMSTAR      MOV @PSP,S      ; U1 = MULTIPLICANDlo
            MOV #0,W        ; 0 -> created MULTIPLICANDhi
            MOV #0,Y        ; 0 -> created RESULTlo
            MOV #0,T        ; 0 -> created RESULThi
            MOV #1,X        ; BIT TEST REGISTER
UMSTARLOOP  BIT X,TOS       ;1 TEST ACTUAL BIT MULTIPLIER
            JZ UMSTARNEXT   ;2 IF 0: DO NOTHING
            ADD S,Y         ;1 IF 1: ADD MULTIPLICAND TO RESULT
            ADDC W,T        ;1
UMSTARNEXT  ADD S,S         ;1 (RLA LSBs) MULTIPLICAND x 2
            ADDC W,W        ;1 (RLC MSBs)
            ADD X,X         ;1 (RLA) NEXT BIT TO TEST
            JNC UMSTARLOOP  ;2 IF BIT IN CARRY: FINISHED    10~ loop
            MOV Y,0(PSP)    ; low result on stack
            MOV T,TOS       ; high result in TOS
            mNEXT

    .ENDIF ; hardware MPY

; ----------------------------------------------------------------------
; ALIGNMENT OPERATORS OPTION
; ----------------------------------------------------------------------
    .IFDEF ALIGNMENT ; included in ANS_COMPLEMENT
    .include "ADDON\ALIGNMENT.asm"
    .ENDIF ; ALIGNMENT
; ----------------------------------------------------------------------
; PORTABILITY OPERATORS OPTION
; ----------------------------------------------------------------------
    .IFDEF PORTABILITY
    .include "ADDON\PORTABILITY.asm"
    .ENDIF ; PORTABILITY
; ----------------------------------------------------------------------
; ARITHMETIC OPERATORS OPTION
; ----------------------------------------------------------------------
    .IFDEF ARITHMETIC ; included in ANS_COMPLEMENT
    .include "ADDON\ARITHMETIC.asm"
    .ENDIF ; ARITHMETIC
; ----------------------------------------------------------------------
; DOUBLE OPERATORS OPTION
; ----------------------------------------------------------------------
    .IFDEF DOUBLE ; included in ANS_COMPLEMENT
    .include "ADDON\DOUBLE.asm"
    .ENDIF ; DOUBLE
; ----------------------------------------------------------------------
; ANS complement OPTION
; ----------------------------------------------------------------------
    .IFDEF ANS_CORE_COMPLIANT
    .include "ADDON\ANS_COMPLEMENT.asm"
    .ENDIF ; ANS_COMPLEMENT

; ----------------------------------------------------------------------
; NUMERIC OUTPUT
; ----------------------------------------------------------------------

; Numeric conversion is done last digit first, so
; the output buffer is built backwards in memory.

;C <#    --             begin numeric conversion (initialize Hold Pointer in PAD area)
            FORTHWORD "<#"
LESSNUM:    MOV     #PAD,&HP
            mNEXT

; unsigned 48-BIT DIVIDEND : 16-BIT DIVISOR --> 32-BIT QUOTIENT, 16-BIT REMAINDER
; DVDhi|DVDmid|DVDlo : DVR --> QUOThi|QUOTlo REMAINDER
; then REMAINDER is converted in ASCII char
; T     = DIVISOR
; S     = DVDlo
; TOS   = DVDmid
; W     = DVDhi
; IP    = count
; X     = QUOTlo
; Y     = QUOThi

;C #     ud1lo:ud1hi -- ud2lo:ud2hi          convert 1 digit of output
            FORTHWORD "#"
NUM         PUSH    IP          ;3
            MOV     &BASE,T     ;3  BASE is the DIVISOR 
            MOV     @PSP,S      ;2  S=DVDlo, TOS=DVDmid
            MOV     #0,W        ;1  W=DVDhi is created as 0
            MOV     #0,X        ;1 clear QUOTlo
            MOV     #0,Y        ;1 clear QUOThi
            MOV     #32,IP      ;3 INITIALIZE LOOP COUNTER
MDIV1:      CMP     T,W         ;1 dividendHI > divisor ?
            JNC     MDIV2       ;2 U<
            SUB     T,W         ;1 DVDhi - DVR
MDIV2:      ADDC    X,X         ;1 RLC quotLO
            ADDC    Y,Y         ;1 RLC quotHI
            SUB     #1,IP       ;1 Decrement loop counter
            JN      ENDMDIVIDE  ;2 If 0< --> end
            ADD     S,S         ;1 RLA DVDlo
            ADDC    TOS,TOS     ;1 RLC DVDmid
            ADDC    W,W         ;1 RLC DVDhi
            JNC     MDIV1       ;2                  14~ loop
            SUB     T,W         ;1 DVDhi - DVR
            BIS     #1,SR       ;1 SETC
            JMP     MDIV2       ;2                  14~ loop
ENDMDIVIDE  MOV     @RSP+,IP    ;2
            MOV     X,0(PSP)    ;3 QUOTlo in 0(PSP)
            MOV     Y,TOS       ;1 QUOThi in TOS
TODIGIT     CMP.B   #10,W       ;2 W = DVDhi = REMAINDER
            JLO     TODIGIT1    ;2 U<
            ADD     #7,W        ;2
TODIGIT1    ADD     #30h,W      ;2
HOLDW       SUB     #1,&HP      ;3 store W=char --> -[HP]
            MOV     &HP,Y       ;3
            MOV.B   W,0(Y)      ;3
            mNEXT               ;4 41 words 475 cycles/char = 60us/char @8MHz

;C #S    udlo:udhi -- udlo:udhi=0       convert remaining digits
            FORTHWORD "#S"
NUMS        mDOCOL
            .word   NUM         ;
NUMS1       FORTHtoASM          ;
            SUB     #2,IP       ;1      define NUM return
            CMP     #0,X        ;1      test udlo first
            JNZ     NUM         ;2
            CMP     #0,TOS      ;1      then udhi
            JNZ     NUM         ;2
            MOV     @RSP+,IP    ;2
            mNEXT               ;4

;C #>    udlo:udhi=0 -- c-addr u    end conversion, get string
            FORTHWORD "#>"
NUMGREATER: MOV     &HP,0(PSP)
            MOV     #PAD,TOS
            SUB     @PSP,TOS
            mNEXT

;C HOLD  char --        add char to output string
            FORTHWORD "HOLD"
HOLD:       MOV     TOS,W
            MOV     @PSP+,TOS
            JMP     HOLDW

;C SIGN  n --           add minus sign if n<0
            FORTHWORD "SIGN"
SIGN:       CMP     #0,TOS
            MOV     @PSP+,TOS
            MOV     #'-',W
            JN      HOLDW   ; 0<
            mNEXT

;C U.    u --           display u unsigned
            FORTHWORD "U."
UDOT:       mDOCOL
            .word   LESSNUM,lit,0,NUMS,NUMGREATER,TYPE
            .word   SPACE,EXIT

;C .     n --           display n signed
            FORTHWORD "."
DOT:        mDOCOL
            .word   LESSNUM,DUP,ABBS,lit,0,NUMS
            .word   ROT,SIGN,NUMGREATER,TYPE,SPACE,EXIT

; ----------------------------------------------------------------------
; DICTIONARY MANAGEMENT
; ----------------------------------------------------------------------

;C HERE    -- addr      returns dictionary ptr
            FORTHWORD "HERE"
HERE        SUB     #2,PSP
            MOV     TOS,0(PSP)
            MOV     &DDP,TOS
            mNEXT

;C ALLOT   n --         allocate n bytes in dict
            FORTHWORD "ALLOT"
ALLOT       ADD     TOS,&DDP
            MOV     @PSP+,TOS
            mNEXT

;C C,   char --        append char to dict
            FORTHWORD "C,"
CCOMMA      MOV     &DDP,W
            MOV.B   TOS,0(W)
            ADD     #1,&DDP
            MOV     @PSP+,TOS
            mNEXT

; ----------------------------------------------------------------------
; INPUT/OUTPUT 
; ----------------------------------------------------------------------

; (CR)     --               send CR to the output terminal (via EMIT)
            FORTHWORD "(CR)"
PARENCR     SUB     #2,PSP
            MOV     TOS,0(PSP)
            MOV     #0Dh,TOS
            JMP     EMIT

;C CR      --               send CR to the output device
            FORTHWORD "CR"
CR          MOV #PARENCR,PC


;C SPACE   --               output a space
            FORTHWORD "SPACE"
SPACE       SUB     #2,PSP
            MOV     TOS,0(PSP)
            MOV     #20h,TOS
            JMP     EMIT

;C SPACES   n --            output n spaces
            FORTHWORD "SPACES"
SPACES      CMP     #0,TOS
            JNZ     SPACES1
            MOV     @PSP+,TOS
            mNEXT
SPACES1     mDOCOL
            .word   lit,0,xdo
SPACESLOOP  .word   SPACE,xloop,SPACESLOOP
            .word   EXIT


;C TYPE    adr len --     type line to terminal

            FORTHWORD "TYPE"
TYPE        CMP     #0,TOS
            JZ      TWODROP
            MOV     @PSP,W
            ADD     TOS,0(PSP)
            MOV     W,TOS
            mDOCOL
            .word   xdo
TYPELOOP    .word   II,CFETCH,EMIT,xloop,TYPELOOP
            .word   EXIT


;Z (S")     -- addr u   run-time code for S"
; get address and length of string.
XSQUOTE:    SUB     #4,PSP          ; 1 -- x x TOS      ; push old TOS on stack
            MOV     TOS,2(PSP)      ; 3 -- TOS x x      ; and reserve one cell on stack
            MOV.B   @IP+,TOS        ; 2 -- x u          ; u = lenght of string
            MOV     IP,0(PSP)       ; 3 -- addr u
            ADD     TOS,IP          ; 1 -- addr u       IP=addr+u=addr(end_of_string)
            BIT     #1,IP           ; 1 -- addr u       IP=addr+u   Carry set/clear if odd/even
            ADDC    #0,IP           ; 1 -- addr u       IP=addr+u aligned
            mNEXT

;C S"       --             compile in-line string
            FORTHWORDIMM "S\34"        ; immediate
SQUOTE:     mDOCOL
            .word   lit,XSQUOTE,COMMA
            .word   lit,'"',WORDD
            .word   CFETCH,ONEMINUS ;   -2 bytes
            .word   ALLOT
            FORTHtoASM
            MOV     @RSP+,IP
CELLPLUSALIGN
            BIT     #1,&DDP   ; 3
            ADDC    #2,&DDP   ; 4       +2 bytes
            mNEXT

    .IFDEF LOWERCASE

;            FORTHWORD "CAPS_ON"
CAPS_ON     MOV     #-1,&CAPS       ; state by default
            mNEXT

;            FORTHWORD "CAPS_OFF"
CAPS_OFF    MOV     #0,&CAPS
            mNEXT

;C ."       --              compile string to print
            FORTHWORDIMM ".\34"        ; immediate
DOTQUOTE:   mDOCOL
            .word   CAPS_OFF
            .word   SQUOTE
            .word   CAPS_ON
            .word   lit,TYPE,COMMA,EXIT

    .ELSE

;C ."       --              compile string to print
            FORTHWORDIMM ".\34"        ; immediate
DOTQUOTE:   mDOCOL
            .word   SQUOTE
            .word   lit,TYPE,COMMA,EXIT

    .ENDIF ; LOWERCASE

; ------------------------------------------------------------------------------
; TERMINAL I/O, input part
; ------------------------------------------------------------------------------

;Z (KEY?)   -- c      get character from the terminal
;            FORTHWORD "(KEY?)"
PARENKEYTST SUB     #2,PSP              ; 1  push old TOS..
            MOV     TOS,0(PSP)          ; 4  ..onto stack
            CALL    #XON
KEYLOOP     BIT     #UCRXIFG,&TERMIFG   ; loop if bit0 = 0 in interupt flag register
            JZ      KEYLOOP             ;
            MOV     &TERMRXBUF,TOS      ;
            CALL    #XOFF               ;
            mNEXT

;F KEY?     -- c      get character from input device ; deferred word
            FORTHWORD "KEY?"
KEYTEST     MOV     #PARENKEYTST,PC


;Z (KEY)    -- c      get character from the terminal
;            FORTHWORD "(KEY)"
PARENKEY    MOV     &TERMRXBUF,Y        ; empty buffer
            JMP     PARENKEYTST

;C KEY      -- c      wait character from input device ; deferred word
            FORTHWORD "KEY"
KEY         MOV     #PARENKEY,PC

; ----------------------------------------------------------------------
; INTERPRETER INPUT, the kernel of kernel !
; ----------------------------------------------------------------------

    .IFDEF SD_CARD_LOADER       ; ACCEPT becomes a DEFERred word
    .include "SD_ACCEPT.asm"    ; that create SD_ACCEPT and (SD_ACCEPT) preamble
    .ENDIF

    .IFDEF TERMSD
    ASMWORD "XOFF"                      ; dont use as FORTH word
    .ENDIF
; ======================================;
XOFF                                    ; NOP11
; ======================================;
    .IFDEF TERMINALXONXOFF              ;
            MOV     #19,&TERMTXBUF      ; 4 move XOFF char into TX_buf
    .IF TERMINALBAUDRATE/FREQUENCY <230400
XOFF_LOOP   BIT     #UCTXIFG,&TERMIFG   ; 3 wait the sending end of previous char, best case if no wait!
            JZ      XOFF_LOOP           ; 2
    .ENDIF
    .ENDIF                              ;
    .IFDEF TERMINALCTSRTS               ;
            BIS.B   #RTS,&HANDSHAKOUT   ; 4 set RTS high
    .ENDIF                              ;
            RET
; --------------------------------------;

;C ACCEPT  addr addr len -- addr len'  get line at addr to interpret len' chars
            FORTHWORD "ACCEPT"
ACCEPT      MOV     #PARENACCEPT,PC

;C (ACCEPT)  addr addr len -- addr len'     get len' (up to len) chars from terminal (TERATERM.EXE) via USBtoUART bridge
            FORTHWORD "(ACCEPT)"
PARENACCEPT

; con speed of TERMINAL link, there are three bottlenecks :
; 1th the time between sending XON/RTS_low and clearing UCRXIFG on first char,
; 2th the char loop time,
; 3th the time between sending XON/RTS_low and sending XOFF/RTS_high on CR (CR+LF=EOL).
; everything must be done to reduce these times, taking into account the necessity of switching to Standby (LPMx mode). 
; --------------------------------------;
; (ACCEPT) part1 : prepare TERMINAL_INT ;
; --------------------------------------;
            PUSH    IP                  ;3
            PUSH    #ENDACCEPT          ;3      as XOFF RET address
            MOV     #AYEMIT_RET,IP      ;2      IP = return for YEMIT
            MOV     #20h,S              ;2      S = 'BL' to speed up char loop in part II
            MOV     #0Dh,T              ;2      T = 'CR' to speed up char loop in part II
            MOV     TOS,W               ;1      -- addr addr len    W=len
            MOV     @PSP,TOS            ;2      -- addr org ptr     W=len
            ADD     TOS,W               ;1      -- addr org ptr     W=Bound
            BIT     #UCRXIFG,&TERMIFG   ;3  RX_Int ?
            JZ      ACCEPTNEXT          ;2  no : case of FORTH init or a char previously kept by KEY or downloading
            MOV     &TERMRXBUF,Y        ;3  yes: possible case of downloading source file; UCRXIFG is cleared
            PUSH    #AKEYREAD1          ;3      preset AKEYREAD1 as XON RET address for case of char <> LF
            CMP     #0Ah,Y              ;2      RX_buf <> LF ?
            JNZ     XON                 ;2      yes: first char of new line is already in TERMRXBUF
            ADD     #2,RSP              ;1      no: remove previous XON RET address
ACCEPTNEXT  MOV     #LPMx_LOOP,X        ;2
            .word   154Dh               ;7      PUSHM IP,S,T,W and set LPMx_LOOP as XON RET address

    .IFDEF TERMSD
            JMP     XON
    ASMWORD "XON"                       ; dont use as FORTH word
    .ENDIF


; ======================================;
XON                                     ;
; ======================================;
    .IFDEF TERMINALXONXOFF              ;
            MOV     #17,&TERMTXBUF      ;4  move char XON into TX_buf
    .IF TERMINALBAUDRATE/FREQUENCY <230400
XON_LOOP    BIT     #UCTXIFG,&TERMIFG   ;3  wait the sending end of previous char, usefull only for low baudrates
            JZ      XON_LOOP            ;2
    .ENDIF
    .ENDIF                              ;
    .IFDEF TERMINALCTSRTS               ;
            BIC.B   #RTS,&HANDSHAKOUT   ;4  set RTS low
    .ENDIF                              ;
; vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv;
; starts first and 3th stopwatches      ;
; ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;
            RET                         ;4  to LPMx_LOOP or AKEYREAD1, see above
; --------------------------------------;



; ======================================;
LPMx_LOOP                               ; XON RET address 1 ; NOP100
; ======================================;
    BIS &LPM_MODE,SR                    ;3  and enter in LPMx sleep mode selected in LPM variable, with GIE=1
; --------------------------------------;   default mode : LPM0.


; ### #     # ####### ####### ######  ######  #     # ######  #######  #####     #     # ####### ######  #######
;  #  ##    #    #    #       #     # #     # #     # #     #    #    #     #    #     # #       #     # #
;  #  # #   #    #    #       #     # #     # #     # #     #    #    #          #     # #       #     # #
;  #  #  #  #    #    #####   ######  ######  #     # ######     #     #####     ####### #####   ######  #####
;  #  #   # #    #    #       #   #   #   #   #     # #          #          #    #     # #       #   #   #
;  #  #    ##    #    #       #    #  #    #  #     # #          #    #     #    #     # #       #    #  #
; ### #     #    #    ####### #     # #     #  #####  #          #     #####     #     # ####### #     # #######


; here, Fast FORTH sleeps, waiting any interrupt.
; IP,S,T,W,X,Y registers are free for any interrupt routine...
; ...and so the parameters stack with its rule of usage
; remember : in any interrupt routine you must include : BIC #0xB8,0(RSP) before RETI


; ======================================;
            JMP     LPMx_LOOP           ;2  and here is the return for any interrupts, else TERMINAL_INT  :-)
; ======================================;


; **************************************;
TERMINAL_INT                            ; <--- UCA0 RX interrupt vector, delayed by the LPMx wake up time
; **************************************;      if wake up time increases, max bauds rate decreases...
; (ACCEPT) part2 under interrupt        ; addr org ptr -- addr len'
; **************************************;
            ADD     #4,RSP              ;1  remove SR and PC from stack
TERM_INT1   .word   173Ah               ;6  POPM W=bound,T=0Dh,S=20h,IP=AYEMIT_RET
; vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv;
; starts the 2th stopwatch              ;
; ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;
AKEYREAD    MOV     &TERMRXBUF,Y        ;3  read character into Y, UCRXIFG is cleared
; vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv;
; stops the first stopwatch             ; first bottleneck result : 17~ + LPMx wake_up time ( + 5~ XON loop if F/Bds<230400 )
; ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;
AKEYREAD1                               ;   XON RET address 2
            CMP.B   T,Y                 ;1      char = CR ?
            JZ      XOFF                ;2      then RET to ENDACCEPT
            CMP.B   S,Y                 ;1      printable char ?
            JGE     ASTORETEST          ;2      yes
; vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv;
; stops the 3th stopwatch               ; 3th bottleneck best case result: 27~ + LPMx wake_up time
; ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;     ...or if first char of new line is already received : 14~
            CMP.B   #8,Y                ;       char = BS ?
            JNE     WAITaKEY            ;       no : do nothing for all other chars
; **************************************;
; start of backspace                    ;
; **************************************;
            CMP     @PSP,TOS            ;       Ptr = Org ?
            JZ      WAITaKEY            ;       yes: do nothing
            SUB     #1,TOS              ;       no : dec Ptr
    .IFDEF BACKSPACE_ERASE
; **************************************;
            MOV     #BS_NEXT,IP         ;
            JMP     YEMIT               ;       send BS
BS_NEXT     FORTHtoASM                  ;
            MOV     #32,Y               ;       send SPACE to rub previous char
            ADD     #8,IP               ;       (BS_NEXT+2) + 8 = FORTHtoASM @ !
            JMP     YEMIT               ;
            FORTHtoASM                  ;
            MOV.B   #8,Y                ;
            MOV     #AYEMIT_RET,IP      ;
; **************************************;
    .ENDIF
            JMP     YEMIT               ;       send BS
; **************************************;
; end of backspace                      ;
; **************************************;
ASTORETEST  CMP     W,TOS               ; 1 Bound is reached ? (protect against big lines without CR, UNIX like)
            JZ      YEMIT               ; 2 yes, send echo without store, then loopback
ASTORE      MOV.B   Y,0(TOS)            ; 3 no, store char @ Ptr before send echo, then loopback
            ADD     #1,TOS              ; 1     increment Ptr
YEMIT       .word   4882h               ; hi7/4~ lo:12/4~ send/send_not  echo to terminal
            .word   TERMTXBUF           ; 3 MOV Y,&TERMTXBUF
    .IF TERMINALBAUDRATE/FREQUENCY <230400
YEMIT1      BIT     #UCTXIFG,&TERMIFG   ; 3 wait the sending end of previous char (usefull for low baudrates)
            JZ      YEMIT1              ; 2
    .ENDIF
            mNEXT                       ; 4
; --------------------------------------;
AYEMIT_RET  FORTHtoASM                  ; 0     YEMII NEXT address; NOP9
            SUB     #2,IP               ; 1 set YEMIT NEXT address to AYEMIT_RET
WAITaKEY    BIT     #UCRXIFG,&TERMIFG   ; 3 new char in TERMRXBUF ?
            JNZ     AKEYREAD            ; 2 yes
; vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv;
; stops the 2th stopwatch               ; F/Bds >=230400 best case result: 29~/26~ (with/without echo) ==> 345/385 kBds/MHz...
; ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;
            JZ      WAITaKEY            ; 2 ...and if we add one WAITaKEY loop: 36~/33~ ==> 278/303 kBds/MHz, retained values.
; **************************************;
ENDACCEPT                               ; XOFF RET address
; **************************************;
            SUB     @PSP+,TOS           ; addr Org Ptr -- addr len'
            MOV     #LPM0+GIE,&LPM_MODE ; reset LPM_MODE to default mode LPM0
            MOV     @RSP+,IP            ; 2 and continue with INTERPRET with GIE=0.
                                        ; So FORTH machine is protected against any interrupt...
            mNEXT                       ; ...until next falling down to LPMx mode of (ACCEPT) part1,
; **************************************;    i.e. when the FORTH interpreter has no more to do.

; ------------------------------------------------------------------------------
; TERMINAL I/O, output part
; ------------------------------------------------------------------------------


;Z (EMIT)   c --    output character (byte) to the terminal
; hardware or software control on TX flow seems not necessary with UARTtoUSB bridges because
; they stop TX when their RX buffer is full. So no problem when the terminal input is echoed to output.
            FORTHWORD "(EMIT)"
PARENEMIT   MOV     TOS,Y               ; 1
            MOV     @PSP+,TOS           ; 2
    .IF TERMINALBAUDRATE/FREQUENCY >=230400
YEMIT1      BIT     #UCTXIFG,&TERMIFG   ; 3 wait the sending end of previous char (usefull for low baudrates)
            JZ      YEMIT1              ; 2
    .ENDIF
            JMP     YEMIT


;C EMIT     c --    output character to the output device ; deferred word
            FORTHWORD "EMIT"
EMIT        MOV     #PARENEMIT,PC       ; 3


;Z ECHO     --      connect console output (default)
            FORTHWORD "ECHO"
ECHO        MOV     #4882h,&YEMIT        ; 4882h = MOV Y,&<next_adr>
            mNEXT

;Z NOECHO   --      disconnect console output
            FORTHWORD "NOECHO"
NOECHO      MOV     #NEXT,&YEMIT        ;  NEXT = 4030h = MOV @IP+,PC
            mNEXT


; ----------------------------------------------------------------------
; INTERPRETER
; ----------------------------------------------------------------------

; find a word delimited by char ( begining usually at [TIB])
;C WORD   char -- addr        Z=1 if len=0
;                                   "word" is capitalized 
;                                   TOIN is the relative displacement into buffer
;                                   empty line = 23 cycles
            FORTHWORD "WORD"
WORDD       MOV     #TICKSOURCE,S   ;2 -- separator 
            MOV     @S+,X           ;2               X = buf_len
            MOV     @S+,W           ;2               W = buf_org
            ADD     W,X             ;1               W = buf_org X = buf_org + buf_len = buf_end
            ADD     @S+,W           ;2               W = buf_org + >IN = buf_ptr X = buf_end
            MOV     @S,Y            ;2 -- separator  W = buf_ptr X = buf_end Y = S = HERE = dest_addr
            ADD     #1,Y            ;1              first reserve one byte for word length
SKIPCHAR    CMP     W,X             ;1               buf_ptr = buf_end ?
            JZ      EOL_END         ;2 -- separator  if yes : End Of Line !
            CMP.B   @W+,TOS         ;               does char = separator ?
            JZ      SKIPCHAR        ; -- separator  if yes
SCANWORD
            SUB     #1,W            ;
            MOV     #96,T           ;               T = 96 = ascii(a)-1 (value test kept in register before SCANWORDLOOP)
SCANWORDLOOP                        ; -- separator  15/28 cycles loop for upper/lower case char...
            MOV.B   S,-1(Y)         ;3              ...don't write FORTH words in lowercase!
            CMP     W,X             ;1              buf_ptr = buf_end ?
            JZ      SCANWORDEND     ;2              if yes
            MOV.B   @W+,S           ;2              move char to HERE post incr
            CMP.B   S,TOS           ;1              does char = separator ?
            JZ      SCANWORDEND     ;2              if yes
            ADD     #1,Y            ;1
            CMP.B   S,T             ;1              char U< 'a' ?  ('a'-1 U>= char) this condition is tested at each loop
            JC      SCANWORDLOOP    ;2              if yes
    .IFDEF LOWERCASE                ;               enable lowercase strings
QCAPS       CMP     #0,&CAPS        ;3              CAPS is OFF ?
            JZ      SCANWORDLOOP    ;2              yes
    .ENDIF ; LOWERCASE              ;               here CAPS is ON
            CMP.B   #123,S          ;2              char U>= 'z'+1 ?
            JC      SCANWORDLOOP    ;2              if yes
            SUB.B   #32,S           ;2              convert lowercase char to uppercase
            JMP     SCANWORDLOOP    ;2

SCANWORDEND SUB     &TICKSOURCE+2,W ; -- separator  W=buf_ptr - buf_org = new >IN (first char separator next)
            MOV     W,&TOIN         ;               update >IN
            MOV.B   #'?',0(Y)       ;               add '?' to end of word i.e. ready to abort if word_not_found
EOL_END     MOV     &DDP,TOS        ;3 -- c-addr     Y--> char separator after word
            SUB     TOS,Y           ;1               Y=Word_Length+1
            SUB     #1,Y            ;1               Y=Word_Length
            MOV.B   Y,0(TOS)        ;3
            mNEXT                   ;4 -- c-addr     48 words      Z=1 <==> lenght=0 <==> EOL


;C FIND   c-addr -- c-addr 0   if not found ; Z=1
;C                  xt  1      if immediate
;C                  xt -1      if "normal"
; compare WORD at c-addr (HERE)  with each of words in each of threads in each of listed vocabularies in CONTEXT
; mismatch on word lenght : 31 cycles,
; mismatch on first char  : 38 cycles,
; word loop : 18 cycles on len + 7 cycles on first char,
; char loop : 10 cycles,
; end of find : 30 cycles.

            FORTHWORD "FIND"
FIND        PUSH    IP              ;3 -- c-addr
            SUB     #2,PSP          ;1 -- ???? c-addr     prepare one cell here because SUB not compatible with end signed test
            MOV     #CONTEXT,T      ;2 -- ???? c-addr
            MOV     TOS,S           ;1 -- ???? c-addr     S=c-addr

VOCLOOP     MOV     @T+,TOS         ;2 -- ???? VOC_PFA    Y=CTXT+2
            CMP     #0,TOS          ;1 -- ???? VOC_PFA      S=c-addr        no more vocabulary in CONTEXT ?
            JZ      FINDEND         ;2 -- ???? 0                            yes ==> exit (Z=1)

    .SWITCH THREADS
    .CASE   1
    .ELSECASE                       ;                       search thread add 6cycles  5words
MAKETHREAD  MOV.B   1(S),Y          ;3 -- ???? VOC_PFA0      Y=CHAR0
            AND.B #(THREADS-1)*2,Y  ;2 -- ???? VOC_PFA0      Y=thread offset
            ADD     Y,TOS           ;1 -- ???? VOC_PFAx
    .ENDCASE

WORDINIT    ADD     #2,TOS          ;1 -- ???? VOC_PFA+2
WORDLOOP    MOV     -2(TOS),TOS     ;3 -- ???? [VOC_PFA] first, then [LFA]
            CMP     #0,TOS          ;1 -- ???? NFA                   no more word in the thread ?
            JZ      VOCLOOP         ;2 -- ???? NFA                   yes ==> search if next voc in context
FINDWORD    MOV     TOS,W           ;1 -- ???? NFA          W= NFA
            MOV     S,X             ;1 -- ???? NFA   S=c-addr W=src X=c-addr
            MOV.B   @X,Y            ;2 -- ???? NFA          W=src X=dst Y=cnt
            MOV.B   @W+,IP          ;2 -- ???? NFA                              IP=char (count of chars first)
            AND.B   #7Fh,IP         ;2 -- ???? NFA                                     hide Immediate bit
LENCOMP     CMP.B   @X,IP           ;2 -- ???? NFA  compare lenght
            JNZ     WORDLOOP        ;2 -- ???? NFA                               
CHARCOMP    ADD     #1,X            ;1
            CMP.B   @W+,0(X)        ;4 -- ???? NFA  compare char
            JNZ     WORDLOOP        ;2              if <>
            SUB.B   #1,Y            ;1 -- ???? NFA  if =
            JNZ     CHARCOMP        ;2 -- ???? NFA                                   
WORDFOUND                           ;  -- ???? NFA    NFA of word found      
            MOV.B   @S,S            ;2 -- ???? NFA    S=count
            BIS.B   #1,S            ;1 -- ???? NFA    S=count odd
            ADD.B   #1,S            ;1 -- ???? NFA    S=count even
            ADD     TOS,S           ;1 -- ???? NFA    S=CFA
            MOV.B   @TOS,W          ;2 -- ???? NFA          W=NFA_first_char
            MOV     #1,TOS          ;1 -- ???? 1                            preset immediate flag
            BIT.B   #80h,W          ;2 -- ???? 1            W=immediate flag
            JNZ     FINDEND         ;2 -- ???? 1                            jmp if immediate
            SUB     #2,TOS          ;1 -- ????  -1|+1       S=xt 
FINDEND                             ;  -- ????  0|-1|+1     S=c-addr|xt
            MOV     S,0(PSP)        ;3 -- c-addr|xt  0|-1|+1
            MOV     @RSP+,IP        ;2 -- c-addr 0 if not found ; -- xt -1|+1 if found (not immediate|immediate)
            mNEXT                   ;4 46 words;        Z=1 <==> not found


THREEDROP   ADD     #2,PSP
TWODROP     ADD     #2,PSP
            MOV     @PSP+,TOS
            mNEXT

    .IFDEF MPY

;C >NUMBER  ud1lo|ud1hi addr1 count1 -- ud2lo|ud2hi addr2 count2
;C                      convert string to number

            FORTHWORD ">NUMBER"     ;               22 cycles + 36/42 cycles DEC/HEX char loop 
TONUMBER    CMP     #0,TOS          ;1 count = 0 ?
            JZ      TONUM3          ;2
            MOV     4(PSP),X        ;3                              X = ud1lo
            MOV     2(PSP),Y        ;3                              Y = ud1hi
            MOV     &BASE,T         ;3
TONUM1      MOV     @PSP,W          ;2 -- ud1lo ud1hi adr count     W=adr
            MOV.B   @W,W            ;2 -- ud1lo ud1hi adr count     W=char
DDIGITQ     SUB.B   #30h,W          ;2
            CMP.B   #0Ah,W          ;2                              char = ':' ?
            JNC     DDIGITQNEXT     ;2 -- ud1lo|ud1hi adr count     U<
            CMP.B   #11h,W          ;2                              char = 'A' ?
            JNC     TONUM3          ;2                              abort
            SUB.B   #07h,W          ;2                              W=digit
DDIGITQNEXT CMP     T,W             ;1 -- ud1lo|ud1hi adr count     digit-base
            JC      TONUM3          ;2                              abort
;UDMSTAR                            ;                               ud1*u2 -- ud2 
            MOV     X,&MPY32L       ;3 -- ud1lo ud1hi adr count     Load 1st operand (ud1lo)
            MOV     Y,&MPY32H       ;3 -- ud1lo ud1hi adr count     Load 1st operand (ud1hi)
            MOV     T,&OP2          ;3 -- ud1lo ud1hi adr count     Load 2nd operand with BASE
            MOV     &RES0,X         ;3 -- ud1lo ud1hi adr count     lo result in X (ud2lo)
            MOV     &RES1,Y         ;3 -- ud1lo ud1hi adr count     hi result in Y (ud2hi)

            ADD     W,X             ;1 -- ud1lo ud1hi adr count     ud2lo + digit
            ADDC    #0,Y            ;1 -- ud1lo ud1hi adr count     ud2hi + carry
            ADD     #1,0(PSP)       ;3 -- ud2lo ud2hi adr+1 count
            SUB     #1,TOS          ;1 -- ud2lo ud2hi adr+1 count-1
            JNZ     TONUM1          ;2
            MOV     Y,2(PSP)        ;3 -- ud1lo ud2hi adr count
            MOV     X,4(PSP)        ;3 -- ud2lo ud2hi adr count
TONUM3      mNEXT                   ; 45 words


    .ELSE

;C >NUMBER  ud1lo|ud1hi addr1 count1 -- ud2lo|ud2hi addr2 count2
;C                      convert string to number

            FORTHWORD ">NUMBER"
TONUMBER    CMP     #0,TOS          ; count = 0 ?
            JZ      TONUM3          ; no conversion
TONUM1      MOV     @PSP,S          ; -- ud1lo ud1hi adr count
            MOV.B   @S,S            ; -- ud1lo ud1hi adr count      S=char
DDIGITQ     SUB.B   #30h,S          ;
            CMP.B   #0Ah,S          ;                               char < ':' ?
            JNC     DDIGITQNEXT     ; -- ud1lo ud1hi adr count      U<
            CMP.B   #11h,S          ;                               char < 'A' ?
            JNC     TONUM3          ;                               U<
            SUB.B   #07h,S          ;                                   S=digit
DDIGITQNEXT CMP     &BASE,S         ; -- ud1lo ud1hi adr count          digit-base
            JC      TONUM3          ; U>=
;UDSTAR                             ; -- ud1lo ud1hi adr count
            .word   152Eh           ; -- ud1lo ud1hi adr count          PUSHM TOS,IP,S (2+1 push,TOS=Eh)
            SUB     #2,PSP          ; -- ud1lo ud1hi adr x count
            MOV     4(PSP),0(PSP)   ; -- ud1lo ud1hi adr ud1hi count
            MOV     &BASE,TOS       ; -- ud1lo ud1hi adr ud1hi u2=base
            MOV     #UMSTARNEXT1,IP ;
UMSTAR1     MOV     #UMSTAR,PC      ; ud1hi * base ; UMSTAR use S,T,W,X,Y
UMSTARNEXT1 FORTHtoASM              ; -- ud1lo ud1hi adr ud3lo ud3hi
            PUSH    @PSP            ;                                   r-- count ud3lo
            MOV     6(PSP),0(PSP)   ; -- ud1lo ud1hi adr ud1lo ud3hi
            MOV     &BASE,TOS       ; -- ud1lo ud1hi adr ud1lo u=base
            MOV     #UMSTARNEXT2,IP
UMSTAR2     MOV     #UMSTAR,PC      ; ud1lo * base ; UMSTAR use S,T,W,X,Y, and S is free for use
UMSTARNEXT2 FORTHtoASM              ; -- ud1lo ud1hi adr ud2lo ud2hi    r-- count IP digit ud3lo
            ADD     @RSP+,TOS       ; -- ud1lo ud1hi adr ud2lo ud2hi    r-- count IP digit       add ud3lo to ud2hi
;MPLUS
            ADD     @RSP+,0(PSP)    ; -- ud1lo ud1hi adr ud2lo ud2hi    Ud2lo + digit
            ADDC    #0,TOS          ; -- ud1lo ud1hi adr ud2lo ud2hi    ud2hi + carry
            MOV     @PSP,6(PSP)     ; -- ud2lo ud1hi adr ud2lo ud2hi
            MOV     TOS,4(PSP)      ; -- ud2lo ud2hi adr ud2lo ud2hi

            .word   171Dh           ; -- ud2lo ud2hi adr ud2lo count    POPM IP,TOS (1+1 pop,IP=D)
            ADD     #2,PSP          ; -- ud2lo ud2hi adr count
            ADD     #1,0(PSP)       ; -- ud2lo ud2hi adr+1 count
            SUB     #1,TOS          ; -- ud2lo ud2hi adr+1 count-1
            JNZ     TONUM1
TONUM3      mNEXT                   ; 55 words

    .ENDIF ; MPY

;Z ?NUMBER  c-addr -- n -1      string -> number convert ok ; Z=0
;Z                 -- c-addr 0  if convert ko ; Z=1
;            FORTHWORD "?NUMBER"
QNUMBER:    PUSH    #0              ;3 -- c-addr    R-- sign
            PUSH    IP              ;3              R-- sign IP
            MOV     #QTONUMNEXT,IP  ;3              return from >NUMBER
CCOUNT      SUB     #8,PSP          ; -- x x x x c-addr
            MOV     TOS,6(PSP)      ; -- c-addr x x x c-addr
            MOV     #0,4(PSP)
            MOV     #0,2(PSP)       ; -- c-addr ud x c-addr
            MOV     TOS,0(PSP)      ; -- c-addr ud c-addr c-addr
            ADD     #1,0(PSP)       ; -- c-addr ud c-addr+1 c-addr
            MOV.B   @TOS,TOS        ; -- c-addr ud adr count    
            MOV     @PSP,W          ;           W=c-adrr
            PUSH    &BASE           ;           R-- sign IP base  W=c-adrr 
            MOV.B   @W+,X           ;           X=char
QPREFIX     CMP.B   #'0',X          ;           char='0' ?
            JNZ     QQSIGN          ;           no prefix, use current base
            MOV.B   @W+,X
QEQUALGX    CMP.B   #'X',X          ;           char='X' : number start with 0x prefix
            JNE     QEQUALGB
EQUAL0x     MOV     #16,&BASE
            JMP     QPREFIXYES
QEQUALGB    CMP.B   #'B',X          ;           char='B' : number start with 0b prefix
            JNE     TONUMBER        ; -- c-addr ud adr count      other cases will cause error
EQUAL0b     MOV     #2,&BASE
QPREFIXYES  ADD     #2,0(PSP)       ;
            SUB     #2,TOS          ; -- c-addr ud adr+2 count-2
            JMP     TONUMBER        ; -- c-addr ud adr count
QQSIGN      CMP.B   #'-',X          ;                           X=first_char, W=adr
            JNZ     TONUMBER        ;
QQSIGNYES   MOV     #-1,4(RSP)      ;                           R-- sign IP BASE 
            ADD     #1,0(PSP)       ;
            SUB     #1,TOS          ; -- c-addr ud adr+1 count-1
            JMP     TONUMBER        ;      use S,T,W,X,Y

QTONUMNEXT: FORTHtoASM              ; -- c-addr ud adr count=0 if convert number ok else ko
            MOV     @RSP+,&BASE     ;                       R-- sign IP
            ADD     #4,PSP          ;
            CMP     #0,TOS          ; -- c-addr udlo count  R-- sign IP   n=0 ? conversion is ok ?
            .word   0171Dh          ; -- c-addr udlo sign   POPM IP,TOS; TOS = sign flag = {-1;0}
            JZ      QNUMOK          ; -- c-addr udlo sign   conversion OK
QNUMKO      ADD     #2,PSP          ; -- c-addr sign
            AND     #0,TOS          ; -- c-addr ff          TOS=0 and Z=1 ==> conversion ko 
            mNEXT

QNUMOK      MOV     @PSP+,0(PSP)    ; -- |n| sign           note : PSP is incremented before write back !!!
            XOR     #-1,TOS         ; -- |n| inv(sign)
            JNZ     QNUMEND         ;                       if jump : TOS=-1 and Z=0 ==> conversion ok
QNEGATE     XOR     #-1,0(PSP)      ; -- n-1 ff             else TOS=0
            ADD     #1,0(PSP)       ; -- n ff
            XOR     #-1,TOS         ; -- n tf               TOS=-1 and Z=0 ==> conversion ok        
QNUMEND     mNEXT                   ; 68 words


;C EXECUTE   i*x xt -- j*x   execute Forth word at 'xt'
            FORTHWORD "EXECUTE"
EXECUTE     MOV     TOS,W       ; 1 put word address into W
            MOV     @PSP+,TOS   ; 2 fetch new TOS
            MOV     W,PC        ; 3 fetch code address into PC
                                ; 6 = ITC - 1

;C ,    x --           append cell to dict
            FORTHWORD ","
COMMA       MOV     &DDP,W
            ADD     #2,&DDP
            MOV     TOS,0(W)
            MOV     @PSP+,TOS
            mNEXT

;C LITERAL  n --        append numeric literal if compiling state
            FORTHWORDIMM "LITERAL"      ; immediate
LITERAL     CMP     #0,&STATE
            JZ      LITERALEND
LITERAL1    MOV     &DDP,W
            ADD     #4,&DDP
            MOV     #lit,0(W)
            MOV     TOS,2(W)
            MOV     @PSP+,TOS
LITERALEND  mNEXT

;C COUNT   c-addr1 -- adr len   counted->adr/len
            FORTHWORD "COUNT"
COUNT:      SUB     #2,PSP      ;1
            ADD     #1,TOS      ;1
            MOV     TOS,0(PSP)  ;3
            MOV.B   -1(TOS),TOS ;3
            mNEXT

;C INTERPRET    i*x c-addr u -- j*x      interpret given buffer
; This is a common factor of EVALUATE and QUIT.
; ref. dpANS-6, 3.4 The Forth Text Interpreter
;   'SOURCE 2!  0 >IN !
;   BEGIN
;       BL WORD DUP C@                   ; -- c-addr fl
;   WHILE                                ; -- c-addr
;       FIND                             ; -- a -1|0|1
;       ?DUP IF                          ; -- xt -1|1    (normal|immediate)
;           STATE @ XOR                  ;
;           IF EXECUTE                   ; execute if immediate word
;           ELSE COMMA                   ; compile if not immediate word
;           THEN                         ;
;       ELSE                             ; -- addr  if word not found
;           ?NUMBER                      ; number conversion  ok ?
;           IF POSTPONE LITERAL          ; yes, leave number on stack or compile it as STATE is
;           ELSE                         ; if not a number
;                HERE CFETCH CHARPLUS    ; prepare error message
;                HERE CSTORE COUNT       ;
;                FQABORTYES              ; not found error : " xxxx?"
;           THEN
;       THEN
;   REPEAT DROP ;                        ; 45 words

            FORTHWORD "INTERPRET"
INTERPRET   MOV     TOS,&TICKSOURCE     ; -- c-addr u    buffer lentgh  ==> ticksource variable
            MOV     @PSP+,&TICKSOURCE+2 ; -- u           buffer address ==> ticksource+2 variable
            MOV     @PSP+,TOS           ; --
            MOV     #0,&TOIN            ;
            mDOCOL                      ;
INTLOOP     .word   FBLANK,WORDD        ; -- c-addr     Z = End Of Line reached
            FORTHtoASM                  ;          
            MOV     #INTFINDNEXT,IP     ;2              ddefine INTFINDNEXT as FIND return
            JNZ     FIND                ;2              if EOL not reached
            MOV     @PSP+,TOS           ; --            else EOL is reached
            MOV     @RSP+,IP            ; --
            mNEXT                       ;               return to QUIT on EOL

INTFINDNEXT FORTHtoASM                  ;               Z = not found
            MOV     TOS,W               ; -- c-addr fl  W = flag =(-1|0|+1)  as (normal|not_found|immediate)
            MOV     @PSP+,TOS           ; -- c-addr     
            MOV     #INTQNUMNEXT,IP     ;2 -- c-addr    define QNUMBER return
            JZ      QNUMBER             ;2 c-addr --    if not found search a number
            MOV     #INTLOOP,IP         ;2 -- c-addr    define (EXECUTE | COMMA) return
            XOR     &STATE,W            ;3 -- c-addr
            JZ      COMMA               ;2 c-addr --    if W xor STATE = 0 compile xt then loop back to INTLOOP
            JNZ     EXECUTE             ;2 c-addr --    if W xor STATE <> 0 execute then loop back to INTLOOP

INTQNUMNEXT FORTHtoASM                  ;               Z = not a number
            MOV     @PSP+,TOS           ;2 -- n|c-addr
            MOV     #INTLOOP,IP         ;2 -- n|c-addr  define LITERAL return
            JNZ     LITERAL             ;2 n --         execute LITERAL then loop back to INTLOOP
NotFoundExe ADD.B   #1,0(TOS)           ; -- c-addr     Not a number : add one to counted string to type "name?"
            MOV     #FQABORTYES,IP      ;               define COUNT return
            JMP     COUNT               ;2 c-addr --    39 words


            FORTHWORD "QUIT"
QUIT        MOV     #RSTACK,RSP
            MOV     #LSTACK,&LEAVEPTR
            MOV     #0,&STATE
            mDOCOL
QUIT1       .word   XSQUOTE
            .byte   4,13,"ok "           ; CR + system prompt
QUIT2       .word   TYPE
            .word   lit,TIB,DUP,lit,TIB_SIZE-2  ; -- StringOrg StringOrg len
            .word   ACCEPT                      ; -- StringOrg len'
QUIT3       .word   SPACE
            .word   INTERPRET
            .word   lit,PSTACK-2,SPFETCH,ULESS
            .word   XSQUOTE
            .byte   13,"stack empty !"
            .word   QABORT
            .word   FSTATE,FETCH
            .word   QBRAN,QUIT1
            .word   XSQUOTE
            .byte   4,13,"   "           ; CR + 3 spaces
            .word   BRAN,QUIT2

;C ABORT    i*x --   R: j*x --   clear stack & QUIT
            FORTHWORD "ABORT"
ABORT:      MOV     #PSTACK,PSP
            MOV     @PSP+,TOS
            JMP     QUIT

RefillUSBtime .equ int(frequency*2730) ; 2730*frequency ==> word size max value @ 24 MHz

;Z ?ABORT   f c-addr u --      abort & print msg
;            FORTHWORD "?ABORT"
QABORT      CMP #0,2(PSP)           ; -- f c-addr u         flag test
QABORTNO    JZ THREEDROP
QABORTYES
    .IFDEF MSP430ASSEMBLER
            MOV #0,&CLRBW1          ; reset all branch labels
            MOV #0,&CLRBW2
            MOV #0,&CLRBW3
            MOV #0,&CLRFW1
            MOV #0,&CLRFW2
            MOV #0,&CLRFW3
    .ENDIF
            MOV #INI_ACCEPT,X
            MOV @X+,&ACCEPT+2       ; restore default ACCEPT
            MOV @X+,&YEMIT          ; 4882h = MOV Y,&<next_adr> : set ECHO
            MOV @X+,&EMIT+2         ; restore default EMIT to send message to the console output
QABORTYESNOECHO                     ; -- c-addr u           <==== WARM jump here (so terminal can be disconnected in your app)
    .IFDEF SD_CARD_LOADER
            CALL #HandlesInit       ; reinit all handles for all types of errors, SD errors or not
    .ENDIF
QABORTTERM
    .IFDEF TERMINALXONXOFF          ;
BEFOREQAT   BIT #UCTXIFG,&TERMIFG   ; TX buffer empty ?
            JZ BEFOREQAT            ; no
            MOV #17,&TERMTXBUF      ; yes move XON char into TX_buf
    .ENDIF                          ;
    .IFDEF TERMINALCTSRTS           ;
        BIC.B   #RTS,&HANDSHAKOUT   ; set /RTS low (connected to /CTS pin of UARTtoUSB bridge)
    .ENDIF                          ;
QABORTLOOP  BIC #UCRXIFG,&TERMIFG   ; reset TERMIFG(UCRXIFG)
            MOV #RefillUSBtime,Y    ; 2730*28 = 75 ms
QABUSBLOOPJ                         ; 28~ loop : PL2303TA seems the slower USB device to refill its buffer.
            MOV #8,X                ; 1~
QABUSBLOOPI                         ; 3~ loop
            SUB #1,X                ; 1~
            JNZ QABUSBLOOPI         ; 2~
            SUB #1,Y                ; 1~
            JNZ QABUSBLOOPJ         ; 2~
            BIT #UCRXIFG,&TERMIFG   ; 4 new char in TERMXBUF ?
            JNZ QABORTLOOP          ; 2 yes, the input stream is still active
            mDOCOL
            .word   XSQUOTE         ; -- c-addr u c-addr1 u1
            .byte   4,1Bh,"[7m"     ;            
            .word   TYPE            ; -- c-addr u       set reverse video
            .word   TYPE            ; --                type abort message
            .word   XSQUOTE         ; -- c-addr2 u2
            .byte   4,1Bh,"[0m"
            .word   TYPE            ; --                set normal video
            .word   FORTH,ONLY      ; to quit assembler and so to abort any ASSEMBLER definitions
            .word   DEFINITIONS     ; reset CURRENT directory
    .IFDEF LOWERCASE
            .word   CAPS_ON         ;
    .ENDIF ; LOWERCASE
            .word   ABORT

    .IFDEF LOWERCASE

;C ABORT"  i*x flag -- i*x   R: j*x -- j*x  flag=0
;C         i*x flag --       R: j*x --      flag<>0
            FORTHWORDIMM "ABORT\34"        ; immediate
ABORTQUOTE  mDOCOL
            .word   CAPS_OFF,SQUOTE,CAPS_ON
            .word   lit,QABORT,COMMA
            .word   EXIT 

    .ELSE

;C ABORT"  i*x flag -- i*x   R: j*x -- j*x  flag=0
;C         i*x flag --       R: j*x --      flag<>0
            FORTHWORDIMM "ABORT\34"        ; immediate
ABORTQUOTE  mDOCOL
            .word   SQUOTE
            .word   lit,QABORT,COMMA
            .word   EXIT 

    .ENDIF ; LOWERCASE

NotFound    .word   NotFoundExe          ; in INTERPRET

;C '    -- xt           find word in dictionary
            FORTHWORD "'"
TICK        SUB     #2,PSP
            MOV     TOS,0(PSP)
            MOV     #32,TOS
SCAN        mDOCOL          ; separator -- xt
            .word   WORDD
            .word   FIND
            .word   QBRAN,NotFound
            .word   EXIT

; \         --      backslash
; everything up to the end of the current line is a comment.
            FORTHWORDIMM "\\"      ; immediate
BACKSLASH   MOV     &TICKSOURCE,&TOIN
            mNEXT

; ----------------------------------------------------------------------
; COMPILER
; ----------------------------------------------------------------------

; HEADER        create an header for a new word.
;               common code for VARIABLE,CONSTANT,CREATE,DEFER,:,CODE.

HEADER      mDOCOL
            .word   CELLPLUSALIGN   ;               ALIGN then make room for LFA
            .word   FBLANK,WORDD
            FORTHtoASM              ; -- HERE       HERE is the NFA of this new word
            MOV     TOS,Y
            MOV.B   @TOS+,W         ; -- xxx        W=Count_of_chars    Y=NFA
;            AND.B   #127,W          ; -- xxx        W=count <= count max
            BIS.B   #1,W            ; -- xxx        W=count odd
            ADD.B   #1,W            ; -- xxx        W=count even
            ADD     Y,W             ; -- xxx        W=Aligned_CFA
            MOV     &CURRENT,X      ; -- xxx        X=VOC_BODY of CURRENT    Y=NFA
    .SWITCH THREADS
    .CASE   1                       ;               nothing to do
    .ELSECASE                       ;               multithreading add 5~ 4words
            MOV.B   @TOS,TOS        ; -- xxx        TOS=first CHAR of new word
            AND #(THREADS-1)*2,TOS  ; -- xxx        TOS= Thread offset
            ADD     TOS,X           ; -- xxx        TOS= Thread   X=VOC_PFAx = thread x of VOC_PFA of CURRENT
    .ENDCASE
;            MOV &LAST_NFA,&LAST_OLD_NFA ;           for MARKER
            MOV     Y,&LAST_NFA     ; -- xxx        NFA --> LAST_NFA
            MOV     X,&LAST_THREAD  ; -- xxx        VOC_PFAx --> LAST_THREAD
            MOV     W,&LAST_CFA     ; -- xxx        HERE=CFA --> LAST_CFA
            MOV     @X,-2(Y)        ; -- xxx        [VOC_PFA]=Old_NFA --> [LFA]
            MOV     Y,0(X)          ; -- xxx        NFA --> [VOC_PFA]
            ADD     #4,W            ; -- xxx        make room for two words by default...
            MOV     W,&DDP          ; -- xxx
            MOV     @PSP+,TOS       ; --
FORTHtoRET  MOV     @RSP+,IP
            MOV     @RSP+,PC        ; 28 words, W is the new DDP value used by VARIABLE, CONSTANT, CREATE, DEFER and :
                                    ;           X is [LAST_THREAD]
                                    ;           Y is NFA

;C VARIABLE <name>       --                      define a Forth VARIABLE
            FORTHWORD "VARIABLE"
VARIABLE    CALL    #HEADER      ; --        W = DDP = CFA + 2 words
            MOV     #DOVAR,-4(W)
            mNEXT

;C CONSTANT <name>     n --                      define a Forth CONSTANT
            FORTHWORD "CONSTANT"
CONSTANT    CALL    #HEADER      ; --        W = DDP
            MOV     #DOCON,-4(W)           ; compile exec
            MOV     TOS,-2(W)              ; compile constant
            MOV     @PSP+,TOS
            mNEXT

;C CREATE <name>        --                      define a CONSTANT with its next address
; Execution: ( -- a-addr )      ; a-addr is the address of name's data field
;                               ; the execution semantics of name may be extended by using DOES>
            FORTHWORD "CREATE"
CREATE      CALL    #HEADER         ; --        W = DDP
            MOV     #DOCON,-4(W)    ;4 first CELL = DOCON
            MOV     W,-2(W)         ;3 second CELL = HERE
            mNEXT                   ;4

;C DOES>    --          set action for the latest CREATEd definition
            FORTHWORD "DOES>"
DOES        MOV     &LAST_CFA,W     ; W = CFA of latest CREATEd word that becomes a master word
            MOV     #DODOES,0(W)    ; remplace code of CFA (DOCON) by DODOES
            MOV     IP,2(W)         ; remplace parameter of PFA (HERE) by the address after DOES> as execution address
            MOV     @RSP+,IP        ; exit of the new created word
            mNEXT

;X DEFER <name>   --                ; create a word to be deferred
            FORTHWORD "DEFER"
            CALL    #HEADER
            MOV     #4030h,-4(W)    ;4 first CELL = MOV @PC+,PC = BR...
            MOV     PC,-2(W)        ;3 second CELL = mNEXT : created word does nothing by default
            mNEXT                   ;4

;X DEFER!       xt CFA_DEFER --     ; store xt to the address after DODEFER
DEFERSTORE  MOV     @PSP+,2(TOS)    ; -- CFA_DEFER          xt --> [CFA_DEFER+2]
            MOV     @PSP+,TOS       ; --
            mNEXT

;X IS <name>        xt --
; used as is :
; DEFER DISPLAY                         create a "do nothing" definition (2 CELLS) 
; inline command : ' U. IS DISPLAY      U. becomes the runtime of the word DISPLAY
; or in a definition : ... ['] U. IS DISPLAY ...
; EMIT, CR, ACCEPT and WARM are DEFERred words

            FORTHWORDIMM "IS"       ; immediate
IS          CMP     #0,&STATE
            JZ      IS_EXEC
IS_COMPILE  mDOCOL
            .word   BRACTICK             ; find the word, compile its CFA as literal  
            .word   lit,DEFERSTORE,COMMA ; compile DEFERSTORE
            .word   EXIT
IS_EXEC     mDOCOL
            .word   TICK,DEFERSTORE     ; find the word, leave its CFA on the stack and execute DEFERSTORE
            .word   EXIT

;C [        --      enter interpretative state
                FORTHWORDIMM "["      ; immediate
LEFTBRACKET     MOV     #0,&STATE
                mNEXT

;C ]        --      enter compiling state
                FORTHWORD "]"
RIGHTBRACKET    MOV     #-1,&STATE
                mNEXT

;;Z REVEAL   --      "reveal" latest definition if no stack mismatch
;            FORTHWORD "REVEAL"
REVEAL      CMP     PSP,&LAST_CSP   ; check actual SP with saved version
            JNZ     BAD_CSP
            MOV     &LAST_THREAD,X  ;
            MOV     &LAST_NFA,0(X)  ; LAST_NFA --> [LAST_THREAD]
            mNEXT
BAD_CSP     mDOCOL
            .word   XSQUOTE
            .byte   15,"stack mismatch!"
FQABORTYES  .word   QABORTYES

;C RECURSE  --      recurse to current definition (compile current definition)
            FORTHWORDIMM "RECURSE"  ; immediate
RECURSE     MOV     &DDP,X          ;
            MOV     &LAST_CFA,0(X)  ;
            ADD     #2,&DDP         ;
            mNEXT

    .SWITCH DTC
    .CASE 1

;C : <name>     --      begin a colon definition
            FORTHWORD ":"
            CALL    #HEADER
            MOV     #DOCOL1,-4(W)   ; compile CALL rDOCOL
            SUB     #2,&DDP
            MOV     #-1,&STATE      ; enter compiling state
;            JMP     HIDE

    .CASE 2

;C : <name>     --      begin a colon definition
            FORTHWORD ":"
            CALL    #HEADER
            MOV     #DOCOL1,-4(W)   ; compile PUSH IP       3~
            MOV     #DOCOL2,-2(W)   ; compile CALL rEXIT
            MOV     #-1,&STATE      ; enter compiling state
;            JMP     HIDE

    .CASE 3 ; inlined DOCOL

;C : <name>     --      begin a colon definition
            FORTHWORD ":"
            CALL    #HEADER
            MOV     #DOCOL1,-4(W)   ; compile PUSH IP       3~
            MOV     #DOCOL2,-2(W)   ; compile MOV PC,IP     1~
            MOV     #DOCOL3,0(W)    ; compile ADD #4,IP     1~
            MOV     #NEXT,+2(W)     ; compile MOV @IP+,PC   4~
            ADD     #4,&DDP
            MOV     #-1,&STATE      ; enter compiling state
;            JMP     HIDE

    .ENDCASE ; DTC

;Z HIDE     --      "hide" latest definition and save PSP
;            FORTHWORD "HIDE"
HIDE    ;    MOV     &LAST_THREAD,X  ;
        ;    MOV     &LAST_NFA,Y     ; DTC1 use W register
            MOV     -2(Y),0(X)      ; [LAST_LFA]=PREV_NFA --> [LAST_THREAD]
            MOV     PSP,&LAST_CSP   ;
            mNEXT

;C ;            --      end a colon definition
            FORTHWORDIMM ";"        ; immediate
SEMICOLON   CMP     #0,&STATE       ; interpret mode : semicolon becomes a comment separator
            JZ      BACKSLASH       ; tip: ; it's transparent to the preprocessor, so semicolon comments are kept in file.4th
            mDOCOL                  ; compile mode
            .word   lit,EXIT,COMMA
            .word   REVEAL,LEFTBRACKET,EXIT

;C IMMEDIATE        --   make last definition immediate
            FORTHWORD "IMMEDIATE"
IMMEDIATE   MOV     &LAST_NFA,W
            BIS.B   #80h,0(W)
            mNEXT

;C ['] <name>        --         find word & compile it as literal
            FORTHWORDIMM "[']"      ; immediate word, i.e. word executed also during compilation
BRACTICK    mDOCOL
            .word   TICK            ; get xt of <name>
            .word   lit,lit,COMMA   ; append LIT action
            .word   COMMA,EXIT      ; append xt literal

            FORTHWORDIMM "POSTPONE"      ; immediate
POSTPONE    mDOCOL
            .word   FBLANK,WORDD,FIND,QDUP
            .word   QBRAN,NotFound
            .word   ZEROLESS
            .word   QBRAN,POST1
            .word   lit,lit,COMMA,COMMA
            .word   lit,COMMA
POST1:      .word   COMMA,EXIT

; ----------------------------------------------------------------------
; CONTROL STRUCTURES
; ----------------------------------------------------------------------
; THEN and BEGIN compile nothing
; DO compile one word
; IF, ELSE, AGAIN, UNTIL, WHILE, REPEAT, LOOP & +LOOP compile two words
; LEAVE compile three words

;C IF       -- IFadr    initialize conditional forward branch
            FORTHWORDIMM "IF"       ; immediate
IFF         SUB     #2,PSP          ;
            MOV     TOS,0(PSP)      ;
            MOV     &DDP,TOS        ; -- HERE
            MOV     #QBRAN,0(TOS)   ; -- HERE
            ADD     #4,&DDP         ; compile two words
CELLPL      ADD     #2,TOS          ; -- HERE+2=IFadr
            mNEXT

;C ELSE     IFadr -- ELSEadr        resolve forward IF branch, leave ELSEadr on stack
            FORTHWORDIMM "ELSE"     ; immediate
ELSS        MOV     &DDP,W
            MOV     #bran,0(W)
            ADD     #4,W            ; W=HERE+4
            MOV     W,&DDP          ; compile two words
            MOV     W,0(TOS)        ; HERE+4 ==> [IFadr]
            SUB     #2,W            ; HERE+2
            MOV     W,TOS           ; -- ELSEadr
            mNEXT

;C THEN     IFadr --                resolve forward branch
            FORTHWORDIMM "THEN"     ; immediate
THEN        MOV     &DDP,0(TOS)     ; -- IFadr
            MOV     @PSP+,TOS       ; --
            mNEXT

;C BEGIN    -- BEGINadr             initialize backward branch
            FORTHWORDIMM "BEGIN"    ; immediate
BEGIN       MOV     #HERE,PC        ; BR HERE

;C UNTIL    BEGINadr --             resolve conditional backward branch
            FORTHWORDIMM "UNTIL"    ; immediate
UNTIL       MOV     #qbran,X
UNTIL1      MOV     &DDP,W          ; W = HERE
            ADD     #4,&DDP         ; compile two words
            MOV     X,0(W)          ; compile Bran or qbran at HERE
            MOV     TOS,2(W)        ; compile bakcward adr at HERE+2
            MOV     @PSP+,TOS
            mNEXT

;X AGAIN    BEGINadr --             resolve uncondionnal backward branch
            FORTHWORDIMM "AGAIN"    ; immediate
AGAIN       MOV     #bran,X
            JMP     UNTIL1

;C WHILE    BEGINadr -- WHILEadr BEGINadr
            FORTHWORDIMM "WHILE"    ; immediate
WHILE       mDOCOL
            .word   IFF,SWAP,EXIT

;C REPEAT   WHILEadr BEGINadr --     resolve WHILE loop
            FORTHWORDIMM "REPEAT"   ; immediate
REPEAT      mDOCOL
            .word   AGAIN,THEN,EXIT

;C DO       -- DOadr   L: -- 0
            FORTHWORDIMM "DO"       ; immediate
DO          SUB     #2,PSP          ;
            MOV     TOS,0(PSP)      ;
            MOV     &DDP,TOS        ; -- HERE
            MOV     #xdo,0(TOS)
            ADD     #2,TOS          ; -- HERE+2
            MOV     TOS,&DDP        ; compile one word
            ADD     #2,&LEAVEPTR    ; -- HERE+2     LEAVEPTR+2
            MOV     &LEAVEPTR,W     ;               
            MOV     #0,0(W)         ; -- HERE+2     L-- 0
            mNEXT

;C LOOP    DOadr --         L-- 0 a1 a2 .. aN
            FORTHWORDIMM "LOOP"     ; immediate
LOO         MOV     #xloop,X
ENDLOOP     MOV     &DDP,W
            ADD     #4,&DDP         ; compile two words
            MOV     X,0(W)          ; xloop --> HERE
            MOV     TOS,2(W)        ; DOadr --> HERE+2
; resolve all "leave" adr
LEAVELOOP   MOV     &LEAVEPTR,TOS   ; -- Adr of first LeaveStack cell
            SUB     #2,&LEAVEPTR    ; -- 
            MOV     @TOS,TOS        ; -- first LeaveStack value
            CMP     #0,TOS          ; -- = value left by DO ?
            JZ      ENDLOOPEND
            MOV     &DDP,0(TOS)     ; move adr after loop as UNLOOP adr
            JMP     LEAVELOOP
ENDLOOPEND  MOV     @PSP+,TOS
            mNEXT

;C +LOOP   adrs --   L: 0 a1 a2 .. aN --
            FORTHWORDIMM "+LOOP"    ; immediate
PLUSLOOP    MOV     #xploop,X
            JMP     ENDLOOP

;C LEAVE    --    L: -- adrs
            FORTHWORDIMM "LEAVE"    ; immediate
LEAV        MOV     &DDP,W          ; compile three words
            MOV     #UNLOOP,0(W)    ; [HERE] = UNLOOP
            MOV     #bran,2(W)      ; [HERE+2] = BRAN
            ADD     #6,&DDP         ; [HERE+4] = take word for AfterLOOPadr
            ADD     #2,&LEAVEPTR
            ADD     #4,W
            MOV     &LEAVEPTR,X
            MOV     W,0(X)          ; leave HERE+4 on LEAVEPTR stack
            mNEXT

;C MOVE    addr1 addr2 u --     smart move
;             VERSION FOR 1 ADDRESS UNIT = 1 CHAR
            FORTHWORD "MOVE"
MOVE        MOV     TOS,W       ; 1
            MOV     @PSP+,Y     ; dest adrs
            MOV     @PSP+,X     ; src adrs
            MOV     @PSP+,TOS   ; pop new TOS
            CMP     #0,W
            JZ      MOVE_X
            CMP     X,Y         ; Y-X ; dst - src
            JZ      MOVE_X      ; already made !
            JC      MOVEUP      ; U>= if dst > src
MOVEDOWN:   MOV.B   @X+,0(Y)    ; if X=src > Y=dst copy W bytes down
            ADD     #1,Y
            SUB     #1,W
            JNZ     MOVEDOWN
            mNEXT
MOVEUP      ADD     W,Y         ; start at end
            ADD     W,X
MOVUP1:     SUB     #1,X
            SUB     #1,Y
            MOV.B   @X,0(Y)     ; if X=src < Y=dst copy W bytes up
            SUB     #1,W
            JNZ     MOVUP1
MOVE_X:     mNEXT


; ----------------------------------------------------------------------
; WORDS SET for VOCABULARY, not ANS compliant
; ----------------------------------------------------------------------

;X VOCABULARY       -- create a vocabulary

    .IFDEF VOCABULARY_SET

            FORTHWORD "VOCABULARY"
VOCABULARY  mDOCOL
            .word   CREATE
    .SWITCH THREADS
    .CASE   1
            .word   lit,0,COMMA             ; will keep the NFA of the last word of the future created vocabularies
    .ELSECASE                               ; multithreading add 7 words
            .word   lit,THREADS,lit,0,xdo
VOCABULOOP  .word   lit,0,COMMA
            .word   xloop,VOCABULOOP
    .ENDCASE
            .word   HERE                    ; link via LASTVOC the future created vocabularies
            .word   LIT,LASTVOC,DUP
            .word   FETCH,COMMA             ; compile [LASTVOC] to HERE+
            .word   STORE                   ; store (HERE - CELL) to LASTVOC
            .word   DOES                    ; compile CFA and PFA for the future defined vocabulary

    .ENDIF ; VOCABULARY_SET

VOCDOES     .word   LIT,CONTEXT,STORE
            .word   EXIT

;X  FORTH    --                         ; set FORTH the first context vocabulary; FORTH is and must be the first vocabulary
    .IFDEF VOCABULARY_SET
            FORTHWORD "FORTH"
    .ENDIF ; VOCABULARY_SET
FORTH       mDODOES                     ; leave FORTH_BODY on the stack and run VOCDOES
            .word   VOCDOES
FORTH_BODY                              ; there is the structure created by VOCABULARY
            .word   lastforthword
    .SWITCH THREADS
    .CASE   2
            .word   lastforthword1
    .CASE   4
            .word   lastforthword1
            .word   lastforthword2
            .word   lastforthword3
    .CASE   8
            .word   lastforthword1
            .word   lastforthword2
            .word   lastforthword3
            .word   lastforthword4
            .word   lastforthword5
            .word   lastforthword6
            .word   lastforthword7
    .CASE   16
            .word   lastforthword1
            .word   lastforthword2
            .word   lastforthword3
            .word   lastforthword4
            .word   lastforthword5
            .word   lastforthword6
            .word   lastforthword7
            .word   lastforthword8
            .word   lastforthword9
            .word   lastforthword10
            .word   lastforthword11
            .word   lastforthword12
            .word   lastforthword13
            .word   lastforthword14
            .word   lastforthword15
    .ELSECASE
    .ENDCASE
            .word   voclink
voclink     .set    $-2

;X  ALSO    --                  make room to put a vocabulary as first in context
    .IFDEF VOCABULARY_SET
            FORTHWORD "ALSO"
    .ENDIF ; VOCABULARY_SET
ALSO        MOV     #12,W           ; -- 6 words
            MOV     #CONTEXT,X      ; X=src
            MOV     #CONTEXT+2,Y    ; Y=dst
            JMP     MOVEUP          ; src < dst

;X  PREVIOUS   --               pop last vocabulary out of context
    .IFDEF VOCABULARY_SET
            FORTHWORD "PREVIOUS"
    .ENDIF ; VOCABULARY_SET
PREVIOUS    MOV     #14,W           ; -- 7 words
            MOV     #CONTEXT+2,X    ; X=src
            MOV     #CONTEXT,Y      ; Y=dst
            JMP     MOVEDOWN        ; src > dst

;X ONLY     --      cut context list to access only first vocabulary, ex.: FORTH ONLY
    .IFDEF VOCABULARY_SET
            FORTHWORD "ONLY"
    .ENDIF ; VOCABULARY_SET
ONLY        MOV     #0,&CONTEXT+2
            mNEXT

;X DEFINITIONS  --      set last context vocabulary as entry for further defining words
    .IFDEF VOCABULARY_SET
            FORTHWORD "DEFINITIONS"
    .ENDIF ; VOCABULARY_SET
DEFINITIONS MOV     &CONTEXT,&CURRENT
            mNEXT

; ----------------------------------------------------------------------
; SD CARD FAT16 OPTIONS
; ----------------------------------------------------------------------
    .IFDEF SD_CARD_LOADER

SD_QABORTYES                    ; insert S as SD error to display it before QABORTYES
    SUB #4,PSP
    MOV TOS,2(PSP)
    MOV &BASE,0(PSP)
    MOV S,TOS
    MOV #10h,&BASE
    mDOCOL
    .word   UDOT
    .word   FBASE,STORE         ; restore base
    .word   QABORTYES

;Z S">HERE  addr u -- HERE            move in-line string to a counted string at HERE
SQUOTE2HERE MOV     @PSP+,X     ; X = src
            MOV     &DDP,Y      ; Y = dst = HERE
            MOV.B   TOS,W       ; W = count
            MOV     Y,TOS       ; -- HERE
            MOV.B   W,0(Y)      ; count at HERE
            ADD     #1,Y        ; inc dst
            JMP     MOVEDOWN    ;

    .include "DTCforthMSP430FRxxxxSD_FAT16.asm" ; SD primitives
    .include "DTCforthMSP430FRxxxxSD_LOAD.asm" ; SD LOAD fonctions
        .IFDEF SD_CARD_READ_WRITE
        .include "DTCforthMSP430FRxxxxSD_RW.asm" ; SD Read/Write fonctions
        .ENDIF
    .ENDIF ; SD_CARD_LOADER

; ----------------------------------------------------------------------
; ASSEMBLER OPTION
; ----------------------------------------------------------------------
    .IFDEF MSP430ASSEMBLER
    .include "DTCforthMSP430FRxxxxASM.asm"
    .ENDIF

; ----------------------------------------------------------------------
; UTILITY WORDS OPTION
; ----------------------------------------------------------------------
    .IFDEF UTILITY
    .include "ADDON\UTILITY.asm"
    .ENDIF ; UTILITY

; ----------------------------------------------------------------------
; POWER ON RESET AND INITIALIZATION
; ----------------------------------------------------------------------

;; CORE EXT  MARKER
;;( "<spaces>name" -- )
;;Skip leading space delimiters. Parse name delimited by a space. Create a definition for name
;;with the execution semantics defined below.

;;name Execution: ( -- )
;;Restore all dictionary allocation and search order pointers to the state they had just prior to the
;;definition of name. Remove the definition of name and all subsequent definitions. Restoration
;;of any structures still existing that could refer to deleted definitions or deallocated data space is
;;not necessarily provided. No other contextual information such as numeric base is affected

;   PFA+2 = old DDP
;   PFA+4 = old VOCLINK
;   PFA+6 = old last NFA
;            FORTHWORD "MARKER"
;MARKER      mDOCOL
;            .word   CREATE
;            FORTHtoASM
;            MOV     &DDP,W          ; W = DDP
;            ADD     #6,&DDP
;            MOV     &LAST_NFA,Y
;            SUB     #2,Y
;            MOV     Y,0(W)          ; LFA of created MARKER as DDP
;            MOV     &LASTVOC,2(W)   ; as VOCLINK
;            MOV     &LAST_OLD_NFA,4(W)  ; NFA before MARKER as last NFA
;            MOV     &LAST_CFA,X     ; X
;            MOV     #DODOES,0(X)    ; replace [CFA] by DODOES
;            MOV     #MARKER_DOES,2(X); replace [PFA] by MARKER_DOES
;            MOV     @RSP+,IP        ; exit of the new created word
;            mNEXT

    .SWITCH THREADS

    .CASE   1
MARKER_DOES
    .IFDEF VOCABULARY_SET
            .word   FORTH,ONLY,DEFINITIONS
    .ENDIF
            FORTHtoASM              ; -- BODY       IP is free
            MOV     @TOS+,&DDP      ; -- BODY+2     restore HERE
            MOV     @TOS+,W         ; -- BODY+4     W= old VOCLINK =VLK
            MOV     W,&LASTVOC      ; -- BODY+4     restore LASTVOC
            MOV     @TOS,TOS        ; -- old_LAST_NFA=OLN
MARKALLVOC  MOV     W,Y             ; -- OLN        W=VLK   Y=VLK
MRKWORDLOOP MOV     -2(Y),Y         ; -- OLN        W=VLK   Y=NFA
            CMP     Y,TOS           ; -- OLN        CMP = TOS-Y : OLN-NFA
            JNC     MRKWORDLOOP     ;               loop back if TOS<Y : OLN<NFA
            MOV     Y,-2(W)         ;               W=VLK   X=THD   Y=NFA   refresh thread with good NFA
            MOV     @W,W            ; -- OLN        W=[VLK] = next voclink
            CMP     #0,W            ; -- OLN        W=[VLK] = next voclink   end of vocs ?
            JNZ     MARKALLVOC      ; -- OLN        W=VLK                   no : loopback
MARKEREND   MOV     @PSP+,TOS
            ASMtoFORTH              ;
            .word   lit,0FF80h,HERE,MINUS,UDOT
            .word   XSQUOTE         ;
            .byte   11,"bytes free ";
            .word   TYPE            ;
            .word   EXIT

    .ELSECASE

MARKER_DOES 
    .IFDEF VOCABULARY_SET
            .word   FORTH,ONLY,DEFINITIONS
    .ENDIF
            FORTHtoASM              ; -- BODY       IP is free
            MOV     @TOS+,&DDP      ; -- BODY+2     restore HERE
            MOV     @TOS+,W         ; -- BODY+4     W= old VOCLINK =VLK
            MOV     W,&LASTVOC      ; -- BODY+4     restore LASTVOC
            MOV     @TOS,TOS        ; -- old_LAST_NFA=OLN
MARKALLVOC  MOV     #THREADS,IP     ; -- OLN        W=VLK
            MOV     W,X             ; -- OLN        W=VLK   X=VLK
MRKTHRDLOOP MOV     X,Y             ; -- OLN        W=VLK   X=VLK   Y=VLK
            SUB     #2,X            ; -- OLN        W=VLK   X=THD (thread ((case-2)to0))
MRKWORDLOOP MOV     -2(Y),Y         ; -- OLN        W=VLK   Y=NFA
            CMP     Y,TOS           ; -- OLN        CMP = TOS-Y : OLN-NFA
            JNC     MRKWORDLOOP     ;               loop back if TOS<Y : OLN<NFA
MARKTHREAD  MOV     Y,0(X)          ;               W=VLK   X=THD   Y=NFA   refresh thread with good NFA
            SUB     #1,IP           ; -- OLN        W=VLK   X=THD   Y=NFA   IP=CFT-1
            JNZ     MRKTHRDLOOP     ;                       loopback to search NFA in next thread (thread-1)
            MOV     @W,W            ; -- OLN        W=[VLK] = next voclink
            CMP     #0,W            ; -- OLN        W=[VLK] = next voclink   end of vocs ?
            JNZ     MARKALLVOC      ; -- OLN        W=VLK                   no : loopback
MARKEREND   MOV     @PSP+,TOS
            ASMtoFORTH              ;
            .word   lit,0FF80h,HERE,MINUS,UDOT
            .word   XSQUOTE         ;
            .byte   11,"bytes free ";
            .word   TYPE            ;
            .word   EXIT

    .ENDCASE ; THREADS

            FORTHWORD "PWR_STATE"   ; set dictionary in same state as OFF/ON
PWR_STATE   mDODOES                 ; DOES part of MARKER : resets pointers DP, voclink and latest
            .word   MARKER_DOES     ; execution vector of MARKER DOES
MARKDP      .word   ROMDICT         ; as DP value       )
MARKVOC     .word   lastvoclink     ; as voclink value  > may be updated by PWR_HERE, RST_STATE, RST_HERE or WIPE
MARKLAST    .word   latestword      ; as LAST_NFA value )

            FORTHWORD "PWR_HERE"    ; define dictionary bound for PWR_STATE
PWR_HERE    MOV     &DDP,&MARKDP
            MOV     &LASTVOC,&MARKVOC
            MOV     &LAST_NFA,&MARKLAST
            JMP     PWR_STATE

            FORTHWORD "RST_STATE"   ; set dictionary in same state as <reset>
RST_STATE   MOV     &INIDP,&MARKDP
            MOV     &INIVOC,&MARKVOC 
            MOV     &INILATEST,&MARKLAST
            JMP     PWR_STATE

            FORTHWORD "RST_HERE"    ; define dictionary bound for RST_STATE
RST_HERE    MOV     &DDP,&INIDP
            MOV     &LASTVOC,&INIVOC
            MOV     &LAST_NFA,&INILATEST
            JMP     PWR_HERE        ; and reset PWR_STATE same as RST_STATE

            FORTHWORD "WIPE"        ; restore the program as it was in FastForth.hex file
WIPE
; reset SBW and JTAG signatures
            MOV     #0,&JTAG_SIG1   ; unlock JTAG and SBW
            MOV     #0,&JTAG_SIG2   ;

; reset all FACTORY defered words else ACCEPT to allow execution from SD_Card
            MOV     #INI_YEMIT,X    ; see INFO section
            MOV     @X+,&YEMIT      ; connect output terminal (set ECHO)
            MOV     @X+,&EMIT+2
            MOV     @X+,&CR+2
            MOV     @X+,&WARM+2

    .SWITCH THREADS
; reset voclinks
    .CASE   16
            MOV     #lastforthword1,&FORTH_BODY+2
            MOV     #lastforthword2,&FORTH_BODY+4
            MOV     #lastforthword3,&FORTH_BODY+6
            MOV     #lastforthword4,&FORTH_BODY+8
            MOV     #lastforthword5,&FORTH_BODY+10
            MOV     #lastforthword6,&FORTH_BODY+12
            MOV     #lastforthword7,&FORTH_BODY+14
            MOV     #lastforthword8,&FORTH_BODY+16
            MOV     #lastforthword9,&FORTH_BODY+18
            MOV     #lastforthword10,&FORTH_BODY+20
            MOV     #lastforthword11,&FORTH_BODY+22
            MOV     #lastforthword12,&FORTH_BODY+24
            MOV     #lastforthword13,&FORTH_BODY+26
            MOV     #lastforthword14,&FORTH_BODY+28
            MOV     #lastforthword15,&FORTH_BODY+30
            .IFDEF MSP430ASSEMBLER
            MOV     #lastasmword1,&ASSEMBLER_BODY+2
            MOV     #lastasmword2,&ASSEMBLER_BODY+4
            MOV     #lastasmword3,&ASSEMBLER_BODY+6
            MOV     #lastasmword4,&ASSEMBLER_BODY+8
            MOV     #lastasmword5,&ASSEMBLER_BODY+10
            MOV     #lastasmword6,&ASSEMBLER_BODY+12
            MOV     #lastasmword7,&ASSEMBLER_BODY+14
            MOV     #lastasmword8,&ASSEMBLER_BODY+16
            MOV     #lastasmword9,&ASSEMBLER_BODY+18
            MOV     #lastasmword10,&ASSEMBLER_BODY+20
            MOV     #lastasmword11,&ASSEMBLER_BODY+22
            MOV     #lastasmword12,&ASSEMBLER_BODY+24
            MOV     #lastasmword13,&ASSEMBLER_BODY+26
            MOV     #lastasmword14,&ASSEMBLER_BODY+28
            MOV     #lastasmword15,&ASSEMBLER_BODY+30
            .ENDIF
    .CASE   8
            MOV     #lastforthword1,&FORTH_BODY+2
            MOV     #lastforthword2,&FORTH_BODY+4
            MOV     #lastforthword3,&FORTH_BODY+6
            MOV     #lastforthword4,&FORTH_BODY+8
            MOV     #lastforthword5,&FORTH_BODY+10
            MOV     #lastforthword6,&FORTH_BODY+12
            MOV     #lastforthword7,&FORTH_BODY+14
            .IFDEF MSP430ASSEMBLER
            MOV     #lastasmword1,&ASSEMBLER_BODY+2
            MOV     #lastasmword2,&ASSEMBLER_BODY+4
            MOV     #lastasmword3,&ASSEMBLER_BODY+6
            MOV     #lastasmword4,&ASSEMBLER_BODY+8
            MOV     #lastasmword5,&ASSEMBLER_BODY+10
            MOV     #lastasmword6,&ASSEMBLER_BODY+12
            MOV     #lastasmword7,&ASSEMBLER_BODY+14
            .ENDIF
    .CASE   4
            MOV     #lastforthword1,&FORTH_BODY+2
            MOV     #lastforthword2,&FORTH_BODY+4
            MOV     #lastforthword3,&FORTH_BODY+6
            .IFDEF MSP430ASSEMBLER
            MOV     #lastasmword1,&ASSEMBLER_BODY+2
            MOV     #lastasmword2,&ASSEMBLER_BODY+4
            MOV     #lastasmword3,&ASSEMBLER_BODY+6
            .ENDIF
    .CASE   2
            MOV     #lastforthword1,&FORTH_BODY+2
            .IFDEF MSP430ASSEMBLER
            MOV     #lastasmword1,&ASSEMBLER_BODY+2
            .ENDIF
    .ELSECASE
    .ENDCASE

; set voclinks, DP, LAST_NFA and LASTVOC with factory values
            MOV     #lastforthword,&FORTH_BODY
            .IFDEF MSP430ASSEMBLER
            MOV     #lastasmword,&ASSEMBLER_BODY
            .ENDIF
            MOV     #ROMDICT,&DDP
            MOV     #lastvoclink,&LASTVOC
            MOV     #latestword,&LAST_NFA

; then reinit RST_STATE and PWR_STATE
            JMP     RST_HERE



; define FREQ  used in WARM message (6)
    .IF     FREQUENCY = 0.5
FREQ    .set " .5MHz"
    .ELSEIF FREQUENCY = 1
FREQ    .set "  1MHz"
    .ELSEIF FREQUENCY = 2
FREQ    .set "  2MHz"
    .ELSEIF FREQUENCY = 4
FREQ    .set "  4MHz"
    .ELSEIF FREQUENCY = 8
FREQ    .set "  8MHz"
    .ELSEIF FREQUENCY = 16
FREQ    .set " 16MHz"
    .ELSEIF FREQUENCY = 24
FREQ    .set " 24MHz"
    .ENDIF

;Z (WARM)   --      ; init some user variables,
                    ; print start message if ECHO is set,
                    ; reset FORTH as CURRENT and CONTEXT vocabulary,
                    ; then ABORT
            FORTHWORD "(WARM)"
PARENWARM
            MOV     #10,&BASE
            MOV     &SAVE_SYSRSTIV,TOS  ; to display it
            MOV     #0,&SAVE_SYSRSTIV   ;
            mDOCOL
            .word   XSQUOTE             ;
            .byte   5,13,1Bh,"[7m"      ; CR + cmd "reverse video"
            .word   TYPE                ;
            .word   DOT                 ; display signed SAVE_SYSRSTIV
            .word   XSQUOTE
            .byte   43," FastForth V1.0",FREQ," (C) J.M.Thoorens 2016"
            .word   QABORTYESNOECHO     ; NOECHO enables any app to execute COLD without terminal connexion !


;Z WARM   --    ; deferred word used to init your application
                ; define this word:  : START ...init app here... LIT RECURSE IS WARM (WARM) ;
            FORTHWORD "WARM"
WARM        MOV     #PARENWARM,PC

;Z COLD     --      performs a software reset
            FORTHWORD "COLD"
COLD        MOV     #0A500h+PMMSWBOR,&PMMCTL0

; -------------------------------------------------------------------------
; in addition to <reset>, DEEP_RST restores the program as it was in the FastForth.hex file and the electronic fuse so.
; -------------------------------------------------------------------------
RESET
; -------------------------------------------------------------------------
; case 1  : Power ON ==> RESET + the volatile program beyond PWR_HERE (not protected by PWR_STATE against POWER OFF) is lost
;           SYSRSTIV = 2

; case 2 : <reset>  ==> RESET + the program beyond RST_HERE (not protected by RST_STATE against reset) is lost
;           SYSRSTIV = 4
; case 2.1 : software <reset> is performed by COLD.
;           SYSRSTIV = 6

; case 3 : TERM_TX wired to GND via 4k7 + <reset> ===> DEEP_RST, works even if the electronic fuse is "blown" !
; case 3.1 : (SYSRSTIV = 0Ah | SYSRSTIV >= 16h) ===> DEEP_RST on failure,
; case 3.2 : writing -1 in SAVE_SYSRSTIV then COLD ===> software DEEP_RST (WARM displays "-1")
; -------------------------------------------------------------------------

; -----------------------------------------------------------
; RESET : basic Target hardware Init, only for FORTH usage :  I/O, FRAM, UART, RTC
; -----------------------------------------------------------

    .include "TargetInit.inc"

; reset all interrupt vectors to RESET vector
            MOV     #RESET,W        ; W = reset vector
            MOV     #INTVECT,X      ; interrupt vectors base address
RESETINT:   MOV     W,0(X)
            ADD     #2,X
;            CMP     #0,X            ; 16 bits comparaison
            JNZ     RESETINT        ; endloop when X = 0

; reset default TERMINAL vector interrupt and LPM0 mode for terminal use
            MOV     &INI_TERM,&TERMVEC
            MOV     #CPUOFF+GIE,&LPM_MODE

; -----------------------------------------------------------
; RESET : INIT FORTH machine 
; -----------------------------------------------------------
            MOV     #RSTACK,SP              ; init return stack
            MOV     #PSTACK,PSP             ; init parameter stack
    .SWITCH DTC
    .CASE 1
            MOV     #xdocol,rDOCOL

    .CASE 2
            MOV     #EXIT,rEXIT

    .CASE 3 ; inlined DOCOL

    .ENDCASE
            MOV     #RFROM,rDOVAR
            MOV     #xdocon,rDOCON
            MOV     #xdodoes,rDODOES
            MOV     #INI_ACCEPT,X   ; see INFO section
            MOV     @X+,&ACCEPT+2   ; always restore default console input 
            ADD     #2,X
            MOV     @X+,&EMIT+2     ; always restore default console output
            MOV     @X+,&CR+2       ; and CR to CR EMIT

; -----------------------------------------------------------
; RESET : test TERM_TXD/Deep_RST before init TERM_UART  I/O
; -----------------------------------------------------------
    BIC #LOCKLPM5,&PM5CTL0          ; activate all previous I/O settings before DEEP_RST test
    MOV &SAVE_SYSRSTIV,Y            ;
    BIT.B #DEEP_RST,&Deep_RST_IN    ; TERM TXD wired to GND via 4k7 resistor ?
    JNZ TERM_INIT                   ; no
    XOR #-1,Y                       ; yes : force DEEP_RST
    ADD #1,Y                        ;       to display SAVE_SYSRSTIV as negative value
    MOV Y,&SAVE_SYSRSTIV

; ----------------------------------------------------------------------
; RESET : INIT TERM_UART
; ----------------------------------------------------------------------
TERM_INIT

    MOV     #0081h,&TERMCTLW0       ; Configure TERM_UART  UCLK = SMCLK

    .include "TERMINALBAUDRATE.inc" ; to configure baudrate


        BIS.B #TERM_TXRX,&TERM_SEL  ; Configure pins TXD & RXD for TERM_UART (PORTx_SEL0 xor PORTx_SEL1)
                                    ; TERM_DIR is controlled by eUSCI_Ax module
        BIC #UCSWRST,&TERMCTLW0     ; release from reset...
        BIS #UCRXIE,&TERMIE         ; ... then enable RX interrupt for wake up on terminal input

; -----------------------------------------------------------
; RESET : Select  POWER_ON|<reset>|DEEP_RST   
; -----------------------------------------------------------

    MOV #COLD_END,IP            ; define return for WIPE|RST_STATE|PWR_STATE
    CMP #0Ah,Y                  ; reset event = security violation BOR ???? not documented...
    JEQ WIPE                    ; Add WIPE to this reset to do DEEP_RST     --------------
    CMP #16h,Y                  ; reset event > software POR : failure or DEEP_RST request
    JHS WIPE                    ; U>= ; Add WIPE to this reset to do DEEP_RST
    CMP #2,Y                    ; reset event = Brownout ?
    JEQ PWR_STATE               ; yes   execute PWR_STATE
    JMP RST_STATE               ; else  execute RST_STATE

; ----------------------------------------------------------------------
; RESET : INIT SD_SPI
; ----------------------------------------------------------------------

    .IFNDEF  SD_CARD_LOADER
COLD_END    .word WARM              ; the next step
         
    .ELSEIF
COLD_END    FORTHtoASM              ; here we assume we are 1ms after brown out reset.
            BIT.B #SD_CD,&SD_CDIN   ; SD_memory in SD_Card module ?
            JNZ WARM                ; no
    .include "SD_INIT.asm"          ; to init eUSCI_SPI and I/O for each target and SPI_CLK = 333 kHz !
;            CMP #2,SAVE_SYSRSTIV    ; POWER ON ?
;            JNE SD_INIT             ; no
SD_POWER_ON MOV  #8,X               ; send 64 clk on SD_clk
            CALL #SPI_X_GET         ; use WX
SD_INIT     CALL #memCardInit       ; yes
            JMP WARM                ; the next step
    .ENDIF

; -----------------------------------------------------------
; IT'S FINISH : SET ASSEMBLY PTR   
; -----------------------------------------------------------

ROMDICT         ; init DDP
lastforthword   .equ forthlink
lastasmword     .equ asmlink
lastvoclink     .equ voclink
latestword      .equ lastword


    .IF THREADS <> 1

lastforthword1  .equ forthlink1
lastforthword2  .equ forthlink2
lastforthword3  .equ forthlink3
lastforthword4  .equ forthlink4
lastforthword5  .equ forthlink5
lastforthword6  .equ forthlink6
lastforthword7  .equ forthlink7
lastforthword8  .equ forthlink8
lastforthword9  .equ forthlink9
lastforthword10 .equ forthlink10
lastforthword11 .equ forthlink11
lastforthword12 .equ forthlink12
lastforthword13 .equ forthlink13
lastforthword14 .equ forthlink14
lastforthword15 .equ forthlink15

lastasmword1    .equ asmlink1
lastasmword2    .equ asmlink2
lastasmword3    .equ asmlink3
lastasmword4    .equ asmlink4
lastasmword5    .equ asmlink5
lastasmword6    .equ asmlink6
lastasmword7    .equ asmlink7
lastasmword8    .equ asmlink8
lastasmword9    .equ asmlink9
lastasmword10   .equ asmlink10
lastasmword11   .equ asmlink11
lastasmword12   .equ asmlink12
lastasmword13   .equ asmlink13
lastasmword14   .equ asmlink14
lastasmword15   .equ asmlink15

    .ENDIF
