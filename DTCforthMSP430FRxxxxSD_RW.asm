; -*- coding: utf-8 -*-
; DTCforthMSP430FR5xxxSD_RW.asm

; and only for FR5xxx and FR6xxx with RTC_B or RTC_C hardware if you want write file with date and time.

; Tested with MSP-EXP430FR5969 launchpad
; Copyright (C) <2015>  <J.M. THOORENS>
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.


; ======================================================================
; READ" primitive as part of OpenPathName
; input : S = OpenError, W = open_type, X = CurrentHdl
;         SectorL = DIRsector, Buffer = [DIRsector]
; output: nothing else abort on error
; ======================================================================

; ----------------------------------;
OPEN_QREAD                          ;
    CMP     #2,W                    ; open_type = READ" ?
    JNZ     OPEN_QWRITE             ; no : goto next step
; ----------------------------------;
OPEN_READ                           ;
; ----------------------------------;
    CMP     #0,S                    ; open file happy end ?
    JNE     OPEN_Error              ; no
    CALL    #LoadClusFirstSect      ;
    mNEXT                           ;
; ----------------------------------;

;-----------------------------------------------------------------------
; WRITE" (CREATE part) subroutines
;-----------------------------------------------------------------------

; Cluster up to 61475 with FAT16 2Go SD_Card.
; parse all FAT sectors until free cluster is found 
; this NewCluster is marked as the end's one (-1)
; then FATs are updated for NewCluster 

; input : CurFATsector, FAToffset
; use SWX registers
; output: new CurFATsector, new FAToffset, new BUFFER = [CurFATsector], NewCluster
;         SectorL is unchanged
;         S = 2 --> Disk FULL error
; ----------------------------------;
GetNewCluster                       ; <== CREATE file, WRITE_File
; ----------------------------------;
    MOV     #2,S                    ; preset return error
GNC_LoadFATsectorInBUF              ;
    MOV     &CurFATsector,W         ;
    MOV     #0,X                    ; X = FATsectorHI = 0
    CALL    #ReadSectorWX           ;SWX
    MOV     &FAToffset,X            ;
    RRA     X                       ; init X =  Cluster count up for a buffer content
GNC_SearchFreeClustInBUF            ;
    MOV     X,W                     ; W = 0,1,2,3,4,... = Cluster NumberLO
    ADD     X,W                     ; W = 0,2,4,6,8,... = buffer offset
    CMP     #0,BUFFER(W)            ; [Cluster] = 0 ?
    JZ      GNC_FreeClusterFound    ; in this FATsector
    ADD.B   #1,X                    ; increment Cluster count
    JNZ     GNC_SearchFreeClustInBUF; loopback while X < 256 words (and so W < 512 bytes) 
; ----------------------------------;
GNC_LoadNextFATsector               ;
; ----------------------------------;
    ADD     #1,&CurFATsector        ;
    CMP     &CurFATsector,&OrgFAT2  ;
    JZ      OPWC_DiskFull           ; ===> abort disk full
    MOV     #0,&FAToffset           ;
    JMP     GNC_LoadFATsectorInBUF  ; loopback
; ----------------------------------;
GNC_FreeClusterFound                ;
; ----------------------------------;
    MOV     #0,S                    ; clear error
    MOV     W,&FAToffset            ; save offset
    MOV     #-1,BUFFER(W)           ; mark NewCluster as end cluster (0xFFFF) in BUFFER
    MOV     &CurFATsector,W         ; W = FATsector where free cluster had found
    MOV     W,&FATsector            ; to update curFATsector below
    SUB     &OrgFAT1,W              ; W = sector offset in FAT1
    MOV.B   W,&NewCluster+1         ; W = NewCluster numberHI (byte)
    MOV.B   X,&NewCluster           ; X = NewCluster numberLO (byte)
    RET
; ----------------------------------;


; update FATs with BUFFER content.
; input : FATsector, FAToffset, BUFFER = [FATsector]
; use : SWX registers
; ----------------------------------; else update FATsector of the old cluster
UpdateFATs                          ;
; ----------------------------------;
    MOV     &FATsector,W            ; update FAT#1
    MOV     #0,X                    ;
    CALL    #WriteSectorWX          ; S,W,X
    MOV     &FATsector,W            ;
    ADD     &FATsize,W              ; update FAT#2
    MOV     #0,X                    ;
    MOV     #WriteSectorWX,PC       ; then ret
; ----------------------------------;



; FAT16 format for date and time in a DIR entry
; create time :     offset 0Dh = 0 to 200 centiseconds, not used.
;                   offset 0Eh = 0bhhhhhmmmmmmsssss, with : s=seconds*2, m=minutes, h=hours
; access time :     offset 14h = always 0, not used
; modified time :   ofsset 16h = 0bhhhhhmmmmmmsssss, with : s=seconds*2, m=minutes, h=hours
; dates :    offset 10, 12, 18 = 0byyyyyyymmmmddddd, with : y=year-1980, m=month, d=day

    .IFDEF    LF_XTAL
    .IFNDEF   RTC   ; no HMS, no DMY
; ----------------------------------; input:
GetYMDHMSforFAT                     ;T=date, W=TIME
; ----------------------------------;
    BIT.B   #RTCHOLD,&RTCCTL1       ; rtc is running ?
    JNZ     SD_RW_RETaddr           ; no
WaitRTC                             ; yes
    BIT.B   #RTCRDY,&RTCCTL1        ; rtc values are valid ?
    JZ      WaitRTC                 ; no
    MOV.B   &RTCSEC,W               ; yes
    RRA.B   W                       ; 2 seconds accuracy time
    MOV.B   &RTCDAY,T               ;
    MOV.B   #32,&MPY                ; common MPY for minutes and months
    MOV.B   &RTCMIN,&OP2            ;
    ADD     &RES0,W                 ;
    MOV.B   &RTCMON,&OP2            ;
    ADD     &RES0,T                 ;
    MOV.B   &RTCHOUR,&MPY           ;
    MOV     #2048,&OP2              ;
    ADD     &RES0,W                 ;
    MOV     &RTCYEAR,&MPY           ;
    SUB     #1980,&MPY              ;
    MOV     #512,&OP2               ;
    ADD     &RES0,T                 ;
SD_RW_RETaddr                       ;
    RET                             ;
; ----------------------------------;
    .ENDIF
    .ENDIF
; ----------------------------------;
OPWC_SkipDot                        ;
; ----------------------------------;
    CMP     #4,X                    ;
    JL      OPWC_WriteDIRentryName  ; X < 4 : no need spaces to complete name entry
    SUB     #3,X                    ;
    CALL    #OPWC_CompleteWithSpaces; complete name entry 
    MOV     #3,X                    ; 
; ----------------------------------;
OPWC_WriteDIRentryName              ;SWXY use
; ----------------------------------;
    MOV.B   @T+,W                   ;
    MOV.B   W,BUFFER(Y)             ;
    CMP     #0,W                    ; end of stringZ ?
    JZ      OPWC_CompleteWithSpaces ;
    SUB     #33,W                   ;
    JL      OPWC_WriteDIRentryName  ; skip char =< SPACE char
    SUB     #1,W                    ; 34
    JZ      OPWC_WriteDIRentryName  ; skip '"'
    SUB     #8,W                    ; 42
    JZ      OPWC_WriteDIRentryName  ; skip '*'
    SUB     #4,W                    ; 46
    JZ      OPWC_SkipDot            ; skip '.'
    SUB     #1,W                    ; 47
    JZ      OPWC_WriteDIRentryName  ; skip '/'
    SUB     #11,W                   ; 58
    JZ      OPWC_WriteDIRentryName  ; skip ':'
    SUB     #2,W                    ; 60
    JZ      OPWC_WriteDIRentryName  ; skip '<'
    SUB     #2,W                    ; 62
    JZ      OPWC_WriteDIRentryName  ; skip '>'
    SUB     #30,W                   ; 92
    JZ      OPWC_WriteDIRentryName  ; skip '\'
    SUB     #32,W                   ; 124
    JZ      OPWC_WriteDIRentryName  ; skip '|'
    ADD     #1,Y                    ; increment dst
    SUB     #1,X                    ; decrement count of chars entry
    JNZ     OPWC_WriteDIRentryName  ;
; ----------------------------------;
OPWC_CompleteWithSpaces             ; 0 to n spaces !
; ----------------------------------;
    CMP     #0,X                    ; 
    JZ      OPWC_CWS_End            ;
; ----------------------------------;
OPWC_CompleteWithSpaceloop          ;
; ----------------------------------;
    MOV.B   #32,BUFFER(Y)           ; remplace dot by char space
    ADD     #1,Y                    ; increment buffer offset 
    SUB     #1,X                    ; dec countdown of chars space
    JNZ OPWC_CompleteWithSpaceloop  ;
OPWC_CWS_End                        ;
    RET                             ;
; ----------------------------------;



; ======================================================================
; WRITE" primitive as part of OpenPathName
; input : S = OpenError, W = open_type, X = CurrentHdl
;         SectorL = DIRsector, Buffer = [DIRsector]
; output: nothing else abort on error
; error 1  : PathNameNotFound
; error 2  : DiskFull       
; error 4  : DirectoryFull  
; error 8  : AlreadyOpen    
; error 16 : NomoreHandle   
; ======================================================================

; ----------------------------------;
OPEN_QWRITE                         ;
    CMP     #4,W                    ; open_type = WRITE" ?
    JNZ     OPEN_QDEL               ; no : goto next step
; ----------------------------------;
; 1 try to open                     ; done
; ----------------------------------;
; 2 select error "no such file"     ;
; ----------------------------------;
    CMP     #2,S                    ; "no such file" error ?
    JZ      OPEN_WRITE_CREATE       ; yes
    CMP     #0,S                    ; no open file" error ?
    JZ      OPEN_WRITE_APPEND       ; yes
; ----------------------------------;
; Write errors                      ;
; ----------------------------------;
OPWC_InvalidPathname                ; S = 1
OPWC_DiskFull                       ; S = 2 
OPWC_DirectoryFull                  ; S = 4
OPWC_AlreadyOpen                    ; S = 8
OPWC_NomoreHandle                   ; S = 16
;OPWW_BadCluster                    ;
; ----------------------------------;
OPW_Error                           ;
    mDOCOL                          ; 
    .word   XSQUOTE                 ;
    .byte   12,"< WriteError",0     ;
    .word   SD_QABORTYES            ; to insert S error as flag, no return
; ----------------------------------;


; ======================================================================
; WRITE" (CREATE part) primitive as part of OpenPathName
; input : S = OpenError, W = open_type, X = CurrentHdl
;         HDLW_FirstClus = HDLW_CurClust
;         CurFATsector = OrgFAT1
;         SectorL = DIRsector, Buffer = [DIRsector]
; output: nothing else abort on error
; error 1  : InvalidPathname
; error 2  : DiskFull       
; error 4  : DirectoryFull  
; error 8  : AlreadyOpen    
; error 16 : NomoreHandle   
; ======================================================================

; ----------------------------------;
OPEN_WRITE_CREATE                   ;
; ----------------------------------;
; 3 get free cluster                ;
; ----------------------------------; input: CurFATsector
    MOV     #0,&FAToffset           ; and FAToffset=0
    CALL    #GetNewCluster          ; output updated CurFATsector=FATsector,NexCluster
    CALL    #UpdateFATs             ; from FATsector
    MOV     &NewCluster,&Cluster    ;
; ----------------------------------;
; 4 create DIRentryName             ; SectorL is unchanged (DIRsector) 
; ----------------------------------;
    CALL    #ReadSectorL            ; reload DIRsector
; ----------------------------------;
    MOV     #1,S                    ; write  pathname error
    MOV     &Pathname,T             ; here, pathname is "xxxxxxxx.yyy" format
OPWC_FirstcharNameTest              ;
    CMP.B   #0,0(T)                 ; forbidden null string
    JZ      OPWC_InvalidPathname    ; write error 1
    CMP.B   #'.',0(T)               ; forbidden "." in first
    JZ      OPWC_InvalidPathname    ; write error 1
    MOV     #11,X                   ; X=countdown of chars entry
    MOV     &EntryOfst,Y            ;
    CALL    #OPWC_WriteDIRentryName ; in buffer ; use W X Y registers
; ----------------------------------;
; 5 init DIRentryAttributes         ;
; ----------------------------------;
OPWC_SetEntryAttribute              ; (cluster=DIRcluster!)
    MOV     &EntryOfst,Y            ; reset entry offset
    MOV.B   #20h,BUFFER+11(Y)       ; file attribute = file
    .IFDEF LF_XTAL                  ; 
    .IFNDEF   RTC   ; no HMS, no DMY
    CALL    #GetYMDHMSforFAT        ;
    .ELSEIF
    MOV     #0,T                    ; T=DATE 
    MOV     #0,W                    ; W=TIME
    .ENDIF
    .ENDIF                          ;
    MOV     #0,BUFFER+12(Y)         ; nt reserved = 0 and centiseconds are 0
    MOV     W,BUFFER+14(Y)          ; time of creation
    MOV     T,BUFFER+16(Y)          ; date of creation      20/08/2001
    MOV     T,BUFFER+18(Y)          ; date of access        20/08/2001
    MOV     #0,BUFFER+20(Y)         ; Time of access = 0
;    MOV     W,BUFFER+22(Y)          ; Time of modification
;    MOV     T,BUFFER+24(Y)          ; date of modification  20/08/2001
    MOV     &Cluster,BUFFER+26(Y)   ; as first cluster    
    MOV     #0,BUFFER+28(Y)         ; file lenghtLO  = 0 
    MOV     #0,BUFFER+30(Y)         ; file lenghtHI  = 0 
; ----------------------------------;
; 6 save DIRsector                  ;
; ----------------------------------;
    CALL    #WriteSectorL           ;S,W,X update DIRsector
; ----------------------------------;
; 7 Get free handle                 ;
; ----------------------------------; input : Cluster as FirstCluster, EntryOfst
    CALL    #GetFreeHandle          ; init handle(HDLL_DIRsect,HDLW_DIRofst,HDLW_FirstClus = HDLW_CurClust,HDLL_CurSize)
; ----------------------------------; output : X = CurrentHdl*, handle errors if any
    CMP     #0,S                    ; no error ?
    JNZ     OPWC_NomoreHandle       ; write error 16
    MOV.B   #2,HDLB_Token(X)        ; marks it as write and clear cluster_sectors_offset
    MOV     #0,HDLW_BUFofst(X)      ; init at 0 the first_free_byte buffer pointer
    MOV     #0,&BufferPtr           ;
    CALL    #ComputeClusFrstSect    ; set SectorL as first sector of file
; ----------------------------------;
; 8- Defer EMIT to SD_EMIT          ;
; ----------------------------------;
    JMP     OPW_DeferEmit           ; goto end of WRITE" part, to change behaviour of EMIT
; ----------------------------------;

;-----------------------------------------------------------------------
; WRITE" subroutines
;-----------------------------------------------------------------------

; ----------------------------------; 
OPWW_WriteBuffer                    ;SWXY this subroutine is called by Write_File and CLOSE
; ----------------------------------;
    MOV     &CurrentHdl,X           ;
OPWW_UpdateHandleSize               ; update handle with CurSizeL and Buffer_offset only, don't update HDLB_ClustOfst
    MOV     &BufferPtr,W            ; position of last written byte
    SUB     HDLW_BUFofst(X),W       ; position of first written byte
    ADD     W,HDLL_CurSize(X)       ; update handle CurrentSizeL
    ADDC    #0,HDLL_CurSize+2(X)    ;
    MOV     #0,HDLW_BUFofst(X)      ; reset buffer start offset  
    MOV     #0,&BufferPtr           ; reset buffer pointer
; ==================================;
WriteSectorL
; ==================================;
    MOV     &SectorL,W              ; Low
    MOV     &SectorL+2,X            ; High
    MOV     #WriteSectorWX,PC        ; ...then RET
; ----------------------------------;

; SectorL is unchanged
; ----------------------------------;
OPWW_UpdateDirectory                ;
; ----------------------------------; Input : current Handle
    MOV     &CurrentHdl,X           ; update  CurSize in DIR entry then write DIRsector
    MOV     HDLL_DIRsect(X),W       ;
    MOV     HDLL_DIRsect+2(X),X     ;
    CALL    #readSectorWX           ; buffer = DIRsector
    .IFDEF LF_XTAL                  ; 
    .IFNDEF   RTC                   ; no HMS, no DMY
    CALL    #GetYMDHMSforFAT        ;
    .ELSEIF
    MOV     #0,T                    ; T=DATE 
    MOV     #0,W                    ; W=TIME
    .ENDIF
    .ENDIF                          ;
    MOV     &CurrentHdl,X           ;
    MOV     HDLW_DIRofst(X),Y       ; Y = DirEntryOffset
    MOV     T,BUFFER+18(Y)          ; access date
;    MOV     #0,BUFFER+20(Y)        ; Time of access always = 0
    MOV     W,BUFFER+22(Y)          ; modified time
    MOV     T,BUFFER+24(Y)          ; modified date
OPWW_UpdateEntryFileSize            ;
    MOV HDLL_CurSize(X),BUFFER+28(Y); save new filesize
    MOV HDLL_CurSize+2(X),BUFFER+30(Y);
    MOV     HDLL_DIRsect(X),W          ; 
    MOV     HDLL_DIRsect+2(X),X        ;
    MOV     #WriteSectorWX,PC       ;S,W,X then RET
; ----------------------------------;


; ==================================; 
Write_File                          ; this subroutine is called by the user SD_EMIT when BUFFER is full;
; ==================================; the first time, SectorL must be initialized  
    CALL    #OPWW_WriteBuffer       ; update handle and write BUFFER
    MOV     &CurrentHdl,X           ;
    ADD.B   #1,HDLB_ClustOfst(X)    ; increment current Cluster offset
    CMP.B &SecPerClus,HDLB_ClustOfst(X) ; if current Cluster = SecPerClus
    JZ      OPWW_GotoNewCluster     ;
    ADD     #1,&SectorL             ;
    ADDC    #0,&SectorL+2           ; it's the new Sector to be written
    RET                             ;
; ----------------------------------;

; ----------------------------------;
OPWW_GotoNewCluster                 ; input : Cluster
; ----------------------------------;
    PUSH    &FAToffset
    PUSH    &CurFATsector           ; push old curFATsector
    CALL    #GetNewCluster          ; output: updated CurFATsector=FATsector,NexCluster
    CMP     @RSP+,&curFATsector     ;
    JZ      OPWW_LinkCluster        ; if old and new clusters are in same FATsector
    CALL    #UpdateFATS             ; save new cluster FATsector BUFFER before loading old Cluster FATsector BUFFER
    CALL    #FindClusterFATsector   ; find (old) Cluster FATsector; BUFFER offset in FAToffset, FATsector in FATsector
    MOV     #0,X                    ;
    CALL    #ReadSectorWX           ; and reload it
OPWW_LinkCluster
    MOV     @RSP+,Y                 ;
    MOV     &NewCluster,BUFFER(Y)   ; link clusters in old FATsector buffer
    CALL    #UpdateFATs             ; update FATsector of old Cluster
;    CALL    #OPWW_UpdateDirectory   ; update DIRentry 
    MOV     &NewCluster,&Cluster    ; update old Cluster
    CALL    #FindClusterFATSector   ; find new Cluster FATsector; BUFFER offset in FAToffset, FATsector in FATsector
    MOV     &CurrentHdl,X           ;
    MOV     &Cluster,HDLW_CurClust(X)  ;
    MOV.B   #0,HDLB_ClustOfst(X)    ; reset ClusterOffset
    MOV     #ComputeClusFrstSect,PC ; update SectorL then RET
; ----------------------------------;



; ======================================================================
; WRITE" (APPEND part) primitive as part of OpenPathName
; input : S = OpenError, W = open_type, X = CurrentHdl
;         HDLW_FirstClus <> HDLW_CurClust
;         SectorL = DIRsector, Buffer = [DIRsector]
; output: nothing else abort on error
; error 2  : DiskFull       
; ======================================================================

;Z SD_EMIT  c --    output char c to a SD_CARD file
; ----------------------------------;
    FORTHWORD "SD_EMIT"             ; c --
; ----------------------------------;
SD_EMIT                             ;
    CMP     #BytsPerSec,&BufferPtr  ; 4 file buffer is full ?
    JNC     SD_EmitNext             ; 2 no (u<)
    CALL    #Write_File             ;VWXY
SD_EmitNext                         ;
    MOV     &BufferPtr,X            ; 
    MOV.B   TOS,BUFFER(X)           ; 3
    ADD     #1,&BufferPtr           ; 4
    MOV     @PSP+,TOS               ; 2
    mNEXT                           ; 4
; ----------------------------------; 19~ for SD_EMIT, 22~ for EMIT


; ----------------------------------;
OPEN_WRITE_APPEND                   ;
; ----------------------------------;
; 1- open file                      ; done
; ----------------------------------;
; 2- mark handle as write           ;
; ----------------------------------;
    MOV.B   #2,HDLB_Token(X)        ; 
; ----------------------------------;
; 3- compute Handle infos           ;
; ----------------------------------;
; 3.1- Compute Current cluster      ; from first cluster
; ----------------------------------;
    MOV  HDLW_FirstClus(X),&Cluster ;
; ----------------------------------;
OPWW_SearchClusterFATsector         ;
; ----------------------------------;
    CALL    #FindClusterFATsector   ; Output: FATsector = OrgFAT1+ClusterHI, FAToffset = ClusterLO*2
    MOV     #0,X                    ;
    CALL    #ReadSectorWX           ; and load it
    MOV     &FATsector,T            ;
    SUB     &OrgFAT1,T              ; T = ClusterHI
    MOV     &FAToffset,W            ; W = FAToffset = ClusterLO*2 
; ----------------------------------;
OPWW_SearchClustertInBuf            ;
; ----------------------------------;
    MOV     BUFFER(W),Y             ; W = FAToffset       
    CMP     #-1,Y                   ; Y = last Cluster ?
    JZ      OPWW_LastClusterFound   ; yes
    MOV     Y,&Cluster              ; no : Y = next Cluster number
    CMP.B   &Cluster+1,T            ; same ClusterHI and FATsectorLow ?
    JNZ     OPWW_SearchClusterFATsector    ; no : get next ClusterFATsector
    MOV.B   Y,W                     ; Cluster number LOW ==> W
    ADD     W,W                     ; W = Cluster buffer offset
    JMP     OPWW_SearchClustertInBuf;
; ----------------------------------;
OPWW_LastClusterFound               ;
; ----------------------------------;
    MOV     &FATsector,&CurFATsector;
    MOV     W,&FAToffset
    MOV.B   T,&CLuster+1            ; relative FATsector = ClusterHI
    RRA     W                       ; convert buffer offset in ClusterLO number
    MOV.B   W,&Cluster              ; save in ClusterLO
    MOV     &CurrentHdl,X           ;
    MOV     &Cluster,HDLW_CurClust(X)  ; update handle Current Cluster
; ----------------------------------;
; 3.2- Compute cluster offset       ; cluster size maxi = 32 kb
; ----------------------------------;
    MOV.B   HDLL_CurSize+1(X),W     ; CurrentSizeLO = 0bxoooooof.ffffffff, o = ClusterOffset, f = buffer address
    BIC.B   #80h,W                  ; W = 0b0oooooof
    RRA.B   W                       ; W = 0b00oooooo
    MOV.B   W,HDLB_ClustOfst(X)     ; update handle
; ----------------------------------;
; 3.3- Compute Buffer Pointer       ;
; ----------------------------------;
    MOV     #BytsPerSec-1,Y         ;
    AND     HDLL_CurSize(X),Y       ; Y = CurrentSizeLO modulo BytsPerSec
OPWW_InitBufferPointer
    MOV     Y,HDLW_BUFofst(X)       ; update handle
    MOV     Y,&BufferPtr            ; init Buffer Pointer
; ----------------------------------;
; 3.4- Compute Current SectorL      ;
; ----------------------------------;
    PUSH    W                       ; = HDLB_ClustOfst
    CALL    #ComputeClusFrstSect    ; use no registers ? no !
    ADD     @RSP+,&SectorL          ;
    ADDC    #0,&Sectorl+2           ;
; ----------------------------------;
; 3.5- Read last written Sector     ; in buffer
; ----------------------------------;
    CALL    #ReadSectorL            ;
; ----------------------------------;
; 5- Defer EMIT to SD_EMIT          ; if and only if no error...
; ----------------------------------;
OPW_DeferEmit                       ;
    MOV     #SD_EMIT,&EMIT+2        ; ...redirect EMIT to SD_EMIT 
    mNEXT                           ; --
; ----------------------------------;


;; ======================================================================
;; DEL" primitive as part of OpenPathName
;; hard DEL : all "DEL"eted clusters are freed
;; --------   DIR entry is marked as free
;; input : S = OpenError, W = open_type, X = CurrentHdl
;;         SectorL = DIRsector, Buffer = [DIRsector]
;; output: nothing (no message if open error)
;; ======================================================================
;
;
;OPEN_QDEL                            ;
;;    CMP     #8,W                    ;   open_type = DEL"
;;    JNZ     OPN_TYPE16              ;
;; ----------------------------------;
;OPEN_DEL                            ;
;; ----------------------------------;
;; 1- open file                      ; done
;; ----------------------------------;
;    CMP     #0,S                    ; open file happy end ?
;    JNE     OPND_END                ; don't send message
;; ----------------------------------;
;; 2- Delete DIR entry               ; hard DEL : the entry is viewed as free
;; ----------------------------------;
;    MOV     &EntryOfst,Y            ;
;    MOV.B   #0,BUFFER(Y)            ;
;    MOV     HDLL_DIRsect(X),W       ; 
;    MOV     HDLL_DIRsect+2(X),X     ;
;    CALL    #WriteSectorWX          ;S,W,X
;; ----------------------------------;
;; 3- free all clusters              ;
;; ----------------------------------;
;    MOV     &EntryOfst,Y            ;
;    MOV     BUFFER+26(Y),&Cluster   ; first cluster of file
;OPND_GetClusterInFAT                ;     
;    CALL    #FindClusterFATsector   ;
;    MOV     #0,X                    ;
;    CALL    #ReadSectorWX           ; and load it
;    MOV     &FATsector,W            ;
;    SUB     &OrgFAT1,W              ; W = relative FATsector
;    MOV     &FAToffset,Y            ;
;OPND_GetClusterInSector     
;    MOV     BUFFER(Y),X             ; get first cluster (number)
;    MOV     #0,BUFFER(Y)            ; free CLuster
;    CMP     #-1,X                   ; last Cluster ?
;    JZ      OPND_LastCluster        ;
;    MOV     X,&Cluster              ;
;    MOV.B   X,Y                     ; Y = next ClusterLO
;    ADD     Y,Y                     ; Y = next FAToffset
;    SWPB    X                       ;
;    CMP.B   X,W                     ; next ClusterHI = relative FATsector ?
;    JZ      OPND_GetClusterInSector ; yes
;    CALL    #UpdateFATs             ;STWXY
;    JMP     OPND_GetClusterInFAT    ;
;OPND_LastCluster                    ;
;    CALL    #UpdateFATs             ;STWXY
;; ----------------------------------;
;; 3- Close Handle                   ;
;; ----------------------------------;
;    CALL    #CloseHandle            ;
;; ----------------------------------;
;OPND_End                            ;
;; ----------------------------------;
;    mNEXT
;
;
;
; ======================================================================
; DEL" primitive as part of OpenPathName
; input : S = OpenError, W = open_type, X = CurrentHdl
;         SectorL = DIRsector, Buffer = [DIRsector]
; output: nothing (no message if open error)
; ======================================================================


OPEN_QDEL                            ;
;    CMP     #8,W                    ;   open_type = DEL"
;    JNZ     OPN_TYPE16              ;
; ----------------------------------;
OPEN_DEL                            ;
; ----------------------------------;
; 1- open file                      ; done
; ----------------------------------;
    CMP     #0,S                    ; open file happy end ?
    JNE     OPND_END                ; don't send message
; ----------------------------------;
; 2- Delete DIR entry               ; the entry is hidden with E5h as first char
; ----------------------------------;
    MOV     &EntryOfst,Y            ;
    MOV.B   #0E5h,BUFFER(Y)         ; 
    MOV     HDLL_DIRsect(X),W       ; 
    MOV     HDLL_DIRsect+2(X),X     ;
    CALL    #WriteSectorWX          ;S,W,X
; ----------------------------------;
; 3- Close Handle                   ;
; ----------------------------------;
    CALL    #CloseHandle            ;
; ----------------------------------;
OPND_End                            ;
; ----------------------------------;
    mNEXT


