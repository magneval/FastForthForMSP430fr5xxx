Fast Forth For MSP430FRxxxx TI's devices
========================================

Fast Forth for TI's LaunchPad MSP-EXP430FR[5969|6989|5994|5739|4133] and all MSP430FRxxxx devices.

	Fully tested on MSP-EXP430FR5969, MSP-EXP430FR6989 and MSP-EXP430FR4133 launchpads at 0.5, 1, 2, 4, 8, 16 MHz,
    plus 24MHz with MSP430fr5738 device.

	With its embedded assembler FASTFORTH is small : 6 kb. Full version with SD_card Load|Read|Write : 10 kb.

	Files launchpad_3Mbd.txt are 16threads 16MHz executables with 3Mbds, XON/XOFF terminal,
    (MSP_EXP430fr5739_6Mbd.txt are 16threads 24MHz executables for 6Mbds, XON/XOFF terminal).
	
    Launchpad_115200.txt files are same except 8MHz and 115200bds for unlucky linux men without TERATERM. 

	You have also 500kHz/115200bds and 8MHz/921600bds versions.
	
	After downloading ANS_COMPLEMENT_(x)W_MPY.f, all versions are able to run \MSP430-FORTH\CORETEST.4th 
	For FR4133 devices choose ANS_COMPLEMENT_SW_MPY.f, for all other ANS_COMPLEMENT_HW_MPY.f.

	To run SD_card demo, you must recompile DTCforthMSP430FR5xxx.asm with SD_CARD_LOADER
    SD_TOOLS SD_CARD_READ_WRITE switches turned ON (uncomment the line).

	\MSP430-FORTH\files.f must be preprocessed because of their symbolic values ; 
	\MSP430-FORTH\files.4th can be directly downloaded.

    Scite editor have specialized commands for each type of source files.

    Notice that in source files FAST FORTH interprets only SPACE(s) as delimiter (no TAB) and CR+LF as end of line (no LF).

What is new ?
-------------

	added words ASM and ENDASM to create assembler words which cannot be interpreted by FORTH, i.e. called by CALL and ended by RET or RETI
    ASM words are made accessible only in ASSEMBLER context.

	accept up to 2GB SD Card, all versions (V1.x and V2.x).

	added 3 backward and 3 forward jumps in the embedded assembler.
	
	you can compile up to 16 threads and/or inlined DOCOL versions ==> FastForth interpretation time is divided by about sqrt(16) !

	In the root folder you can directly download program files *.txt to any target by using drag and drop on their respective load.bat file. 

	In the folder MSP430-FORTH you can directly download source files *.f and *.4th to any target by using drag and drop. 
	See \MSP430-FORTH\send_source_file_to_target_readme.txt.

	Memory management :
	Fast Forth defines 4 levels of programm memory :
		WIPE that resets programm memory as hex file was
		RST_HERE / RST_STATE that set / reset the boundary of program protected against <reset>
		PWR_HERE / PWR_STATE that set / reset the boundary of program protected against power ON/OFF
		and nothing, i.e. volatile memory.

	You can download source files with software XON/XOFF protocol, without line or char delays and up to:
		134400 bds  @ 500kHz
        268800 bds  @ 1MHz
        614400 bds  @ 2MHz
        1228800 bds @ 4MHz
        1843200 bds @ 8MHz
        3000000 bds @ 16MHZ
        and 6000000 bds @ 24MHz with MSP430FR57xx family devices!
    In the main source file DTCforthMSP430FR5xxx.asm, you have the list of available baudrates 
	for each UARTtoUSB device and each clock Frequency.

    If an error occurs while downloading the source file, Fast Forth disregards the rest of the file
	before displaying error. Since then no crash to deplore despite the lack of FRAM write protection.

	FAST FORTH can be adjusted by selection of SWITCHES in the source file to reduce its size according   
	to your convenience. To do, uncomment their line.

	FAST FORTH has its embedded assembler that takes the Ti's syntax, a must have in the world FORTH!
	With the syntactic preprocessor GEMA, the assembler becomes symbolic.

    You can select mode LPM{0,1,2,3,4} in your application, depending of family, FR2xxx : LPM0,
    FR57xx : LPM0 to 2, FR59xx : LPM0 to 4.

    DEEP_RST (RESET + WIPE) can be hardware performed via the programmation interface (Vcc,RX,TX,RST,TEST,GND).


Many thanks to Brad Rodriguez
-----------------------------

for his CamelForth which served me as a kind of canvas.

Unlike CamelForth this FORTH is a "Direct Threaded Code", with an embedded assembler following the standard syntax,
not the one used in the world Forth.

Its core is fully compliant with the standard ANS.

This is a FORTH optimized for the speed, especially in the interpreter mode, so that you can load an application program written in FORTH/Assembler faster than its binary via MSP430 Flasher.exe : everything can be done from your text editor, the preprocessor and a serial terminal.

What's this and why?
---

I have first programmed atmel tiny devices.
Particularly I2C master driver to have both I2C slave and I2C master on a ATtiny461.
which means a lot of back and forth between the editor, assembler, the programmer and the test in situ.

Previously I had programmed a FORTH on a Motorola 6809 and had been seduced by the possibility of sending a source file directly to the target using a serial terminal. Target which compiled and executed the program. At the time FORTH program lay in a battery backed RAM addressed by the 6809.

The advent of chip MSP430 TEXAS INSTRUMENT with embedded FRAM gave me the idea to do it again : FAST FORTH was born.

Today I dropped the ATMEL chips and proprietary interfaces, I program my applications in a mix 80%/20% of assembler/FORTH I then sent on MSP430FR5738 chips with embedded FAST FORTH.

And that's the magic: After I finished editing (or modify) the source file, I press the "send" button in my text editor and I can test result on target in the second following. This is the whole point of an IDE reduced to its simplest form: a text editor, a cable, a target.


Content
-------

With a size < 6 kb, Fast Forth contains :

    COLD            WARM            (WARM)          WIPE            RST_HERE        RST_STATE       PWR_HERE        PWR_STATE       
    ASM             CODE            HI2LO           MOVE            LEAVE           +LOOP           LOOP            DO              
    REPEAT          WHILE           AGAIN           UNTIL           BEGIN           THEN            ELSE            IF              
    POSTPONE        [']             IMMEDIATE       ;               :               RECURSE         ]               [               
    IS              DEFER           DOES>           CREATE          CONSTANT        VARIABLE        \               '               
    ABORT"          ABORT           QUIT            INTERPRET       COUNT           LITERAL         ,               EXECUTE         
    >NUMBER         FIND            WORD            (ACCEPT)        ACCEPT          NOECHO          ECHO            EMIT            
    (EMIT)          KEY             ."              S"              TYPE            SPACES          SPACE           CR              
    (CR)            C,              ALLOT           HERE            .               U.              SIGN            HOLD            
    #>              #S              #               <#              BL              STATE           BASE            >IN             
    UNLOOP          J               I               U<              >               <               =               0<              
    0=              MIN             MAX             1-              1+              ABS             NEGATE          XOR             
    OR              AND             -               +               C!              C@              !               @               
    DEPTH           R@              R>              >R              ROT             OVER            SWAP            DROP            
    ?DUP            DUP             LIT             EXIT

plus an embedded assembler (symbolic assembler with external preprocessor) :

    ?GOTO           GOTO            FW3             FW2             FW1             BW3             BW2             BW1             
    ?JMP            JMP             REPEAT          WHILE           AGAIN           UNTIL           ELSE            THEN            
    IF              0=              0<>             U>=             U<              0<              0>=             S<              
    S>=             RRUM            RLAM            RRAM            RRCM            POPM            PUSHM           CALL            
    PUSH.B          PUSH            SXT             RRA.B           RRA             SWPB            RRC.B           RRC             
    AND.B           AND             XOR.B           XOR             BIS.B           BIS             BIC.B           BIC             
    BIT.B           BIT             DADD.B          DADD            CMP.B           CMP             SUB.B           SUB             
    SUBC.B          SUBC            ADDC.B          ADDC            ADD.B           ADD             MOV.B           MOV             
    RETI            LO2HI           COLON           ENDASM          ENDCODE


...everything you need to program effectively in assembly or FORTH or mix, as you want. See examples in MSP430-FORTH folder.



Organize your gitlab copy of FastForth
-------

download zip of last version

copy it to a subfolder, i.e. FastForth, created in your user folder

right clic on it to share it with yourself.

remember its shared name i.e. : //myPC/users/my/FastForth.

in file explorer then right clic on root to connect a network drive, copy shared name in drive name and choose a free drive letter a:, b: ...

Thus all relative paths will be linked to this drive, except the three links in the folder \MSP430-forth.

WARNING! if you erase a file in this drive or in one of its subfolders, no trash, the file is lost!


	


MINIMAL SOFTWARE
--

If you are under WINDOWS :

	First, you download the TI's programmer from TI : http://www.ti.com/tool/MSP430-FLASHER.
	And the MSP430Drivers : 
	http://software-dl.ti.com/msp430/msp430_public_sw/mcu/msp430/MSP430_FET_Drivers/latest/index_FDS.html

	The next tool is TERATERM.EXE : http://logmett.com/index.php?/products/teraterm.html.
	
	As scite is my editor, this github repository is fully configured for scite users.
    download the single file executable called sc1 (not the full download) :
    http://www.scintilla.org/SciTEDownload.html, then save it as \prog\wscite\scite.exe.

	download GEMA preprocessor : https://sourceforge.net/projects/gema/files/gema/gema-1.4-RC/

	The MacroAssembler AS :	http://john.ccac.rwth-aachen.de:8000/as/

	and Srecord : http://srecord.sourceforge.net/download.html

	copy last 3 items onto their respective subfolders of folder \prog. 

	ask windows to open .asm, .inc, .map files with scite.exe
	

If you are linux or OS X men, sorry...


Build the program file
----------------------
 

\DTCforthMSP430FR5xxx.asm is the main file to compile FastForth. It calls :	

	- mspregister.asm that defines the TI symbolic instructions,
	- DeviceMAP.map that defines for each device the eUSCI used as Terminal
	  and then selects the declarations file family.map, 
	- ForthThreads.mac in case of multithread vocabularies,
	- optionally, DTCforthMSP430FR5xxxSD.asm file(s) for SD_Card,
	- optionally, DTCforthMSP430FR5xxxASM.asm for assembler,
	- TargetInit.inc that selects the target.inc,
	- and then TERMINALBAUDRATE.inc.

open it with scite

uncomment the target as you want, i.e. MSP_EXP430fr5969

choose frequency, baudrate, UART handshake.

uncomment options switches as your convenience.

save file.

assemble (CTRL+0). A window asks you for 4 parameters:

set device as first param, i.e. MSP430FR5969,

set your target as 2th param, i.e. MSP_EXP430fr5969,

then execute.



Load Txt file (TI format) to target
-----------------------------------

	drag your target.txt file and drop it on TARGETprog.bat
    or use scite internal command TOOLS:FET prog (CTRL+1).

nota : programming the device use SBW2 interface, so UART0 is free for serial terminal use.

If you want to program your own MSP430FRxxxx board, wire its pins TST, RST, 3V3 and GND to same pins of the launchpad, on eZ-FET side of the programming connector.



Connect the FAST FORTH target to a serial terminal
-------------------------------------------------

you will need an USBtoUART cable with a PL2303TA device that allows XON/XOFF control flow :

	http://www.google.com/search?q=PL2303TA
    http://www.prolific.com.tw/UserFiles/files/PL2303_Prolific_DriverInstaller_v1_14_0.zip

or USBtoUART bridge, with a CP2102 device that allows XON/XOFF control flow :

	http://www.google.com/search?q=cp2102+module+3.3V
	http://www.silabs.com/products/mcu/Pages/USBtoUARTBridgeVCPDrivers.aspx

you must program CP2102 device to access 1382400 and 1843200 bds rates :

	http://www.silabs.com/Support%20Documents/Software/install_USBXpress_SDK.exe
	http://www.silabs.com/Support%20Documents/TechnicalDocs/an169.pdf

or a USBtoUART bridge, with a FT232RL device for only hardware control flow:

	http://www.google.com/search?q=FT232RL+module+3.3V
  	http://www.ftdichip.com/Drivers/CDM/CDM%20v2.12.00%20WHQL%20Certified.exe


How to configure the connection ?
-------------------------------

1-    the best ! Launchpad UARTn  <--> USBtoUART bridge with cp2102|PL2303TA chipset <--> TERATERM

       UARTn <--> CP2102 module | PL2303TA cable
         TXn ---> RX    
         RXn <--- TX    
        (GND <--> GND)  
		WARNING! DON'T CONNECT 5V RED WIRE! 

      TeraTerm configuration : see DTCforthMSP430fr5xxx.asm

If you plan to supply your target vith a PL2303 cable, open its box to weld red wire onto 3.3V pad.

2-    Launchpad UARTn <--> USBtoUART bridge with FT232RL chipset <--> TERATERM
	  WARNING! buy a FT232RL module with a switch 5V/3V3 and select 3V3.
 
       UARTn <--> FT232RL module
         TXn ---> RX    
         RXn <--- TX    
         RTS ---> CTS    
        (GND <--> GND)     
		WARNING! DON'T CONNECT 5V ! 

      TeraTerm configuration : see DTCforthMSP430fr5xxx.asm


Send a source file to the FAST FORH target
------------------

Two .bat files are done in the folder \MSP430-FORTH that enable you to send any source file to any target.

See how to in the file \MSP430-FORTH\send\_source\_file\_to\_target\_readme.txt.

you can also open this source file with scite, and do all via menu Tools.


SD_Card (FAT16) Load, Read, Write and Delete 
=============================================

First, hardware
---------------
for SD card socket be carefull : pin CD must be present !

 	http://www.ebay.com/itm/2-PCS-SD-Card-Module-Slot-Socket-Reader-For-Arduino-MCU-/181211954262?pt=LH_DefaultDomain_0&hash=item2a3112fc56

the commands
------------

With the LOAD" pathame" command you can download your source files from a SD_CARD memory (FAT16 format = SD card up to 2 Gb) in both execute and compile modes. Idem for READ", WRITE" and DEL" commands.

See "SD_TESTS.f", a FORTH program done for example

_So, we obtain a microcomputer with (up to) 64k of (F)RAM and with 2 Gb of flash memory, to store many source files that we can download in (F)RAM, at any time as you want !_

And if you remove the SD memory card reader and its you reset, all SD\_IO pins are available except SD_CD obviously.

HowTo LOAD a sourcefile
--------------

	LOAD" path\filename.4th".

The file is interpreted by FORTH in same manner than from the serial terminal.

When EOF is reached, the file is automatically closed.

A source file can _LOAD"_ an other source file, and so on in the limit of available handles (up to 6).

HowTo READ a file
--------------

	READ" path\filename.ext".

The first sector of this file is loaded in BUFFER  (@=0x1E00, size=0x200).
To read next sectors, use the command READ that loads the next sector in the buffer, and leaves on the stack a flag that is true when the EOF is reached. The file is automatically closed. See tstwords.4th for basic usage.

The variable BufferLen keep the count of bytes to be read (0 to 512).

If you want to anticipate the end, use the CLOSE command.

HowTo WRITE a file
---------------

	WRITE" path\filename.ext".

If the file does not exist, create it, else open it and set the write pointer at the end of the file, ready to append chars.
the command EMIT is redirected to SD_EMIT, so that all chars send by the words EMIT, TYPE and ." are written (the standard output - the console - is redirected to the file).

To end the writing and to close the file, use CLOSE command that also redirects EMIT to the console output via (restores the standard output).

HowTo delete a file
---------------

	DEL" path\filename.ext".

HowTo change DIRectory
---------------

	READ" a:\misc". 		\misc becomes the current DIR.
	READ" ..\"    			paren DIR becomes the current DIR.
	READ" \"				Root becomes the current DIR.

Drive letters are always ignored.

I2C DRIVERS
===========

The I2C\_Soft\_Master driver with normal/fast mode allows you to add then use any couple of pins to drive a bus I2C :

 - without use of eUSCI UCBx
 - I2C\_Soft\_MultiMaster driver : same plus detection collision
 - plus I2C\_Slave driver that uses the eUSCI UCBx hardware


Other interesting specificities :
=====

Management of vocabularies (not ANSI). VOCABULARY, DEFINITIONS, ONLY, ALSO, PREVIOUS, CONTEXT, CURRENT, FORTH, ASSEMBLER. 
In fact, it's the operation of the assembler that requires the vocabularies management.

Recognizing prefixed numbers 0b101011 (base 2) and 0x00FE (hex base).

CAPS ON/OFF add on

ECHO / NOECHO add on

The words DEFER and IS are implemented. CR, EMIT, KEY, ACCEPT and WARM are deferred words.

Error messages are colored (reverse video on ANSI terminal).

Assembly jumps are as FORTH jumps : IF, ELSE, THEN, BEGIN, AGAIN, UNTIL, WHILE.
Not canonical jumps are also available with JMP|?JMP to a defined word and GOTO|?GOTO to backward labels BW1 BW2 BW3 and forward labels FW1 FW2 FW3.

Switch  within definitions between FORTH and Assembly contexts with words HI2LO and LO2HI. See examples in the TstWords.f file. This is perhaps the most interesting feature for development...


The system is not responding ?
======

First, swich off then switch on. FORTH restarts as it was after the last PWR\_HERE command.

If the system is not restarted, press [reset] button on the MSP-EXP430FR5xxx ; FORTH restarts as it was after the last RST_HERE command.

If the system does not restart again, wire the TERMINAL TX pin to GND via 4k7 resistor then [reset] ; FORTH restarts as it is in the HEX file.
Equivalent word : COLD + WIPE.

Here is the reset architecture :

	case 1 : Power ON ==> reset + erases the volatile program beyond PWR_HERE (i.e. not protected by PWR_HERE command against POWER OFF)
			 The WARM display is preceded by "2"

	case 2 : <reset>  ==> reset + erases the program beyond RST_HERE (i.e. not protected by RST_HERE command against reset)
		 	 The WARM display is preceded by "4"
	case 2.1 : software <reset> is performed by COLD.
			 The WARM display is preceded by "6"

	case 3 : TERM_TX wired to GND via 4k7 + <reset> ===> hardware DEEP_RST, works even if the electronic fuse is "blown" !
			 The WARM display is preceded by "-4"
	case 3.1 : (SYSRSTIV = n = 0Ah | SYSRSTIV = n >= 16h) ===> DEEP_RST on failure,
			 The WARM display is preceded by "n"
	case 3.2 : writing -1 in SAVE_SYSRSTIV before COLD ===> software DEEP_RST
			 The WARM display is preceded by "-1"


EMBEDDED ASSEMBLER
======

With the preprocessor GEMA and the file MSP430FR\_FastForth.pat, the embedded assembler allows access to all system variables. See \\config\\gema\\MSP430FR\_FastForth.pat. You can also access to VARIABLE, CONSTANT or DOES type words in immediate (#), absolute (&) and indexed (Rx) assembly modes.

Clearly, after the instruction "MOV &BASE,R6", R6 equals the contents of the FORTH variable "BASE", and after "MOV #BASE,R6" R6 contains its address.

If you want to create a buffer of 8 bytes (4 words) :

	CREATE BUFFER_OUT 8 ALLOT
the access to this buffer is done by :

	MOV #BUFFER_OUT,R8
	MOV.B @R8,R9
with R8 as org address of the buffer.

Or by indexed addressing :

	MOV.B BUFFER_OUT(R8),R9
with R8 register as buffer pointer.

see TESTASM.4th in \MSP430-FORTH folder.

Below is the TeraTerm echo of the JMJ_BOX.4th download that drives an audio channel with Automatic Level Control, Noise Gate, IR remote receiver and a LifePO4 cells monitoring.
I hope you see better how to work with FAST FORTH and its embedded assembler. Notice assembly structures for conditionnal jumps.
The word INPUT shows you nested FORTH / ASSEMBLER conditional jumps and so the power of this mix. 



	6  FastForth V1.0  8MHz (C) J.M.Thoorens 2016
	ok ; -------------------------------------------
	ok ; MSP430FR5738 JMJ_BOX 8MHz 921600bds JMJ-BOX
	ok ; -------------------------------------------
	ok ECHO
	ok WIPE        ; 9226 bytes free
	ok
	ok CREATE PRS
	ok 0x14 C,
	ok 0x0C C,
	ok 0 C,
	ok 0 C,
	ok 1 C,
	ok 0 C,
	ok 0 C,
	ok 0 C,
	ok 0 C,
	ok 0 C,
	ok 0 C,
	ok 0 C,
	ok 0 C,
	ok 0 C,
	ok 32 C,
	ok 0 C,
	ok 0 C,
	ok 0 C,
	ok 0x284 ,
	ok 0 ,
	ok 0 ,
	ok 0 ,
	ok
	ok VARIABLE I2CS_ADR
	ok 2 ALLOT
	ok VARIABLE I2CM_BUF
	ok 2 ALLOT
	ok
	ok ASM I2C_M					; I2C master driver
	ok BIS #1,&0x640
	ok MOV #0x0FD3,&0x640
	ok MOV #0x00C8,&0x642
	ok MOV #0x14,&0x646
	ok MOV #I2CM_BUF,R12
	ok MOV.B @R12+,&0x64A
	ok CMP.B #0,&0x64A
	ok 0= IF
	ok     BIS #4,&0x640
	ok THEN
	ok MOV #I2CS_ADR,R11
	ok MOV.B @R11+,&0x660
	ok RRA &0x660
	ok U>= IF
	ok     MOV R11,R12
	ok     BIC #0x10,&0x640
	ok THEN
	ok MOV.B #-1,R11
	ok BIC #1,&0x640
	ok BIT.B #1,&I2CS_ADR
	ok 0= IF						; master TX
	ok     BEGIN
	ok         MOV.B &0x649,R11
	ok         BIT #8,&0x66C
	ok         0<> IF
	ok             MOV @R1+,R0 		; RET
	ok         THEN
	ok         BIT #0x20,&0x66C
	ok         0<> IF
	ok             BIS #4,&0x640
	ok             MOV @R1+,R0
	ok         THEN
	ok         BIT #2,&0x66C
	ok         0<> IF
	ok            MOV.B @R12+,&0x64E
	ok         THEN
	ok     AGAIN
	ok ELSE							; master RX
	ok     BEGIN
	ok         MOV.B &0x649,R11
	ok         BIT #8,&0x66C
	ok         0<> IF
	ok             MOV @R1+,R0		; RET
	ok         THEN
	ok         BIT #1,&0x66C
	ok         0<> IF
	ok           MOV.B &0x64C,0(R12)
	ok           ADD #1,R12
	ok         THEN
	ok     AGAIN
	ok THEN
	ok ENDASM
	ok
	ok ASM CHNGPOTY
	ok MOV #I2CM_BUF,R10
	ok MOV.B #3,0(R10)
	ok MOV.B R8,1(R10)
	ok MOV.B R8,2(R10)
	ok BIS.B #0b01000000,2(R10)
	ok MOV.B 12(R13),3(R10)
	ok MOV.B #0b01010000,&I2CS_ADR
	ok JMP I2C_M
	ok ENDASM
	ok
	ok CREATE ROM_LED
	ok 0b11111111 C, 0b11111110 C,
	ok 0b11111100 C, 0b11111000 C,
	ok 0b11110000 C, 0b11100000 C,
	ok 0b11000000 C, 0b10000000 C,
	ok 0b00000000 C, 0b01111111 C,
	ok 0b00111111 C, 0b00011111 C,
	ok 0b00001111 C, 0b00000111 C,
	ok 0b00000011 C, 0b00111100 C,
	ok
	ok ASM BLINKY
	ok ADD #ROM_LED,R8
	ok MOV.B @R8,R8
	ok SWPB R8
	ok ADD #1,R8
	ok MOV R8,&I2CM_BUF
	ok MOV.B #0b01110000,&I2CS_ADR
	ok JMP I2C_M
	ok ENDASM
	ok
	ok ASM DISPLAYY
	ok MOV.B #0x28,7(R13)
	ok JMP BLINKY
	ok ENDASM
	ok
	ok ASM CHNGGAINY
	ok MOV.B R8,R9
	ok BIT.B #0x10,4(R13)
	ok 0<> IF
	ok     SUB.B #0x0C,R9
	ok THEN
	ok CMP.B #0x24,R9
	ok U< IF
	ok     PUSH R9
	ok     CMP.B R8,10(R13)
	ok     0<> IF
	ok         BIT.B #0x10,4(R13)
	ok         0<> IF
	ok             MOV.B R8,2(R13)
	ok         ELSE
	ok             MOV.B R8,3(R13)
	ok         THEN
	ok         MOV.B R8,10(R13)
	ok         MOV.B R8,11(R13)
	ok         CALL #CHNGPOTY
	ok         MOV.B #0,5(R13)
	ok     THEN
	ok     MOV @R1+,R8
	ok     RRUM #2,R8
	ok     JMP DISPLAYY
	ok THEN
	ok MOV @R1+,R0					; RET
	ok ENDASM
	ok
	ok ASM CHNGALC
	ok MOV.B 11(R13),R8
	ok CMP.B 11(R13),10(R13)
	ok 0= IF
	ok     MOV.B #0x86,12(R13)
	ok     CALL #CHNGPOTY
	ok     MOV #-9,&0x350
	ok     MOV #0,R8
	ok     JMP BLINKY
	ok THEN
	ok MOV.B #2,5(R13)
	ok CALL #CHNGPOTY
	ok SUB.B 10(R13),R8
	ok XOR.B #-1,R8
	ok RRUM #3,R8
	ok ADD.B #9,R8
	ok JMP BLINKY
	ok ENDASM
	ok
	ok ASM SCANCAP
	ok MOV.B #0x18,R9
	ok BIC.B R9,&0x203
	ok BIS.B R9,&0x205
	ok BIC.B R9,&0x207
	ok MOV.B #0x17,R8
	ok MOV #0x2E5,&0x380
	ok BIC.B R9,&0x205
	ok BEGIN
	ok     CMP R8,&0x390
	ok U>= UNTIL
	ok MOV.B &0x201,R8
	ok MOV #0x2C5,&0x380
	ok BIS.B R9,&0x203
	ok BIS.B R9,&0x207
	ok AND.B #0x18,R8
	ok BIT.B #8,R8
	ok 0= IF
	ok     BIS.B #4,4(R13)
	ok THEN
	ok BIT.B #0x10,R8
	ok 0= IF
	ok     BIS.B #2,4(R13)
	ok     BIT.B #4,4(R13)
	ok     0<> IF
	ok         XOR.B #0x0E,4(R13)
	ok     THEN
	ok THEN
	ok MOV @R1+,R0					; RET
	ok ENDASM
	ok
	ok ASM INT_TA0					; WATCHDOG process
	ok BIC #0xF8,0(R1)
	ok MOV #0x18,&0x1814
	ok BIC #1,&0x340
	ok MOV #0,&0x700
	ok MOV #0,&0x71C
	ok MOV #2,&0x70A
	ok MOV #PRS,R13
	ok BIT.B #1,4(R13)
	ok 0<> IF
	ok     BIT.B #0x80,4(R13)
	ok     0<> IF
	ok         BIC.B #0x80,4(R13)
	ok         BIT.B #0x60,16(R13)
	ok         0<> IF
	ok             BIS.B #0x20,4(R13)
	ok             MOV 0(R13),2(R13)
	ok             MOV #0x2EE0,24(R13)
	ok             MOV.B #0,5(R13)
	ok             MOV.B #0,9(R13)
	ok             BIS.B #8,&0x324
	ok             MOV.B #0x28,8(R13)
	ok             BIT.B #0x40,16(R13)
	ok             0<> IF
	ok                 MOV.B 2(R13),10(R13)
	ok                 BIS.B #0x10,4(R13)
	ok             ELSE
	ok                 MOV.B 3(R13),10(R13)
	ok                 BIC.B #0x10,4(R13)
	ok             THEN
	ok             MOV.B 10(R13),R8
	ok             ADD.B #1,10(R13)
	ok             MOV.B #0x86,12(R13)
	ok             CALL #CHNGGAINY
	ok             MOV #-9,&0x350
	ok             RETI
	ok         ELSE
	ok             BIC.B #1,&0x204
	ok             BIC.B #0x20,4(R13)
	ok
	ok             MOV #COLD,R0
	ok         THEN
	ok     THEN
	ok     BIT.B #0x20,4(R13)
	ok     0<> IF
	ok         MOV #-9,&0x350
	ok         CMP.B #0,7(R13)
	ok         0<> IF
	ok             SUB.B #1,7(R13)
	ok             0= IF
	ok                 MOV.B #0,R8
	ok                 CALL #BLINKY
	ok             THEN
	ok         THEN
	ok         CMP.B #0,8(R13)
	ok         0<> IF
	ok             SUB.B #1,8(R13)
	ok             0= IF
	ok                 BIS.B #1,&0x204
	ok                 MOV.B #1,6(R13)
	ok             THEN
	ok         ELSE
	ok             CMP.B #0,9(R13)
	ok             0<> IF
	ok                 SUB.B #1,9(R13)
	ok                 0= IF
	ok                     BIC.B #4,&0x324
	ok                 THEN
	ok             THEN
	ok             CMP.B #0,5(R13)
	ok             0<> IF
	ok                 MOV #-3,&0x350
	ok                 SUB.B #1,5(R13)
	ok                 0= IF
	ok                     CMP.B 11(R13),10(R13)
	ok                     0<> IF
	ok                         ADD.B #1,11(R13)
	ok                         CALL #CHNGALC
	ok                     THEN
	ok                 THEN
	ok             THEN
	ok             SUB.B #1,6(R13)
	ok             0= IF
	ok                 MOV.B #3,6(R13)
	ok                 CALL #SCANCAP
	ok                 MOV.B 10(R13),R8
	ok                 BIT.B #4,4(R13)
	ok                 0<> IF
	ok                     CMP.B #0,7(R13)
	ok                     0<> IF
	ok                         ADD.B #1,R8
	ok                     THEN
	ok                     CALL #CHNGGAINY
	ok                 ELSE
	ok                     BIT.B #2,4(R13)
	ok                     0<> IF
	ok                         CMP.B #0,7(R13)
	ok                         0<> IF
	ok                             SUB.B #1,R8
	ok                         THEN
	ok                         CALL #CHNGGAINY
	ok                     ELSE
	ok                         BIT.B #8,4(R13)
	ok                         0<> IF
	ok                             CMP.B #0,7(R13)
	ok                             0<> IF
	ok                                 MOV 2(R13),0(R13)
	ok                                 MOV #0x0F,R8
	ok                                 CALL #BLINKY
	ok                             THEN
	ok                         THEN
	ok                     THEN
	ok                 THEN
	ok             THEN
	ok             BIC.B #0x0E,4(R13)
	ok             SUB #1,24(R13)
	ok             0= IF
	ok                 BIC.B #1,&0x204
	ok             THEN
	ok         THEN
	ok     THEN
	ok
	ok THEN
	ok BIS #0x10,&0x700
	ok BIS #0x03,&0x700
	ok RETI
	ok ENDASM
	ok
	ok ASM INT_ADC						; ADC process
	ok BIC #0xF8,0(R1)
	ok MOV #0x18,&0x1814
	ok MOV #0,&0x700
	ok MOV #0,&0x71C
	ok MOV #PRS,R13
	ok CMP.B #2,&0x70A
	ok 0= IF
	ok     MOV &0x712,18(R13)
	ok     BIT.B #1,4(R13)
	ok     0= IF
	ok         CMP #0x284,&0x712
	ok         U< IF
	ok             RETI
	ok         THEN
	ok         BIS.B #1,4(R13)
	ok         BIS #1,&0x340
	ok         RETI
	ok     THEN
	ok     CMP #0x239,&0x712
	ok     U< IF
	ok         BIC.B #1,4(R13)
	ok         MOV #WARM,R0
	ok     THEN
	ok     BIT.B #0x20,4(R13)
	ok     0= IF
	ok         RETI
	ok     THEN
	ok     MOV #4,&0x70A
	ok ELSE
	ok     CMP.B #4,&0x70A
	ok     0= IF
	ok         MOV &0x712,20(R13)
	ok         MOV #5,&0x70A
	ok     ELSE
	ok         MOV &0x712,R8
	ok         SUB 20(R13),R8
	ok         S< IF
	ok             XOR #-1,R8
	ok         THEN
	ok         CMP #2,R8
	ok         U>= IF
	ok             MOV.B #0x14,9(R13)
	ok             MOV #0x2EE0,24(R13)
	ok             BIT.B #4,&0x320
	ok             0<> IF
	ok                 BIS.B #1,&0x204
	ok                 BIS.B #4,&0x324
	ok             THEN
	ok             ADD R8,R8
	ok             CMP 18(R13),R8
	ok             U>= IF
	ok                 CMP.B #0,11(R13)
	ok                 0<> IF
	ok                     MOV.B #0x84,12(R13)
	ok                     SUB.B #1,11(R13)
	ok                     CALL #CHNGALC
	ok                 THEN
	ok             THEN
	ok         THEN
	ok     THEN
	ok THEN
	ok BIS #0x10,&0x700
	ok BIS #0x03,&0x700
	ok RETI
	ok ENDASM
	ok
	ok ASM INT_P2						; Inputs detect + IR RC5 process
	ok BIC #0xF8,0(R1)
	ok MOV #0x18,&0x1814
	ok MOV.B &0x201,R8
	ok MOV.B R8,&0x219
	ok MOV.B #0,&0x21D
	ok MOV #PRS,R13
	ok BIT.B #1,4(R13)
	ok 0= IF
	ok     RETI
	ok THEN
	ok XOR.B 16(R13),R8
	ok AND.B #0x60,R8
	ok 0<> IF
	ok     XOR.B R8,16(R13)
	ok     BIS.B #0x80,4(R13)
	ok     BIC.B #4,&0x324
	ok     MOV #-20,&0x350
	ok     BIC #1,&0x340
	ok     RETI
	ok THEN
	ok     MOV     #0,&0x3E0
	ok     MOV     #1778,R9
	ok     MOV     #14,R10
	ok BEGIN
	ok     MOV #0b1011100100,&0x3C0
	ok     RRUM    #1,R9
	ok     MOV     R9,R8
	ok     RRUM    #1,R8
	ok     ADD     R9,R8
	ok     BEGIN   CMP R8,&0x3D0
	ok     0= UNTIL
	ok     BIT.B   #4,&0x201
	ok     ADDC    R11,R11
	ok     MOV     &0x201,&0x219
	ok     BIC.B   #4,&0x21D
	ok     SUB     #1,R10
	ok 0<> WHILE
	ok     ADD     R9,R8
	ok     BEGIN
	ok         CMP R8,&0x3D0
	ok         0>= IF
	ok           BIC  #0x30,&0x3C0
	ok           RETI
	ok         THEN
	ok         BIT.B #4,&0x21D
	ok     0<> UNTIL
	ok     MOV     &0x3D0,R9
	ok REPEAT
	ok BIC     #0x30,&0x3C0
	ok RLAM    #1,R11
	ok MOV.B   R11,R9
	ok RRUM    #2,R9
	ok BIT     #0x4000,R11
	ok 0= IF   BIS #0x40,R9
	ok THEN
	ok CMP.B #0x3B,R9
	ok 0= IF
	ok     BIS.B #8,4(R13)
	ok ELSE
	ok     CMP.B #0x57,R9
	ok     0= IF
	ok         BIS.B #8,4(R13)
	ok     ELSE
	ok         CMP.B #0x0C,R9
	ok         0= IF
	ok             BIS.B #8,4(R13)
	ok         ELSE
	ok             RRA R9
	ok             U< IF
	ok                 BIS.B #4,4(R13)
	ok             ELSE
	ok                 BIS.B #2,4(R13)
	ok             THEN
	ok         THEN
	ok     THEN
	ok THEN
	ok MOV.B #1,6(R13)
	ok RETI
	ok ENDASM
	ok
	ok CODE IO_INIT
	ok BIS.B #0b00111100,&0x20A
	ok BIS.B #0b11111100,&0x20C
	ok   BIC.B #0b00111111,&0x204
	ok   BIC.B #0b11111101,&0x206
	ok   BIC.B #0b00000001,&0x202
	ok   BIC.B #0b11111100,&0x205
	ok   BIC.B #0b00000100,&0x207
	ok   MOV.B #0b01100100,&0x21B
	ok   MOV.B #0b00000100,&0x218
	ok   BIC.B #0b11111111,&0x21D
	ok
	ok   BIC.B #0b00001100,&0x324
	ok   BIC.B #0b00001100,&0x326
	ok   BIC.B #0b00001100,&0x322
	ok MOV #0,&0x700
	ok MOV #0x0280,&0x702
	ok MOV #0x0014,&0x704
	ok MOV #0,&0x71C
	ok MOV #1,&0x71A
	ok MOV @R13+,R0
	ok ENDCODE
	ok
	ok : START						; Init app
	   IO_INIT
	   HI2LO
	ok MOV #PRS,R13
	ok AND.B #1,4(R13)
	ok CMP.B #0x31,0(R13)
	ok U>= IF
	ok     MOV.B #0x14,0(R13)
	ok THEN
	ok CMP.B #0x0C,0(R13)
	ok U< IF
	ok     MOV.B #0x14,0(R13)
	ok THEN
	ok CMP.B #0x25,1(R13)
	ok U>= IF
	ok     MOV.B #0x0C,1(R13)
	ok THEN
	ok MOV.B 10(R13),11(R13)
	ok MOV.B #0x20,14(R13)
	ok MOV #0,R8
	ok BEGIN
	ok     SUB #1,R8
	ok 0= UNTIL
	ok CALL #BLINKY
	ok BIC.B #8,&0x324
	ok MOV.B #0x05,&0x360
	ok MOV #0x01E6,&0x340
	ok MOV #-11250,&0x350
	ok MOV #0x18,&0x1814
	ok MOV #INT_ADC,&0xFFEC		; init vectors interrupt
	ok MOV #INT_TA0,&0xFFE8
	ok MOV #INT_P2,&0xFFD8
	ok LO2HI
	   ." type stop to stop :-)"
	   NOECHO
	   LIT RECURSE IS WARM
	   (WARM)
	   ;
	ok
	ok : STOP					; Stop App
	       ECHO
	       ['] (WARM) IS WARM
	       CR ."     "
	       COLD
	   ;
	ok
	ok RST_HERE    ; protect app against <reset> 7452 bytes free
	ok
	ok ; ===================================================
	ok ; ===================================================
	ok ; ####### #######  #####  #######  #####
	ok ;    #    #       #     #    #    #     #
	ok ;    #    #       #          #    #
	ok ;    #    #####    #####     #     #####
	ok ;    #    #             #    #          #
	ok ;    #    #       #     #    #    #     #
	ok ;    #    #######  #####     #     #####
	ok ; ===================================================
	ok ; ===================================================
	ok
	ok CODE M*
	ok MOV @R15,&0x4C2
	ok MOV R14,&0x4C8
	ok MOV &0x4E4,0(R15)
	ok MOV &0x4E6,R14
	ok MOV @R13+,R0
	ok ENDCODE
	ok
	ok CODE UM/MOD
	ok     MOV @R15+,R10
	ok     MOV @R15,R12
	ok     MOV #0,R8
	ok     MOV #16,R9
	ok BW1 CMP R14,R10
	ok     U< ?GOTO FW1
	ok     SUB R14,R10
	ok FW1
	ok BW2 ADDC R8,R8
	ok     U>= ?GOTO FW1
	ok     SUB #1,R9
	ok     0< ?GOTO FW2
	ok     ADD R12,R12
	ok     ADDC R10,R10
	ok     U< ?GOTO BW1
	ok     SUB R14,R10
	ok     BIS #1,R2
	ok     GOTO BW2
	ok FW2 BIC #1,R2
	ok FW1
	ok     MOV R10,0(R15)
	ok     MOV R8,R14
	ok     MOV @R13+,R0
	ok ENDCODE
	ok
	ok CODE UBAT					; display U bat
	ok PUSH R13
	ok MOV #PRS,R13
	ok SUB #2,R15
	ok MOV R14,0(R15)
	ok MOV 18(R13),R14
	ok MOV #0x0A,&0x1850
	ok LO2HI
	   ." = "
	   3600
	   M* 170 UM/MOD 0
	   <# 86 HOLD 32 HOLD			; " V"
	   # # # 46 HOLD #S #> TYPE		; xx.xxx
	   DROP
	   ;
	ok
	ok : U.R
	     >R  <# 0 # #S #>
	     R> OVER - 0 MAX SPACES TYPE
	   ;
	ok
	ok
	ok CODE WAIT25ms
	ok BEGIN
	ok     MOV #0,R8
	ok     BEGIN
	ok         SUB #1,R8
	ok     0= UNTIL
	ok     SUB #1,R14
	ok 0= UNTIL
	ok MOV @R15+,R14
	ok MOV @R13+,R0
	ok ENDCODE
	ok
	ok : PA_ON
	       IO_INIT
	   HI2LO
	ok   BIC.B #0b01100100,&0x21B
	ok   BIC.B #0b11111111,&0x21D
	ok
	ok     BIS.B #8,&0x324
	ok LO2HI
	   ;
	ok
	ok : PA_OFF
	       IO_INIT
	   ;
	ok
	ok : OUT_ON
	       PA_ON
	       4 WAIT25ms
	   HI2LO
	ok     BIS.B   #4,&0x324
	ok LO2HI
	   ;
	ok
	ok : OUT_OFF
	       IO_INIT
	   ;
	ok
	ok : AMP_ON
	       PA_ON
	   HI2LO
	ok     BIS.B #1,&0x204
	ok LO2HI
	   ;
	ok
	ok : AMP_OFF
	       IO_INIT
	   ;
	ok
	ok
	ok : LEDS						; PCA9674A test
	   IO_INIT
	   9 0 DO
	       I
	       HI2LO
	ok         MOV R14,R8
	ok         MOV @R15+,R14
	ok         CALL #BLINKY
	ok     LO2HI
	       4 WAIT25ms
	   LOOP
	   HI2LO
	ok     MOV #0,R8
	ok     CALL #BLINKY
	ok LO2HI
	   IO_INIT
	   ;
	ok
	ok : INPUT						; NCJ10FI-V0 test
	   IO_INIT
	   BEGIN
	       CR
	       HI2LO
	ok     BIT.B #0x40,&0x201
	ok     0<> IF
	ok         LO2HI
	           ." XLR"
	           HI2LO
	ok     ELSE
	ok         BIT.B #0x20,&0x201
	ok         0<> IF
	ok             LO2HI
	               ." TRS"
	               HI2LO
	ok         ELSE
	ok             LO2HI
	               ." NO INPUT"
	               HI2LO
	ok         THEN
	ok     THEN
	ok     LO2HI
	       KEY 0x0D
	   = UNTIL
	   IO_INIT
	   ;
	ok
	ok : WIPER						; DS1881 test
	       IO_INIT
	   HI2LO
	ok     MOV #PRS,R13
	ok     MOV.B #0,10(R13)
	ok BEGIN
	ok     MOV.B 10(R13),R8
	ok     MOV.B #0x86,12(R13)
	ok     CALL #CHNGPOTY
	ok     MOV #I2CM_BUF,R10
	ok     MOV.B #3,0(R10)
	ok     MOV.B #0b01010000,&I2CS_ADR
	ok     BIS.B #1,&I2CS_ADR
	ok     CALL #I2C_M
	ok     SUB #6,R15				; make room for 3 words
	ok     MOV R14,4(R15)			; save TOS 
	ok     MOV #I2CS_ADR,R9
	ok     MOV.B 1(R9),R14
	ok     MOV.B 2(R9),R8
	ok     SUB.B #0x40,R8
	ok     MOV R8,0(R15)
	ok     MOV.B 3(R9),R8
	ok     SUB.B #0x80,R8
	ok     MOV R8,2(R15)
	ok     LO2HI
	       CR 3 U.R 3 U.R 3 U.R
	       HI2LO
	ok     MOV #PRS,R13
	ok     ADD.B #8,10(R13)
	ok     CMP.B #64,10(R13)
	ok 0= UNTIL
	ok LO2HI
	   IO_INIT
	   ;
	ok
	ok : DB			; <value> --    set <value> then display it
	   IO_INIT
	   HI2LO
	ok     MOV #PRS,R13
	ok     MOV.B R14,10(R13)
	ok     MOV.B 10(R13),R8
	ok     MOV.B #0x86,12(R13)
	ok     CALL #CHNGPOTY
	ok     MOV #I2CM_BUF,R10
	ok     MOV.B #1,0(R10)
	ok     MOV.B #0b01010000,&I2CS_ADR
	ok     BIS.B #1,&I2CS_ADR		; I2C read
	ok     CALL #I2C_M
	ok     MOV #I2CS_ADR,R9
	ok     MOV.B 1(R9),R14
	ok LO2HI
	       CR  3 U.R ."  dB"
	   IO_INIT
	   ;
	ok
	ok : CAP+
	   IO_INIT
	   HI2LO
	ok MOV.B #0x18,R8
	ok BIC.B R8,&0x203
	ok BIS.B R8,&0x205
	ok BIC.B R8,&0x207
	ok MOV #0x2E5,&0x3C0
	ok BIC.B R8,&0x205
	ok BEGIN
	ok     BIT.B #8,&0x201
	ok 0<> UNTIL
	ok SUB #2,R15
	ok MOV R14,0(R15)
	ok MOV &0x3D0,R14
	ok MOV #0x2C5,&0x3C0
	ok LO2HI
	   0x10 0x1850 ! U. 0x0A 0x1850 !
	   IO_INIT
	   ;
	ok
	ok : CAP-
	   IO_INIT
	   HI2LO
	ok MOV.B #0x18,R8
	ok BIC.B R8,&0x203
	ok BIS.B R8,&0x205
	ok BIC.B R8,&0x207
	ok MOV #0x2E5,&0x3C0
	ok BIC.B #0x18,&0x205
	ok BEGIN
	ok     BIT.B #0x10,&0x201
	ok 0<> UNTIL
	ok SUB #2,R15
	ok MOV R14,0(R15)
	ok MOV &0x3D0,R14
	ok MOV #0x2C5,&0x3C0
	ok LO2HI
	   0x10 0x1850 ! U. 0x0A 0x1850 !
	   IO_INIT
	   ;
	ok
	ok PWR_HERE    ; protect app against power down 6474 bytes free
	ok
	ok START TYPE STOP TO STOP :-)


What is the interest of a very fast baud rate ?
---------------------

This seems obvious: you can edit a source program and then test it immediatly on the target: above, from my text editor, the download, compile and start are done in less than 1 sec.

