; -----------------------------------------------------------------
; BASIC TOOLS for FAT16 SD Card : DIR FAT SD_VIEW ; include UTILITY
; -----------------------------------------------------------------

\ Fast Forth For Texas Instrument MSP430FRxxxx FRAM devices
\ Copyright (C) <2015>  <J.M. THOORENS>
\
\ This program is free software: you can redistribute it and/or modify
\ it under the terms of the GNU General Public License as published by
\ the Free Software Foundation, either version 3 of the License, or
\ (at your option) any later version.
\
\ This program is distributed in the hope that it will be useful,
\ but WITHOUT ANY WARRANTY; without even the implied warranty of
\ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
\ GNU General Public License for more details.
\
\ You should have received a copy of the GNU General Public License
\ along with this program.  If not, see <http://www.gnu.org/licenses/>.

\ REGISTERS USAGE
\ R4 to R7 must be saved before use and restored after
\ scratch registers Y to S are free for use
\ under interrupt, IP is free for use

\ PUSHM order : PSP,TOS, IP,  S,  T,  W,  X,  Y, R7, R6, R5, R4
\ example : PUSHM IP,Y
\
\ POPM  order :  R4, R5, R6, R7,  Y,  X,  W,  T,  S, IP,TOS,PSP
\ example : POPM Y,IP

\ ASSEMBLER conditionnal usage after IF UNTIL WHILE : S< S>= U< U>= 0= 0<> 0>=
\ ASSEMBLER conditionnal usage before GOTO ?GOTO     : S< S>= U< U>= 0= 0<> <0 

\ FORTH conditionnal usage after IF UNTIL WHILE : 0= 0< = < > U<


\ WIPE        ; remove all previous apps against RESET,
RST_STATE   ; preserve all previous apps against RESET,
\ NOECHO      ; if an error occurs, comment this line before new download to find it.
    \

CODE ?          \ adr --            display the content of adr
    MOV @TOS,TOS
    MOV #U.,PC  \ goto U.
ENDCODE
    \

CODE SP@        \ -- SP
    SUB #2,PSP
    MOV TOS,0(PSP)
    MOV PSP,TOS
    MOV @IP+,PC
ENDCODE

: .S                \ --            print <number> of cells and stack contents if not empty
0x3C EMIT           \ --            char "<"
DEPTH .
8 EMIT              \               backspace
0x3E EMIT SPACE     \               char ">"
SP@  PSTACK OVER OVER U<    \
IF  2 -
    DO I @ U.
    -2 +LOOP
ELSE
    DROP DROP
THEN
;
    \

: WORDS                             \ --            list all words in all dicts.
  BASE @
  0x10 BASE !
  CONTEXT                           \ -- CONTEXT
  BEGIN                             \                                       search dictionnary
        DUP 
        2 + SWAP                    \ -- CONTEXT+2 CONTEXT
        @ ?DUP                      \ -- CONTEXT+2 (VOC_PFA VOC_PFA or 0)
  WHILE                             \ -- CONTEXT+2 VOC_PFA                  dictionnary found
    INI_THREAD @ 0                  \ -- CONTEXT+2 VOC_PFA thread 0         for all threads
    DO                              \ -- CONTEXT+2 VOC_PFA
        CR INI_THREAD @ 1 =         \                                       if thread = 1
        IF ."    "                  \                                       display nothing
        ELSE SPACE  I .             \                                       else display #thread
        THEN
        DUP                         \ -- CONTEXT+2 VOC_PFA VOC_PFA
        I DUP + +                   \ -- CONTEXT+2 VOC_PFA VOC_PFA(x)
        BEGIN                       \                                       search word
            @ ?DUP                  \ -- CONTEXT+2 VOC_PFA (NFA NFA or 0)  
        WHILE                       \ -- CONTEXT+2 VOC_PFA NFA              word found
            DUP                 
                DUP
            COUNT 0x7F AND TYPE     \ -- CONTEXT+2 VOC_PFA NFA
                C@ 0x0F AND         
                0x10 SWAP - SPACES  
            2 -                     \ -- CONTEXT+2 VOC_PFA LFA
        REPEAT                      \ -- CONTEXT+2 VOC_PFA
    LOOP                            \ -- CONTEXT+2
    DROP
    CR                              \ -- CONTEXT+2  
  REPEAT 
  DROP
  BASE !
;
    \

: U.R                       \ u n --           display u unsigned in n width (n >= 2)
  >R  <# 0 # #S #>  
  R> OVER - 0 MAX SPACES TYPE
;
    \

: DUMP                      \ adr n  --   dump memory
  BASE @ >R 0x10 BASE !
  SWAP 0xFFF0 AND SWAP
  OVER + SWAP
  DO  CR                    \ generate line
    I 7 U.R SPACE           \ generate address
      I 0x10 + I            \ display 16 bytes
      DO I C@ 3 U.R LOOP  
      SPACE SPACE
      I 0x10 + I            \ display 16 chars
      DO I C@ 0x7F AND 0x7E MIN BL MAX EMIT LOOP
  0x10 +LOOP
  R> BASE !
;

\ dump FAT sector of last entry
\ ----------------------------------\
CODE FAT                            \VWXY Display CurFATsector
\ ----------------------------------\
    MOV     &CurFATsector,W         \ W = FATsectorLO
BW1                                 \
    SUB     #2,PSP                  \
    MOV     TOS,0(PSP)              \
    MOV     W,TOS                   \
BW2                                 \
    BIT.B   #SD_CD,&SD_CDIN         \ memory card present ?
    MOV     #0,X                    \ X = FATsectorHI = SD_VIEWsectorHI = DIRsectorHI = 0
    CALL    &SD_READ                \ W = SectorLO  X = SectorHI
COLON                               \
    U.                              \ display the sector number
    0x1E00 0x200 DUMP CR            \ then dump the sector
;
    \

\ dump DIR sector of opened file or first sector of current DIR by default
\ ----------------------------------\
CODE DIR                            \TWXY Display DIR sector of CurrentHdl or CurrentDir sector by default 
\ ----------------------------------\
    MOV     &CurrentHdl,X           \
    CMP     #0,X                    \ handle ?
0<> IF                              \ 
    MOV HDLL_DIRsect(X),W           \ load DIRsector of handle
    MOV HDLH_DIRsect(X),X           \
ELSE  
    MOV     &DIRcluster,&Cluster    \ initialised at 1
\ ----------------------------------; Input : Cluster
\ ComputeClusFrstSect                 \ If Cluster = 1 ==> RootDirectory ==> SectorL = OrgRootDir
\ ----------------------------------; Output: SectorL of Cluster
    MOV     #0,X                    \
    MOV     &OrgRootDir,W           \
    CMP     #1,&Cluster             \ cluster 1 ?
    0<> IF                          \ yes, sectorL is done
        MOV &OrgClusters,W          \ OrgClusters = sector of virtual cluster 0
\        .IFDEF MPY
        MOV &Cluster,&MPY           \
        MOV &SecPerClus,&OP2        \
        ADD &RES0,W                 \
        ADDC &RES1,X                \
\        .ELSE
\        PUSHM TOS,Y                 \  R:-- TOS IP S T W X Y
\        PUSH @PSP                   \  R:-- TOS IP S T W X Y 0(PSP)
\        MOV &Cluster,0(PSP)         \
\        MOV &SecPerClus,TOS         \
\        LO2HI   
\           UM*                      \
\        HI2LO                       \
\        ADD @PSP,6(RSP)             \ add to W 
\        ADDC TOS,4(RSP)             \ addc to X
\        MOV @RSP+,0(PSP)            \
\        POPM Y,TOS                  \
\        .ENDIF
    THEN
THEN
    GOTO BW1
ENDCODE
    \
\ read sector and dump it
\ ----------------------------------\
CODE SD_VIEW                        \       0< sector <65536 --
\ ----------------------------------\
    MOV     TOS,W                   \ W = sectorLO
    GOTO BW2                        \
ENDCODE
\ ----------------------------------\
    \
ECHO
            ; added UTILITY : ? SP@ .S U.R WORDS DUMP 
            ; added : FAT DIR SD_VIEW
            ;    v--- use backspaces before hit "CR" if you want decrease level of app protection
PWR_HERE RST_HERE