; ----------------------------------------------------------------
; DOUBLE complement
; ----------------------------------------------------------------

\ REGISTERS USAGE
\ R4 to R7 must be saved before use and restored after
\ scratch registers Y to S are free for use
\ under interrupt, IP is free for use

\ PUSHM order : PSP,TOS, IP,  S,  T,  W,  X,  Y, R7, R6, R5, R4
\ example : PUSHM IP,Y
\
\ POPM  order :  R4, R5, R6, R7,  Y,  X,  W,  T,  S, IP,TOS,PSP
\ example : POPM Y,IP

\ ASSEMBLER conditionnal usage after IF UNTIL WHILE : S< S>= U< U>= 0= 0<> 0>=
\ ASSEMBLER conditionnal usage before GOTO ?GOTO     : S< S>= U< U>= 0= 0<> <0 

\ FORTH conditionnal usage after IF UNTIL WHILE : 0= 0< = < > U<

\ WIPE        ; remove all previous apps,
RST_STATE   ; preserve all previous apps,
NOECHO      ; if an error occurs, comment this line before new download to find it.
    \


\ ----------------------------------------------------------------------
\ DOUBLE OPERATORS
\ ----------------------------------------------------------------------

CODE 2@        \ a-addr -- x1 x2    fetch 2 cells \ the lower address will appear on top of stack
SUB     #2, PSP
MOV     2(TOS),0(PSP)
MOV     @TOS,TOS
MOV     @IP+,PC
ENDCODE
    \

CODE 2!         \ x1 x2 a-addr --    store 2 cells \ the top of stack is stored at the lower adr
MOV     @PSP+,0(TOS)
MOV     @PSP+,2(TOS)
MOV     @PSP+,TOS
MOV     @IP+,PC
ENDCODE
    \

CODE 2DUP       \ x1 x2 -- x1 x2 x1 x2   dup top 2 cells
SUB     #4,PSP          \ -- x1 x x x2
MOV     TOS,2(PSP)      \ -- x1 x2 x x2
MOV     4(PSP),0(PSP)   \ -- x1 x2 x1 x2
MOV     @IP+,PC
ENDCODE
    \

CODE 2DROP      \ x1 x2 --      drop 2 cells
ADD     #2,PSP
MOV     @PSP+,TOS
MOV     @IP+,PC
ENDCODE
    \

CODE 2SWAP      \ x1 x2 x3 x4 -- x3 x4 x1 x2
MOV     @PSP,W          \ -- x1 x2 x3 x4    W=x3
MOV     4(PSP),0(PSP)   \ -- x1 x2 x1 x4
MOV     W,4(PSP)        \ -- x3 x2 x1 x4
MOV     TOS,W           \ -- x3 x2 x1 x4    W=x4
MOV     2(PSP),TOS      \ -- x3 x2 x1 x2    W=x4
MOV     W,2(PSP)        \ -- x3 x4 x1 x2
MOV     @IP+,PC
ENDCODE
    \

CODE 2OVER      \ x1 x2 x3 x4 -- x1 x2 x3 x4 x1 x2
SUB     #4,PSP          \ -- x1 x2 x3 x x x4
MOV     TOS,2(PSP)      \ -- x1 x2 x3 x4 x x4
MOV     8(PSP),0(PSP)   \ -- x1 x2 x3 x4 x1 x4
MOV     6(PSP),TOS      \ -- x1 x2 x3 x4 x1 x2
MOV     @IP+,PC
ENDCODE
    \

ECHO
            ; added : 2@ 2! 2DUP 2DROP 2SWAP 2OVER
RST_HERE    ; the app is protected against <reset>,


