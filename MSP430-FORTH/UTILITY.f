\ UTILITY.f
\ must be preprocessed with yourtarget.pat file, for PSTACK,CONTEXT,NUMNUM

\ REGISTERS USAGE
\ R4 to R7 must be saved before use and restored after
\ scratch registers Y to S are free for use
\ under interrupt, IP is free for use

\ PUSHM order : PSP,TOS, IP,  S,  T,  W,  X,  Y, R7, R6, R5, R4
\ example : PUSHM IP,Y
\
\ POPM  order :  R4, R5, R6, R7,  Y,  X,  W,  T,  S, IP,TOS,PSP
\ example : POPM Y,IP

\ ASSEMBLER conditionnal usage after IF UNTIL WHILE : S< S>= U< U>= 0= 0<> 0>=
\ ASSEMBLER conditionnal usage before GOTO ?GOTO     : S< S>= U< U>= 0= 0<> <0 

\ FORTH conditionnal usage after IF UNTIL WHILE : 0= 0< = < > U<


RST_STATE
NOECHO      \ if an error occurs, comment this line before new download to find it.
    \

CODE ?          \ adr --            display the content of adr
    MOV @TOS,TOS
    MOV #U.,PC  \ goto U.
ENDCODE
    \

CODE SP@        \ -- SP
    SUB #2,PSP
    MOV TOS,0(PSP)
    MOV PSP,TOS
    MOV @IP+,PC
ENDCODE

: .S                \ --            print <number> of cells and stack contents if not empty
0x3C EMIT           \ --            char "<"
DEPTH .
8 EMIT              \               backspace
0x3E EMIT SPACE     \               char ">"
SP@  PSTACK OVER OVER U<    \
IF  2 -
    DO I @ U.
    -2 +LOOP
ELSE
    DROP DROP
THEN
;
    \

: WORDS                             \ --            list all words in all dicts.
  BASE @
  0x10 BASE !
  CONTEXT                           \ -- CONTEXT
  BEGIN                             \                                       search dictionnary
        DUP 
        2 + SWAP                    \ -- CONTEXT+2 CONTEXT
        @ ?DUP                      \ -- CONTEXT+2 (VOC_PFA VOC_PFA or 0)
  WHILE                             \ -- CONTEXT+2 VOC_PFA                  dictionnary found
    NUMNUM @ 0                      \ -- CONTEXT+2 VOC_PFA thread 0         for all threads
    DO                              \ -- CONTEXT+2 VOC_PFA
        CR NUMNUM @ 1 =             \                                       if thread = 1
        IF ."    "                  \                                       display nothing
        ELSE SPACE  I .             \                                       else display #thread
        THEN
        DUP                         \ -- CONTEXT+2 VOC_PFA VOC_PFA
        I 2* +                      \ -- CONTEXT+2 VOC_PFA VOC_PFA(x)
        BEGIN                       \                                       search word
            @ ?DUP                  \ -- CONTEXT+2 VOC_PFA (NFA NFA or 0)  
        WHILE                       \ -- CONTEXT+2 VOC_PFA NFA              word found
            DUP                 
                DUP
            COUNT 0x7F AND TYPE     \ -- CONTEXT+2 VOC_PFA NFA
                C@ 0x0F AND         
                0x10 SWAP - SPACES  
            2 -                     \ -- CONTEXT+2 VOC_PFA LFA
        REPEAT                      \ -- CONTEXT+2 VOC_PFA
    LOOP                            \ -- CONTEXT+2
    DROP
    CR                              \ -- CONTEXT+2  
  REPEAT 
  DROP
  BASE !
;
    \

: U.R                       \ u n --           display u unsigned in n width (n >= 2)
  >R  <# 0 # #S #>  
  R> OVER - 0 MAX SPACES TYPE
;
    \

: DUMP                      \ adr n  --   dump memory
  BASE @ >R 0x10 BASE !
  SWAP 0xFFF0 AND SWAP
  OVER + SWAP
  DO  CR                    \ generate line
    I 7 U.R SPACE           \ generate address
      I 0x10 + I            \ display 16 bytes
      DO I C@ 3 U.R LOOP  
      SPACE SPACE
      I 0x10 + I            \ display 16 chars
      DO I C@ 0x7F AND 0x7E MIN BL MAX EMIT LOOP
  0x10 +LOOP
  R> BASE !
;

ECHO
RST_HERE

