; ----------------------------------------------------------------
; ALIGMENT complement
; ----------------------------------------------------------------

\ REGISTERS USAGE
\ R4 to R7 must be saved before use and restored after
\ scratch registers Y to S are free for use
\ under interrupt, IP is free for use

\ PUSHM order : PSP,TOS, IP,  S,  T,  W,  X,  Y, R7, R6, R5, R4
\ example : PUSHM IP,Y
\
\ POPM  order :  R4, R5, R6, R7,  Y,  X,  W,  T,  S, IP,TOS,PSP
\ example : POPM Y,IP

\ ASSEMBLER conditionnal usage after IF UNTIL WHILE : S< S>= U< U>= 0= 0<> 0>=
\ ASSEMBLER conditionnal usage before GOTO ?GOTO     : S< S>= U< U>= 0= 0<> <0 

\ FORTH conditionnal usage after IF UNTIL WHILE : 0= 0< = < > U<

\ WIPE        ; remove all previous apps,
RST_STATE   ; preserve all previous apps,
NOECHO      ; if an error occurs, comment this line before new download to find it.
    \

\ ----------------------------------------------------------------------
\ ALIGNMENT OPERATORS
\ ----------------------------------------------------------------------

CODE ALIGNED    \ addr -- a-addr       align given addr
BIT     #1,TOS
ADDC    #0,TOS
MOV     @IP+,PC
ENDCODE
    \

CODE ALIGN      \ --                         align HERE
BIT     #1,&DP  \ 3
ADDC    #0,&DP  \ 4
MOV     @IP+,PC
ENDCODE
    \

ECHO
            ; added : ALIGNED ALIGN
RST_HERE    ; the app is protected against <reset>,


