; ----------------------------------------------------------------------------------
; ARITHMETIC for MSP430FR4xxx devices without hardware_MPY
; ----------------------------------------------------------------------------------

\ REGISTERS USAGE
\ R4 to R7 must be saved before use and restored after
\ scratch registers Y to S are free for use
\ under interrupt, IP is free for use

\ PUSHM order : PSP,TOS, IP,  S,  T,  W,  X,  Y, R7, R6, R5, R4
\ example : PUSHM IP,Y
\
\ POPM  order :  R4, R5, R6, R7,  Y,  X,  W,  T,  S, IP,TOS,PSP
\ example : POPM Y,IP

\ ASSEMBLER conditionnal usage after IF UNTIL WHILE : S< S>= U< U>= 0= 0<> 0>=
\ ASSEMBLER conditionnal usage before GOTO ?GOTO     : S< S>= U< U>= 0= 0<> <0 

\ FORTH conditionnal usage after IF UNTIL WHILE : 0= 0< = < > U<

\ WIPE        ; remove all previous apps,
RST_STATE   ; preserve all previous apps,
NOECHO      ; if an error occurs, comment this line before new download to find it.
    \

\ --------------------
\ ARITHMETIC OPERATORS
\ --------------------

CODE NIP        \ a b c -- a c
ADD #2,PSP
MOV @IP+,PC
ENDCODE
    \

: S>D           \ n -- d      single -> double
    DUP 0<
;
    \

\ \ C UM*     u1 u2 -- ud   unsigned 16x16->32 mult.
\ CODE UM*
\             MOV @PSP,S
\ \ u2          = TOS register
\ \ MULTIPLIERl = S
\ \ MULTIPLIERh = W
\ \ BIT         = X
\ \ RESULTlo    = Y
\ \ RESULThi    = T
\ \ T.I. SIGNED MULTIPLY SUBROUTINE: u2 x u1 -> ud
\             MOV #0,Y        \  0 -> LSBs RESULT
\             MOV #0,T        \  0 -> MSBs RESULT
\             MOV #0,W        \  0 -> MSBs MULTIPLIER
\             MOV #1,X        \  BIT TEST REGISTER
\ BEGIN       BIT X,TOS       \ 1 TEST ACTUAL BIT ; IF 0: DO NOTHING
\     0<> IF                  \ 2 IF 1: ADD MULTIPLIER TO RESULT
\             ADD S,Y         \ 1 
\             ADDC W,T        \ 1
\     THEN    ADD S,S         \ 1 (RLA LSBs) MULTIPLIER x 2
\             ADDC W,W        \ 1 (RLC MSBs)
\             ADD X,X         \ 1 (RLA) NEXT BIT TO TEST
\ U>= UNTIL                   \ 2 IF BIT IN CARRY: FINISHED    10~ loop
\             MOV Y,0(PSP)    \  low result on stack
\             MOV T,TOS       \  high result in TOS
\             MOV @IP+,PC
\ ENDCODE
\     \

CODE M*             \ n1 n2 -- dlo dhi  signed 16*16->32 multiply             
MOV TOS,S         \ TOS= n2
XOR @PSP,S        \ S contains sign of result
CMP #0,0(PSP)       \ n1 > -1 ?
S< IF
    XOR #-1,0(PSP)  \ n1 --> u1
    ADD #1,0(PSP)   \
THEN
CMP #0,TOS          \ n2 > -1 ?
S< IF
    XOR #-1,TOS     \ n2 --> u2 
    ADD #1,TOS      \
THEN
PUSHM IP,S
LO2HI               \ -- ud1 u2
UM*                 \ UMSTAR use S,T,W,X,Y
HI2LO
POPM S,IP
CMP #0,S          \ sign of result > -1 ?
S< IF
    XOR #-1,0(PSP)  \ ud --> d
    XOR #-1,TOS
    ADD #1,0(PSP)
    ADDC #0,TOS
THEN
MOV     @IP+,PC
ENDCODE
    \

\ TOS = DIVISOR
\ S   = DIVIDENDlo
\ W   = DIVIDENDhi
\ X   = count
\ Y   = QUOTIENT
\ T.I. UNSIGNED DIVISION SUBROUTINE 32-BIT BY 16-BIT
\ DVDhi|DVDlo : DIVISOR -> QUOT in Y, REM in DVDhi
\ RETURN: CARRY = 0: OK CARRY = 1: QUOTIENT > 16 BITS

\ C UM/MOD   udlo|udhi u1 -- ur uq
CODE UM/MOD
    MOV @PSP+,W     \ W = DIVIDENDhi
    MOV @PSP,S      \ S = DIVIDENDlo
    MOV #0,Y        \  CLEAR RESULT
    MOV #16,X       \  INITIALIZE LOOP COUNTER
BW1 CMP TOS,W       \ 1 dividendHI-divisor
    U< ?GOTO FW1    \ 2 if not carry
    SUB TOS,W       \ 1 if carry
FW1 
BW2 ADDC Y,Y        \ 1 RLC quotient
    U>= ?GOTO FW2   \ 2 if carry Error: result > 16 bits
    SUB #1,X        \ 1 Decrement loop counter
    0< ?GOTO FW1    \ 2 if <0: terminate w/o error
    ADD S,S         \ 1 RLA
    ADDC W,W        \ 1 RLC
    U< ?GOTO BW1    \ 2 if not carry    14~ loop
    SUB TOS,W       \ 1
    BIS #1,SR       \ 1 SETC
    GOTO BW2        \ 2                 14~ loop
FW1 BIC #1,SR       \  CLRC  No error, C = 0
FW2                 \  Error indication in C
\ END T.I. ROUTINE  Section 5.1.5 of MSP430 Family Application Reports
    MOV W,0(PSP)    \ remainder on stack
    MOV Y,TOS       \ quotient in TOS
    MOV @IP+,PC
ENDCODE
    \

CODE SM/REM         \ d1lo d1hi n2 -- n3 n4  symmetric signed div
MOV TOS,S         \            S=divisor
MOV @PSP,T        \            T=dividend_sign=rem_sign
CMP #0,TOS          \            n2 >= 0 ?
S< IF               \
    XOR #-1,TOS
    ADD #1,TOS      \ -- d1 u2
THEN
CMP #0,0(PSP)       \           d1hi >= 0 ?
S< IF               \
    XOR #-1,2(PSP)  \           d1lo
    XOR #-1,0(PSP)  \           d1hi
    ADD #1,2(PSP)   \           d1lo+1
    ADDC #0,0(PSP)  \           d1hi+C
THEN
PUSHM IP,S
LO2HI               \ -- ud1 u2
UM/MOD              \           UM/MOD use S,W,X,Y, not T
HI2LO               \ -- u3 u4
POPM S,IP
CMP #0,T          \           T=rem_sign
S< IF
    XOR #-1,0(PSP)
    ADD #1,0(PSP)
THEN                \ -- n3 u4
XOR S,T         \           S=divisor T=quot_sign
CMP #0,T          \           T=quot_sign
S< IF
    XOR #-1,TOS
    ADD #1,TOS
THEN                \ -- n3 n4  S=divisor
MOV @IP+,PC
ENDCODE
    \


: FM/MOD            \ d1 n1 -- n2 n3   floored signed div'n
SM/REM
HI2LO               \ -- remainder quotient       S=divisor
CMP #0,0(PSP)       \
0<> IF
    CMP #1,TOS      \ quotient < 1 ?
    S< IF
      ADD S,0(PSP)  \ add divisor to remainder
      SUB #1,TOS    \ decrement quotient
    THEN
THEN
MOV @RSP+,IP
MOV @IP+,PC
ENDCODE
    \

: *         \ n1 n2 -- n3           n1*n2 --> n3
M* DROP
;
    \

: /MOD      \ n1 n2 -- n3 n4        n1/n2 --> rem quot
>R DUP 0< R> FM/MOD
;
    \

: /         \ n1 n2 -- n3           n1/n2 --> quot
>R DUP 0< R> FM/MOD NIP
;
    \

: MOD       \ n1 n2 -- n3           n1/n2 --> rem
>R DUP 0< R> FM/MOD DROP
;
    \

: */MOD     \ n1 n2 n3 -- n4 n5     n1*n2/n3 --> rem quot
>R M* R> FM/MOD
;
    \

: */        \ n1 n2 n3 -- n4        n1*n2/n3 --> quot
>R M* R> FM/MOD NIP
;
    \

ECHO
            ; added : NIP S>D M* UM/MOD SM/REM FM/MOD * /MOD / MOD */MOD */
RST_HERE    ; the app is protected against <reset>,
