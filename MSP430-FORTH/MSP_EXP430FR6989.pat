! -*- coding: utf-8 -*-
! MSP_EXP430FR6989.pat
!
! Fast Forth For Texas Instrument MSP_EXP430FR6989
!
! Copyright (C) <2016>  <J.M. THOORENS>
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>.
!
!
!
! ======================================================================
! MSP430FR6989 Config
! ======================================================================

@define{@read{/config/gema/MSP430FR6989.pat}}
@define{@read{/config/gema/MSP430FR_FastForth.pat}}
@define{@read{/config/gema/FastForthREGtoTI.pat}}
@define{@read{/config/gema/RemoveComments.pat}}

! ======================================================================
! MSP_EXP430FR6989 board
! ======================================================================

! ---------------------------------------------------
! MSP  - MSP-EXP430FR6989 LAUNCHPAD <--> OUTPUT WORLD
! ---------------------------------------------------
! P1.0 - LED1 red
! P9.7 - LED2 green
!
! P1.1 - Switch S1              <--- LCD contrast + (finger :-)
! P1.2 - Switch S2              <--- LCD contrast - (finger ;-)
!                                   
! note : ESI1.1 = lowest left pin
! note : ESI1.2 is not connected to 3.3V
!  GND                     J6.2 <-------+---0V0---------->  1 LCD_Vss
!  VCC                     J6.1 >------ | --3V3-----+---->  2 LCD_Vdd
!                                       |           |
!                                     |___    470n ---
!                                       ^ |        ---
!                                      / \ BAT54    |
!                                      ---          |
!                                  100n |    2k2    |
! P3.6 - UCA1 CLK TB0.2 J4.37   >---||--+--^/\/\/v--+---->  3 LCD_Vo (=0V6 without modulation)
! P9.0/ESICH0 -         ESI1.14 <------------------------> 11 LCD_DB4 brown
! P9.1/ESICH1 -         ESI1.13 <------------------------> 12 LCD_DB5 red
! P9.2/ESICH2 -         ESI1.12 <------------------------> 13 LCD_DB5 orange
! P9.3/ESICH3 -         ESI1.11 <------------------------> 14 LCD_DB7 yellow
! P4.1                          ------------------------->  4 LCD_RS  yellow
! P4.2                          ------------------------->  5 LCD_R/W green
! P4.3                          ------------------------->  6 LCD_EN  blue
!
!                                 +--4k7-< DeepRST <-- GND 
!                                 |
! P3.4 - UCA1 TXD       J101.8  <-+-> RX  UARTtoUSB bridge
! P3.5 - UCA1 RXD       J101.10 <---- TX  UARTtoUSB bridge
! P3.0 - RTS            J101.14 ----> CTS UARTtoUSB bridge (optional hardware control flow)
!  VCC -                J101.16 <---- VCC (optional supply from UARTtoUSB bridge - WARNING ! 3.3V !)
!  GND -                J101.20 <---> GND (optional supply from UARTtoUSB bridge)
!
!  VCC -                J1.1    ----> VCC SD_CardAdapter
!  GND -                J2.20   <---> GND SD_CardAdapter
! P2.2 -  UCA0 CLK      J4.35   ----> CLK SD_CardAdapter (SCK)  
! P2.6 -                J4.39   ----> CS  SD_CardAdapter (Card Select)
! P2.0 -  UCA0 TXD/SIMO J1.8    ----> SDI SD_CardAdapter (MOSI)
! P2.1 -  UCA0 RXD/SOMI J2.19   <---- SDO SD_CardAdapter (MISO)
! P2.7 -                J4.40   <---- CD  SD_CardAdapter (Card Detect)
!
! P4.0 -                J1.10   <---- OUT IR_Receiver (1 TSOP32236)
!  VCC -                J1.1    ----> VCC IR_Receiver (2 TSOP32236)
!  GND -                J2.20   <---> GND IR_Receiver (3 TSOP32236)
!
! P1.3 -                J4.34   <---> SDA software I2C Master
! P1.5 -                J2.18   ----> SCL software I2C Master
!
! P1.4 -UCB0 CLK TA1.0  J1.7    <---> free
!
! P1.6 -UCB0 SDA/SIMO   J2.15   <---> SDA hardware I2C Master or Slave
! P1.7 -UCB0 SCL/SOMI   J2.14   ----> SCL hardware I2C Master or Slave
!
! P3.0 -UCB1 CLK        J4.33   ----> free (if UARTtoUSB with software control flow)
! P3.1 -UCB1 SDA/SIMO   J4.32   <---> free
! P3.2 -UCB1 SCL/SOMI   J1.5    ----> free
! P3.3 -         TA1.1  J1.5    <---> free
!
! PJ.4 - LFXI 32768Hz quartz  
! PJ.5 - LFXO 32768Hz quartz  
! PJ.6 - HFXI 
! PJ.7 - HFXO 


LED1_OUT=0x202!
LED1=0x01!      P1.0

LED2_OUT=0x282!
LED2=0x80!      P9.7

SW1_IN=0x200!
SW1=0x02!       P1.1

SW2_IN=0x200!
SW2=0x04!       P1.2

LCDVo_DIR=0x224!
LCDVo_SEL=0x22C!  SEL1
LCDVo=0x40!     P3.6

LCD_CMD_IN=0x221!
LCD_CMD_OUT=0x223!
LCD_CMD_DIR=0x225!
LCD_CMD_REN=0x227!
LCD_RS=0x02!    P4.1
LCD_RW=0x04!    P4.2
LCD_EN=0x08!    P4.3
LCD_CMD=0x0E!

LCD_DB_IN=0x280!
LCD_DB_OUT=0x282!
LCD_DB_DIR=0x284!
LCD_DB_REN=0x286!
LCD_DB=0x0F!    P9.3-0


IR_IN=0x221!  
IR_OUT=0x223! 
IR_DIR=0x225! 
IR_REN=0x227! 
IR_IES=0x239!
IR_IE=0x23B!
IR_IFG=0x23D!
RC5_=RC5_!
RC5=0x01!       P4.0
IR_Vec=0xFFCC!    P4 int

I2CSM_IN=0x200!
I2CSM_OUT=0x202!
I2CSM_DIR=0x204!
I2CSM_REN=0x206!
SMSDA=0x08!     P1.3
SMSCL=0x20!     P1.5
SM_BUS=0x28!     

I2CSMM_IN=0x200!
I2CSMM_OUT=0x202!
I2CSMM_DIR=0x204!
I2CSMM_REN=0x206!
SMMSDA=0x08!    P1.3
SMMSCL=0x20!    P1.5
SMM_BUS=0x28!    

I2CMM_IN=0x200!
I2CMM_OUT=0x202!
I2CMM_DIR=0x204!
I2CMM_REN=0x206!
I2CMM_SEL=0x20C!  SEL1
I2CMM_Vec=0xFFEC!
MMSDA=0x40!     P1.6
MMSCL=0x80!     P1.7
MM_BUS=0xC0!    

I2CM_IN=0x200!
I2CM_OUT=0x202!
I2CM_DIR=0x204!
I2CM_REN=0x206!
I2CM_SEL=0x20C!
I2CM_Vec=0xFFEC!
MSDA=0x40!      P1.6
MSCL=0x80!      P1.7
M_BUS=0xC0!    

I2CS_IN=0x200!
I2CS_OUT=0x202!
I2CS_DIR=0x204!
I2CS_REN=0x206!
I2CS_SEL=0x20C!
I2CS_Vec=0xFFEC!
SSDA=0x40!      P1.6
SSCL=0x80!      P1.7
S_BUS=0xC0!

