; ----------------------------------------------------------------
; ARITHMETIC complement for devices with hardware_MPY
; ----------------------------------------------------------------

\ REGISTERS USAGE
\ R4 to R7 must be saved before use and restored after
\ scratch registers Y to S are free for use
\ under interrupt, IP is free for use

\ PUSHM order : PSP,TOS, IP,  S,  T,  W,  X,  Y, R7, R6, R5, R4
\ example : PUSHM IP,Y
\
\ POPM  order :  R4, R5, R6, R7,  Y,  X,  W,  T,  S, IP,TOS,PSP
\ example : POPM Y,IP

\ ASSEMBLER conditionnal usage after IF UNTIL WHILE : S< S>= U< U>= 0= 0<> 0>=
\ ASSEMBLER conditionnal usage before GOTO ?GOTO     : S< S>= U< U>= 0= 0<> 0< 

\ FORTH conditionnal usage after IF UNTIL WHILE : 0= 0< = < > U<

\ WIPE        ; remove all previous apps,
RST_STATE   ; preserve all previous apps,
NOECHO      ; if an error occurs, comment this line before new download to find it.
    \

\ --------------------
\ ARITHMETIC OPERATORS
\ --------------------

CODE NIP        \ a b c -- a c
ADD #2,PSP
MOV @IP+,PC
ENDCODE
    \

: S>D           \ n -- d      single -> double
    DUP 0<
;
    \


CODE UM*        \ u1 u2 -- udlo udhi    unsigned 16x16->32 mult.
MOV @PSP,&MPY       \ Load 1st operand
MOV TOS,&OP2        \ Load 2nd operand
MOV &RES0,0(PSP)    \ low result on stack
MOV &RES1,TOS       \ high result in TOS
MOV @IP+,PC
ENDCODE
    \

CODE M*         \ n1 n2 -- dlo dhi      signed 16*16->32 multiply
MOV @PSP,&MPYS
MOV TOS,&OP2
MOV &RES0,0(PSP)
MOV &RES1,TOS
MOV @IP+,PC
ENDCODE
    \


\ TOS = DIVISOR
\ S   = DIVIDENDlo
\ W   = DIVIDENDhi
\ X   = count
\ Y   = QUOTIENT
\ T.I. UNSIGNED DIVISION SUBROUTINE 32-BIT BY 16-BIT
\ DVDhi|DVDlo : DIVISOR -> QUOT in Y, REM in DVDhi
\ RETURN: CARRY = 0: OK CARRY = 1: QUOTIENT > 16 BITS

\ C UM/MOD   udlo|udhi u1 -- ur uq
CODE UM/MOD
    MOV @PSP+,W     \ 2 W = DIVIDENDhi
    MOV @PSP,S      \ 2 S = DIVIDENDlo
    MOV #0,Y        \ 1 CLEAR RESULT
    MOV #16,X       \ 2 INITIALIZE LOOP COUNTER
BW1 CMP TOS,W       \ 1 dividendHI-divisor
    U< ?GOTO FW1    \ 2 if not carry
    SUB TOS,W       \ 1 if carry
FW1                 \   FW1 is resolved therefore reusable
BW2 ADDC Y,Y        \ 1 RLC quotient
    U>= ?GOTO FW1   \ 2 if carry Error: result > 16 bits
    SUB #1,X        \ 1 Decrement loop counter
    0< ?GOTO FW2    \ 2 if 0< terminate w/o error
    ADD S,S         \ 1 RLA
    ADDC W,W        \ 1 RLC
    U< ?GOTO BW1    \ 2 if not carry    14~ loop
    SUB TOS,W       \ 1
    BIS #1,SR       \ 1 SETC
    GOTO BW2        \ 2                 14~ loop
FW2 BIC #1,SR       \ 1 CLRC  No error, C = 0
FW1                 \  Error indication in C
\ END T.I. ROUTINE  Section 5.1.5 of MSP430 Family Application Reports
    MOV W,0(PSP)    \ 3 remainder on stack
    MOV Y,TOS       \ 1 quotient in TOS
    MOV @IP+,PC     \ 4
ENDCODE
    \

CODE SM/REM         \ d1 n2 -- n3 n4    symmetric signed div
MOV TOS,S           \           S=divisor
MOV @PSP,T          \           T=dividend_sign=rem_sign
CMP #0,TOS          \           n2 >= 0 ?
S< IF               \
    XOR #-1,TOS
    ADD #1,TOS      \ -- d1 u2
THEN
CMP #0,0(PSP)       \           d1hi >= 0 ?
S< IF               \
    XOR #-1,2(PSP)  \           d1lo
    XOR #-1,0(PSP)  \           d1hi
    ADD #1,2(PSP)   \           d1lo+1
    ADDC #0,0(PSP)  \           d1hi+C
THEN                \ -- ud1 u2
PUSHM IP,S          \  
LO2HI               \
UM/MOD              \           UM/MOD use S,W,X,Y, not T
HI2LO               \ -- u3 u4
POPM S,IP           \  
CMP #0,T            \           T=rem_sign
S< IF
    XOR #-1,0(PSP)
    ADD #1,0(PSP)
THEN
XOR S,T             \           S=divisor T=quot_sign
CMP #0,T            \ -- n3 u4  T=quot_sign
S< IF
    XOR #-1,TOS
    ADD #1,TOS
THEN                \ -- n3 n4  S=divisor
MOV @IP+,PC
ENDCODE
    \


: FM/MOD            \ d1 n2 -- n3 n4   floored signed div
SM/REM
HI2LO               \ -- remainder quotient       S=divisor
CMP #0,0(PSP)       \ remainder <> 0 ?
0<> IF
    CMP #1,TOS      \ quotient < 1 ?
    S< IF
      ADD S,0(PSP)  \ add divisor to remainder
      SUB #1,TOS    \ decrement quotient
    THEN
THEN
MOV @RSP+,IP
MOV @IP+,PC
ENDCODE
    \

: *         \ n1 n2 -- n3           n1*n2 --> n3
M* DROP
;
    \

: /MOD      \ n1 n2 -- n3 n4        n1/n2 --> rem quot
>R DUP 0< R> FM/MOD
;
    \

: /         \ n1 n2 -- n3           n1/n2 --> quot
>R DUP 0< R> FM/MOD NIP
;
    \

: MOD       \ n1 n2 -- n3           n1/n2 --> rem
>R DUP 0< R> FM/MOD DROP
;
    \

: */MOD     \ n1 n2 n3 -- n4 n5     n1*n2/n3 --> rem quot
>R M* R> FM/MOD
;
    \

: */        \ n1 n2 n3 -- n4        n1*n2/n3 --> quot
>R M* R> FM/MOD NIP
;
    \

ECHO
            ; added : NIP S>D UM* M* UM/MOD SM/REM FM/MOD * /MOD / MOD */MOD */
RST_HERE    ; the app is protected against <reset>,


