; ---------
; prog10k.f
; ---------
RST_STATE   ; reset all previous apps,
NOECHO      ; if an error occurs during download, comment this line then download again
VARIABLE I2CS_ADR
2 ALLOT
VARIABLE I2CM_BUF
2 ALLOT

CODE I2C_MTX
BEGIN
    ADD.B   R9,R9
    U>= IF
        BIC.B #MSDA,&I2CSM_DIR
    ELSE
        BIS.B #MSDA,&I2CSM_DIR
    THEN
    BIC.B #MSCL,&I2CSM_DIR
    BEGIN
        BIT.B #MSCL,&I2CSM_IN
    0<> UNTIL
    BIS.B #MSCL,&I2CSM_DIR
    SUB #1,R10
0= UNTIL
BIC.B   #MSDA,&I2CSM_DIR
MOV @R1+,R0
ENDCODE

CODE INT_RC5
BIC #0xF8,0(R1)
    MOV     #0,&0x360
    MOV     #1778,R9
    MOV     #14,R10
BEGIN
    MOV #0b1011100100,&0x340
    RRUM    #1,R9
    MOV     R9,R8
    RRUM    #1,R8
    ADD     R9,R8
    BEGIN   CMP R8,&0x350
    0= UNTIL
    BIT.B   #RC5,&IR_IN
    ADDC    R13,R13
    MOV     &IR_IN,&IR_IES
    BIC.B   #RC5,&IR_IFG
    SUB     #1,R10
0<> WHILE
    ADD     R9,R8
    BEGIN
        CMP R8,&0x350
        0>= IF
          BIC #0x30,&0x340
          RETI
        THEN
        BIT.B #RC5,&IR_IFG
    0<> UNTIL
    MOV &0x350,R9
REPEAT
    MOV #0x30,&0x340
    RLAM #1,R13
    MOV @R1,R9
    RLAM #4,R9
    XOR R13,R9
    BIT #0x2000,R9
    0= IF RETI
    THEN
    XOR #0x200,0(R1)
    MOV.B R13,R12
    RRUM #2,R12
    BIT #0x4000,R13
    0= IF   BIS #0x40,R12
    THEN
    SWPB R12
    ADD #1,R12
    MOV R12,&I2CM_BUF
    MOV #0b0010100,&I2CS_ADR
    BIC.B #M_BUS,&I2CSM_DIR
    BIC.B #M_BUS,&I2CSM_OUT
    MOV #2,R11
    BEGIN
        BEGIN
            BEGIN
               BIT.B #MSCL,&I2CSM_IN
            0<> UNTIL
            BIT.B #MSDA,&I2CSM_IN
        0<> UNTIL
            SUB #1,R11
    0= UNTIL
BIS.B   #MSDA,&I2CSM_DIR
MOV     #I2CM_BUF,R10
MOV.B   @R10+,R11
MOV     #I2CS_ADR,R12
MOV.B   @R12+,R9
BIT.B   #1,R9
0= IF
    MOV R10,R12
THEN
BIS.B   #MSCL,&I2CSM_DIR
ADD     #1,R11
MOV     #8,R10
CALL    #I2C_MTX
BEGIN
    BIC.B   #MSCL,&I2CSM_DIR
    BEGIN
        BIT.B #MSCL,&I2CSM_IN
    0<> UNTIL
    BIT.B   #MSDA,&I2CSM_IN
    BIS.B   #MSCL,&I2CSM_DIR
    0<> IF  BIS #2,R2
    ELSE    SUB.B #1,R11
    THEN
    0= IF
        BIS.B #MSDA,&I2CSM_DIR
        SUB.B R11,&I2CM_BUF
        BIC.B #MSCL,&I2CSM_DIR
        BEGIN
            BIT.B #MSCL,&I2CSM_IN
        0<> UNTIL
        BIC.B #MSDA,&I2CSM_DIR
        RETI
    THEN
    MOV.B #8,R10
    BIT.B #1,&I2CS_ADR
    0= IF
        MOV.B @R12+,R9
        CALL #I2C_MTX
    ELSE
        BIC.B #MSDA,&I2CSM_DIR
        BEGIN
            BIC.B #MSCL,&I2CSM_DIR
            BEGIN
                BIT.B #MSCL,&I2CSM_IN
            0<> UNTIL
            BIT.B #MSDA,&I2CSM_IN
            BIS.B #MSCL,&I2CSM_DIR
            ADDC.B R9,R9
            SUB #1,R10
        0= UNTIL
        MOV.B R9,0(R12)
        ADD #1,R12
        CMP.B #1,R11
        0<> IF
            BIS.B #MSDA,&I2CSM_DIR
        THEN
    THEN
AGAIN
ENDCODE

CODE START
    BIC.B #M_BUS,&I2CSM_OUT
    BIC.B #M_BUS,&I2CSM_REN
    BIS.B #RC5,&IR_IE
    BIC.B #RC5,&IR_IFG
    MOV #INT_RC5,&IR_Vec
    LO2HI
    ." RC5toI2CF_Master is running. Type STOP to quit"
    LIT RECURSE IS WARM
    (WARM) ;

CODE STOP
MOV #WARM,R8     
MOV #(WARM),2(R8)
MOV #COLD,R0
ENDCODE

RST_STATE
VARIABLE I2CS_ADR
2 ALLOT
VARIABLE I2CM_BUF
2 ALLOT

CODE I2C_MTX
BEGIN
    ADD.B   R9,R9
    U>= IF
        BIC.B #MSDA,&I2CSM_DIR
    ELSE
        BIS.B #MSDA,&I2CSM_DIR
    THEN
    BIC.B #MSCL,&I2CSM_DIR
    BEGIN
        BIT.B #MSCL,&I2CSM_IN
    0<> UNTIL
    BIS.B #MSCL,&I2CSM_DIR
    SUB #1,R10
0= UNTIL
BIC.B   #MSDA,&I2CSM_DIR
MOV @R1+,R0
ENDCODE

CODE INT_RC5
BIC #0xF8,0(R1)
    MOV     #0,&0x360
    MOV     #1778,R9
    MOV     #14,R10
BEGIN
    MOV #0b1011100100,&0x340
    RRUM    #1,R9
    MOV     R9,R8
    RRUM    #1,R8
    ADD     R9,R8
    BEGIN   CMP R8,&0x350
    0= UNTIL
    BIT.B   #RC5,&IR_IN
    ADDC    R13,R13
    MOV     &IR_IN,&IR_IES
    BIC.B   #RC5,&IR_IFG
    SUB     #1,R10
0<> WHILE
    ADD     R9,R8
    BEGIN
        CMP R8,&0x350
        0>= IF
          BIC #0x30,&0x340
          RETI
        THEN
        BIT.B #RC5,&IR_IFG
    0<> UNTIL
    MOV &0x350,R9
REPEAT
    MOV #0x30,&0x340
    RLAM #1,R13
    MOV @R1,R9
    RLAM #4,R9
    XOR R13,R9
    BIT #0x2000,R9
    0= IF RETI
    THEN
    XOR #0x200,0(R1)
    MOV.B R13,R12
    RRUM #2,R12
    BIT #0x4000,R13
    0= IF   BIS #0x40,R12
    THEN
    SWPB R12
    ADD #1,R12
    MOV R12,&I2CM_BUF
    MOV #0b0010100,&I2CS_ADR
    BIC.B #M_BUS,&I2CSM_DIR
    BIC.B #M_BUS,&I2CSM_OUT
    MOV #2,R11
    BEGIN
        BEGIN
            BEGIN
               BIT.B #MSCL,&I2CSM_IN
            0<> UNTIL
            BIT.B #MSDA,&I2CSM_IN
        0<> UNTIL
            SUB #1,R11
    0= UNTIL
BIS.B   #MSDA,&I2CSM_DIR
MOV     #I2CM_BUF,R10
MOV.B   @R10+,R11
MOV     #I2CS_ADR,R12
MOV.B   @R12+,R9
BIT.B   #1,R9
0= IF
    MOV R10,R12
THEN
BIS.B   #MSCL,&I2CSM_DIR
ADD     #1,R11
MOV     #8,R10
CALL    #I2C_MTX
BEGIN
    BIC.B   #MSCL,&I2CSM_DIR
    BEGIN
        BIT.B #MSCL,&I2CSM_IN
    0<> UNTIL
    BIT.B   #MSDA,&I2CSM_IN
    BIS.B   #MSCL,&I2CSM_DIR
    0<> IF  BIS #2,R2
    ELSE    SUB.B #1,R11
    THEN
    0= IF
        BIS.B #MSDA,&I2CSM_DIR
        SUB.B R11,&I2CM_BUF
        BIC.B #MSCL,&I2CSM_DIR
        BEGIN
            BIT.B #MSCL,&I2CSM_IN
        0<> UNTIL
        BIC.B #MSDA,&I2CSM_DIR
        RETI
    THEN
    MOV.B #8,R10
    BIT.B #1,&I2CS_ADR
    0= IF
        MOV.B @R12+,R9
        CALL #I2C_MTX
    ELSE
        BIC.B #MSDA,&I2CSM_DIR
        BEGIN
            BIC.B #MSCL,&I2CSM_DIR
            BEGIN
                BIT.B #MSCL,&I2CSM_IN
            0<> UNTIL
            BIT.B #MSDA,&I2CSM_IN
            BIS.B #MSCL,&I2CSM_DIR
            ADDC.B R9,R9
            SUB #1,R10
        0= UNTIL
        MOV.B R9,0(R12)
        ADD #1,R12
        CMP.B #1,R11
        0<> IF
            BIS.B #MSDA,&I2CSM_DIR
        THEN
    THEN
AGAIN
ENDCODE

CODE START
    BIC.B #M_BUS,&I2CSM_OUT
    BIC.B #M_BUS,&I2CSM_REN
    BIS.B #RC5,&IR_IE
    BIC.B #RC5,&IR_IFG
    MOV #INT_RC5,&IR_Vec
    LO2HI
    ." RC5toI2CF_Master is running. Type STOP to quit"
    LIT RECURSE IS WARM
    (WARM) ;

CODE STOP
MOV #WARM,R8     
MOV #(WARM),2(R8)
MOV #COLD,R0
ENDCODE

RST_STATE
VARIABLE I2CS_ADR
2 ALLOT
VARIABLE I2CM_BUF
2 ALLOT

CODE I2C_MTX
BEGIN
    ADD.B   R9,R9
    U>= IF
        BIC.B #MSDA,&I2CSM_DIR
    ELSE
        BIS.B #MSDA,&I2CSM_DIR
    THEN
    BIC.B #MSCL,&I2CSM_DIR
    BEGIN
        BIT.B #MSCL,&I2CSM_IN
    0<> UNTIL
    BIS.B #MSCL,&I2CSM_DIR
    SUB #1,R10
0= UNTIL
BIC.B   #MSDA,&I2CSM_DIR
MOV @R1+,R0
ENDCODE

CODE INT_RC5
BIC #0xF8,0(R1)
    MOV     #0,&0x360
    MOV     #1778,R9
    MOV     #14,R10
BEGIN
    MOV #0b1011100100,&0x340
    RRUM    #1,R9
    MOV     R9,R8
    RRUM    #1,R8
    ADD     R9,R8
    BEGIN   CMP R8,&0x350
    0= UNTIL
    BIT.B   #RC5,&IR_IN
    ADDC    R13,R13
    MOV     &IR_IN,&IR_IES
    BIC.B   #RC5,&IR_IFG
    SUB     #1,R10
0<> WHILE
    ADD     R9,R8
    BEGIN
        CMP R8,&0x350
        0>= IF
          BIC #0x30,&0x340
          RETI
        THEN
        BIT.B #RC5,&IR_IFG
    0<> UNTIL
    MOV &0x350,R9
REPEAT
    MOV #0x30,&0x340
    RLAM #1,R13
    MOV @R1,R9
    RLAM #4,R9
    XOR R13,R9
    BIT #0x2000,R9
    0= IF RETI
    THEN
    XOR #0x200,0(R1)
    MOV.B R13,R12
    RRUM #2,R12
    BIT #0x4000,R13
    0= IF   BIS #0x40,R12
    THEN
    SWPB R12
    ADD #1,R12
    MOV R12,&I2CM_BUF
    MOV #0b0010100,&I2CS_ADR
    BIC.B #M_BUS,&I2CSM_DIR
    BIC.B #M_BUS,&I2CSM_OUT
    MOV #2,R11
    BEGIN
        BEGIN
            BEGIN
               BIT.B #MSCL,&I2CSM_IN
            0<> UNTIL
            BIT.B #MSDA,&I2CSM_IN
        0<> UNTIL
            SUB #1,R11
    0= UNTIL
BIS.B   #MSDA,&I2CSM_DIR
MOV     #I2CM_BUF,R10
MOV.B   @R10+,R11
MOV     #I2CS_ADR,R12
MOV.B   @R12+,R9
BIT.B   #1,R9
0= IF
    MOV R10,R12
THEN
BIS.B   #MSCL,&I2CSM_DIR
ADD     #1,R11
MOV     #8,R10
CALL    #I2C_MTX
BEGIN
    BIC.B   #MSCL,&I2CSM_DIR
    BEGIN
        BIT.B #MSCL,&I2CSM_IN
    0<> UNTIL
    BIT.B   #MSDA,&I2CSM_IN
    BIS.B   #MSCL,&I2CSM_DIR
    0<> IF  BIS #2,R2
    ELSE    SUB.B #1,R11
    THEN
    0= IF
        BIS.B #MSDA,&I2CSM_DIR
        SUB.B R11,&I2CM_BUF
        BIC.B #MSCL,&I2CSM_DIR
        BEGIN
            BIT.B #MSCL,&I2CSM_IN
        0<> UNTIL
        BIC.B #MSDA,&I2CSM_DIR
        RETI
    THEN
    MOV.B #8,R10
    BIT.B #1,&I2CS_ADR
    0= IF
        MOV.B @R12+,R9
        CALL #I2C_MTX
    ELSE
        BIC.B #MSDA,&I2CSM_DIR
        BEGIN
            BIC.B #MSCL,&I2CSM_DIR
            BEGIN
                BIT.B #MSCL,&I2CSM_IN
            0<> UNTIL
            BIT.B #MSDA,&I2CSM_IN
            BIS.B #MSCL,&I2CSM_DIR
            ADDC.B R9,R9
            SUB #1,R10
        0= UNTIL
        MOV.B R9,0(R12)
        ADD #1,R12
        CMP.B #1,R11
        0<> IF
            BIS.B #MSDA,&I2CSM_DIR
        THEN
    THEN
AGAIN
ENDCODE

CODE START
    BIC.B #M_BUS,&I2CSM_OUT
    BIC.B #M_BUS,&I2CSM_REN
    BIS.B #RC5,&IR_IE
    BIC.B #RC5,&IR_IFG
    MOV #INT_RC5,&IR_Vec
    LO2HI
    ." RC5toI2CF_Master is running. Type STOP to quit"
    LIT RECURSE IS WARM
    (WARM) ;

CODE STOP
MOV #WARM,R8     
MOV #(WARM),2(R8)
MOV #COLD,R0
ENDCODE

RST_STATE
VARIABLE I2CS_ADR
2 ALLOT
VARIABLE I2CM_BUF
2 ALLOT

CODE I2C_MTX
BEGIN
    ADD.B   R9,R9
    U>= IF
        BIC.B #MSDA,&I2CSM_DIR
    ELSE
        BIS.B #MSDA,&I2CSM_DIR
    THEN
    BIC.B #MSCL,&I2CSM_DIR
    BEGIN
        BIT.B #MSCL,&I2CSM_IN
    0<> UNTIL
    BIS.B #MSCL,&I2CSM_DIR
    SUB #1,R10
0= UNTIL
BIC.B   #MSDA,&I2CSM_DIR
MOV @R1+,R0
ENDCODE

CODE INT_RC5
BIC #0xF8,0(R1)
    MOV     #0,&0x360
    MOV     #1778,R9
    MOV     #14,R10
BEGIN
    MOV #0b1011100100,&0x340
    RRUM    #1,R9
    MOV     R9,R8
    RRUM    #1,R8
    ADD     R9,R8
    BEGIN   CMP R8,&0x350
    0= UNTIL
    BIT.B   #RC5,&IR_IN
    ADDC    R13,R13
    MOV     &IR_IN,&IR_IES
    BIC.B   #RC5,&IR_IFG
    SUB     #1,R10
0<> WHILE
    ADD     R9,R8
    BEGIN
        CMP R8,&0x350
        0>= IF
          BIC #0x30,&0x340
          RETI
        THEN
        BIT.B #RC5,&IR_IFG
    0<> UNTIL
    MOV &0x350,R9
REPEAT
    MOV #0x30,&0x340
    RLAM #1,R13
    MOV @R1,R9
    RLAM #4,R9
    XOR R13,R9
    BIT #0x2000,R9
    0= IF RETI
    THEN
    XOR #0x200,0(R1)
    MOV.B R13,R12
    RRUM #2,R12
    BIT #0x4000,R13
    0= IF   BIS #0x40,R12
    THEN
    SWPB R12
    ADD #1,R12
    MOV R12,&I2CM_BUF
    MOV #0b0010100,&I2CS_ADR
    BIC.B #M_BUS,&I2CSM_DIR
    BIC.B #M_BUS,&I2CSM_OUT
    MOV #2,R11
    BEGIN
        BEGIN
            BEGIN
               BIT.B #MSCL,&I2CSM_IN
            0<> UNTIL
            BIT.B #MSDA,&I2CSM_IN
        0<> UNTIL
            SUB #1,R11
    0= UNTIL
BIS.B   #MSDA,&I2CSM_DIR
MOV     #I2CM_BUF,R10
MOV.B   @R10+,R11
MOV     #I2CS_ADR,R12
MOV.B   @R12+,R9
BIT.B   #1,R9
0= IF
    MOV R10,R12
THEN
BIS.B   #MSCL,&I2CSM_DIR
ADD     #1,R11
MOV     #8,R10
CALL    #I2C_MTX
BEGIN
    BIC.B   #MSCL,&I2CSM_DIR
    BEGIN
        BIT.B #MSCL,&I2CSM_IN
    0<> UNTIL
    BIT.B   #MSDA,&I2CSM_IN
    BIS.B   #MSCL,&I2CSM_DIR
    0<> IF  BIS #2,R2
    ELSE    SUB.B #1,R11
    THEN
    0= IF
        BIS.B #MSDA,&I2CSM_DIR
        SUB.B R11,&I2CM_BUF
        BIC.B #MSCL,&I2CSM_DIR
        BEGIN
            BIT.B #MSCL,&I2CSM_IN
        0<> UNTIL
        BIC.B #MSDA,&I2CSM_DIR
        RETI
    THEN
    MOV.B #8,R10
    BIT.B #1,&I2CS_ADR
    0= IF
        MOV.B @R12+,R9
        CALL #I2C_MTX
    ELSE
        BIC.B #MSDA,&I2CSM_DIR
        BEGIN
            BIC.B #MSCL,&I2CSM_DIR
            BEGIN
                BIT.B #MSCL,&I2CSM_IN
            0<> UNTIL
            BIT.B #MSDA,&I2CSM_IN
            BIS.B #MSCL,&I2CSM_DIR
            ADDC.B R9,R9
            SUB #1,R10
        0= UNTIL
        MOV.B R9,0(R12)
        ADD #1,R12
        CMP.B #1,R11
        0<> IF
            BIS.B #MSDA,&I2CSM_DIR
        THEN
    THEN
AGAIN
ENDCODE

CODE START
    BIC.B #M_BUS,&I2CSM_OUT
    BIC.B #M_BUS,&I2CSM_REN
    BIS.B #RC5,&IR_IE
    BIC.B #RC5,&IR_IFG
    MOV #INT_RC5,&IR_Vec
    LO2HI
    ." RC5toI2CF_Master is running. Type STOP to quit"
    LIT RECURSE IS WARM
    (WARM) ;

CODE STOP
MOV #WARM,R8     
MOV #(WARM),2(R8)
MOV #COLD,R0
ENDCODE

RST_STATE
VARIABLE I2CS_ADR
2 ALLOT
VARIABLE I2CM_BUF
2 ALLOT

CODE I2C_MTX
BEGIN
    ADD.B   R9,R9
    U>= IF
        BIC.B #MSDA,&I2CSM_DIR
    ELSE
        BIS.B #MSDA,&I2CSM_DIR
    THEN
    BIC.B #MSCL,&I2CSM_DIR
    BEGIN
        BIT.B #MSCL,&I2CSM_IN
    0<> UNTIL
    BIS.B #MSCL,&I2CSM_DIR
    SUB #1,R10
0= UNTIL
BIC.B   #MSDA,&I2CSM_DIR
MOV @R1+,R0
ENDCODE

CODE INT_RC5
BIC #0xF8,0(R1)
    MOV     #0,&0x360
    MOV     #1778,R9
    MOV     #14,R10
BEGIN
    MOV #0b1011100100,&0x340
    RRUM    #1,R9
    MOV     R9,R8
    RRUM    #1,R8
    ADD     R9,R8
    BEGIN   CMP R8,&0x350
    0= UNTIL
    BIT.B   #RC5,&IR_IN
    ADDC    R13,R13
    MOV     &IR_IN,&IR_IES
    BIC.B   #RC5,&IR_IFG
    SUB     #1,R10
0<> WHILE
    ADD     R9,R8
    BEGIN
        CMP R8,&0x350
        0>= IF
          BIC #0x30,&0x340
          RETI
        THEN
        BIT.B #RC5,&IR_IFG
    0<> UNTIL
    MOV &0x350,R9
REPEAT
    MOV #0x30,&0x340
    RLAM #1,R13
    MOV @R1,R9
    RLAM #4,R9
    XOR R13,R9
    BIT #0x2000,R9
    0= IF RETI
    THEN
    XOR #0x200,0(R1)
    MOV.B R13,R12
    RRUM #2,R12
    BIT #0x4000,R13
    0= IF   BIS #0x40,R12
    THEN
    SWPB R12
    ADD #1,R12
    MOV R12,&I2CM_BUF
    MOV #0b0010100,&I2CS_ADR
    BIC.B #M_BUS,&I2CSM_DIR
    BIC.B #M_BUS,&I2CSM_OUT
    MOV #2,R11
    BEGIN
        BEGIN
            BEGIN
               BIT.B #MSCL,&I2CSM_IN
            0<> UNTIL
            BIT.B #MSDA,&I2CSM_IN
        0<> UNTIL
            SUB #1,R11
    0= UNTIL
BIS.B   #MSDA,&I2CSM_DIR
MOV     #I2CM_BUF,R10
MOV.B   @R10+,R11
MOV     #I2CS_ADR,R12
MOV.B   @R12+,R9
BIT.B   #1,R9
0= IF
    MOV R10,R12
THEN
BIS.B   #MSCL,&I2CSM_DIR
ADD     #1,R11
MOV     #8,R10
CALL    #I2C_MTX
BEGIN
    BIC.B   #MSCL,&I2CSM_DIR
    BEGIN
        BIT.B #MSCL,&I2CSM_IN
    0<> UNTIL
    BIT.B   #MSDA,&I2CSM_IN
    BIS.B   #MSCL,&I2CSM_DIR
    0<> IF  BIS #2,R2
    ELSE    SUB.B #1,R11
    THEN
    0= IF
        BIS.B #MSDA,&I2CSM_DIR
        SUB.B R11,&I2CM_BUF
        BIC.B #MSCL,&I2CSM_DIR
        BEGIN
            BIT.B #MSCL,&I2CSM_IN
        0<> UNTIL
        BIC.B #MSDA,&I2CSM_DIR
        RETI
    THEN
    MOV.B #8,R10
    BIT.B #1,&I2CS_ADR
    0= IF
        MOV.B @R12+,R9
        CALL #I2C_MTX
    ELSE
        BIC.B #MSDA,&I2CSM_DIR
        BEGIN
            BIC.B #MSCL,&I2CSM_DIR
            BEGIN
                BIT.B #MSCL,&I2CSM_IN
            0<> UNTIL
            BIT.B #MSDA,&I2CSM_IN
            BIS.B #MSCL,&I2CSM_DIR
            ADDC.B R9,R9
            SUB #1,R10
        0= UNTIL
        MOV.B R9,0(R12)
        ADD #1,R12
        CMP.B #1,R11
        0<> IF
            BIS.B #MSDA,&I2CSM_DIR
        THEN
    THEN
AGAIN
ENDCODE

CODE START
    BIC.B #M_BUS,&I2CSM_OUT
    BIC.B #M_BUS,&I2CSM_REN
    BIS.B #RC5,&IR_IE
    BIC.B #RC5,&IR_IFG
    MOV #INT_RC5,&IR_Vec
    LO2HI
    ." RC5toI2CF_Master is running. Type STOP to quit"
    LIT RECURSE IS WARM
    (WARM) ;

CODE STOP
MOV #WARM,R8     
MOV #(WARM),2(R8)
MOV #COLD,R0
ENDCODE

RST_STATE
VARIABLE I2CS_ADR
2 ALLOT
VARIABLE I2CM_BUF
2 ALLOT

CODE I2C_MTX
BEGIN
    ADD.B   R9,R9
    U>= IF
        BIC.B #MSDA,&I2CSM_DIR
    ELSE
        BIS.B #MSDA,&I2CSM_DIR
    THEN
    BIC.B #MSCL,&I2CSM_DIR
    BEGIN
        BIT.B #MSCL,&I2CSM_IN
    0<> UNTIL
    BIS.B #MSCL,&I2CSM_DIR
    SUB #1,R10
0= UNTIL
BIC.B   #MSDA,&I2CSM_DIR
MOV @R1+,R0
ENDCODE

CODE INT_RC5
BIC #0xF8,0(R1)
    MOV     #0,&0x360
    MOV     #1778,R9
    MOV     #14,R10
BEGIN
    MOV #0b1011100100,&0x340
    RRUM    #1,R9
    MOV     R9,R8
    RRUM    #1,R8
    ADD     R9,R8
    BEGIN   CMP R8,&0x350
    0= UNTIL
    BIT.B   #RC5,&IR_IN
    ADDC    R13,R13
    MOV     &IR_IN,&IR_IES
    BIC.B   #RC5,&IR_IFG
    SUB     #1,R10
0<> WHILE
    ADD     R9,R8
    BEGIN
        CMP R8,&0x350
        0>= IF
          BIC #0x30,&0x340
          RETI
        THEN
        BIT.B #RC5,&IR_IFG
    0<> UNTIL
    MOV &0x350,R9
REPEAT
    MOV #0x30,&0x340
    RLAM #1,R13
    MOV @R1,R9
    RLAM #4,R9
    XOR R13,R9
    BIT #0x2000,R9
    0= IF RETI
    THEN
    XOR #0x200,0(R1)
    MOV.B R13,R12
    RRUM #2,R12
    BIT #0x4000,R13
    0= IF   BIS #0x40,R12
    THEN
    SWPB R12
    ADD #1,R12
    MOV R12,&I2CM_BUF
    MOV #0b0010100,&I2CS_ADR
    BIC.B #M_BUS,&I2CSM_DIR
    BIC.B #M_BUS,&I2CSM_OUT
    MOV #2,R11
    BEGIN
        BEGIN
            BEGIN
               BIT.B #MSCL,&I2CSM_IN
            0<> UNTIL
            BIT.B #MSDA,&I2CSM_IN
        0<> UNTIL
            SUB #1,R11
    0= UNTIL
BIS.B   #MSDA,&I2CSM_DIR
MOV     #I2CM_BUF,R10
MOV.B   @R10+,R11
MOV     #I2CS_ADR,R12
MOV.B   @R12+,R9
BIT.B   #1,R9
0= IF
    MOV R10,R12
THEN
BIS.B   #MSCL,&I2CSM_DIR
ADD     #1,R11
MOV     #8,R10
CALL    #I2C_MTX
BEGIN
    BIC.B   #MSCL,&I2CSM_DIR
    BEGIN
        BIT.B #MSCL,&I2CSM_IN
    0<> UNTIL
    BIT.B   #MSDA,&I2CSM_IN
    BIS.B   #MSCL,&I2CSM_DIR
    0<> IF  BIS #2,R2
    ELSE    SUB.B #1,R11
    THEN
    0= IF
        BIS.B #MSDA,&I2CSM_DIR
        SUB.B R11,&I2CM_BUF
        BIC.B #MSCL,&I2CSM_DIR
        BEGIN
            BIT.B #MSCL,&I2CSM_IN
        0<> UNTIL
        BIC.B #MSDA,&I2CSM_DIR
        RETI
    THEN
    MOV.B #8,R10
    BIT.B #1,&I2CS_ADR
    0= IF
        MOV.B @R12+,R9
        CALL #I2C_MTX
    ELSE
        BIC.B #MSDA,&I2CSM_DIR
        BEGIN
            BIC.B #MSCL,&I2CSM_DIR
            BEGIN
                BIT.B #MSCL,&I2CSM_IN
            0<> UNTIL
            BIT.B #MSDA,&I2CSM_IN
            BIS.B #MSCL,&I2CSM_DIR
            ADDC.B R9,R9
            SUB #1,R10
        0= UNTIL
        MOV.B R9,0(R12)
        ADD #1,R12
        CMP.B #1,R11
        0<> IF
            BIS.B #MSDA,&I2CSM_DIR
        THEN
    THEN
AGAIN
ENDCODE

CODE START
    BIC.B #M_BUS,&I2CSM_OUT
    BIC.B #M_BUS,&I2CSM_REN
    BIS.B #RC5,&IR_IE
    BIC.B #RC5,&IR_IFG
    MOV #INT_RC5,&IR_Vec
    LO2HI
    ." RC5toI2CF_Master is running. Type STOP to quit"
    LIT RECURSE IS WARM
    (WARM) ;

CODE STOP
MOV #WARM,R8     
MOV #(WARM),2(R8)
MOV #COLD,R0
ENDCODE

RST_STATE
VARIABLE I2CS_ADR
2 ALLOT
VARIABLE I2CM_BUF
2 ALLOT

CODE I2C_MTX
BEGIN
    ADD.B   R9,R9
    U>= IF
        BIC.B #MSDA,&I2CSM_DIR
    ELSE
        BIS.B #MSDA,&I2CSM_DIR
    THEN
    BIC.B #MSCL,&I2CSM_DIR
    BEGIN
        BIT.B #MSCL,&I2CSM_IN
    0<> UNTIL
    BIS.B #MSCL,&I2CSM_DIR
    SUB #1,R10
0= UNTIL
BIC.B   #MSDA,&I2CSM_DIR
MOV @R1+,R0
ENDCODE

CODE INT_RC5
BIC #0xF8,0(R1)
    MOV     #0,&0x360
    MOV     #1778,R9
    MOV     #14,R10
BEGIN
    MOV #0b1011100100,&0x340
    RRUM    #1,R9
    MOV     R9,R8
    RRUM    #1,R8
    ADD     R9,R8
    BEGIN   CMP R8,&0x350
    0= UNTIL
    BIT.B   #RC5,&IR_IN
    ADDC    R13,R13
    MOV     &IR_IN,&IR_IES
    BIC.B   #RC5,&IR_IFG
    SUB     #1,R10
0<> WHILE
    ADD     R9,R8
    BEGIN
        CMP R8,&0x350
        0>= IF
          BIC #0x30,&0x340
          RETI
        THEN
        BIT.B #RC5,&IR_IFG
    0<> UNTIL
    MOV &0x350,R9
REPEAT
    MOV #0x30,&0x340
    RLAM #1,R13
    MOV @R1,R9
    RLAM #4,R9
    XOR R13,R9
    BIT #0x2000,R9
    0= IF RETI
    THEN
    XOR #0x200,0(R1)
    MOV.B R13,R12
    RRUM #2,R12
    BIT #0x4000,R13
    0= IF   BIS #0x40,R12
    THEN
    SWPB R12
    ADD #1,R12
    MOV R12,&I2CM_BUF
    MOV #0b0010100,&I2CS_ADR
    BIC.B #M_BUS,&I2CSM_DIR
    BIC.B #M_BUS,&I2CSM_OUT
    MOV #2,R11
    BEGIN
        BEGIN
            BEGIN
               BIT.B #MSCL,&I2CSM_IN
            0<> UNTIL
            BIT.B #MSDA,&I2CSM_IN
        0<> UNTIL
            SUB #1,R11
    0= UNTIL
BIS.B   #MSDA,&I2CSM_DIR
MOV     #I2CM_BUF,R10
MOV.B   @R10+,R11
MOV     #I2CS_ADR,R12
MOV.B   @R12+,R9
BIT.B   #1,R9
0= IF
    MOV R10,R12
THEN
BIS.B   #MSCL,&I2CSM_DIR
ADD     #1,R11
MOV     #8,R10
CALL    #I2C_MTX
BEGIN
    BIC.B   #MSCL,&I2CSM_DIR
    BEGIN
        BIT.B #MSCL,&I2CSM_IN
    0<> UNTIL
    BIT.B   #MSDA,&I2CSM_IN
    BIS.B   #MSCL,&I2CSM_DIR
    0<> IF  BIS #2,R2
    ELSE    SUB.B #1,R11
    THEN
    0= IF
        BIS.B #MSDA,&I2CSM_DIR
        SUB.B R11,&I2CM_BUF
        BIC.B #MSCL,&I2CSM_DIR
        BEGIN
            BIT.B #MSCL,&I2CSM_IN
        0<> UNTIL
        BIC.B #MSDA,&I2CSM_DIR
        RETI
    THEN
    MOV.B #8,R10
    BIT.B #1,&I2CS_ADR
    0= IF
        MOV.B @R12+,R9
        CALL #I2C_MTX
    ELSE
        BIC.B #MSDA,&I2CSM_DIR
        BEGIN
            BIC.B #MSCL,&I2CSM_DIR
            BEGIN
                BIT.B #MSCL,&I2CSM_IN
            0<> UNTIL
            BIT.B #MSDA,&I2CSM_IN
            BIS.B #MSCL,&I2CSM_DIR
            ADDC.B R9,R9
            SUB #1,R10
        0= UNTIL
        MOV.B R9,0(R12)
        ADD #1,R12
        CMP.B #1,R11
        0<> IF
            BIS.B #MSDA,&I2CSM_DIR
        THEN
    THEN
AGAIN
ENDCODE

CODE START
    BIC.B #M_BUS,&I2CSM_OUT
    BIC.B #M_BUS,&I2CSM_REN
    BIS.B #RC5,&IR_IE
    BIC.B #RC5,&IR_IFG
    MOV #INT_RC5,&IR_Vec
    LO2HI
    ." RC5toI2CF_Master is running. Type STOP to quit"
    LIT RECURSE IS WARM
    (WARM) ;

CODE STOP
MOV #WARM,R8     
MOV #(WARM),2(R8)
MOV #COLD,R0
ENDCODE

RST_STATE
VARIABLE I2CS_ADR
2 ALLOT
VARIABLE I2CM_BUF
2 ALLOT

CODE I2C_MTX
BEGIN
    ADD.B   R9,R9
    U>= IF
        BIC.B #MSDA,&I2CSM_DIR
    ELSE
        BIS.B #MSDA,&I2CSM_DIR
    THEN
    BIC.B #MSCL,&I2CSM_DIR
    BEGIN
        BIT.B #MSCL,&I2CSM_IN
    0<> UNTIL
    BIS.B #MSCL,&I2CSM_DIR
    SUB #1,R10
0= UNTIL
BIC.B   #MSDA,&I2CSM_DIR
MOV @R1+,R0
ENDCODE

CODE INT_RC5
BIC #0xF8,0(R1)
    MOV     #0,&0x360
    MOV     #1778,R9
    MOV     #14,R10
BEGIN
    MOV #0b1011100100,&0x340
    RRUM    #1,R9
    MOV     R9,R8
    RRUM    #1,R8
    ADD     R9,R8
    BEGIN   CMP R8,&0x350
    0= UNTIL
    BIT.B   #RC5,&IR_IN
    ADDC    R13,R13
    MOV     &IR_IN,&IR_IES
    BIC.B   #RC5,&IR_IFG
    SUB     #1,R10
0<> WHILE
    ADD     R9,R8
    BEGIN
        CMP R8,&0x350
        0>= IF
          BIC #0x30,&0x340
          RETI
        THEN
        BIT.B #RC5,&IR_IFG
    0<> UNTIL
    MOV &0x350,R9
REPEAT
    MOV #0x30,&0x340
    RLAM #1,R13
    MOV @R1,R9
    RLAM #4,R9
    XOR R13,R9
    BIT #0x2000,R9
    0= IF RETI
    THEN
    XOR #0x200,0(R1)
    MOV.B R13,R12
    RRUM #2,R12
    BIT #0x4000,R13
    0= IF   BIS #0x40,R12
    THEN
    SWPB R12
    ADD #1,R12
    MOV R12,&I2CM_BUF
    MOV #0b0010100,&I2CS_ADR
    BIC.B #M_BUS,&I2CSM_DIR
    BIC.B #M_BUS,&I2CSM_OUT
    MOV #2,R11
    BEGIN
        BEGIN
            BEGIN
               BIT.B #MSCL,&I2CSM_IN
            0<> UNTIL
            BIT.B #MSDA,&I2CSM_IN
        0<> UNTIL
            SUB #1,R11
    0= UNTIL
BIS.B   #MSDA,&I2CSM_DIR
MOV     #I2CM_BUF,R10
MOV.B   @R10+,R11
MOV     #I2CS_ADR,R12
MOV.B   @R12+,R9
BIT.B   #1,R9
0= IF
    MOV R10,R12
THEN
BIS.B   #MSCL,&I2CSM_DIR
ADD     #1,R11
MOV     #8,R10
CALL    #I2C_MTX
BEGIN
    BIC.B   #MSCL,&I2CSM_DIR
    BEGIN
        BIT.B #MSCL,&I2CSM_IN
    0<> UNTIL
    BIT.B   #MSDA,&I2CSM_IN
    BIS.B   #MSCL,&I2CSM_DIR
    0<> IF  BIS #2,R2
    ELSE    SUB.B #1,R11
    THEN
    0= IF
        BIS.B #MSDA,&I2CSM_DIR
        SUB.B R11,&I2CM_BUF
        BIC.B #MSCL,&I2CSM_DIR
        BEGIN
            BIT.B #MSCL,&I2CSM_IN
        0<> UNTIL
        BIC.B #MSDA,&I2CSM_DIR
        RETI
    THEN
    MOV.B #8,R10
    BIT.B #1,&I2CS_ADR
    0= IF
        MOV.B @R12+,R9
        CALL #I2C_MTX
    ELSE
        BIC.B #MSDA,&I2CSM_DIR
        BEGIN
            BIC.B #MSCL,&I2CSM_DIR
            BEGIN
                BIT.B #MSCL,&I2CSM_IN
            0<> UNTIL
            BIT.B #MSDA,&I2CSM_IN
            BIS.B #MSCL,&I2CSM_DIR
            ADDC.B R9,R9
            SUB #1,R10
        0= UNTIL
        MOV.B R9,0(R12)
        ADD #1,R12
        CMP.B #1,R11
        0<> IF
            BIS.B #MSDA,&I2CSM_DIR
        THEN
    THEN
AGAIN
ENDCODE

CODE START
    BIC.B #M_BUS,&I2CSM_OUT
    BIC.B #M_BUS,&I2CSM_REN
    BIS.B #RC5,&IR_IE
    BIC.B #RC5,&IR_IFG
    MOV #INT_RC5,&IR_Vec
    LO2HI
    ." RC5toI2CF_Master is running. Type STOP to quit"
    LIT RECURSE IS WARM
    (WARM) ;

CODE STOP
MOV #WARM,R8     
MOV #(WARM),2(R8)
MOV #COLD,R0
ENDCODE

RST_STATE
VARIABLE I2CS_ADR
2 ALLOT
VARIABLE I2CM_BUF
2 ALLOT

CODE I2C_MTX
BEGIN
    ADD.B   R9,R9
    U>= IF
        BIC.B #MSDA,&I2CSM_DIR
    ELSE
        BIS.B #MSDA,&I2CSM_DIR
    THEN
    BIC.B #MSCL,&I2CSM_DIR
    BEGIN
        BIT.B #MSCL,&I2CSM_IN
    0<> UNTIL
    BIS.B #MSCL,&I2CSM_DIR
    SUB #1,R10
0= UNTIL
BIC.B   #MSDA,&I2CSM_DIR
MOV @R1+,R0
ENDCODE

CODE INT_RC5
BIC #0xF8,0(R1)
    MOV     #0,&0x360
    MOV     #1778,R9
    MOV     #14,R10
BEGIN
    MOV #0b1011100100,&0x340
    RRUM    #1,R9
    MOV     R9,R8
    RRUM    #1,R8
    ADD     R9,R8
    BEGIN   CMP R8,&0x350
    0= UNTIL
    BIT.B   #RC5,&IR_IN
    ADDC    R13,R13
    MOV     &IR_IN,&IR_IES
    BIC.B   #RC5,&IR_IFG
    SUB     #1,R10
0<> WHILE
    ADD     R9,R8
    BEGIN
        CMP R8,&0x350
        0>= IF
          BIC #0x30,&0x340
          RETI
        THEN
        BIT.B #RC5,&IR_IFG
    0<> UNTIL
    MOV &0x350,R9
REPEAT
    MOV #0x30,&0x340
    RLAM #1,R13
    MOV @R1,R9
    RLAM #4,R9
    XOR R13,R9
    BIT #0x2000,R9
    0= IF RETI
    THEN
    XOR #0x200,0(R1)
    MOV.B R13,R12
    RRUM #2,R12
    BIT #0x4000,R13
    0= IF   BIS #0x40,R12
    THEN
    SWPB R12
    ADD #1,R12
    MOV R12,&I2CM_BUF
    MOV #0b0010100,&I2CS_ADR
    BIC.B #M_BUS,&I2CSM_DIR
    BIC.B #M_BUS,&I2CSM_OUT
    MOV #2,R11
    BEGIN
        BEGIN
            BEGIN
               BIT.B #MSCL,&I2CSM_IN
            0<> UNTIL
            BIT.B #MSDA,&I2CSM_IN
        0<> UNTIL
            SUB #1,R11
    0= UNTIL
BIS.B   #MSDA,&I2CSM_DIR
MOV     #I2CM_BUF,R10
MOV.B   @R10+,R11
MOV     #I2CS_ADR,R12
MOV.B   @R12+,R9
BIT.B   #1,R9
0= IF
    MOV R10,R12
THEN
BIS.B   #MSCL,&I2CSM_DIR
ADD     #1,R11
MOV     #8,R10
CALL    #I2C_MTX
BEGIN
    BIC.B   #MSCL,&I2CSM_DIR
    BEGIN
        BIT.B #MSCL,&I2CSM_IN
    0<> UNTIL
    BIT.B   #MSDA,&I2CSM_IN
    BIS.B   #MSCL,&I2CSM_DIR
    0<> IF  BIS #2,R2
    ELSE    SUB.B #1,R11
    THEN
    0= IF
        BIS.B #MSDA,&I2CSM_DIR
        SUB.B R11,&I2CM_BUF
        BIC.B #MSCL,&I2CSM_DIR
        BEGIN
            BIT.B #MSCL,&I2CSM_IN
        0<> UNTIL
        BIC.B #MSDA,&I2CSM_DIR
        RETI
    THEN
    MOV.B #8,R10
    BIT.B #1,&I2CS_ADR
    0= IF
        MOV.B @R12+,R9
        CALL #I2C_MTX
    ELSE
        BIC.B #MSDA,&I2CSM_DIR
        BEGIN
            BIC.B #MSCL,&I2CSM_DIR
            BEGIN
                BIT.B #MSCL,&I2CSM_IN
            0<> UNTIL
            BIT.B #MSDA,&I2CSM_IN
            BIS.B #MSCL,&I2CSM_DIR
            ADDC.B R9,R9
            SUB #1,R10
        0= UNTIL
        MOV.B R9,0(R12)
        ADD #1,R12
        CMP.B #1,R11
        0<> IF
            BIS.B #MSDA,&I2CSM_DIR
        THEN
    THEN
AGAIN
ENDCODE

CODE START
    BIC.B #M_BUS,&I2CSM_OUT
    BIC.B #M_BUS,&I2CSM_REN
    BIS.B #RC5,&IR_IE
    BIC.B #RC5,&IR_IFG
    MOV #INT_RC5,&IR_Vec
    LO2HI
    ." RC5toI2CF_Master is running. Type STOP to quit"
    LIT RECURSE IS WARM
    (WARM) ;

CODE STOP
MOV #WARM,R8     
MOV #(WARM),2(R8)
MOV #COLD,R0
ENDCODE

RST_STATE
VARIABLE I2CS_ADR
2 ALLOT
VARIABLE I2CM_BUF
2 ALLOT

CODE I2C_MTX
BEGIN
    ADD.B   R9,R9
    U>= IF
        BIC.B #MSDA,&I2CSM_DIR
    ELSE
        BIS.B #MSDA,&I2CSM_DIR
    THEN
    BIC.B #MSCL,&I2CSM_DIR
    BEGIN
        BIT.B #MSCL,&I2CSM_IN
    0<> UNTIL
    BIS.B #MSCL,&I2CSM_DIR
    SUB #1,R10
0= UNTIL
BIC.B   #MSDA,&I2CSM_DIR
MOV @R1+,R0
ENDCODE

CODE INT_RC5
BIC #0xF8,0(R1)
    MOV     #0,&0x360
    MOV     #1778,R9
    MOV     #14,R10
BEGIN
    MOV #0b1011100100,&0x340
    RRUM    #1,R9
    MOV     R9,R8
    RRUM    #1,R8
    ADD     R9,R8
    BEGIN   CMP R8,&0x350
    0= UNTIL
    BIT.B   #RC5,&IR_IN
    ADDC    R13,R13
    MOV     &IR_IN,&IR_IES
    BIC.B   #RC5,&IR_IFG
    SUB     #1,R10
0<> WHILE
    ADD     R9,R8
    BEGIN
        CMP R8,&0x350
        0>= IF
          BIC #0x30,&0x340
          RETI
        THEN
        BIT.B #RC5,&IR_IFG
    0<> UNTIL
    MOV &0x350,R9
REPEAT
    MOV #0x30,&0x340
    RLAM #1,R13
    MOV @R1,R9
    RLAM #4,R9
    XOR R13,R9
    BIT #0x2000,R9
    0= IF RETI
    THEN
    XOR #0x200,0(R1)
    MOV.B R13,R12
    RRUM #2,R12
    BIT #0x4000,R13
    0= IF   BIS #0x40,R12
    THEN
    SWPB R12
    ADD #1,R12
    MOV R12,&I2CM_BUF
    MOV #0b0010100,&I2CS_ADR
    BIC.B #M_BUS,&I2CSM_DIR
    BIC.B #M_BUS,&I2CSM_OUT
    MOV #2,R11
    BEGIN
        BEGIN
            BEGIN
               BIT.B #MSCL,&I2CSM_IN
            0<> UNTIL
            BIT.B #MSDA,&I2CSM_IN
        0<> UNTIL
            SUB #1,R11
    0= UNTIL
BIS.B   #MSDA,&I2CSM_DIR
MOV     #I2CM_BUF,R10
MOV.B   @R10+,R11
MOV     #I2CS_ADR,R12
MOV.B   @R12+,R9
BIT.B   #1,R9
0= IF
    MOV R10,R12
THEN
BIS.B   #MSCL,&I2CSM_DIR
ADD     #1,R11
MOV     #8,R10
CALL    #I2C_MTX
BEGIN
    BIC.B   #MSCL,&I2CSM_DIR
    BEGIN
        BIT.B #MSCL,&I2CSM_IN
    0<> UNTIL
    BIT.B   #MSDA,&I2CSM_IN
    BIS.B   #MSCL,&I2CSM_DIR
    0<> IF  BIS #2,R2
    ELSE    SUB.B #1,R11
    THEN
    0= IF
        BIS.B #MSDA,&I2CSM_DIR
        SUB.B R11,&I2CM_BUF
        BIC.B #MSCL,&I2CSM_DIR
        BEGIN
            BIT.B #MSCL,&I2CSM_IN
        0<> UNTIL
        BIC.B #MSDA,&I2CSM_DIR
        RETI
    THEN
    MOV.B #8,R10
    BIT.B #1,&I2CS_ADR
    0= IF
        MOV.B @R12+,R9
        CALL #I2C_MTX
    ELSE
        BIC.B #MSDA,&I2CSM_DIR
        BEGIN
            BIC.B #MSCL,&I2CSM_DIR
            BEGIN
                BIT.B #MSCL,&I2CSM_IN
            0<> UNTIL
            BIT.B #MSDA,&I2CSM_IN
            BIS.B #MSCL,&I2CSM_DIR
            ADDC.B R9,R9
            SUB #1,R10
        0= UNTIL
        MOV.B R9,0(R12)
        ADD #1,R12
        CMP.B #1,R11
        0<> IF
            BIS.B #MSDA,&I2CSM_DIR
        THEN
    THEN
AGAIN
ENDCODE

CODE START
    BIC.B #M_BUS,&I2CSM_OUT
    BIC.B #M_BUS,&I2CSM_REN
    BIS.B #RC5,&IR_IE
    BIC.B #RC5,&IR_IFG
    MOV #INT_RC5,&IR_Vec
    LO2HI
    ." RC5toI2CF_Master is running. Type STOP to quit"
    LIT RECURSE IS WARM
    (WARM) ;

CODE STOP
MOV #WARM,R8     
MOV #(WARM),2(R8)
MOV #COLD,R0
ENDCODE

RST_STATE
VARIABLE I2CS_ADR
2 ALLOT
VARIABLE I2CM_BUF
2 ALLOT

CODE I2C_MTX
BEGIN
    ADD.B   R9,R9
    U>= IF
        BIC.B #MSDA,&I2CSM_DIR
    ELSE
        BIS.B #MSDA,&I2CSM_DIR
    THEN
    BIC.B #MSCL,&I2CSM_DIR
    BEGIN
        BIT.B #MSCL,&I2CSM_IN
    0<> UNTIL
    BIS.B #MSCL,&I2CSM_DIR
    SUB #1,R10
0= UNTIL
BIC.B   #MSDA,&I2CSM_DIR
MOV @R1+,R0
ENDCODE

CODE INT_RC5
BIC #0xF8,0(R1)
    MOV     #0,&0x360
    MOV     #1778,R9
    MOV     #14,R10
BEGIN
    MOV #0b1011100100,&0x340
    RRUM    #1,R9
    MOV     R9,R8
    RRUM    #1,R8
    ADD     R9,R8
    BEGIN   CMP R8,&0x350
    0= UNTIL
    BIT.B   #RC5,&IR_IN
    ADDC    R13,R13
    MOV     &IR_IN,&IR_IES
    BIC.B   #RC5,&IR_IFG
    SUB     #1,R10
0<> WHILE
    ADD     R9,R8
    BEGIN
        CMP R8,&0x350
        0>= IF
          BIC #0x30,&0x340
          RETI
        THEN
        BIT.B #RC5,&IR_IFG
    0<> UNTIL
    MOV &0x350,R9
REPEAT
    MOV #0x30,&0x340
    RLAM #1,R13
    MOV @R1,R9
    RLAM #4,R9
    XOR R13,R9
    BIT #0x2000,R9
    0= IF RETI
    THEN
    XOR #0x200,0(R1)
    MOV.B R13,R12
    RRUM #2,R12
    BIT #0x4000,R13
    0= IF   BIS #0x40,R12
    THEN
    SWPB R12
    ADD #1,R12
    MOV R12,&I2CM_BUF
    MOV #0b0010100,&I2CS_ADR
    BIC.B #M_BUS,&I2CSM_DIR
    BIC.B #M_BUS,&I2CSM_OUT
    MOV #2,R11
    BEGIN
        BEGIN
            BEGIN
               BIT.B #MSCL,&I2CSM_IN
            0<> UNTIL
            BIT.B #MSDA,&I2CSM_IN
        0<> UNTIL
            SUB #1,R11
    0= UNTIL
BIS.B   #MSDA,&I2CSM_DIR
MOV     #I2CM_BUF,R10
MOV.B   @R10+,R11
MOV     #I2CS_ADR,R12
MOV.B   @R12+,R9
BIT.B   #1,R9
0= IF
    MOV R10,R12
THEN
BIS.B   #MSCL,&I2CSM_DIR
ADD     #1,R11
MOV     #8,R10
CALL    #I2C_MTX
BEGIN
    BIC.B   #MSCL,&I2CSM_DIR
    BEGIN
        BIT.B #MSCL,&I2CSM_IN
    0<> UNTIL
    BIT.B   #MSDA,&I2CSM_IN
    BIS.B   #MSCL,&I2CSM_DIR
    0<> IF  BIS #2,R2
    ELSE    SUB.B #1,R11
    THEN
    0= IF
        BIS.B #MSDA,&I2CSM_DIR
        SUB.B R11,&I2CM_BUF
        BIC.B #MSCL,&I2CSM_DIR
        BEGIN
            BIT.B #MSCL,&I2CSM_IN
        0<> UNTIL
        BIC.B #MSDA,&I2CSM_DIR
        RETI
    THEN
    MOV.B #8,R10
    BIT.B #1,&I2CS_ADR
    0= IF
        MOV.B @R12+,R9
        CALL #I2C_MTX
    ELSE
        BIC.B #MSDA,&I2CSM_DIR
        BEGIN
            BIC.B #MSCL,&I2CSM_DIR
            BEGIN
                BIT.B #MSCL,&I2CSM_IN
            0<> UNTIL
            BIT.B #MSDA,&I2CSM_IN
            BIS.B #MSCL,&I2CSM_DIR
            ADDC.B R9,R9
            SUB #1,R10
        0= UNTIL
        MOV.B R9,0(R12)
        ADD #1,R12
        CMP.B #1,R11
        0<> IF
            BIS.B #MSDA,&I2CSM_DIR
        THEN
    THEN
AGAIN
ENDCODE

CODE START
    BIC.B #M_BUS,&I2CSM_OUT
    BIC.B #M_BUS,&I2CSM_REN
    BIS.B #RC5,&IR_IE
    BIC.B #RC5,&IR_IFG
    MOV #INT_RC5,&IR_Vec
    LO2HI
    ." RC5toI2CF_Master is running. Type STOP to quit"
    LIT RECURSE IS WARM
    (WARM) ;

CODE STOP
MOV #WARM,R8     
MOV #(WARM),2(R8)
MOV #COLD,R0
ENDCODE

RST_STATE
VARIABLE I2CS_ADR
2 ALLOT
VARIABLE I2CM_BUF
2 ALLOT

CODE I2C_MTX
BEGIN
    ADD.B   R9,R9
    U>= IF
        BIC.B #MSDA,&I2CSM_DIR
    ELSE
        BIS.B #MSDA,&I2CSM_DIR
    THEN
    BIC.B #MSCL,&I2CSM_DIR
    BEGIN
        BIT.B #MSCL,&I2CSM_IN
    0<> UNTIL
    BIS.B #MSCL,&I2CSM_DIR
    SUB #1,R10
0= UNTIL
BIC.B   #MSDA,&I2CSM_DIR
MOV @R1+,R0
ENDCODE

CODE INT_RC5
BIC #0xF8,0(R1)
    MOV     #0,&0x360
    MOV     #1778,R9
    MOV     #14,R10
BEGIN
    MOV #0b1011100100,&0x340
    RRUM    #1,R9
    MOV     R9,R8
    RRUM    #1,R8
    ADD     R9,R8
    BEGIN   CMP R8,&0x350
    0= UNTIL
    BIT.B   #RC5,&IR_IN
    ADDC    R13,R13
    MOV     &IR_IN,&IR_IES
    BIC.B   #RC5,&IR_IFG
    SUB     #1,R10
0<> WHILE
    ADD     R9,R8
    BEGIN
        CMP R8,&0x350
        0>= IF
          BIC #0x30,&0x340
          RETI
        THEN
        BIT.B #RC5,&IR_IFG
    0<> UNTIL
    MOV &0x350,R9
REPEAT
    MOV #0x30,&0x340
    RLAM #1,R13
    MOV @R1,R9
    RLAM #4,R9
    XOR R13,R9
    BIT #0x2000,R9
    0= IF RETI
    THEN
    XOR #0x200,0(R1)
    MOV.B R13,R12
    RRUM #2,R12
    BIT #0x4000,R13
    0= IF   BIS #0x40,R12
    THEN
    SWPB R12
    ADD #1,R12
    MOV R12,&I2CM_BUF
    MOV #0b0010100,&I2CS_ADR
    BIC.B #M_BUS,&I2CSM_DIR
    BIC.B #M_BUS,&I2CSM_OUT
    MOV #2,R11
    BEGIN
        BEGIN
            BEGIN
               BIT.B #MSCL,&I2CSM_IN
            0<> UNTIL
            BIT.B #MSDA,&I2CSM_IN
        0<> UNTIL
            SUB #1,R11
    0= UNTIL
BIS.B   #MSDA,&I2CSM_DIR
MOV     #I2CM_BUF,R10
MOV.B   @R10+,R11
MOV     #I2CS_ADR,R12
MOV.B   @R12+,R9
BIT.B   #1,R9
0= IF
    MOV R10,R12
THEN
BIS.B   #MSCL,&I2CSM_DIR
ADD     #1,R11
MOV     #8,R10
CALL    #I2C_MTX
BEGIN
    BIC.B   #MSCL,&I2CSM_DIR
    BEGIN
        BIT.B #MSCL,&I2CSM_IN
    0<> UNTIL
    BIT.B   #MSDA,&I2CSM_IN
    BIS.B   #MSCL,&I2CSM_DIR
    0<> IF  BIS #2,R2
    ELSE    SUB.B #1,R11
    THEN
    0= IF
        BIS.B #MSDA,&I2CSM_DIR
        SUB.B R11,&I2CM_BUF
        BIC.B #MSCL,&I2CSM_DIR
        BEGIN
            BIT.B #MSCL,&I2CSM_IN
        0<> UNTIL
        BIC.B #MSDA,&I2CSM_DIR
        RETI
    THEN
    MOV.B #8,R10
    BIT.B #1,&I2CS_ADR
    0= IF
        MOV.B @R12+,R9
        CALL #I2C_MTX
    ELSE
        BIC.B #MSDA,&I2CSM_DIR
        BEGIN
            BIC.B #MSCL,&I2CSM_DIR
            BEGIN
                BIT.B #MSCL,&I2CSM_IN
            0<> UNTIL
            BIT.B #MSDA,&I2CSM_IN
            BIS.B #MSCL,&I2CSM_DIR
            ADDC.B R9,R9
            SUB #1,R10
        0= UNTIL
        MOV.B R9,0(R12)
        ADD #1,R12
        CMP.B #1,R11
        0<> IF
            BIS.B #MSDA,&I2CSM_DIR
        THEN
    THEN
AGAIN
ENDCODE

CODE START
    BIC.B #M_BUS,&I2CSM_OUT
    BIC.B #M_BUS,&I2CSM_REN
    BIS.B #RC5,&IR_IE
    BIC.B #RC5,&IR_IFG
    MOV #INT_RC5,&IR_Vec
    LO2HI
    ." RC5toI2CF_Master is running. Type STOP to quit"
    LIT RECURSE IS WARM
    (WARM) ;

CODE STOP
MOV #WARM,R8     
MOV #(WARM),2(R8)
MOV #COLD,R0
ENDCODE

RST_STATE
VARIABLE I2CS_ADR
2 ALLOT
VARIABLE I2CM_BUF
2 ALLOT

CODE I2C_MTX
BEGIN
    ADD.B   R9,R9
    U>= IF
        BIC.B #MSDA,&I2CSM_DIR
    ELSE
        BIS.B #MSDA,&I2CSM_DIR
    THEN
    BIC.B #MSCL,&I2CSM_DIR
    BEGIN
        BIT.B #MSCL,&I2CSM_IN
    0<> UNTIL
    BIS.B #MSCL,&I2CSM_DIR
    SUB #1,R10
0= UNTIL
BIC.B   #MSDA,&I2CSM_DIR
MOV @R1+,R0
ENDCODE

CODE INT_RC5
BIC #0xF8,0(R1)
    MOV     #0,&0x360
    MOV     #1778,R9
    MOV     #14,R10
BEGIN
    MOV #0b1011100100,&0x340
    RRUM    #1,R9
    MOV     R9,R8
    RRUM    #1,R8
    ADD     R9,R8
    BEGIN   CMP R8,&0x350
    0= UNTIL
    BIT.B   #RC5,&IR_IN
    ADDC    R13,R13
    MOV     &IR_IN,&IR_IES
    BIC.B   #RC5,&IR_IFG
    SUB     #1,R10
0<> WHILE
    ADD     R9,R8
    BEGIN
        CMP R8,&0x350
        0>= IF
          BIC #0x30,&0x340
          RETI
        THEN
        BIT.B #RC5,&IR_IFG
    0<> UNTIL
    MOV &0x350,R9
REPEAT
    MOV #0x30,&0x340
    RLAM #1,R13
    MOV @R1,R9
    RLAM #4,R9
    XOR R13,R9
    BIT #0x2000,R9
    0= IF RETI
    THEN
    XOR #0x200,0(R1)
    MOV.B R13,R12
    RRUM #2,R12
    BIT #0x4000,R13
    0= IF   BIS #0x40,R12
    THEN
    SWPB R12
    ADD #1,R12
    MOV R12,&I2CM_BUF
    MOV #0b0010100,&I2CS_ADR
    BIC.B #M_BUS,&I2CSM_DIR
    BIC.B #M_BUS,&I2CSM_OUT
    MOV #2,R11
    BEGIN
        BEGIN
            BEGIN
               BIT.B #MSCL,&I2CSM_IN
            0<> UNTIL
            BIT.B #MSDA,&I2CSM_IN
        0<> UNTIL
            SUB #1,R11
    0= UNTIL
BIS.B   #MSDA,&I2CSM_DIR
MOV     #I2CM_BUF,R10
MOV.B   @R10+,R11
MOV     #I2CS_ADR,R12
MOV.B   @R12+,R9
BIT.B   #1,R9
0= IF
    MOV R10,R12
THEN
BIS.B   #MSCL,&I2CSM_DIR
ADD     #1,R11
MOV     #8,R10
CALL    #I2C_MTX
BEGIN
    BIC.B   #MSCL,&I2CSM_DIR
    BEGIN
        BIT.B #MSCL,&I2CSM_IN
    0<> UNTIL
    BIT.B   #MSDA,&I2CSM_IN
    BIS.B   #MSCL,&I2CSM_DIR
    0<> IF  BIS #2,R2
    ELSE    SUB.B #1,R11
    THEN
    0= IF
        BIS.B #MSDA,&I2CSM_DIR
        SUB.B R11,&I2CM_BUF
        BIC.B #MSCL,&I2CSM_DIR
        BEGIN
            BIT.B #MSCL,&I2CSM_IN
        0<> UNTIL
        BIC.B #MSDA,&I2CSM_DIR
        RETI
    THEN
    MOV.B #8,R10
    BIT.B #1,&I2CS_ADR
    0= IF
        MOV.B @R12+,R9
        CALL #I2C_MTX
    ELSE
        BIC.B #MSDA,&I2CSM_DIR
        BEGIN
            BIC.B #MSCL,&I2CSM_DIR
            BEGIN
                BIT.B #MSCL,&I2CSM_IN
            0<> UNTIL
            BIT.B #MSDA,&I2CSM_IN
            BIS.B #MSCL,&I2CSM_DIR
            ADDC.B R9,R9
            SUB #1,R10
        0= UNTIL
        MOV.B R9,0(R12)
        ADD #1,R12
        CMP.B #1,R11
        0<> IF
            BIS.B #MSDA,&I2CSM_DIR
        THEN
    THEN
AGAIN
ENDCODE

CODE START
    BIC.B #M_BUS,&I2CSM_OUT
    BIC.B #M_BUS,&I2CSM_REN
    BIS.B #RC5,&IR_IE
    BIC.B #RC5,&IR_IFG
    MOV #INT_RC5,&IR_Vec
    LO2HI
    ." RC5toI2CF_Master is running. Type STOP to quit"
    LIT RECURSE IS WARM
    (WARM) ;

CODE STOP
MOV #WARM,R8     
MOV #(WARM),2(R8)
MOV #COLD,R0
ENDCODE

RST_STATE
VARIABLE I2CS_ADR
2 ALLOT
VARIABLE I2CM_BUF
2 ALLOT

CODE I2C_MTX
BEGIN
    ADD.B   R9,R9
    U>= IF
        BIC.B #MSDA,&I2CSM_DIR
    ELSE
        BIS.B #MSDA,&I2CSM_DIR
    THEN
    BIC.B #MSCL,&I2CSM_DIR
    BEGIN
        BIT.B #MSCL,&I2CSM_IN
    0<> UNTIL
    BIS.B #MSCL,&I2CSM_DIR
    SUB #1,R10
0= UNTIL
BIC.B   #MSDA,&I2CSM_DIR
MOV @R1+,R0
ENDCODE

CODE INT_RC5
BIC #0xF8,0(R1)
    MOV     #0,&0x360
    MOV     #1778,R9
    MOV     #14,R10
BEGIN
    MOV #0b1011100100,&0x340
    RRUM    #1,R9
    MOV     R9,R8
    RRUM    #1,R8
    ADD     R9,R8
    BEGIN   CMP R8,&0x350
    0= UNTIL
    BIT.B   #RC5,&IR_IN
    ADDC    R13,R13
    MOV     &IR_IN,&IR_IES
    BIC.B   #RC5,&IR_IFG
    SUB     #1,R10
0<> WHILE
    ADD     R9,R8
    BEGIN
        CMP R8,&0x350
        0>= IF
          BIC #0x30,&0x340
          RETI
        THEN
        BIT.B #RC5,&IR_IFG
    0<> UNTIL
    MOV &0x350,R9
REPEAT
    MOV #0x30,&0x340
    RLAM #1,R13
    MOV @R1,R9
    RLAM #4,R9
    XOR R13,R9
    BIT #0x2000,R9
    0= IF RETI
    THEN
    XOR #0x200,0(R1)
    MOV.B R13,R12
    RRUM #2,R12
    BIT #0x4000,R13
    0= IF   BIS #0x40,R12
    THEN
    SWPB R12
    ADD #1,R12
    MOV R12,&I2CM_BUF
    MOV #0b0010100,&I2CS_ADR
    BIC.B #M_BUS,&I2CSM_DIR
    BIC.B #M_BUS,&I2CSM_OUT
    MOV #2,R11
    BEGIN
        BEGIN
            BEGIN
               BIT.B #MSCL,&I2CSM_IN
            0<> UNTIL
            BIT.B #MSDA,&I2CSM_IN
        0<> UNTIL
            SUB #1,R11
    0= UNTIL
BIS.B   #MSDA,&I2CSM_DIR
MOV     #I2CM_BUF,R10
MOV.B   @R10+,R11
MOV     #I2CS_ADR,R12
MOV.B   @R12+,R9
BIT.B   #1,R9
0= IF
    MOV R10,R12
THEN
BIS.B   #MSCL,&I2CSM_DIR
ADD     #1,R11
MOV     #8,R10
CALL    #I2C_MTX
BEGIN
    BIC.B   #MSCL,&I2CSM_DIR
    BEGIN
        BIT.B #MSCL,&I2CSM_IN
    0<> UNTIL
    BIT.B   #MSDA,&I2CSM_IN
    BIS.B   #MSCL,&I2CSM_DIR
    0<> IF  BIS #2,R2
    ELSE    SUB.B #1,R11
    THEN
    0= IF
        BIS.B #MSDA,&I2CSM_DIR
        SUB.B R11,&I2CM_BUF
        BIC.B #MSCL,&I2CSM_DIR
        BEGIN
            BIT.B #MSCL,&I2CSM_IN
        0<> UNTIL
        BIC.B #MSDA,&I2CSM_DIR
        RETI
    THEN
    MOV.B #8,R10
    BIT.B #1,&I2CS_ADR
    0= IF
        MOV.B @R12+,R9
        CALL #I2C_MTX
    ELSE
        BIC.B #MSDA,&I2CSM_DIR
        BEGIN
            BIC.B #MSCL,&I2CSM_DIR
            BEGIN
                BIT.B #MSCL,&I2CSM_IN
            0<> UNTIL
            BIT.B #MSDA,&I2CSM_IN
            BIS.B #MSCL,&I2CSM_DIR
            ADDC.B R9,R9
            SUB #1,R10
        0= UNTIL
        MOV.B R9,0(R12)
        ADD #1,R12
        CMP.B #1,R11
        0<> IF
            BIS.B #MSDA,&I2CSM_DIR
        THEN
    THEN
AGAIN
ENDCODE

CODE START
    BIC.B #M_BUS,&I2CSM_OUT
    BIC.B #M_BUS,&I2CSM_REN
    BIS.B #RC5,&IR_IE
    BIC.B #RC5,&IR_IFG
    MOV #INT_RC5,&IR_Vec
    LO2HI
    ." RC5toI2CF_Master is running. Type STOP to quit"
    LIT RECURSE IS WARM
    (WARM) ;

CODE STOP
MOV #WARM,R8     
MOV #(WARM),2(R8)
MOV #COLD,R0
ENDCODE

RST_STATE
VARIABLE I2CS_ADR
2 ALLOT
VARIABLE I2CM_BUF
2 ALLOT

CODE I2C_MTX
BEGIN
    ADD.B   R9,R9
    U>= IF
        BIC.B #MSDA,&I2CSM_DIR
    ELSE
        BIS.B #MSDA,&I2CSM_DIR
    THEN
    BIC.B #MSCL,&I2CSM_DIR
    BEGIN
        BIT.B #MSCL,&I2CSM_IN
    0<> UNTIL
    BIS.B #MSCL,&I2CSM_DIR
    SUB #1,R10
0= UNTIL
BIC.B   #MSDA,&I2CSM_DIR
MOV @R1+,R0
ENDCODE

CODE INT_RC5
BIC #0xF8,0(R1)
    MOV     #0,&0x360
    MOV     #1778,R9
    MOV     #14,R10
BEGIN
    MOV #0b1011100100,&0x340
    RRUM    #1,R9
    MOV     R9,R8
    RRUM    #1,R8
    ADD     R9,R8
    BEGIN   CMP R8,&0x350
    0= UNTIL
    BIT.B   #RC5,&IR_IN
    ADDC    R13,R13
    MOV     &IR_IN,&IR_IES
    BIC.B   #RC5,&IR_IFG
    SUB     #1,R10
0<> WHILE
    ADD     R9,R8
    BEGIN
        CMP R8,&0x350
        0>= IF
          BIC #0x30,&0x340
          RETI
        THEN
        BIT.B #RC5,&IR_IFG
    0<> UNTIL
    MOV &0x350,R9
REPEAT
    MOV #0x30,&0x340
    RLAM #1,R13
    MOV @R1,R9
    RLAM #4,R9
    XOR R13,R9
    BIT #0x2000,R9
    0= IF RETI
    THEN
    XOR #0x200,0(R1)
    MOV.B R13,R12
    RRUM #2,R12
    BIT #0x4000,R13
    0= IF   BIS #0x40,R12
    THEN
    SWPB R12
    ADD #1,R12
    MOV R12,&I2CM_BUF
    MOV #0b0010100,&I2CS_ADR
    BIC.B #M_BUS,&I2CSM_DIR
    BIC.B #M_BUS,&I2CSM_OUT
    MOV #2,R11
    BEGIN
        BEGIN
            BEGIN
               BIT.B #MSCL,&I2CSM_IN
            0<> UNTIL
            BIT.B #MSDA,&I2CSM_IN
        0<> UNTIL
            SUB #1,R11
    0= UNTIL
BIS.B   #MSDA,&I2CSM_DIR
MOV     #I2CM_BUF,R10
MOV.B   @R10+,R11
MOV     #I2CS_ADR,R12
MOV.B   @R12+,R9
BIT.B   #1,R9
0= IF
    MOV R10,R12
THEN
BIS.B   #MSCL,&I2CSM_DIR
ADD     #1,R11
MOV     #8,R10
CALL    #I2C_MTX
BEGIN
    BIC.B   #MSCL,&I2CSM_DIR
    BEGIN
        BIT.B #MSCL,&I2CSM_IN
    0<> UNTIL
    BIT.B   #MSDA,&I2CSM_IN
    BIS.B   #MSCL,&I2CSM_DIR
    0<> IF  BIS #2,R2
    ELSE    SUB.B #1,R11
    THEN
    0= IF
        BIS.B #MSDA,&I2CSM_DIR
        SUB.B R11,&I2CM_BUF
        BIC.B #MSCL,&I2CSM_DIR
        BEGIN
            BIT.B #MSCL,&I2CSM_IN
        0<> UNTIL
        BIC.B #MSDA,&I2CSM_DIR
        RETI
    THEN
    MOV.B #8,R10
    BIT.B #1,&I2CS_ADR
    0= IF
        MOV.B @R12+,R9
        CALL #I2C_MTX
    ELSE
        BIC.B #MSDA,&I2CSM_DIR
        BEGIN
            BIC.B #MSCL,&I2CSM_DIR
            BEGIN
                BIT.B #MSCL,&I2CSM_IN
            0<> UNTIL
            BIT.B #MSDA,&I2CSM_IN
            BIS.B #MSCL,&I2CSM_DIR
            ADDC.B R9,R9
            SUB #1,R10
        0= UNTIL
        MOV.B R9,0(R12)
        ADD #1,R12
        CMP.B #1,R11
        0<> IF
            BIS.B #MSDA,&I2CSM_DIR
        THEN
    THEN
AGAIN
ENDCODE

CODE START
    BIC.B #M_BUS,&I2CSM_OUT
    BIC.B #M_BUS,&I2CSM_REN
    BIS.B #RC5,&IR_IE
    BIC.B #RC5,&IR_IFG
    MOV #INT_RC5,&IR_Vec
    LO2HI
    ." RC5toI2CF_Master is running. Type STOP to quit"
    LIT RECURSE IS WARM
    (WARM) ;

CODE STOP
MOV #WARM,R8     
MOV #(WARM),2(R8)
MOV #COLD,R0
ENDCODE

RST_STATE
VARIABLE I2CS_ADR
2 ALLOT
VARIABLE I2CM_BUF
2 ALLOT

CODE I2C_MTX
BEGIN
    ADD.B   R9,R9
    U>= IF
        BIC.B #MSDA,&I2CSM_DIR
    ELSE
        BIS.B #MSDA,&I2CSM_DIR
    THEN
    BIC.B #MSCL,&I2CSM_DIR
    BEGIN
        BIT.B #MSCL,&I2CSM_IN
    0<> UNTIL
    BIS.B #MSCL,&I2CSM_DIR
    SUB #1,R10
0= UNTIL
BIC.B   #MSDA,&I2CSM_DIR
MOV @R1+,R0
ENDCODE

CODE INT_RC5
BIC #0xF8,0(R1)
    MOV     #0,&0x360
    MOV     #1778,R9
    MOV     #14,R10
BEGIN
    MOV #0b1011100100,&0x340
    RRUM    #1,R9
    MOV     R9,R8
    RRUM    #1,R8
    ADD     R9,R8
    BEGIN   CMP R8,&0x350
    0= UNTIL
    BIT.B   #RC5,&IR_IN
    ADDC    R13,R13
    MOV     &IR_IN,&IR_IES
    BIC.B   #RC5,&IR_IFG
    SUB     #1,R10
0<> WHILE
    ADD     R9,R8
    BEGIN
        CMP R8,&0x350
        0>= IF
          BIC #0x30,&0x340
          RETI
        THEN
        BIT.B #RC5,&IR_IFG
    0<> UNTIL
    MOV &0x350,R9
REPEAT
    MOV #0x30,&0x340
    RLAM #1,R13
    MOV @R1,R9
    RLAM #4,R9
    XOR R13,R9
    BIT #0x2000,R9
    0= IF RETI
    THEN
    XOR #0x200,0(R1)
    MOV.B R13,R12
    RRUM #2,R12
    BIT #0x4000,R13
    0= IF   BIS #0x40,R12
    THEN
    SWPB R12
    ADD #1,R12
    MOV R12,&I2CM_BUF
    MOV #0b0010100,&I2CS_ADR
    BIC.B #M_BUS,&I2CSM_DIR
    BIC.B #M_BUS,&I2CSM_OUT
    MOV #2,R11
    BEGIN
        BEGIN
            BEGIN
               BIT.B #MSCL,&I2CSM_IN
            0<> UNTIL
            BIT.B #MSDA,&I2CSM_IN
        0<> UNTIL
            SUB #1,R11
    0= UNTIL
BIS.B   #MSDA,&I2CSM_DIR
MOV     #I2CM_BUF,R10
MOV.B   @R10+,R11
MOV     #I2CS_ADR,R12
MOV.B   @R12+,R9
BIT.B   #1,R9
0= IF
    MOV R10,R12
THEN
BIS.B   #MSCL,&I2CSM_DIR
ADD     #1,R11
MOV     #8,R10
CALL    #I2C_MTX
BEGIN
    BIC.B   #MSCL,&I2CSM_DIR
    BEGIN
        BIT.B #MSCL,&I2CSM_IN
    0<> UNTIL
    BIT.B   #MSDA,&I2CSM_IN
    BIS.B   #MSCL,&I2CSM_DIR
    0<> IF  BIS #2,R2
    ELSE    SUB.B #1,R11
    THEN
    0= IF
        BIS.B #MSDA,&I2CSM_DIR
        SUB.B R11,&I2CM_BUF
        BIC.B #MSCL,&I2CSM_DIR
        BEGIN
            BIT.B #MSCL,&I2CSM_IN
        0<> UNTIL
        BIC.B #MSDA,&I2CSM_DIR
        RETI
    THEN
    MOV.B #8,R10
    BIT.B #1,&I2CS_ADR
    0= IF
        MOV.B @R12+,R9
        CALL #I2C_MTX
    ELSE
        BIC.B #MSDA,&I2CSM_DIR
        BEGIN
            BIC.B #MSCL,&I2CSM_DIR
            BEGIN
                BIT.B #MSCL,&I2CSM_IN
            0<> UNTIL
            BIT.B #MSDA,&I2CSM_IN
            BIS.B #MSCL,&I2CSM_DIR
            ADDC.B R9,R9
            SUB #1,R10
        0= UNTIL
        MOV.B R9,0(R12)
        ADD #1,R12
        CMP.B #1,R11
        0<> IF
            BIS.B #MSDA,&I2CSM_DIR
        THEN
    THEN
AGAIN
ENDCODE

CODE START
    BIC.B #M_BUS,&I2CSM_OUT
    BIC.B #M_BUS,&I2CSM_REN
    BIS.B #RC5,&IR_IE
    BIC.B #RC5,&IR_IFG
    MOV #INT_RC5,&IR_Vec
    LO2HI
    ." RC5toI2CF_Master is running. Type STOP to quit"
    LIT RECURSE IS WARM
    (WARM) ;

CODE STOP
MOV #WARM,R8     
MOV #(WARM),2(R8)
MOV #COLD,R0
ENDCODE

RST_STATE
VARIABLE I2CS_ADR
2 ALLOT
VARIABLE I2CM_BUF
2 ALLOT

CODE I2C_MTX
BEGIN
    ADD.B   R9,R9
    U>= IF
        BIC.B #MSDA,&I2CSM_DIR
    ELSE
        BIS.B #MSDA,&I2CSM_DIR
    THEN
    BIC.B #MSCL,&I2CSM_DIR
    BEGIN
        BIT.B #MSCL,&I2CSM_IN
    0<> UNTIL
    BIS.B #MSCL,&I2CSM_DIR
    SUB #1,R10
0= UNTIL
BIC.B   #MSDA,&I2CSM_DIR
MOV @R1+,R0
ENDCODE

CODE INT_RC5
BIC #0xF8,0(R1)
    MOV     #0,&0x360
    MOV     #1778,R9
    MOV     #14,R10
BEGIN
    MOV #0b1011100100,&0x340
    RRUM    #1,R9
    MOV     R9,R8
    RRUM    #1,R8
    ADD     R9,R8
    BEGIN   CMP R8,&0x350
    0= UNTIL
    BIT.B   #RC5,&IR_IN
    ADDC    R13,R13
    MOV     &IR_IN,&IR_IES
    BIC.B   #RC5,&IR_IFG
    SUB     #1,R10
0<> WHILE
    ADD     R9,R8
    BEGIN
        CMP R8,&0x350
        0>= IF
          BIC #0x30,&0x340
          RETI
        THEN
        BIT.B #RC5,&IR_IFG
    0<> UNTIL
    MOV &0x350,R9
REPEAT
    MOV #0x30,&0x340
    RLAM #1,R13
    MOV @R1,R9
    RLAM #4,R9
    XOR R13,R9
    BIT #0x2000,R9
    0= IF RETI
    THEN
    XOR #0x200,0(R1)
    MOV.B R13,R12
    RRUM #2,R12
    BIT #0x4000,R13
    0= IF   BIS #0x40,R12
    THEN
    SWPB R12
    ADD #1,R12
    MOV R12,&I2CM_BUF
    MOV #0b0010100,&I2CS_ADR
    BIC.B #M_BUS,&I2CSM_DIR
    BIC.B #M_BUS,&I2CSM_OUT
    MOV #2,R11
    BEGIN
        BEGIN
            BEGIN
               BIT.B #MSCL,&I2CSM_IN
            0<> UNTIL
            BIT.B #MSDA,&I2CSM_IN
        0<> UNTIL
            SUB #1,R11
    0= UNTIL
BIS.B   #MSDA,&I2CSM_DIR
MOV     #I2CM_BUF,R10
MOV.B   @R10+,R11
MOV     #I2CS_ADR,R12
MOV.B   @R12+,R9
BIT.B   #1,R9
0= IF
    MOV R10,R12
THEN
BIS.B   #MSCL,&I2CSM_DIR
ADD     #1,R11
MOV     #8,R10
CALL    #I2C_MTX
BEGIN
    BIC.B   #MSCL,&I2CSM_DIR
    BEGIN
        BIT.B #MSCL,&I2CSM_IN
    0<> UNTIL
    BIT.B   #MSDA,&I2CSM_IN
    BIS.B   #MSCL,&I2CSM_DIR
    0<> IF  BIS #2,R2
    ELSE    SUB.B #1,R11
    THEN
    0= IF
        BIS.B #MSDA,&I2CSM_DIR
        SUB.B R11,&I2CM_BUF
        BIC.B #MSCL,&I2CSM_DIR
        BEGIN
            BIT.B #MSCL,&I2CSM_IN
        0<> UNTIL
        BIC.B #MSDA,&I2CSM_DIR
        RETI
    THEN
    MOV.B #8,R10
    BIT.B #1,&I2CS_ADR
    0= IF
        MOV.B @R12+,R9
        CALL #I2C_MTX
    ELSE
        BIC.B #MSDA,&I2CSM_DIR
        BEGIN
            BIC.B #MSCL,&I2CSM_DIR
            BEGIN
                BIT.B #MSCL,&I2CSM_IN
            0<> UNTIL
            BIT.B #MSDA,&I2CSM_IN
            BIS.B #MSCL,&I2CSM_DIR
            ADDC.B R9,R9
            SUB #1,R10
        0= UNTIL
        MOV.B R9,0(R12)
        ADD #1,R12
        CMP.B #1,R11
        0<> IF
            BIS.B #MSDA,&I2CSM_DIR
        THEN
    THEN
AGAIN
ENDCODE

CODE START
    BIC.B #M_BUS,&I2CSM_OUT
    BIC.B #M_BUS,&I2CSM_REN
    BIS.B #RC5,&IR_IE
    BIC.B #RC5,&IR_IFG
    MOV #INT_RC5,&IR_Vec
    LO2HI
    ." RC5toI2CF_Master is running. Type STOP to quit"
    LIT RECURSE IS WARM
    (WARM) ;

CODE STOP
MOV #WARM,R8     
MOV #(WARM),2(R8)
MOV #COLD,R0
ENDCODE

ECHO
            ; compiling done
PWR_HERE    ;
 
