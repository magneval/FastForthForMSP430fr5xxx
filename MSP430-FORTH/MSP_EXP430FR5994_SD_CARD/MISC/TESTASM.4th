PWR_STATE
CODE TESTPUSHM
            MOV     #22222,R8
            MOV     #3,R9
            MOV     #2,R10
            MOV     #1,R11
            MOV     #0,R12
            PUSHM   R13,R10
            POPM    R10,R13
            SUB     #10,R15
            MOV     R14,8(R15)
            MOV     R12,6(R15)
            MOV     R11,4(R15)
            MOV     R10,2(R15)
            MOV     R9,0(R15)
            MOV     R8,R14
            RRAM    #1,R14
            RLAM    #2,R14
            RRCM    #1,R14
            RRUM    #1,R14
            COLON
            space . . . . .
            ;

TESTPUSHM  ; you should see 11111 3 2 1 0 -->
CODE TESTPOPM
            MOV     #22222,R8
            MOV     #3,R9
            MOV     #2,R10
            MOV     #1,R11
            MOV     #0,R12
            PUSHM   R13,R10
            POPM    R10,R13
            SUB     #10,R15
            MOV     R14,8(R15)
            MOV     R12,6(R15)
            MOV     R11,4(R15)
            MOV     R10,2(R15)
            MOV     R9,0(R15)
            MOV     R8,R14
            RRAM    #1,R14
            RLAM    #2,R14
            RRCM    #1,R14
            RRUM    #1,R14
            COLON
            space . . . . .
            ;

TESTPOPM  ; you should see 11111 3 2 1 0 -->
CODE TEST1
            MOV &0x1842,&0x1842
            CMP #0b10,&0x1842
0<> IF      MOV #2, &0x1842
ELSE        MOV #0x0A,&0x1842
THEN        
            COLON
            0x1842 @ U.
            ;

: TEST2
            0x1842 @ U.
            HI2LO


            CMP #2, &0x1842
0<> IF      MOV #2, &0x1842
ELSE        MOV #10,&0x1842
THEN
            MOV @R1+,R13
            MOV @R13+,R0
ENDCODE

CODE TEST3
            CMP #2, &0x1842
0<> IF      MOV #2, &0x1842
ELSE        MOV #10,&0x1842
THEN        COLON
            0x1842 @  U.
;

: TEST5
            SPACE
            HI2LO
            SUB #2,R15
            MOV R14,0(R15)
            MOV #0b1010,R14
BEGIN       SUB #0x0001,R14
            LO2HI

            DUP U.
            HI2LO
            CMP #0,R14
0= UNTIL    MOV @R15+,R14
            MOV @R1+,R13
            MOV @R13+,R0
ENDCODE

TEST5  ; you should see :  9 8 7 6 5 4 3 2 1 0 -->

CODE C,
    MOV &0x182C,R10
    MOV.B R14,0(R10)
    MOV @R15+,R14
    ADD #1,&0x182C
    MOV @R13+,R0
ENDCODE

: TABLE
CREATE 
0 DO I C,
LOOP
DOES>
+
;
8 TABLE BYTES_TABLE

here 100 - 100 dump

2 BYTES_TABLE C@ . ; you should see 2 -->
VARIABLE BYTES_TABLE1
0x0201 BYTES_TABLE1 !
CODE IDX_TEST1
    MOV.B   BYTES_TABLE1(R14),R14
COLON
    U. 
;      
0 IDX_TEST1     ; you should see 1 -->
CODE TEST6
            MOV 0(R15),0(R15)
            MOV @R13+,R0
ENDCODE
1 TEST6 .       ; you should see 1 -->
; CODE TEST7
;            MOV 0(R15),0(R16)  ; display an error "out of bounds" -->
