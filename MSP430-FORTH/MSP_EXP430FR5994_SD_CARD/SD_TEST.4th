WIPE
NOECHO
CODE STARTWATCH
    MOV #0b0111100100,&0x3C0
    MOV #0,&0x3E0
    MOV @R13+,R0
ENDCODE
CODE STOPWATCH
    SUB #4,R15
    MOV R14,2(R15)
    MOV &0x3D0,0(R15)
    MOV #0,R14
    MOV #0b0100000100,&0x3C0
COLON
    0x1000 UM/MOD DROP U. ." ms"
;
 
: SD_TEST
    CR
    ."    1 Load ANS core tests" CR
    ."    2 Load, compile and run a 10k program "
            ." from its source file (quiet mode)" CR
    ."    3 Read only this source file (quiet mode)" CR
    ."    4 Write a dump of the FORTH kernel to yourfile.txt" CR
    ."    5 append a dump of the FORTH kernel to yourfile.txt" CR
    ."    6 Load truc (test error)" CR
    ."    your choice : "
    KEY
    48 - 
    DUP 1 = 
    IF  .
        LOAD" CORETEST.4TH"
    ELSE DUP 2 =
        IF  .
            LOAD" Prog10k.4th"
        ELSE DUP 3 =
            IF  .
                STARTWATCH
                READ" Prog10k.4th"
                BEGIN
                    READ
                UNTIL
                STOPWATCH
            ELSE DUP 4 =
                IF  .
                    DEL" yourfile.txt"
                    WRITE" yourfile.txt" 
                    0x4000 HERE OVER - DUMP
                    CLOSE
                ELSE DUP 5 =
                    IF  .
                        WRITE" yourfile.txt"
                        0x4000 HERE OVER - DUMP
                        CLOSE
                    ELSE DUP 6 =
                        IF  .
                            LOAD" truc"
                        ELSE 
                            DROP ." ?"
                            CR ."    loading TSTWORDS.4th..."
                            LOAD" TSTWORDS.4TH"
                        THEN
                    THEN
                THEN
            THEN
        THEN
    THEN
    CR
;
ECHO
RST_HERE
SD_TEST