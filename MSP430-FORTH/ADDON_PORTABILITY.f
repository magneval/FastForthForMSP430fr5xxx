; ----------------------------------------------------------------
; PORTABILITY complement
; ----------------------------------------------------------------

\ REGISTERS USAGE
\ R4 to R7 must be saved before use and restored after
\ scratch registers Y to S are free for use
\ under interrupt, IP is free for use

\ PUSHM order : PSP,TOS, IP,  S,  T,  W,  X,  Y, R7, R6, R5, R4
\ example : PUSHM IP,Y
\
\ POPM  order :  R4, R5, R6, R7,  Y,  X,  W,  T,  S, IP,TOS,PSP
\ example : POPM Y,IP

\ ASSEMBLER conditionnal usage after IF UNTIL WHILE : S< S>= U< U>= 0= 0<> 0>=
\ ASSEMBLER conditionnal usage before GOTO ?GOTO     : S< S>= U< U>= 0= 0<> <0 

\ FORTH conditionnal usage after IF UNTIL WHILE : 0= 0< = < > U<

\ WIPE        ; remove all previous apps,
RST_STATE   ; preserve all previous apps,
NOECHO      ; if an error occurs, comment this line before new download to find it.
    \

\ ---------------------
\ PORTABILITY OPERATORS
\ ---------------------

CODE CHARS      \ n1 -- n2            chars->adrs units
MOV     @IP+,PC
ENDCODE
    \

CODE CHAR+      \ c-addr1 -- c-addr2   add char size
ADD     #1,TOS
MOV     @IP+,PC
ENDCODE
    \

CODE CELLS      \ n1 -- n2            cells->adrs units
ADD     TOS,TOS
MOV     @IP+,PC
ENDCODE
    \

CODE CELL+      \ a-addr1 -- a-addr2      add cell size
ADD     #2,TOS
MOV     @IP+,PC
ENDCODE
    \

ECHO
            ; added : CHARS CHAR+ CELLS CELL+
RST_HERE    ; the app is protected against <reset>,


