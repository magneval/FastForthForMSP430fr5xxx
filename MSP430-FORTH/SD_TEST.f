

\ how to test SDcard_FAT16 on your target :

\ remind you : files.f must be preprocessed before downloading.

\ in the folder MSP430-FORTH : 

\ create subfolders \SD_CARD and \SD_CARD\MISC 
\ copy CORETEST.4th and TSTWORDS.4th in subfolder \SD_CARD

\ preprocess SD_TEST.f, PROG10k.f, TESTASM.f with convert_file.f_to_file.4th.bat
\       (see how to in send_source_file_to_target_readme.txt),
\ move SD_TEST.4th, PROG10k.4th in subfolder \SD_CARD
\ move TESTASM.4th in subfolder \SD_CARD\MISC,


\ copy the content of \SD_CARD subfolder to a micro SD Card formatted FAT16,

\ put it in the target board SD socket wired as described in MSP430-FORTH\target.pat,
\ type COLD on the console input to reset FASTFORTH,
\ then type LOAD" SD_TEST.4th".

\ if your target device is FR5xxx or FR6xxx, you can also beforehand download MSP430FR5xxx_RTC_B.f
\         (see send_source_file_to_target_readme.txt)
\ then set DATE and TIME (see MSP430FR5xxx_RTC_B.f) to create files with good timestamp.

WIPE
NOECHO

CODE STARTWATCH
    MOV #0b0111100100,&TB0CTL   \ ACLK/8, continuous mode, clear timer, no int
    MOV #0,&TB0EX0              \ predivide by 1 in TB0EX0 register
    NEXT
ENDCODE

CODE STOPWATCH
    SUB #4,PSP
    MOV TOS,2(PSP)
    MOV &TB0R,0(PSP)
    MOV #0,TOS
    MOV #0b0100000100,&TB0CTL   \ ACLK/1, stop timer, clear timer, no int
COLON
    0x1000 UM/MOD DROP U. ." ms"
;
 

\ you can see interest of preprocessing that allows the use of the PROGRAMSTART address, not recognized by FASTFORTH
\ in the preprocessed file SD_TEST.4th, PROGRAMSTART will be replaced by its value.
\ PROGRAMSTART is defined in \config\gema\MSP430FR_FastForth.pat
: SD_TEST
    CR
    ."    1 Load ANS core tests" CR
    ."    2 Load, compile and run a 10k program "
            ." from its source file (quiet mode)" CR
    ."    3 Read only this source file (quiet mode)" CR
    ."    4 Write a dump of the FORTH kernel to yourfile.txt" CR
    ."    5 append a dump of the FORTH kernel to yourfile.txt" CR
    ."    6 Load truc (test error)" CR
    ."    your choice : "
    KEY
    48 - 
    DUP 1 = 
    IF  .
        LOAD" CORETEST.4TH"
    ELSE DUP 2 =
        IF  .
            LOAD" Prog10k.4th"
        ELSE DUP 3 =
            IF  .
                STARTWATCH
                READ" Prog10k.4th"
                BEGIN
                    READ
                UNTIL
                STOPWATCH
            ELSE DUP 4 =
                IF  .
                    DEL" yourfile.txt"
                    WRITE" yourfile.txt" 
                    PROGRAMSTART HERE OVER - DUMP
                    CLOSE
                ELSE DUP 5 =
                    IF  .
                        WRITE" yourfile.txt"
                        PROGRAMSTART HERE OVER - DUMP
                        CLOSE
                    ELSE DUP 6 =
                        IF  .
                            LOAD" truc"
                        ELSE 
                            DROP ." ?"
                            CR ."    loading TSTWORDS.4th..."
                            LOAD" TSTWORDS.4TH"
                        THEN
                    THEN
                THEN
            THEN
        THEN
    THEN
    CR
;

ECHO
RST_HERE

SD_TEST